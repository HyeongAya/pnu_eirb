USE [eIRB]
GO
/****** Object:  StoredProcedure [dbo].[eIRB_심의진행등록]    Script Date: 04/02/2018 17:22:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***************************************************************************************************
  - 작  성  자  명	: 
  - 작    성    일	: 17.04.19
  - 설 	     명	: eIRB_심의진행등록
---------------------------------------------------------------------------------------------------
  - 수  정  이  력  : 
***************************************************************************************************/
ALTER PROC [dbo].[eIRB_심의진행등록]  
@DELI_SCHE_KEYNO VARCHAR(100), --1 심의일정번호
@DELI_APPL_KEYNO VARCHAR(200), --2 심의신청접수번호
@DELI_ACCS_STEP_CD VARCHAR(5), --3 심의단계코드
@DELI_ACCS_STATUS VARCHAR(5), --4 심의진행상태
@DELI_ACCS_NOTE VARCHAR(500), --5 심의진행상태비고
@USER_ID VARCHAR(20), --6 등록아이디
@USER_IP VARCHAR(20), --7 등록IP

@DELI_APPL_OBJ_CD VARCHAR(10), -- 8 연구대상코드
@DELI_APPL_KEYNO_CH VARCHAR(20) --9 심의신청접수번호 변경번호
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @DELI_ACCS_KEYNO INT 

	SELECT @DELI_ACCS_KEYNO = ISNULL(MAX(CONVERT(int,DELI_ACCS_KEYNO)), 1) + 1
	FROM eIRB.dbo.TBL_DELI_ACCESS
	
	DECLARE @APPL_KEYNO VARCHAR(20) --심의신청접수번호
	DECLARE @APPL_OBJ_CD VARCHAR(10) --연구대상코드
	
	IF @DELI_ACCS_STATUS = '05'
		BEGIN
			IF @DELI_APPL_OBJ_CD = '01'
				BEGIN
					SET @APPL_OBJ_CD = 'HR'
				END 
			ELSE 
				BEGIN
					SET @APPL_OBJ_CD = 'BR'
				END 
				
			SET @APPL_KEYNO = @DELI_APPL_KEYNO_CH + '_' + @APPL_OBJ_CD
		END 
	ELSE 
		BEGIN
			SET @APPL_KEYNO = @DELI_APPL_KEYNO
		END 
		
	INSERT INTO eIRB.dbo.TBL_DELI_ACCESS 
	(DELI_ACCS_KEYNO, DELI_SCHE_KEYNO, DELI_APPL_KEYNO, DELI_ACCS_STEP_CD, DELI_ACCS_STATUS, DELI_ACCS_NOTE, DELI_ACCS_STAT_DT , INS_ID, INS_DT, INS_IP) 
	VALUES (@DELI_ACCS_KEYNO, @DELI_SCHE_KEYNO, @APPL_KEYNO, @DELI_ACCS_STEP_CD, @DELI_ACCS_STATUS, @DELI_ACCS_NOTE, GETDATE(), @USER_ID, GETDATE(), @USER_IP)

	SET NOCOUNT OFF
	
	IF @DELI_ACCS_STATUS = '05'
		BEGIN
			UPDATE TBL_DELI_APPLY SET DELI_APPL_KEYNO = @APPL_KEYNO WHERE DELI_APPL_KEYNO = @DELI_APPL_KEYNO
			
			UPDATE TBL_ATTACH_FILE SET REL_KEYNO = @APPL_KEYNO WHERE REL_KEYNO = @DELI_APPL_KEYNO

			UPDATE TBL_DELI_RESEARCHER SET DELI_APPL_KEYNO = @APPL_KEYNO WHERE DELI_APPL_KEYNO = @DELI_APPL_KEYNO
			
			UPDATE TBL_DELI_ACCESS SET DELI_APPL_KEYNO = @APPL_KEYNO WHERE DELI_APPL_KEYNO = @DELI_APPL_KEYNO
		END
END