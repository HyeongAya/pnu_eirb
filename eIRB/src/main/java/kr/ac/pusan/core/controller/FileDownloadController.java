/**
 * 
 */
package kr.ac.pusan.core.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

@SuppressWarnings("unchecked")
public class FileDownloadController extends AbstractView {
	
	protected final Log logger = LogFactory.getLog("pnu_cms");
	
	public FileDownloadController() {
		setContentType("application/download; utf-8");
	}
	
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> map = (Map<String, Object>)model.get("downloadFile");
		File file			   = new File((String)map.get("FILE_REAL_PATH"));
		
		response.setContentType(getContentType());
		response.setContentLength((int)file.length());
		
		String userAgent	= request.getHeader("User-Agent");
		boolean ie		  = userAgent.indexOf("MSIE") > -1;
		String fileName	 = "";
		
		if(ie) {
			fileName = URLEncoder.encode((String)map.get("FILE_ORIGINAL_NAME"), "utf-8");
		} else {
			fileName = new String(((String)map.get("FILE_ORIGINAL_NAME")).getBytes("utf-8"));
		}
		
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		OutputStream out	= response.getOutputStream();
		FileInputStream fis = null;
		
		try {
			fis = new FileInputStream(file);
			FileCopyUtils.copy(fis, out);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(fis != null) { try { fis.close(); } catch(Exception e) {} }
		}
		
		out.flush();
	}

}
