/**
 *
 */
package kr.ac.pusan.core.dao;

import java.util.List;
import java.util.Map;

public interface ICommonDao<V> {

	public List<Map<String, V>> core_selectList(String queryId, Map<String, V> parameterObject, int page, int limit);
	public List<Map<String, V>> core_selectList(String queryId, Map<String, V> parameterObject);
	public List<List<Map<String, Object>>> selectMultiList(String queryId, Map<String, V> parameterObject);
	public Object core_select(String queryId, Map<String, V> parameterObject);
	public int core_setInsert(String queryId, Map<String, V> parameterObject);
	public int core_setUpdate(String queryId, Map<String, V> parameterObject);
	public int core_setDelete(String queryId, Map<String, V> parameterObject);

}
