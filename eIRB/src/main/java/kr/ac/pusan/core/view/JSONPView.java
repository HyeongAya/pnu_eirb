/**
 * 
 */
package kr.ac.pusan.core.view;

import java.io.Writer;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.web.servlet.view.AbstractView;

public class JSONPView extends AbstractView {
	
	protected final Log logger = LogFactory.getLog("pnu_cms");
	
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String callback = request.getParameter("callback");
		ObjectMapper om = new ObjectMapper();
		String json	 = om.writeValueAsString(model);
		Writer writer   = response.getWriter();
		
		writer.append(callback).append("(").append(json).append("");
	}

}
