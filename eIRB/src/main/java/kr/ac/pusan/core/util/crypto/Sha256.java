package kr.ac.pusan.core.util.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Sha256 {

	public String toEncryption(String plainTxt) throws NoSuchAlgorithmException {
		MessageDigest sh = MessageDigest.getInstance("SHA-256");

		sh.update(plainTxt.getBytes());

		byte byteData[] = sh.digest();
		StringBuffer sb = new StringBuffer();

		for(int idx = 0; idx < byteData.length; idx++) {
			sb.append(Integer.toString((byteData[idx]&0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}

}
