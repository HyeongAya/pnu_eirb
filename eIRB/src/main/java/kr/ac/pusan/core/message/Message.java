/**
 * 
 */
package kr.ac.pusan.core.message;

import java.text.MessageFormat;
import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.NoSuchMessageException;

public class Message implements MessageSourceAware {
	
	private static MessageSource msg;

	public void setMessageSource (MessageSource msg) {
		Message.msg = msg;
	}

	public static String prop(String key) {
		String ret = "";
		
		try {
			ret = Message.msg(key);
		} catch(NoSuchMessageException nsme) {
			ret = "등록되지 않은 메세지 입니다.";
		}
		return ret;
	}
	
	public static String prop(String key, Locale locale) {
		String ret = "";
		
		try {
			ret = Message.msg(key, locale);
		} catch(NoSuchMessageException nsme) {
			ret = "등록되지 않은 메세지 입니다.";
		}
		return ret;
	}
	
	public static String propFormat(String key, Object...objects) {
		String ret = "";
		
		try {
			ret = MessageFormat.format(Message.msg(key),objects);
		} catch(NoSuchMessageException nsme) {
			ret = "등록되지 않은 메세지 입니다.";
		}
		return ret;
	}
	
	public static String propFormat(String key, Locale locale, Object...objects) {
		String ret = "";
		
		try {
			ret = MessageFormat.format(Message.msg(key, locale),objects);
		} catch(NoSuchMessageException nsme) {
			ret = "등록되지 않은 메세지 입니다.";
		}
		return ret;
	}

	public static String msg(String key){
		String ret = "";
		
		try {
			ret = msg.getMessage(key, null, Locale.getDefault());
		} catch(NoSuchMessageException nsme) {
			ret = "등록되지 않은 메세지 입니다.";
		}
		return ret;
	}
	
	public static String msg(String key, Locale locale){
		String ret = "";
		
		try {
			ret = msg.getMessage(key, null, locale);
		} catch(NoSuchMessageException nsme) {
			ret = "등록되지 않은 메세지 입니다.";
		}
		return ret;
	}

}
