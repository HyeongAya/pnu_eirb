/**
 *
 */
package kr.ac.pusan.core.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.service.ICommonService;

@SuppressWarnings("unchecked")
public class BaseController {

	protected final Log logger = LogFactory.getLog("pnu_eirb");

	protected Map<String, Object> paramMap;

	@Autowired
	protected ICommonService commonService;

	public void initParamMap() {
		paramMap = new HashMap<String, Object>();
	}

	public void clearParamMap() {
		if(paramMap != null) {
			paramMap.clear();
			paramMap = null;
		}
	}

	public void setParamMap(Map<String, Object> paramMap) {
		this.paramMap = paramMap;
	}

	public Map<String, Object> getParamMap() {
		return paramMap;
	}

	public ModelAndView downloadExcel(List<Map<String, Object>> voMapList) {
		return new ModelAndView(new ExcelController(), "voMapList", voMapList);
	}

	public ModelAndView downloadFile(String fileRealPath, String fileOriginalName) {
		HashMap<String, Object> hm  = new HashMap<String, Object>();

		hm.put("FILE_REAL_PATH", fileRealPath);
		hm.put("FILE_ORIGINAL_NAME", fileOriginalName);

		return new ModelAndView(new FileDownloadController(), "downloadFile", hm);
	}
	
	public List<HashMap<String, String>> getListFromJson(String itemKey) {
		JSONArray jsonArray				 = JSONArray.fromObject(paramMap.get(itemKey));
		List<HashMap<String, String>> list  = new ArrayList<HashMap<String, String>>();

		for (int idx = 0; idx < jsonArray.size(); idx++) {
			HashMap<String, String> map = new HashMap<String, String>();
			JSONObject jsonObject	   = jsonArray.optJSONObject(idx);
			Iterator<String> iterator   = jsonObject.keys();

			while (iterator.hasNext()) {
				String key = iterator.next();
				map.put(key.toUpperCase(), jsonObject.getString(key));
			}

			map.put("INSERT_IP",	(String)paramMap.get("INSERT_IP"));
			map.put("INSERT_PG",	(String)paramMap.get("INSERT_PG"));
			map.put("INSERT_ID",	(String)paramMap.get("INSERT_ID"));
			map.put("INSERT_NAME",  (String)paramMap.get("INSERT_NAME"));
			map.put("UPDATE_IP",	(String)paramMap.get("UPDATE_IP"));
			map.put("UPDATE_PG",	(String)paramMap.get("UPDATE_PG"));
			map.put("UPDATE_ID",	(String)paramMap.get("UPDATE_ID"));
			map.put("UPDATE_NAME",  (String)paramMap.get("UPDATE_NAME"));

			list.add(map);
		}

		return list;
	}

	public List<Map<String, Object>> getListJson(String itemKey) {
		JSONArray jsonArray				 = JSONArray.fromObject(paramMap.get(itemKey));
		ObjectMapper mapper				 = new ObjectMapper();
		List<Map<String, Object>> jsonList  = null;

		try {
			jsonList = mapper.readValue(jsonArray.toString().replace("\",\"@", "\",\"").replace("{\"@", "{\""), new TypeReference<List<Map<String, Object>>>(){});
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonList;
	}

}
