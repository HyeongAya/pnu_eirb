/**
 *
 */
package kr.ac.pusan.core.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.service.ICommonService;
import kr.ac.pusan.core.util.StringUtil;

@SuppressWarnings("unchecked")
public class ParameterLoggingInterceptor extends HandlerInterceptorAdapter {

	protected final Log logger = LogFactory.getLog("pnu_eirb");

	@Autowired
	private ICommonService commonService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if(handler instanceof HandlerMethod) {
			logger.info("# [ParameterLoggingInterceptor] HttpServletRequest Parameter Logging Start.");
			logger.info("\t\t\tHTTP REQUEST MATHOD : " + request.getMethod());
			logger.info("\t\t\tHTTP REQUEST ENCODING : " + request.getCharacterEncoding());

			Map<String, String[]> requestMap	= request.getParameterMap();
			StringUtil su = new StringUtil();

			for(String key : requestMap.keySet()) {
				StringBuilder sb	= new StringBuilder();
				String[] values	 = requestMap.get(key);

				sb.append("[" + key.toUpperCase() + "\t: ");

				for(int idx = 0; idx < values.length; idx++) {
					if(idx > 0) {
						if("GET".equals(request.getMethod())) {
							sb.append("," + su.encoding(values[idx]));
						} else {
							sb.append("," + values[idx]);
						}
					} else {
						if("GET".equals(request.getMethod())) {
							sb.append(su.encoding(values[idx]));
						} else {
							sb.append(values[idx]);
						}
					}
				}

				sb.append("]");
				logger.info("\t\t\t" + sb.toString());
			}
		}

		return super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		try {
			if(handler instanceof HandlerMethod) {
				Map<String, String[]> requestMap	= request.getParameterMap();
				StringUtil su					   = new StringUtil();
				String param						= "";

				for(String key : requestMap.keySet()) {
					StringBuilder sb	= new StringBuilder();
					String[] values	 = requestMap.get(key);

					sb.append("[" + key.toUpperCase() + "\t: ");

					for(int idx = 0; idx < values.length; idx++) {
						if(idx > 0) {
							if("GET".equals(request.getMethod())) {
								sb.append("," + su.encoding(values[idx]));
							} else {
								sb.append("," + values[idx]);
							}
						} else {
							if("GET".equals(request.getMethod())) {
								sb.append(su.encoding(values[idx]));
							} else {
								sb.append(values[idx]);
							}
						}
					}

					sb.append("]");
					param += sb.toString();
				}

				HttpSession session = request.getSession();
				String user_id	  = su.isEmpty(String.valueOf(session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID))) ? "anonymous":String.valueOf(session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
				String user_ip	  = request.getRemoteAddr();
				String referer	  = request.getHeader("referer");
				String class_nm	 = ((HandlerMethod)handler).getBean().getClass().getName();
				String method_nm	= ((HandlerMethod)handler).getMethod().getName();
				String parameter	= param;
				String view_nm	  = su.isEmpty(modelAndView.getViewName()) ? "":modelAndView.getViewName();

				Map logMap = new HashMap();
				logMap.put("USER_ID", user_id);
				logMap.put("USER_IP", user_ip);
				logMap.put("REFERER", referer);
				logMap.put("LOC_CLASS", class_nm);
				logMap.put("LOC_METHOD", method_nm);
				logMap.put("RESULT_VIEW", view_nm);
				logMap.put("PARAMETER", parameter);

				if(!view_nm.startsWith("inc/") && !view_nm.startsWith("comm/")) {
					//commonService.setInsert("TM01_10000107_t.INSERT_LOGS_M", logMap);
				}
			}
		} catch(Exception e) {}

		super.postHandle(request, response, handler, modelAndView);
	}

}
