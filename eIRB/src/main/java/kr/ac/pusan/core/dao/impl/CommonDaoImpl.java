/**
 *
 */
package kr.ac.pusan.core.dao.impl;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.Clob;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import kr.ac.pusan.core.dao.ICommonDao;
import kr.ac.pusan.core.exception.DAOException;

import egovframework.rte.psl.dataaccess.EgovAbstractMapper;

@Repository("commonDao")
public class CommonDaoImpl<V> extends EgovAbstractMapper implements ICommonDao<V> {

	protected final Log logger = LogFactory.getLog("pnu_cms");

	public List<Map<String, V>> core_selectList(String queryId, Map<String, V> parameterObject, int page, int limit) {
		List<Map<String, V>> ret;
		RowBounds rowBounds = new RowBounds((limit * (page - 1)), limit);

		try {
			if(parameterObject == null) {
				ret = super.selectList(queryId, rowBounds);
			} else {
				ret = super.selectList(queryId, parameterObject, rowBounds);
			}
		} catch(Exception e) {
			logger.error("selectList DAO Exception : " + e.getMessage());
			throw new DAOException(e.getMessage());
		}

		return ret;
	}

	public List<Map<String, V>> core_selectList(String queryId, Map<String, V> parameterObject) {
		List<Map<String, V>> ret;

		try {
			if(parameterObject == null) {
				ret = super.selectList(queryId);
			} else {
				ret = super.selectList(queryId, parameterObject);
			}

			for(int idx = 0; idx < ret.size(); idx++) {
				Map hm		  = ret.get(idx);
				Iterator iter   = hm.entrySet().iterator();

				while(iter.hasNext()) {
					Map.Entry entry = (Map.Entry)iter.next();

					if(entry.getValue() != null && (entry.getValue() instanceof java.sql.Clob)) {
						entry.setValue(this.getStringForClob((Clob)entry.getValue()));
					} else if(entry.getValue() != null && (entry.getValue() instanceof java.math.BigDecimal)) {
						BigDecimal value	= (BigDecimal)entry.getValue();
						double doubleValue  = value.doubleValue();

						if(doubleValue % 1 > 0) {
							entry.setValue(value.doubleValue());
						} else {
							entry.setValue(value.intValue());
						}
					}
				}
			}
		} catch(Exception e) {
			logger.error("selectList DAO Exception : " + e.getMessage());
			throw new DAOException(e.getMessage());
		}

		return ret;
	}
	
	/**
	  * @MethodName : selectMultiList
	  * @작성일 : 2017. 5. 10.
	  * @작성자 : jyseo
	  * @변경이력 : 
	  * 	2017. 5. 10. 최초생성
	  * @Method 설명 : SP dataset 2건 이상
	  * @param queryId 쿼리명
	  * @param parameterObject Map<String, V>
	  * @return
	 */
	@Override
	public List<List<Map<String, Object>>> selectMultiList(String queryId, Map<String, V> parameterObject) {
		List<List<Map<String, Object>>> ret;

		try {
			if(parameterObject == null) {
				ret = super.selectList(queryId);
			} else {
				ret = super.selectList(queryId, parameterObject);
			}
			
		} catch(Exception e) {
			logger.error("selectMultiList DAO Exception : " + e.getMessage());
			throw new DAOException(e.getMessage());
		}

		return ret;
	}

	public Object core_select(String queryId, Map<String, V> parameterObject) {
		Object ret;

		try {
			if(parameterObject == null) {
				ret = super.selectOne(queryId);
			} else {
				ret = super.selectOne(queryId, parameterObject);
			}

			if(ret != null) {
				if(ret instanceof Map) {
					Iterator iter = ((Map) ret).entrySet().iterator();

					while(iter.hasNext()) {
						Map.Entry entry = (Map.Entry)iter.next();

						if(entry.getValue() != null && (entry.getValue() instanceof java.sql.Clob)) {
							entry.setValue(this.getStringForClob((Clob)entry.getValue()));
						} else if(entry.getValue() != null && (entry.getValue() instanceof java.math.BigDecimal)) {
							BigDecimal value	= (BigDecimal)entry.getValue();
							double doubleValue  = value.doubleValue();

							if(doubleValue % 1 > 0) {
								entry.setValue(value.doubleValue());
							} else {
								entry.setValue(value.intValue());
							}
						}
					}
				}
			}
		} catch(Exception e) {
			logger.error("select DAO Exception : " + e.getMessage());
			throw new DAOException(e.getMessage());
		}

		return ret;
	}

	public int core_setInsert(String queryId, Map<String, V> parameterObject) {
		int ret = 0;

		try {
			if(parameterObject == null) {
				ret = super.insert(queryId);
			} else {
				ret = super.insert(queryId, parameterObject);
			}
		} catch(Exception e) {
			logger.error("setInsert DAO Exception : " + e.getMessage());
			throw new DAOException(e.getMessage());
		}

		return ret;
	}

	public int core_setUpdate(String queryId, Map<String, V> parameterObject) {
		int ret = 0;

		try {
			if(parameterObject == null) {
				ret = super.update(queryId);
			} else {
				ret = super.update(queryId, parameterObject);
			}
		} catch(Exception e) {
			logger.error("setUpdate DAO Exception : " + e.getMessage());
			throw new DAOException(e.getMessage());
		}

		return ret;
	}

	public int core_setDelete(String queryId, Map<String, V> parameterObject) {
		int ret = 0;

		try {
			if(parameterObject == null) {
				ret = super.delete(queryId);
			} else {
				ret = super.delete(queryId, parameterObject);
			}
		} catch(Exception e) {
			logger.error("setDelete DAO Exception : " + e.getMessage());
			throw new DAOException(e.getMessage());
		}

		return ret;
	}

	private String getStringForClob(Clob clob) {
		StringBuffer sbf	= new StringBuffer();
		Reader br		   = null;
		char[] buf		  = new char[1024];
		int readcnt;

		try {
			br = clob.getCharacterStream();
			while ((readcnt=br.read(buf,0,1024))!=-1) {
				sbf.append(buf,0,readcnt);
			}
		} catch (Exception e) {
			logger.error("Failed to create String object from CLOB" + e);
			throw new DAOException(e.getMessage());
		} finally {
			if(br != null)
				try {
					br.close();
				} catch (IOException e) {
					logger.error("Failed to close BufferedReader object" + e);
					throw new DAOException(e.getMessage());
			   }
		}

		return sbf.toString();
	}

}
