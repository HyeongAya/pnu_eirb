/**
 *
 */
package kr.ac.pusan.core;

public class ProjectConstant {

	/**
	 * 페이지에서 넘어오는 인코딩 값을 의미한다.
	 */
	public static final String ENCODING_PAGE = "UTF-8";

	/**
	 * 서버에서 처리되는 인코딩 값을 의미한다.
	 */
	public static final String ENCODING_SERVER = "UTF-8";

	/**
	 * 사용자 이름 세션값
	 */
	public static final String KEY_SESSION_MEMBER_NAME = "u_nm";

	/**
	 * 사용자 아이디 세션값
	 */
	public static final String KEY_SESSION_MEMBER_ID	 = "u_id";

	/**
	 * 사용자 소속 세션값
	 */
	public static final String KEY_SESSION_MEMBER_DEPT = "u_dept";
	
	/**
	 * 사용자 소속명 세션값
	 */
	public static final String KEY_SESSION_MEMBER_DEPT_NAME = "u_dept_nm";
	
	/**
	 * 사용자 권한구분 세션값
	 */
	public static final String KEY_SESSION_MEMBER_AUTH_GCD = "u_auth_gcd";
	
	/**
	 * 사용자 권한코드 세션값
	 */
	public static final String KEY_SESSION_MEMBER_AUTH_CODE = "u_auth_cd";
	
	/**
	 * 사용자 권한속성 세션값
	 */
	public static final String KEY_SESSION_MEMBER_AUTH_STR = "u_auth_str";
	
	/**
	 * 사용자 이메일 세션값
	 */
	public static final String KEY_SESSION_MEMBER_EMAIL = "u_email";

	/**
	 * 사용자 연락처 세션값
	 */
	public static final String KEY_SESSION_MEMBER_TEL = "u_tel";

	/**
	 * 사용자 그룹 세션값
	 */
	public static final String KEY_SESSION_MEMBER_GROUP = "u_group";

	/**
	 * 최고관리자 여부의 세션값
	 */
	public static final String KEY_SESSION_SUPER_ADMIN = "isSuperAdmin";

	public static final String KEY_INTERCEPTOR_VALUES_POSTFIX = "_LIST";
	public static final String KEY_INTERCEPTOR_STRING_VALUES_POSTFIX = "_LIST_STRING";
	public static final String KEY_INTERCEPTOR_QUOTE_POSTFIX = "_QUOTE";
	public static final String KEY_INTERCEPTOR_MULTI_LIST  = "INTC_MULTI_LIST";

	public static final String KEY_FORM_DOMAIN_UPLOAD_DIR = "upload";
	public static final String KEY_FORM_DOMAIN_UPLOAD_DIR_SUB = "upload_sub";
	public static final String KEY_FORM_DOMAIN_UPLOAD_ORIGINAL_NM_YN = "original_nm_yn";
	public static final String KEY_FORM_FILE = "u_file";
	public static final String KEY_FORM_EDITORFILE = "upload";
	public static final String KEY_FORM_EXCELFILE = "u_excelFile";
	public static final String KEY_FORM_FILE_MAX_SIZE = "max_size";
}
