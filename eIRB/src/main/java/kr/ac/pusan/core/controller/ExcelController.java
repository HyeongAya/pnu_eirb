/**
 * 
 */
package kr.ac.pusan.core.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

@SuppressWarnings({"rawtypes","unchecked"})
public class ExcelController extends AbstractExcelView {
	
	protected final Log logger = LogFactory.getLog("pnu_cms");
	
	protected void buildExcelDocument(Map model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String fileName = request.getParameter("file_name");
		
		if(fileName == null || "".equals(fileName)) {
			fileName = "download.xls";
		}
		response.setHeader("Content-Disposition", "attachment; fileName=\"" + fileName + "\";"); 
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		List<Map> voMapList = (List<Map>) model.get("voMapList");
		String sheetTitle   = request.getParameter("sheet");
		
		if(sheetTitle == null || "".equals(sheetTitle)) {
			sheetTitle = "Sheet1";
		}
		
		executeExcel(workbook, sheetTitle, voMapList);
	}
	
	protected void executeExcel(HSSFWorkbook hssfWorkbook, String sheetTitle, List<Map> voMapList) throws IOException {
		Workbook workbook   = hssfWorkbook;		
		Sheet sheet		 = createFirstSheet(workbook, sheetTitle);
		
		createColumnLabel(sheet, voMapList);
		createColumn(sheet, voMapList);
		fileOutput(workbook);
	}
	
	private Sheet createFirstSheet(Workbook workbook, String sheetTitle) {
		Sheet sheet = workbook.createSheet();
		workbook.setSheetName(0, sheetTitle);
		sheet.setColumnWidth(1, 256 * 20);
		
		return sheet;
	}
	
	private void createColumnLabel(Sheet sheet, List<Map> voMapList) {
		if(voMapList.size() > 0) {
			Row firstRow					= sheet.createRow(0);			
			Map<String, Object> labelMap	= voMapList.get(0);
			int colNum					  = 0;
			
			for(String key : labelMap.keySet()) {
				Cell cell = firstRow.createCell(colNum);
				Object obj = labelMap.get(key);
				setMapToCellValue(cell, obj);
				colNum++;
			}
		}
	}

	private void createColumn(Sheet sheet, List<Map> voMapList) {
		for(int rowNum = 1; rowNum < voMapList.size(); rowNum++) {
			if(rowNum == voMapList.size()) {
				break;
			}
			
			Row row					 = sheet.createRow(rowNum);				
			Map<String, String> dataMap = voMapList.get(rowNum);
			int colNum				  = 0;
			
			for(String key : dataMap.keySet()) {
				Cell cell   = row.createCell(colNum);
				Object obj  = dataMap.get(key);
				setMapToCellValue(cell, obj);
				
				colNum++;
			}
		}
	}
	
	private void setMapToCellValue(Cell cell, Object value) {
		if(value instanceof BigDecimal) {
			BigDecimal valueTemp = (BigDecimal) value;
			cell.setCellValue(valueTemp.doubleValue());
		} else {
			String valueTemp = String.valueOf(value);
			if(valueTemp == null || valueTemp.equals("null")) {
				valueTemp = "";
			}
			cell.setCellValue(valueTemp);
		}
	}
	
	private void fileOutput(Workbook workbook) throws IOException {
		FileOutputStream fout = new FileOutputStream("download.xls");
		workbook.write(fout);
		fout.close();
	}

}
