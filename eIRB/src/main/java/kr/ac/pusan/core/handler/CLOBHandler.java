package kr.ac.pusan.core.handler;

import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

public class CLOBHandler implements TypeHandler {

	public Object getResult(ResultSet arg0, String arg1) throws SQLException {
		return arg0.getString(arg1);
	}

	public Object getResult(ResultSet arg0, int arg1) throws SQLException {
		return arg0.getString(arg1);
	}

	public Object getResult(CallableStatement arg0, int arg1)
			throws SQLException {
		return arg0.getString(arg1);
	}

	public void setParameter(PreparedStatement arg0, int arg1, Object arg2,
			JdbcType arg3) throws SQLException {
		String s = (String) arg2;
		StringReader reader = new StringReader(s);
		arg0.setCharacterStream(arg1, reader, s.length());
	}

}
