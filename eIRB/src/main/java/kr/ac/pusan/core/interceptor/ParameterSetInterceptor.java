/**
 *
 */
package kr.ac.pusan.core.interceptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.util.StringUtil;

@SuppressWarnings({"rawtypes", "unchecked"})
public class ParameterSetInterceptor extends HandlerInterceptorAdapter {

	@Value("#{prop['project.dbms']}") String P_DBMS;

	protected final Log logger = LogFactory.getLog("pnu_eirb");

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if(handler instanceof HandlerMethod) {
			Map<String, String[]> requestMap = request.getParameterMap();

			if(((HandlerMethod)handler).getBean() instanceof BaseController) {
				logger.info("# [ParameterSetInterceptor] Create ParamMap");

				BaseController controller	   = (BaseController)((HandlerMethod)handler).getBean();
				Map<String, Object> paramMap	= new HashMap<String, Object>();
				StringUtil su				   = new StringUtil();

				for(String key : requestMap.keySet()) {
					String[] values = requestMap.get(key);

					StringBuilder sb					= new StringBuilder();
					StringBuilder sbQuote			   = new StringBuilder();
					List<Object> multiValueMapList	  = new ArrayList<Object>();
					List<Object> multiValueStringList   = new ArrayList<Object>();

					if(values.length == 1) {
						String parameterValue = values[0];

						if("GET".equals(request.getMethod())){
							parameterValue = su.encoding(parameterValue);
						}

						paramMap.put(key.toUpperCase(), parameterValue);
						sbQuote.append("'" + parameterValue + "'");
						multiValueStringList.add(parameterValue);
						setMultiValue(request, multiValueMapList, handler, key.toUpperCase(), parameterValue);

						paramMap.put(key.toUpperCase() + ProjectConstant.KEY_INTERCEPTOR_QUOTE_POSTFIX, sbQuote.toString());
						paramMap.put(key.toUpperCase() + ProjectConstant.KEY_INTERCEPTOR_STRING_VALUES_POSTFIX, multiValueStringList);
						paramMap.put(key.toUpperCase() + ProjectConstant.KEY_INTERCEPTOR_VALUES_POSTFIX, multiValueMapList);
					} else {
						for(int idx = 0; idx < values.length; idx++) {
							String parameterValue = values[idx];

							if("GET".equals(request.getMethod())){
								parameterValue = su.encoding(parameterValue);
							}

							if(idx > 0) {
								sb.append("," + parameterValue);
								sbQuote.append(",'" + parameterValue + "'");
							} else {
								sb.append(parameterValue);
								sbQuote.append("'" + parameterValue + "'");
							}

							multiValueStringList.add(parameterValue);
							setMultiValue(request, multiValueMapList, handler, key.toUpperCase(), parameterValue);
						}

						paramMap.put(key.toUpperCase(), sb.toString());
						paramMap.put(key.toUpperCase() + ProjectConstant.KEY_INTERCEPTOR_QUOTE_POSTFIX, sbQuote.toString());
						paramMap.put(key.toUpperCase() + ProjectConstant.KEY_INTERCEPTOR_STRING_VALUES_POSTFIX, multiValueStringList);
						paramMap.put(key.toUpperCase() + ProjectConstant.KEY_INTERCEPTOR_VALUES_POSTFIX, multiValueMapList);
					}
				}

				setParamMapETC(request, paramMap, handler);
				copyParamMapToMultiMap(paramMap);

				if("mysql".equals(P_DBMS) || "mssql".equals(P_DBMS)) {
					convertEmptyToNull(paramMap);
				}

				controller.setParamMap(paramMap);
			}
		}

		return super.preHandle(request, response, handler);
	}

	/**
	 * 콤보로 생성된 값을 맵형태로 저장
	 * @param request HttpServletRequest
	 * @param multiValueList 저장할 맵리스트
	 * @param handler 핸들러 오브젝트
	 * @param key 맵에 저장될 키값
	 * @param value 맵에 저장될 값
	 */
	private void setMultiValue(HttpServletRequest request, List<Object> multiValueList, Object handler, String key, String value) {
		Map<String, Object> multiMap = new HashMap<String, Object>();

		multiMap.put(key, value);
		multiValueList.add(multiMap);
		setParamMapETC(request, multiMap, handler);
	}

	/**
	 * 파라미터맵에 부가정보 저장 (아이피, 등록자ID)
	 * @param request HttpServletRequest
	 * @param paramMap 파라미터맵
	 * @param handler  핸들러 오브젝트
	 */
	private void setParamMapETC(HttpServletRequest request, Map<String, Object> paramMap, Object handler) {
		HttpSession session = request.getSession();
		String insertId = (String) session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID);
		
		paramMap.put("INS_IP", request.getRemoteAddr());
		paramMap.put("INS_ID", insertId);
		paramMap.put("UPD_IP", request.getRemoteAddr());
		paramMap.put("UPD_ID", insertId);
	}
	
	/**
	 * paramMap 에 있는 데이터를 multiMap으로 저장
	 * @param paramMap 파라미터맵
	 */
	private void copyParamMapToMultiMap(Map<String, Object> paramMap) {
		Iterator iterator = paramMap.entrySet().iterator();

		while(iterator.hasNext()) {
			Map.Entry  value = (Map.Entry) iterator.next();

			if(value.getValue() instanceof List) {
				List mapList = (List)value.getValue();

				for(int idx = 0; idx < mapList.size(); idx++) {
					if(mapList.get(idx) instanceof Map) {
						copyMap(paramMap, (Map)mapList.get(idx));
					}
				}
			}
		}
	}

	/**
	 * multiMap으로 저장시 데이터가 맵일 경우 풀어서 multiMap에 저장
	 * @param paramMap 파라미터맵
	 * @param paramMap 멀티값맵
	 */
	private void copyMap(Map<String, Object> paramMap, Map multiMap) {
		Iterator iterator = paramMap.entrySet().iterator();

		while(iterator.hasNext()) {
			Map.Entry value = (Map.Entry)iterator.next();

			if(value.getValue() instanceof String) {
				String itemValue	= (String)value.getValue();
				String itemKey	  = (String)value.getKey();

				if(!multiMap.containsKey(itemKey)) {
					multiMap.put(itemKey, itemValue);
				}
			}
		}
	}

	/**
	 * 파라미터의 값에 ''(공백) 값을 null로 치환시킨다. (mysql일 경우 ''값이 null로 들어가지 않음)
	 * @param paramMap 파라미터맵
	 */
	private void convertEmptyToNull(Map<String, Object> paramMap) {
		Iterator iterator = paramMap.entrySet().iterator();

		while(iterator.hasNext()) {
			Map.Entry value = (Map.Entry)iterator.next();

			if(value.getValue() instanceof String) {
				if("".equals(value.getValue())) {
					value.setValue(null);
				}
			}
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		if(handler instanceof HandlerMethod) {
			if(((HandlerMethod)handler).getBean() instanceof BaseController) {
				logger.info("# [ParameterSetInterceptor] Remove ParamMap");

				BaseController controller = (BaseController)((HandlerMethod)handler).getBean();
				controller.clearParamMap();
			}
		}

		super.afterCompletion(request, response, handler, ex);
	}

}
