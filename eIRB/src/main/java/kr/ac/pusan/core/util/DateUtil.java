/**
 * 
 */
package kr.ac.pusan.core.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	/**
	 * <p>현재날짜를 <strong>yyyyMMdd</strong>와 같은 형식으로 리턴한다.</p>
	 * @return 현재날짜
	 */
	public String getNowDate() {
		return getNowDate("");
	}
	
	/**
	 * <p>현재날짜를 지정된 형식으로 리턴한다.</p>
	 * @param returnType 리턴값의 구분 {<strong>y</strong>|<strong>m</strong>|<strong>d</strong>|<i>그외</i>}
	 * @return returnType에 따라 y는 현재년도 m은 현재월 d는 현재일자 그외에는 년월일의 구분자로 나타난다.
	 */
	public String getNowDate(String returnType) {
		StringUtil su   = new StringUtil();
		Calendar cal	= Calendar.getInstance();
		String yyyy	 = String.valueOf(cal.get(Calendar.YEAR));
		String mm	   = su.lpad(String.valueOf(cal.get(Calendar.MONTH) + 1), 2, "0");
		String dd	   = su.lpad(String.valueOf(cal.get(Calendar.DATE)), 2, "0");
		
		if("y".equals(returnType.toLowerCase())) {
			return yyyy;
		} else if("m".equals(returnType.toLowerCase())) {
			return mm;
		} else if("d".equals(returnType.toLowerCase())) {
			return dd;
		} else {
			return yyyy + returnType + mm + returnType + dd;
		}
	}
	
	/**
	  * @MethodName : getCurrentDate
	  * @작성일 : 2017. 6. 26.
	  * @작성자 : jyseo
	  * @변경이력 : 
	  * 	2017. 6. 26. 최초생성
	  * @Method 설명 :
	  * @param fmt 날짜포맷
	  * @return
	 */
	public static String getCurrentDate(String fmt) {
		return getFormatDate(new Date(), fmt);
	}
	
	/**
	 * @MethodName : getFormatDate
	 * @작성일 : 2017. 6. 26.
	 * @작성자 : jyseo
	 * @변경이력 : 
	 * 	2017. 6. 26. 최초생성
	 * @Method 설명 :
	 * @param fmt 날짜포맷
	 * @return
	 */
	public static String getFormatDate(Date date, String fmt) {
		SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		return sdf.format(date);
	}
	/**
	 * <p>날짜형식의 유효성을 검사한다.</p>
	 * @param date 문자열 형식의 날짜
	 * @return 유효한 날짜여부
	 */
	public boolean validateDate(String date) {
		boolean returnValue	 = true;		
		SimpleDateFormat sdf	= new SimpleDateFormat("yyyyMMdd");
		
		sdf.setLenient(false);
		
		try {
			sdf.parse(date.replaceAll("\\D", ""));
		} catch(ParseException pe) {
			returnValue = false;
		} catch(IllegalArgumentException iae) {
			returnValue = false;
		}
		
		return returnValue;
	}
	
	/**
	 * <p>대상날짜에 지정된 일수를 더한다.</p>
	 * @param date 문자열 형식의 대상날짜
	 * @param type 더하고자 하는 위치의 구분<br />{&lt;년&gt;[<strong>year</strong>|<strong>y</strong>|<strong>yy</strong>|<strong>yyyy</strong>]|&lt;월&gt;[<strong>month</strong>|<strong>mon</strong>|<strong>m</strong>|<strong>mm</strong>]|&lt;일&gt;[<strong>date</strong>|<strong>day</strong>|<strong>d</strong>|<strong>dd</strong>]}
	 * @param addValue 더하고자 하는 정수
	 * @return 대상날짜에서 지정된 수많큼 더해진 날짜
	 */
	public String addDate(String date, String type, int addValue) {
		DateFormat df	   = new SimpleDateFormat("yyyyMMdd");
		String returnValue  = date;
		
		if(validateDate(date)) {
			try {
				Date dt		 = df.parse(date.replaceAll("\\D", ""));
				Calendar cal	= Calendar.getInstance();
				
				cal.setTime(dt);
				
				if("year".equals(type.toLowerCase()) || "y".equals(type.toLowerCase()) || "yy".equals(type.toLowerCase()) || "yyyy".equals(type.toLowerCase())) {
					cal.add(Calendar.YEAR, addValue);
				} else if("month".equals(type.toLowerCase()) || "mon".equals(type.toLowerCase()) || "m".equals(type.toLowerCase()) || "mm".equals(type.toLowerCase())) {
					cal.add(Calendar.MONTH, addValue);
				} else if("day".equals(type.toLowerCase()) || "date".equals(type.toLowerCase()) || "d".equals(type.toLowerCase()) || "dd".equals(type.toLowerCase())) {
					cal.add(Calendar.DATE, addValue);
				}
				
				returnValue = df.format(cal.getTime());
			} catch(ParseException pe) {
				pe.printStackTrace();
			}
		}
		
		return returnValue;
	}
	
	/**
	 * <p>두 날짜의 차이를 구한다.</p>
	 * @param strBeginDate 문자열 형식의 시작일자
	 * @param strEndDate 문자열 형식의 종료일자
	 * @return 종료일자에서 시작일자를 뺀 일수
	 */
	public long differentDate(String strBeginDate, String strEndDate) {
		SimpleDateFormat sdf	= new SimpleDateFormat("yyyyMMdd");
		long returnValue		= 0;

		if(validateDate(strBeginDate) && validateDate(strEndDate)) {
			try {
				Date beginDate  = sdf.parse(strBeginDate.replaceAll("\\D", ""));
				Date endDate	= sdf.parse(strEndDate.replaceAll("\\D", ""));
				long diffTime   = endDate.getTime() - beginDate.getTime();
				
				returnValue	 = diffTime / (24 * 60 * 60 * 1000);
			} catch(ParseException pe) {
				pe.printStackTrace();
			}
		}
		
		return returnValue;
	}

}
