/**
 * 
 */
package kr.ac.pusan.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

public class FileUtil {
	
	@Value("#{prop['project.root']}") String P_ROOT_PATH;
	
	/**
	 * <p>지정된 경로에 파일저장</p>
	 * @param originalFileNameYN 원본파일명 사용여부
	 * @param path 파일이 저장될 경로
	 * @param srcFile 대상파일
	 * @return 저장된 파일명
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public String saveFile(String originalFileNameYN, String path, MultipartFile srcFile) throws IllegalStateException, IOException {
		if(srcFile == null || srcFile.isEmpty()) {
			return null;
		}
		
		File saveDir = new File(path);
		if(!saveDir.exists() || saveDir.isFile()) {
			saveDir.mkdirs();
		}
		
		String fileName = "";
		if("Y".equals(originalFileNameYN)) {
			fileName = srcFile.getOriginalFilename();
		} else {
			fileName = getUUID() + "." + getFileExt(srcFile.getOriginalFilename());
		}
		
		String destFilePath = path + fileName;
		srcFile.transferTo(new File(destFilePath));
		
		return fileName;
	}
	
	/**
	 * <p>지정된 경로에 파일들을 저장</p>
	 * @param originalFileNameYN 원본파일명 사용여부
	 * @param path 파일이 저장될 경로
	 * @param srcFile 대상파일들
	 * @return 저장된 파일명들
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public List<String> saveFiles(String originalFileNameYN, String path, MultipartFile[] srcFiles) throws IllegalStateException, IOException {
		if(srcFiles == null) {
			return null;
		}
		
		List<String> fileNameList = new ArrayList<String>();
		for(int idx = 0; idx < srcFiles.length; idx++) {
			String fileName = saveFile(originalFileNameYN, path, srcFiles[idx]);
			fileNameList.add(fileName);
		}
		
		return fileNameList;
	}
	
	/**
	 * <p>현재 시간값 가져오기</p>
	 * @return yyyyMMddHHmmssSSS형태의 현재 시간값
	 */
	public String getTimeKey() {
		SimpleDateFormat formatter  = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.KOREA);
		Date currentTime			= new Date();
		
		return formatter.format(currentTime);
	}
	
	/**
	 * <p>UUID값 가져오기</p>
	 * @return UUID값
	 */
	public String getUUID() {
		UUID uuid = UUID.randomUUID();
		
		return uuid.toString();
	}
	
	/**
	 * <p>파일 확장자 가져오기</p>
	 * @param fileName 파일명
	 * @return 파일 확장자
	 */
	public String getFileExt(String fileName) {
		return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
	}
	
	/**
	 * <p>파일 확장자를 제외한 파일명 가져오기</p>
	 * @param fileName 파일명
	 * @return 파일명
	 */
	public String getFileNameWithoutExt(String fileName) {
		return fileName.substring(0, fileName.lastIndexOf("."));
	}
	
	/**
	 * <p>파일을 복사한다.</p>
	 * @param srcFile 복사할파일명
	 * @param destFile 복사될파일명
	 * @throws IOException
	 */
	public void copyFile(File srcFile, File destFile) throws IOException {
		FileUtils.copyFile(srcFile, destFile);
	}
	
	/**
	 * <p>파일의 내용을 읽어온다.</p>
	 * @param path 읽어올 파일의 웹루트부터의 경로
	 * @return 읽은 파일내용
	 */
	public String readFile(String path) {
		String fullPath	 = System.getProperty(P_ROOT_PATH) + StringUtils.replace(path, "/", File.separator);
		File file		   = new File(fullPath);
		StringBuilder sb	= new StringBuilder();
		
		try {
			sb.append(FileUtils.readFileToString(file, "UTF-8"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	/**
	 * <p>지정된 내용으로 파일을 생성한다.</p>
	 * @param path 생성될 파일의 웹루트부터의 경로
	 * @param content 생성될 파일내용
	 */
	public void writeFile(String path, String content) {
		if(path != null && path.startsWith("/")) {
			path = path.substring(1);
		}
		
		String fullPath = System.getProperty(P_ROOT_PATH) + StringUtils.replace(path, "/", File.separator);
		
		try {
			FileUtils.writeStringToFile(new File(fullPath), StringUtils.chomp(content), "UTF-8");
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * <p>엑셀파일의 내용을 읽어온다.</p>
	 * @param srcFileName 읽어올 엑셀파일의 실제 물리경로
	 * @param startRow 엑셀파일에서 읽어들일 시작행
	 * @param startColumn 엑셀파일에서 읽어들일 시작열
	 * @return 읽은 파일내용
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public List<Map<String, String>> readExcel(String srcFileName, int startRow, int startColumn) throws FileNotFoundException, IOException {
		List<Map<String, String>> list  = new ArrayList<Map<String, String>>();
		HSSFWorkbook workbook		   = null;
		HSSFSheet sheet				 = null;
		HSSFRow row					 = null;
		HSSFCell cell				   = null;
		
		workbook = new HSSFWorkbook(new FileInputStream(new File(srcFileName)));
		
		if(workbook != null) {
			sheet = workbook.getSheetAt(0);
			
			if(sheet != null) {
				int startRowIdx	 = startRow;
				int endRowIdx	   = sheet.getLastRowNum();
				int startColumnIdx  = startColumn;
				int endColumnIdx	= sheet.getRow(startRow).getLastCellNum();
				String value		= "";
				
				for(int rowIdx = startRowIdx; rowIdx <= endRowIdx; rowIdx++) {
					row = sheet.getRow(rowIdx);
					
					Map<String, String> cellMap = new HashMap<String, String>();
					
					for(int columnIdx = startColumnIdx; columnIdx <= endColumnIdx; columnIdx++) {
						cell = row.getCell(columnIdx);
						
						if(cell == null) {
							continue;
						}
						
						if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							value = String.valueOf(cell.getNumericCellValue());
						} else {
							value = cell.getStringCellValue();
						}
						
						cellMap.put("COLUMN" + columnIdx, value);
					}
					
					list.add(cellMap);
				}
			}
		}
		
		return list;
	}

}
