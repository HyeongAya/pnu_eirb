/**
 * 
 */
package kr.ac.pusan.core.vo;

import org.springframework.web.multipart.MultipartFile;

public class MultipartFileWrapper {
	
	private MultipartFile file;
	private String fileURL;
	private String filePath;
	private String webPath;
	private String fileName;
	
	/**
	 * <p>MultipartFileWrapper생성자</p>
	 * @param file MultipartFile
	 */
	public MultipartFileWrapper(MultipartFile file) {
		this.file	   = file;
	}
	
	/**
	 * <p>MultipartFileWrapper생성자</p>
	 * @param file MultipartFile
	 * @param fileName 파일명
	 * @param fileURL 파일URL
	 * @param filePath 파일경로
	 */
	public MultipartFileWrapper(MultipartFile file, String fileName, String fileURL, String filePath, String webPath) {
		this.file = file;
		this.fileName   = fileName;
		this.filePath   = filePath;
		this.fileURL	= fileURL;
		this.webPath	= webPath;
	}
	
	/**
	 * <p>파일크기를 가져온다.</p>
	 * @return 파일 byte 크기
	 */
	public long getSize() {
		return file.getSize();
	}
	
	/**
	 * <p>업로드되는 파일의 실제 저장되는 이름을 가져온다.</p>
	 * @return 파일이 서버에 저장되는 실제명
	 */
	public String getName() {
		return file.getName();
	}
	
	/**
	 * <p>업로드되는 파일의 이름을 가져온다.</p>
	 * @return 업로드되는 파일명
	 */
	public String getOriginalFileName() {
		return file.getOriginalFilename();
	}
	
	/**
	 * <p>파일의 존재여부를 가져온다.</p>
	 * @return 파일의 존재여부
	 */
	public boolean isEmpty() {
		return file.isEmpty();
	}
	
	/**
	 * <p>파일의 MIME Type을 가져온다.</p>
	 * @return 파일의 MIME Type
	 */
	public String getContentType() {
		return file.getContentType();
	}

	/**
	 * <p>파일의 URL을 가져온다.</p>
	 * @return 파일URL
	 */
	public String getFileURL() {
		return fileURL;
	}

	/**
	 * <p>파일의 URL을 설정한다.</p>
	 * @param 파일URL
	 */
	public void setFileURL(String fileURL) {
		this.fileURL = fileURL;
	}

	/**
	 * <p>파일의 웹경로를 가져온다.</p>
	 * @return 파일의 웹경로
	 */
	public String getWebPath() {
		return webPath;
	}

	/**
	 * <p>파일의 웹경로를 설정한다.</p>
	 * @param 파일의 웹경로
	 */
	public void setWebPath(String webPath) {
		this.webPath = webPath;
	}

	/**
	 * <p>파일의 전체경로를 가져온다.</p>
	 * @return 파일의 전체경로
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * <p>파일의 전체경로를 설정한다.</p>
	 * @param 파일의 전체경로
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * <p>파일이 서버에 저장된 실제 이름을 가져온다.</p>
	 * @return 서버에 저장된 실제 파일명
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * <p>파일이 서버에 저장된 실제 이름을 설정한다.</p>
	 * @param 서버에 저장된 실제 파일명
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
