package kr.ac.pusan.core.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unchecked")
public class Tree {

	private class Node {

		private Object data	 = null;
		private Node prevNode   = null;
		private Node nextNode   = null;

		private Node(Object data) {
			this.data	   = data;
			this.prevNode   = null;
			this.nextNode   = null;
		}
	}

	private class Item extends HashMap<String, Object> implements Comparable<Item> {

		private static final long serialVersionUID = 6617313461945559825L;
		private String keyName = "";

		public Item(Map<String, Object> item, String keyNm) {
			this.keyName = keyNm;

			Set<String> key = item.keySet();

			for (Iterator<String> iterator = key.iterator(); iterator.hasNext();) {
				String keyName = iterator.next();

				put(keyName, item.get(keyName));
			}
		}

		public Map<String, Object> getMap() {
			Map<String, Object> map = new HashMap<String, Object>();
			Set<String> key		 = keySet();

			for (Iterator<String> iterator = key.iterator(); iterator.hasNext();) {
				String keyName = iterator.next();

				map.put(keyName, get(keyName));
			}

			return map;
		}

		public int compareTo(Item o) {
			return String.valueOf(get(this.keyName)).compareTo(String.valueOf(o.get(this.keyName)));
		}

	}

	private static String _KEY	  = "";
	private static String _P_KEY	= "";
	private static String _N_P_KEY  = "";
	private static String _SORT	 = "";
	private static String _V_SORT   = "";

	private final static Comparator<Item> ADescCompare = new Comparator<Item>() {
		public int compare(Item object1, Item object2) {
			int ret = 0;
			if(Long.parseLong(String.valueOf(object1.get(Tree._P_KEY))) < Long.parseLong(String.valueOf(object2.get(Tree._P_KEY)))) {
				ret = -1;
			} else if (Long.parseLong(String.valueOf(object1.get(Tree._P_KEY))) == Long.parseLong(String.valueOf(object2.get(Tree._P_KEY)))) {
				if("-1".equals(String.valueOf(object1.get(Tree._P_KEY)))) {
					if (Long.parseLong(String.valueOf(object1.get(Tree._SORT))) < Long.parseLong(String.valueOf(object2.get(Tree._SORT)))) {
						ret = -1;
					} else if (Long.parseLong(String.valueOf(object1.get(Tree._SORT))) > Long.parseLong(String.valueOf(object2.get(Tree._SORT)))) {
						ret = 1;
					}
				} else {
					if (Long.parseLong(String.valueOf(object1.get(Tree._SORT))) > Long.parseLong(String.valueOf(object2.get(Tree._SORT)))) {
						ret = -1;
					} else if (Long.parseLong(String.valueOf(object1.get(Tree._SORT))) < Long.parseLong(String.valueOf(object2.get(Tree._SORT)))) {
						ret = 1;
					}
				}
			}

			return ret;
		}
	};

	private final static Comparator<Item> AscCompare = new Comparator<Item>() {
		public int compare(Item object1, Item object2) {
			int ret = 0;
			if(Long.parseLong(String.valueOf(object1.get(Tree._P_KEY))) < Long.parseLong(String.valueOf(object2.get(Tree._P_KEY)))) {
				ret = -1;
			} else if (Long.parseLong(String.valueOf(object1.get(Tree._P_KEY))) == Long.parseLong(String.valueOf(object2.get(Tree._P_KEY)))) {
				if (Long.parseLong(String.valueOf(object1.get(Tree._SORT))) < Long.parseLong(String.valueOf(object2.get(Tree._SORT)))) {
					ret = -1;
				} else if (Long.parseLong(String.valueOf(object1.get(Tree._SORT))) > Long.parseLong(String.valueOf(object2.get(Tree._SORT)))) {
					ret = 1;
				}
			}

			return ret;
		}
	};

	private Node headNode   = null;
	private List<Item> list = null;

	public Tree(String key, String p_key, String sort, String n_p_key, String v_sort) {
		Tree._KEY	   = key;
		Tree._P_KEY	 = p_key;
		Tree._N_P_KEY   = n_p_key;
		Tree._SORT	  = sort;
		Tree._V_SORT	= v_sort;
		list			= new ArrayList<Item>();
	}

	public void add(Object data) {
		Node newNode = new Node(data);

		if(headNode == null) {
			headNode = newNode;
		} else {
			Node tailNode = headNode;

			while(tailNode.nextNode != null) {
				tailNode = tailNode.nextNode;
			}

			tailNode.nextNode   = newNode;
			newNode.prevNode	= tailNode;
		}

		Item item = new Item((Map<String, Object>)data, Tree._SORT);
		list.add(item);
	}

	private void add(Item data) {
		Node newNode = new Node(data.getMap());

		if(headNode == null) {
			headNode = newNode;
		} else {
			Node tailNode = headNode;

			while(tailNode.nextNode != null) {
				tailNode = tailNode.nextNode;
			}

			tailNode.nextNode   = newNode;
			newNode.prevNode	= tailNode;
		}
	}

	public void add(List<Map<String, Object>> list) {
		headNode	= null;

		this.list.clear();

		for(Map<String, Object> map : list) {
			add(map);
		}
	}

	public void add(int KEY, int P_KEY, int SORT) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Tree._KEY, String.valueOf(KEY));
		map.put(Tree._P_KEY, String.valueOf(P_KEY));
		map.put(Tree._SORT, String.valueOf(SORT));

		add(map);
	}

	public void add(String KEY, String P_KEY, String SORT) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Tree._KEY, KEY);
		map.put(Tree._P_KEY, P_KEY);
		map.put(Tree._SORT, SORT);

		add(map);
	}

	private void addList(List<Item> list) {
		headNode	= null;

		for(Item item : list) {
			add(item);
		}
	}

	private Node getNode(int loc) {
		Node currentNode = headNode;

		while ((--loc) >= 0) {
			currentNode = currentNode.nextNode;
		}

		return currentNode;
	}

	public void sort() {
		Collections.sort(this.list, AscCompare);

		int v_sort  = 1;
		long v_pKey = 0;

		for(int idx = 0; idx < this.list.size(); idx++) {
			Map<String, Object> mp = this.list.get(idx);

			if(idx == 0 || Long.valueOf(String.valueOf(mp.get(Tree._P_KEY))).equals(v_pKey)) {
				mp.put(Tree._V_SORT, v_sort++);
			} else {
				v_sort = 1;
				mp.put(Tree._V_SORT, v_sort++);
			}

			v_pKey = Long.valueOf(String.valueOf(mp.get(Tree._P_KEY)));
		}

		Collections.sort(this.list, ADescCompare);

		addList(this.list);

		for(int idx = 0; idx < this.list.size(); idx++) {
			Node currentNode				= getNode(idx);
			Node currentPrev				= getNode(idx - 1);
			Map<String, Object> targetMap   = (Map<String, Object>)currentNode.data;
			Map<String, Object> prevMap	 = (Map<String, Object>)currentPrev.data;
			if(Long.valueOf(String.valueOf(prevMap.get(Tree._P_KEY))) != -1 && !"Y".equals(prevMap.get("USED"))) {
				currentNode = getNode(--idx);
				targetMap   = (Map<String, Object>)currentNode.data;
			}

			if(Long.valueOf(String.valueOf(targetMap.get(Tree._P_KEY))) != -1 && !"Y".equals(targetMap.get("USED"))) {
				targetMap.put("USED", "Y");

				Node targetNode = currentNode;
				Node parentNode = headNode;

				while(parentNode != null) {
					Map<String, Object> parentMap   = (Map<String, Object>)parentNode.data;

					if(Long.valueOf(String.valueOf(parentMap.get(Tree._KEY))).equals(Long.valueOf(String.valueOf(targetMap.get(Tree._P_KEY))))) {
						Node nextNode = targetNode.nextNode;
						Node prevNode = targetNode.prevNode;
						if(nextNode != null) {
							nextNode.prevNode = prevNode;
						}
						if(prevNode != null) {
							prevNode.nextNode = nextNode;
						}

						Node parentNextNode = parentNode.nextNode;
						parentNode.nextNode = targetNode;
						targetNode.prevNode = parentNode;
						targetNode.nextNode = parentNextNode;
						if(parentNextNode != null) {
							parentNextNode.prevNode = targetNode;
						}

						break;
					}

					parentNode = parentNode.nextNode;
				}
			}
		}
	}

	public List<Map<String, Object>> getList() {
		List<Map<String, Object>> list  = new ArrayList<Map<String, Object>>();
		Node tailNode				   = headNode;

		while(tailNode != null) {
			Map<String, Object> map = (Map<String, Object>)tailNode.data;

			list.add(map);

			tailNode = tailNode.nextNode;
		}

		return list;
	}

	public void sort(List<Map<String, Object>> list) {
		add(list);
		sort();

		List<Map<String, Object>> temp_list = new ArrayList<Map<String, Object>>(list);
		Node tailNode					   = headNode;

		Collections.copy(temp_list, list);
		list.clear();

		int idx = 0;
		while(tailNode != null) {
			Map<String, Object> item = (Map<String, Object>)tailNode.data;
			String nextKey		   = "";
			String nextPath		  = "";
			Node nextNode			= tailNode.nextNode;

			if(nextNode != null) {
				Map<String, Object> nextMap = (Map<String, Object>)nextNode.data;
				nextKey	 = String.valueOf(nextMap.get(Tree._P_KEY));
				nextPath	= String.valueOf(nextMap.get("PAGE_PATH"));
			}

			for(Map<String, Object> map : temp_list) {
				if(String.valueOf(item.get(Tree._KEY)).equals(String.valueOf(map.get(Tree._KEY)))) {
					if(item.get("PAGE_PATH") == null || String.valueOf(item.get("PAGE_PATH")).equals("null") || String.valueOf(item.get("PAGE_PATH")).equals("")) {
						map.put("PAGE_PATH", nextPath);
					}
					map.put(Tree._N_P_KEY, nextKey);
					map.put(Tree._V_SORT, String.valueOf(item.get(Tree._V_SORT)));
					list.add(idx++, map);
					break;
				}
			}

			tailNode = tailNode.nextNode;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb	= new StringBuilder();
		Node tailNode	   = headNode;

		while(tailNode != null) {
			Map<String, String> map = (Map<String, String>)tailNode.data;

			if(tailNode.nextNode == null) {
				sb.append("KEY: " + String.valueOf(map.get(Tree._KEY)) + " / P_KEY: " + String.valueOf(map.get(Tree._P_KEY)) + " / SORT: " + String.valueOf(map.get(Tree._SORT)));
			} else {
				sb.append("KEY: " + String.valueOf(map.get(Tree._KEY)) + " / P_KEY: " + String.valueOf(map.get(Tree._P_KEY)) + " / SORT: " + String.valueOf(map.get(Tree._SORT)) + "\n");
			}

			tailNode = tailNode.nextNode;
		}

		return sb.toString();
	}

	public static void main(String[] args) {
		System.out.println("//BEGIN");
		System.out.println("");

		Tree menu = new Tree("KEY", "P_KEY", "SORT", "N_P_KEY", "V_SORT");

		// 사용법 1
//		menu.add(100,  -1,  1);
//		menu.add(200,  -1,  3);
//		menu.add(700,  200,  1);
//		menu.add(400,  200,  2);
//		menu.add(600,  400,  1);
//		menu.add(1100, 400,  2);
//		menu.add(800,  200,  3);
//		menu.add(1200, 200,  4);
//		menu.add(300,  -1,  2);
//		menu.add(1400, 300,  1);
//		menu.add(1500, 300,  2);
//		menu.add(1000, -1,  4);
//		menu.add(1300, 1100, 1);
//		menu.add(500,  -1,  5);
//		menu.add(900,  500,  1);

//		menu.add(100001,   -1,	 1);
//		menu.add(10000101, 100001, 1);
//		menu.add(10000102, 100001, 2);
//		menu.add(10000103, 100001, 3);
//		menu.add(10000104, 100001, 4);
//		menu.add(10000105, 100001, 5);
//		menu.add(10000106, 100001, 6);
//
//		menu.add(100002,   -1,	 2);
//		menu.add(10000201, 100002, 1);
//		menu.add(10000202, 100002, 2);
//		menu.add(10000203, 100002, 3);
//		menu.add(10000204, 100002, 4);
//		menu.add(10000205, 100002, 5);
//		menu.add(10000206, 100002, 6);
//		menu.add(10000207, 100002, 7);
//		menu.add(10000208, 100002, 8);
//		menu.add(10000209, 100002, 9);
//
//		menu.add(100003, -1, 3);
//		menu.add(100004, -1, 4);
//		menu.add(100005, -1, 5);
//
//		menu.sort();
//
//		List<Map<String, Object>> list1 = menu.getList();
//		for(int idx = 0; idx < list1.size(); idx++) {
//			Map<String, Object> mp = list1.get(idx);
//			System.out.println("KEY: " + mp.get("KEY") + " / P_KEY: " + mp.get("P_KEY") + " / SORT: " + mp.get("SORT") + " / N_P_KEY: " + mp.get("N_P_KEY"));
//		}

		// 사용법 2
		List<Map<String, Object>> list2 = new ArrayList<Map<String,Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("KEY", 100001);map.put("P_KEY", -1);map.put("SORT", 1);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000101);map.put("P_KEY", 100001);map.put("SORT", 1);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000102);map.put("P_KEY", 100001);map.put("SORT", 2);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000103);map.put("P_KEY", 100001);map.put("SORT", 3);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000104);map.put("P_KEY", 100001);map.put("SORT", 4);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000209);map.put("P_KEY", 100002);map.put("SORT", 9);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000105);map.put("P_KEY", 100001);map.put("SORT", 5);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000106);map.put("P_KEY", 100001);map.put("SORT", 6);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 100002);map.put("P_KEY", -1);map.put("SORT", 2);
		list2.add(map);
//		map = new HashMap<String, Object>();
//		map.put("KEY", 10000201);map.put("P_KEY", 100002);map.put("SORT", 1);
//		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000202);map.put("P_KEY", 100002);map.put("SORT", 2);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000203);map.put("P_KEY", 100002);map.put("SORT", 3);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000204);map.put("P_KEY", 100002);map.put("SORT", 4);
		list2.add(map);
//		map = new HashMap<String, Object>();
//		map.put("KEY", 10000205);map.put("P_KEY", 100002);map.put("SORT", 5);
//		list2.add(map);
//		map = new HashMap<String, Object>();
//		map.put("KEY", 10000206);map.put("P_KEY", 100002);map.put("SORT", 6);
//		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000207);map.put("P_KEY", 100002);map.put("SORT", 7);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 10000208);map.put("P_KEY", 100002);map.put("SORT", 8);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 100003);map.put("P_KEY", -1);map.put("SORT", 3);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 100004);map.put("P_KEY", -1);map.put("SORT", 4);
		list2.add(map);
		map = new HashMap<String, Object>();
		map.put("KEY", 100005);map.put("P_KEY", -1);map.put("SORT", 5);
		list2.add(map);

		menu.sort(list2);

		for(int idx = 0; idx < list2.size(); idx++) {
			Map<String, Object> mp = list2.get(idx);
			System.out.println("KEY: " + mp.get("KEY") + " / P_KEY: " + mp.get("P_KEY") + " / SORT: " + mp.get("SORT") + " / N_P_KEY: " + mp.get("N_P_KEY") + " / V_SORT: " + mp.get("V_SORT"));
		}

		System.out.println("");
		System.out.println("//End");
	}

}
