package kr.ac.pusan.core.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.service.ICommonService;

/**
  * @FileName : FileController.java  
  * @Date : 2017. 5. 11. 
  * @작성자 : guncho
  * @변경이력 :
  * 	2017. 5. 11. 최초생성  
  * @프로그램 설명 : 파일 Controller
 */
@Controller
public class FileController {
	
	@Autowired
	protected ICommonService commonService;
	
	public static final String KEY_FORM_FILE_ROOT = "E:\\";
	public static final String KEY_FORM_DOMAIN = "eIRB";
	public static final String KEY_FORM_DOMAIN_UPLOAD_DIR = "upload";
	public static final String KEY_FORM_FILE = "file";
	public static final String KEY_FORM_EDITORFILE = "upload";
	
	protected final Logger logger = LoggerFactory.getLogger(FileController.class);
	
	@RequestMapping(value = "/getFileImage", method = RequestMethod.GET)
	public void getFileImage(@RequestParam("filePath") String filePath, HttpServletResponse response ) 
					throws Exception {
		File file = null;
		FileInputStream fis = null;
		
		BufferedInputStream in = null;
		ByteArrayOutputStream bStream = null;
		
		try {
			file = new File(KEY_FORM_FILE_ROOT + KEY_FORM_DOMAIN + filePath);
			if (file.exists()) {
				fis = new FileInputStream(file);

				in = new BufferedInputStream(fis);
				bStream = new ByteArrayOutputStream();
				int vodByte;
				while ((vodByte = in.read()) != -1) {
					bStream.write(vodByte);
				}
				response.setHeader("Content-Type", "application/octet-stream");
				response.setHeader("Content-Transfer-Encoding", "binary;");
				response.setContentLength(bStream.size());
				
				bStream.writeTo(response.getOutputStream());

				response.getOutputStream().flush();
				response.getOutputStream().close();
			}
			
			
		} finally {
			if (bStream != null) {
				try {
					bStream.close();
				} catch (Exception ignore) {
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (Exception ignore) {
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception ignore) {
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/deleteFile", method=RequestMethod.POST)
	public @ResponseBody Object deleteFile(@RequestBody Map<String, Object> jsonMap, ModelMap model) throws Exception {
		HashMap<String, Object> msg = (HashMap<String, Object>) commonService.select("AttachFile.DELETE_CMS_ATTACH_FILE", jsonMap);
		model.addAttribute("msg", msg);
		
		return msg;
	}
	
	@RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
	public ModelAndView downloadFile(@RequestParam("filePath") String filePath, @RequestParam("fileNm") String fileName, HttpServletResponse response ) {
		HashMap<String, Object> map  = new HashMap<String, Object>();
		
		map.put("FILE_REAL_PATH", KEY_FORM_FILE_ROOT + KEY_FORM_DOMAIN + filePath);
		map.put("FILE_ORIGINAL_NAME", fileName);
		
		logger.info("FilePath : " + map.get("FILE_REAL_PATH") + " || File Name : " + map.get("FILE_ORIGINAL_NAME"));
		
		return new ModelAndView(new FileDownloadController(), "downloadFile", map);
	}
}
