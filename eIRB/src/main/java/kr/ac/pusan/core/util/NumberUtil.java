/**
 * 
 */
package kr.ac.pusan.core.util;

import java.math.BigDecimal;

public class NumberUtil {
	
	/**
	 * <p>문자열로된 숫자의 정수여부를 판단한다.</p>
	 * @param num 판단 대상의 문자열로된 숫자
	 * @return 정수여부의 참, 거짓값
	 */
	public boolean isInteger(String num) {
		return num.matches("^[-+]?[\\d]+$");
	}
	
	/**
	 * <p>문자열로된 숫자의 실수여부를 판단한다.</p>
	 * @param num 판단 대상의 문자열로된 숫자
	 * @return 실수여부의 참, 거짓값
	 */
	public boolean isRealNumber(String num) {
		return num.matches("^[-+]?[\\d]+.?[\\d]+$");
	}
	
	/**
	 * <p>문자열로된 숫자의 소수점 반올림을 한다.</p>
	 * @param targetValue 반올리할 문자열로된 숫자
	 * @param precision 반올림 처리할 자릿수
	 * @return 반올림 처리한 문자열 결과
	 */
	public String precisionFormatRoundUp(String targetValue, int precision) {
		BigDecimal bd = new BigDecimal(targetValue);
		return bd.setScale(precision, BigDecimal.ROUND_HALF_UP).toPlainString();
	}
	
	/**
	 * <p>문자열로된 숫자의 소수점 버림을 한다.</p>
	 * @param targetValue 버림할 문자열로된 숫자
	 * @param precision 버림 처리할 자릿수
	 * @return 버림 처리한 문자열 결과
	 */
	public String precisionFormatDown(String targetValue, int precision) {
		BigDecimal bd = new BigDecimal(targetValue);
		return bd.setScale(precision, BigDecimal.ROUND_DOWN).toPlainString();
	}

}
