package kr.ac.pusan.core.advice;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.util.StopWatch;

@Aspect
public class LoggingAdvice {
	
	protected final Log logger = LogFactory.getLog("pnu_eirb");
	
	@Around("SystemPointcut.daoExecution()")
	public Object logging(ProceedingJoinPoint pjp) throws Throwable {
		String targetClassName  = pjp.getTarget().getClass().getName();
		String targetMethodName = pjp.getSignature().getName();
		
		if(logger.isInfoEnabled()) {
			logger.info("##  " + targetClassName + "." + targetMethodName + " Start !  ##");
			
			Object[] args = pjp.getArgs();
			
			for(int idx = 0; idx < args.length; idx++) {
				if(idx == 0) {
					logger.info("args [" + idx + "] mapId : " + args[idx]);
				} else {
					logger.info("args [" + idx + "] : " + args[idx]);
				}
			}
		}
		
		Object returnValue = pjp.proceed();
		
		if(logger.isInfoEnabled()) {
			logger.info("##  " + targetClassName + "." + targetMethodName + " End !  ##");
		}
		
		return returnValue;
	}
	
	@Around("SystemPointcut.daoExecution()")
	public Object daoMonitoring(ProceedingJoinPoint pjp) throws Throwable {
		StopWatch clock = new StopWatch("Profiling..");
		Object returnValue;
		
		try {
			clock.start(pjp.toShortString());
			returnValue = pjp.proceed();
		} finally {
			clock.stop();
		}
		
		if(clock.getTotalTimeMillis() > 500) {
			if(logger.isWarnEnabled()) {
				logger.warn(" ## DAO Execution Location : " + pjp.getTarget().getClass().getName() + " ##");
				logger.warn(" ## DAO Execution Method : " + pjp.toShortString() + " ##");
				logger.warn(" ## DAO Execution Time : " + clock.prettyPrint() + " ##");
			}
		}
		
		return returnValue;
	}

}
