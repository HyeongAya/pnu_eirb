/**
 * 
 */
package kr.ac.pusan.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.ac.pusan.core.controller.BaseController;

public class InitMapInterceptor extends HandlerInterceptorAdapter {
	
	protected final Log logger = LogFactory.getLog("pnu_eirb");
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if(handler instanceof HandlerMethod) {
			Object obj = ((HandlerMethod)handler).getBean();
			
			if(obj instanceof BaseController) {
				logger.info("# [InitMapInterceptor] Create Map");
	
				BaseController controller = (BaseController)obj;
				
				controller.initParamMap();
			}
		}
		
		return super.preHandle(request, response, handler);
	}
	
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		if(handler instanceof HandlerMethod) {
			Object obj = ((HandlerMethod)handler).getBean();
			
			if(obj instanceof BaseController) {
				logger.info("# [InitMapInterceptor] Remove Map");
				
				BaseController controller = (BaseController)obj;
				
				controller.clearParamMap();
			}
		}
		
		super.afterCompletion(request, response, handler, ex);
	}

}
