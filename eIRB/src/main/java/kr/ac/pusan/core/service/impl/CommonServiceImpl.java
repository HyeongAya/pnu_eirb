/**
 *
 */
package kr.ac.pusan.core.service.impl;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.ac.pusan.core.dao.ICommonDao;
import kr.ac.pusan.core.service.ICommonService;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("commonService")
public class CommonServiceImpl<V> extends EgovAbstractServiceImpl implements ICommonService<V> {

	protected final Log logger = LogFactory.getLog("pnu_eirb");

	@Resource(name="commonDao")
	private ICommonDao<V> commonDao;

	public List<Map<String, V>> selectList(String mapId, Map<String, V> paramMap, int page, int limit) {
		return commonDao.core_selectList(mapId, paramMap, page, limit);
	}

	public List<Map<String, V>> selectList(String mapId, Map<String, V> paramMap) {
		return commonDao.core_selectList(mapId, paramMap);
	}

	public List<Map<String, V>> selectList(String mapId) {
		return commonDao.core_selectList(mapId, null);
	}
	
	public List<List<Map<String, Object>>> selectMultiList(String mapId, Map<String, V> paramMap) {
		return commonDao.selectMultiList(mapId, paramMap);
	}
	
	public List<List<Map<String, Object>>> selectMultiList(String mapId) {
		return commonDao.selectMultiList(mapId, null);
	}

	public Object select(String mapId, Map<String, V> paramMap) {
		return commonDao.core_select(mapId, paramMap);
	}

	public Object select(String mapId) {
		return commonDao.core_select(mapId, null);
	}

	@Transactional
	public int setInsert(String mapId, Map<String, V> paramMap) {
		return commonDao.core_setInsert(mapId, paramMap);
	}

	@Transactional
	public int setInsert(String mapId) {
		return commonDao.core_setInsert(mapId, null);
	}

	@Transactional
	public int setUpdate(String mapId, Map<String, V> paramMap) {
		return commonDao.core_setUpdate(mapId, paramMap);
	}

	@Transactional
	public int setUpdate(String mapId) {
		return commonDao.core_setUpdate(mapId, null);
	}

	@Transactional
	public int setDelete(String mapId, Map<String, V> paramMap) {
		return commonDao.core_setDelete(mapId, paramMap);
	}

	@Transactional
	public int setDelete(String mapId) {
		return commonDao.core_setDelete(mapId, null);
	}

	@Transactional
	public Object setChainInsert(String keyName, String masterId, Map<String, V> masterMap, String childId, Map<String, V> childMap) {
		commonDao.core_setInsert(masterId, masterMap);
		childMap.put(keyName, masterMap.get(keyName));
		commonDao.core_setInsert(childId, childMap);

		return masterMap.get(keyName);
	}

	@Transactional
	public Object setChainUpdate(String keyName, String masterId, Map<String, V> masterMap, String childId, Map<String, V> childMap) {
		commonDao.core_setInsert(masterId, masterMap);
		childMap.put(keyName, masterMap.get(keyName));
		commonDao.core_setUpdate(childId, childMap);

		return masterMap.get(keyName);
	}

	@Transactional
	public Object setChainDelete(String keyName, String masterId, Map<String, V> masterMap, String childId, Map<String, V> childMap) {
		commonDao.core_setInsert(masterId, masterMap);
		childMap.put(keyName, masterMap.get(keyName));
		commonDao.core_setDelete(childId, childMap);

		return masterMap.get(keyName);
	}

	@Transactional
	public Object setChainMultiBatch(String keyName, String masterId, Map<String, V> masterMap, List<Map<String, Map<String, V>>> childMapList) {
		commonDao.core_setInsert(masterId, masterMap);

		if(childMapList != null) {
			for(int idx = 0; idx < childMapList.size(); idx++) {
				Map<String, Map<String, V>> map = childMapList.get(idx);
				Set<String> key				 = map.keySet();
				Iterator<String> iterator	   = key.iterator();
				String childKeyName			 = iterator.next();
				Map<String, V> childMap		 = map.get(childKeyName);

				childMap.put(keyName, masterMap.get(keyName));

				if(childKeyName.matches("(?i).*DELETE.*")) {
					commonDao.core_setDelete(childKeyName, childMap);
				} else if(childKeyName.matches("(?i).*UPDATE.*")) {
					commonDao.core_setUpdate(childKeyName, childMap);
				} else {
					commonDao.core_setInsert(childKeyName, childMap);
				}
			}
		}

		return masterMap.get(keyName);
	}

	@Transactional
	public void setBatchInsert(String insertId, List<Map<String, V>> insertMapList) {
		if(insertMapList != null) {
			for(Map<String, V> insertMap : insertMapList) {
				commonDao.core_setInsert(insertId, insertMap);
			}
		}
	}

	@Transactional
	public void setBatchUpdate(String updateId, List<Map<String, V>> updateMapList) {
		if(updateMapList != null) {
			for(Map<String, V> updateMap : updateMapList) {
				commonDao.core_setUpdate(updateId, updateMap);
			}
		}
	}

	@Transactional
	public void setBatchDelete(String deleteId, List<Map<String, V>> deleteMapList) {
		if(deleteMapList != null) {
			for(Map<String, V> deleteMap : deleteMapList) {
				commonDao.core_setDelete(deleteId, deleteMap);
			}
		}
	}

	@Transactional
	public void setMultiBatch(List<Map<String, Map<String, V>>> mapList) {
		if(mapList != null) {
			for(int idx = 0; idx < mapList.size(); idx++) {
				Map<String, Map<String, V>> map = mapList.get(idx);
				Set<String> key				 = map.keySet();
				Iterator<String> iterator	   = key.iterator();
				String KeyName				  = iterator.next();

				if(KeyName.matches("(?i).*DELETE.*")) {
					commonDao.core_setDelete(KeyName, map.get(KeyName));
				} else if(KeyName.matches("(?i).*UPDATE.*")) {
					commonDao.core_setUpdate(KeyName, map.get(KeyName));
				} else {
					commonDao.core_setInsert(KeyName, map.get(KeyName));
				}
			}
		}
	}
	
	/**
     * @MethodName : setDatasetsToHashMap
     * @작성일 : 2017. 5. 11.
     * @작성자 : jyseo
     * @변경이력 : 
     * 	2017. 5. 11. 최초생성
     * @Method 설명 :
     * @param list DAO에서 호출한 결과
     * @return
    */
	public HashMap<String, Object> setDatasetsToHashMap(List<List<Map<String, Object>>> list) {
		HashMap<String, Object> hmJson = new HashMap<String, Object>();
		
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				hmJson.put("dataset" + (i + 1), list.get(i));
			}
		} else {
			hmJson.put("dataset1", list);
		}
		return hmJson;
	}
	
	
	//PUSH(SMS)발송
	public void pushCall(String userId, String pushChk, String pushUserId, String pushTitle, String pushContent, String tickerText, String pushTime, String cdrId, String smsContent, String smsNb) throws Exception {
		try {
			//전송 파라미터
			String param = "user_id=" + URLEncoder.encode(userId, "UTF-8");
			param += "&" + "push_chk=" + URLEncoder.encode(pushChk, "UTF-8");
			param += "&" + "push_userid=" + URLEncoder.encode(pushUserId, "UTF-8");
			param += "&" + "push_title=" + URLEncoder.encode(pushTitle, "UTF-8");
			param += "&" + "push_content=" + URLEncoder.encode(pushContent, "UTF-8");
			param += "&" + "push_url=" + URLEncoder.encode(tickerText, "UTF-8");
			param += "&" + "push_time=" + URLEncoder.encode(pushTime, "UTF-8");
			param += "&" + "cdr_id=" + URLEncoder.encode(cdrId, "UTF-8");
			param += "&" + "sms_content=" + URLEncoder.encode(smsContent, "UTF-8");
			param += "&" + "sms_nb=" + URLEncoder.encode(smsNb, "UTF-8");
			
			//전송
			URL url = new URL("http://msg.pusan.ac.kr/api/push.asp");
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			
			DataOutputStream out = null;
			try {
				out = new DataOutputStream(conn.getOutputStream());
				out.writeBytes(param);
				out.flush();
			} finally {
				if (out != null) out.close();
			}
			
			//수신
			InputStream is = conn.getInputStream();
			Scanner scan = new Scanner(is);
			
			int line = 1;
			while (scan.hasNext()) {
				String str = scan.nextLine();
				System.out.println((line++) + ":" + str);
			}
			
		} catch (MalformedURLException e) {
			System.out.println("The URL addres is incorrect");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Can't connect to the web page");
			e.printStackTrace();
		}
	}

}
