/**
 *
 */
package kr.ac.pusan.core.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ICommonService<V> {

    public List<Map<String, V>> selectList(String mapId, Map<String, V> paramMap, int page, int limit);
    public List<Map<String, V>> selectList(String mapId, Map<String, V> paramMap);
    public List<Map<String, V>> selectList(String mapId);
    public List<List<Map<String, Object>>> selectMultiList(String mapId, Map<String, V> paramMap);
    public List<List<Map<String, Object>>> selectMultiList(String mapId);
    public Object select(String mapId, Map<String, V> paramMap);
    public Object select(String mapId);
    public int setInsert(String mapId, Map<String, V> paramMap);
    public int setInsert(String mapId);
    public int setUpdate(String mapId, Map<String, V> paramMap);
    public int setUpdate(String mapId);
    public int setDelete(String mapId, Map<String, V> paramMap);
    public int setDelete(String mapId);

    public Object setChainInsert(String keyName, String masterId, Map<String, V> masterMap, String childId, Map<String, V> childMap);
    public Object setChainUpdate(String keyName, String masterId, Map<String, V> masterMap, String childId, Map<String, V> childMap);
    public Object setChainDelete(String keyName, String masterId, Map<String, V> masterMap, String childId, Map<String, V> childMap);
    public Object setChainMultiBatch(String keyName, String masterId, Map<String, V> masterMap, List<Map<String, Map<String, V>>> childMapList);

    public void setBatchInsert(String insertId, List<Map<String, V>> insertMapList);
    public void setBatchUpdate(String updateId, List<Map<String, V>> updateMapList);
    public void setBatchDelete(String deleteId, List<Map<String, V>> deleteMapList);
    public void setMultiBatch(List<Map<String, Map<String, V>>> mapList);
    
    /**
     * @MethodName : setDatasetsToHashMap
     * @작성일 : 2017. 5. 11.
     * @작성자 : jyseo
     * @변경이력 : 
     * 	2017. 5. 11. 최초생성
     * @Method 설명 :
     * @param list DAO에서 호출한 결과
     * @return
    */
    public HashMap<String, Object> setDatasetsToHashMap(List<List<Map<String, Object>>> list);
    
    /**
	 * PUSH(SMS)발송
	 * @param userId
	 * @param pushChk
	 * @param pushUserId
	 * @param pushTitle
	 * @param pushContent
	 * @param tickerText
	 * @param pushTime
	 * @param cdrId
	 * @param smsContent
	 * @param smsNb
	 * @throws Exception
	 */
	public void pushCall(String userId, String pushChk, String pushUserId, String pushTitle, String pushContent, String tickerText, String pushTime, String cdrId, String smsContent, String smsNb) throws Exception;

}
