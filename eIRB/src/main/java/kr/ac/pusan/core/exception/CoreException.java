/**
 *
 */
package kr.ac.pusan.core.exception;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CoreException extends RuntimeException {

	private static final long serialVersionUID = 539948968591040154L;

	protected final Log logger = LogFactory.getLog("core_exception");

	public CoreException() {
		super();
		logger.error("[CORE EXCEPTION]");
	}

	public CoreException(String message, Throwable cause) {
		super(message, cause);
		logger.error("[CORE EXCEPTION] - " + message + "\n" + cause.getMessage());
	}

	public CoreException(String message) {
		super(message);
		logger.error("[CORE EXCEPTION] - " + message);
	}

	public CoreException(Throwable cause) {
		super(cause);
		logger.error("[CORE EXCEPTION] - " + cause.getMessage());
	}

	public CoreException(String message, HttpServletRequest request, HttpServletResponse response) {
		logger.error("[CORE EXCEPTION] - " + message);

		String referer	  = request.getHeader("referer");
		StringBuilder body  = new StringBuilder();

		response.setHeader("Content-Type", "application/html");
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("utf-8");

		if(referer == null || "null".equals(referer) || "".equals(referer)) {
			referer = "/";
		}

		PrintWriter out;
		try {
			out = response.getWriter();

			body.append("<script type=\"text/javascript\">");
			body.append("alert('" + message + "');");
			body.append("if(self==top) { ");
			body.append("document.location.href = '" + referer + "';");
			body.append(" } else { ");
			body.append("top.location.href = '/';");
			body.append(" } ");
			body.append("</script>");
			out.write(body.toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public CoreException(String message, HttpServletRequest request, HttpServletResponse response, String redirectUrl) {
		logger.error("[CORE EXCEPTION] - " + message);

		String referer	  = redirectUrl;
		StringBuilder body  = new StringBuilder();

		response.setHeader("Content-Type", "application/html");
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("utf-8");

		if(referer == null || "null".equals(referer) || "".equals(referer)) {
			referer = "/";
		}
		PrintWriter out;
		try {
			out = response.getWriter();

			if(referer.equals("back")) {
				body.append("<script type=\"text/javascript\">");
				body.append("alert('" + message + "');");
				body.append("history.go(-1);");
				body.append("</script>");
			} else {
				body.append("<script type=\"text/javascript\">");
				body.append("alert('" + message + "');");
				body.append("if(self==top) { ");
				body.append("document.location.href = '" + referer + "';");
				body.append(" } else { ");
				body.append("top.location.href = '/';");
				body.append(" } ");
				body.append("</script>");
			}
			out.write(body.toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
