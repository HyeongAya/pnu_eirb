package kr.ac.pusan.core.advice;

import org.aspectj.lang.annotation.Pointcut;

public final class SystemPointcut {
	
	private SystemPointcut() {}
	
	@Pointcut("execution(* *..dao..*(..))")
	public void daoExecution() {}
	
	@Pointcut("execution(* *..controller..*(..))")
	public void controllerExecution() {}

}
