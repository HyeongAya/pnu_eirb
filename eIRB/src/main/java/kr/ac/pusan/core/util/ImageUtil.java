/**
 * 
 */
package kr.ac.pusan.core.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class ImageUtil {
	
	/**
	 * <p>업로드된 이미지를 리사이징해서 저장한다.</p>
	 * @param src 원본 이미지 파일
	 * @param dest 리사이징되어서 저장될 이미지 파일
	 * @param width 리사이징할 가로 사이즈<br />{&lt;세로 사이즈의 비율로 조정&gt;<strong>0</strong>|&lt;원본 사이즈 유지&gt;<strong>-1</strong>|&lt;지정된 사이즈로 조정&gt;<i>사이즈</i>}
	 * @param height 리사이징할 세로 사이즈<br />{&lt;가로 사이즈의 비율로 조정&gt;<strong>0</strong>|&lt;원본 사이즈 유지&gt;<strong>-1</strong>|&lt;지정된 사이즈로 조정&gt;<i>사이즈</i>}
	 * @exception IOException 원본이미지 파일 및 리사이징 이미지 파일에 대한 입출력 예외
	 */
	public void resize(File src, File dest, int width, int height) throws IOException {
		int RATIO	   = 0;
		int SAME		= -1;
		Image srcImg	= null;
		String suffix   = src.getName().substring(src.getName().lastIndexOf('.') + 1).toLowerCase();
		
		if(suffix.equals("bmp") || suffix.equals("png") || suffix.equals("gif")) {
			srcImg = ImageIO.read(src);
		} else {
			srcImg = new ImageIcon(src.toURI().toURL()).getImage();
		}
		
		int srcWidth	= srcImg.getWidth(null);
		int srcHeight   = srcImg.getHeight(null);
		int destWidth   = -1;
		int destHeight  = -1;
		
		if(width == SAME) {
			destWidth = srcWidth;
		} else if(width > 0) {
			destWidth = width;
		}
		
		if(height == SAME) {
			destHeight = srcHeight;
		} else if(height > 0) {
			destHeight = height;
		}
		
		if(width == RATIO && height == RATIO) {
			destWidth   = srcWidth;
			destHeight  = srcHeight;
		} else if(width == RATIO) {
			double ratio	= ((double)destHeight) / ((double)srcHeight);
			destWidth	   = (int)((double)srcWidth * ratio);
		} else if(height == RATIO) {
			double ratio	= ((double)destWidth) / ((double)srcWidth);
			destHeight	  = (int)((double)srcHeight * ratio);
		}
		
		Image imgTarget = srcImg.getScaledInstance(destWidth, destHeight, Image.SCALE_SMOOTH);
		int pixels[]	= new int[destWidth * destHeight];
		PixelGrabber pg = new PixelGrabber(imgTarget, 0, 0, destWidth, destHeight, pixels, 0, destWidth);
		
		try {
			pg.grabPixels();
		} catch(InterruptedException e) {
			throw new IOException(e.getMessage());
		}
		
		BufferedImage destImg = new BufferedImage(destWidth, destHeight, BufferedImage.TYPE_INT_RGB);
		
		destImg.setRGB(0, 0, destWidth, destHeight, pixels, 0, destWidth);
		
		ImageIO.write(destImg, "jpg", dest);
	}

}
