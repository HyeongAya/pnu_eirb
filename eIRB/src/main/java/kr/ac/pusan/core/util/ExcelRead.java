/**
 *
 */
package kr.ac.pusan.core.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelRead {
	public List<List<Map<String, String>>> getExcel(String fileName, String fileUrl) throws IOException {
		List<List<Map<String, String>>> retList = null;
		String ext							  = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());

		if("xls".equals(ext)) {
			retList = getXls(fileUrl);
		} else if("xlsx".equals(ext)) {
			retList = getXlsx(fileUrl);
		}

		return retList;
	}

	private List<List<Map<String, String>>> getXls(String file) throws IOException {
		List<List<Map<String, String>>> retList = new ArrayList<List<Map<String,String>>>();
		FileInputStream inputStream			 = new FileInputStream(file);
		POIFSFileSystem fileSystem			  = new POIFSFileSystem(inputStream);
		HSSFWorkbook workbook				   = new HSSFWorkbook(fileSystem);
		int sheetCnt							= workbook.getNumberOfSheets();
		HSSFRow row;
		HSSFCell cell;

		for(int idx = 0; idx < sheetCnt; idx++) {
			HSSFSheet sheet				 = workbook.getSheetAt(idx);
			int rowCnt					  = sheet.getPhysicalNumberOfRows();
			int cellCnt					 = sheet.getRow(0).getPhysicalNumberOfCells();
			List<Map<String, String>> list  = new ArrayList<Map<String,String>>();

			for(int rowIdx = 0; rowIdx < rowCnt; rowIdx++) {
				Map<String, String> map = null;

				row = sheet.getRow(rowIdx);

				if(row != null) {
					map = new HashMap<String, String>();

					for(int cellIdx = 0; cellIdx < cellCnt; cellIdx++) {
						String value			= null;

						cell = row.getCell(cellIdx);

						if(cell != null) {
							switch(cell.getCellType()) {
							case HSSFCell.CELL_TYPE_FORMULA:
								value = cell.getCellFormula();
								break;
							case HSSFCell.CELL_TYPE_NUMERIC:
								value = "" + cell.getNumericCellValue();
								break;
							case HSSFCell.CELL_TYPE_BLANK:
								value = "" + cell.getBooleanCellValue();
								break;
							case HSSFCell.CELL_TYPE_ERROR:
								value = "" + cell.getErrorCellValue();
								break;
							default:
								value = cell.getStringCellValue();
								break;
							}
						}
						map.put("X" + cellIdx, value);
					}
				}
				list.add(map);
			}
			retList.add(list);
		}
		
		inputStream.close();
	   
		return retList;
	}

	private List<List<Map<String, String>>> getXlsx(String file) throws IOException {
		List<List<Map<String, String>>> retList = new ArrayList<List<Map<String,String>>>();
		FileInputStream inputStream			 = new FileInputStream(file);
		XSSFWorkbook workbook				   = new XSSFWorkbook(inputStream);
		int sheetCnt							= workbook.getNumberOfSheets();
		XSSFRow row;
		XSSFCell cell;

		for(int idx = 0; idx < sheetCnt; idx++) {
			XSSFSheet sheet				 = workbook.getSheetAt(idx);
			int rowCnt					  = sheet.getPhysicalNumberOfRows();
			int cellCnt					 = sheet.getRow(0).getPhysicalNumberOfCells();
			List<Map<String, String>> list  = new ArrayList<Map<String,String>>();

			for(int rowIdx = 0; rowIdx < rowCnt; rowIdx++) {
				Map<String, String> map = null;

				row = sheet.getRow(rowIdx);

				if(row != null) {
					map = new HashMap<String, String>();

					for(int cellIdx = 0; cellIdx < cellCnt; cellIdx++) {
						String value			= null;

						cell = row.getCell(cellIdx);

						if(cell != null) {
							switch(cell.getCellType()) {
							case HSSFCell.CELL_TYPE_FORMULA:
								value = cell.getCellFormula();
								break;
							case HSSFCell.CELL_TYPE_NUMERIC:
								value = "" + cell.getNumericCellValue();
								break;
							case HSSFCell.CELL_TYPE_BLANK:
								value = "" + cell.getBooleanCellValue();
								break;
							case HSSFCell.CELL_TYPE_ERROR:
								value = "" + cell.getErrorCellValue();
								break;
							default:
								value = cell.getStringCellValue();
								break;
							}
						}
						map.put("X" + cellIdx, value);
					}
				}
				list.add(map);
			}
			retList.add(list);
		}
		
		inputStream.close();

		return retList;
	}

}
