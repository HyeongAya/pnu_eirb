/**
 *
 */
package kr.ac.pusan.core.interceptor;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.exception.CoreException;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.CommonUtil;
import kr.ac.pusan.core.util.FileUtil;
import kr.ac.pusan.core.util.StringUtil;
import kr.ac.pusan.core.vo.MultipartFileWrapper;

@SuppressWarnings({"rawtypes", "unchecked"})
public class ParameterFileInterceptor extends HandlerInterceptorAdapter {

	@Value("#{prop['project.root']}")			String   P_ROOT_PATH;
	@Value("#{prop['fileUpload.root']}")		String   FILE_ROOT_PATH;
	@Value("#{prop['project.allowExt']}")		String[] P_ALLOW_EXT;
	@Value("#{prop['project.fileMaxSize']}")	String   P_FILE_MAX;

	protected final Log logger = LogFactory.getLog("pnu_cms");

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if(ServletFileUpload.isMultipartContent(request)) {
			logger.info("# [ParameterFileInterceptor] File Wrapping Start");

			StringUtil su   = new StringUtil();
			CommonUtil cu   = new CommonUtil();
			//String rootPath = System.getProperty(P_ROOT_PATH);
			String rootPath = FILE_ROOT_PATH;
			
			if(su.isEmpty(rootPath)) {
				rootPath = System.getProperty("user.home");
				logger.info("# project.root Path : 'user.home' Setting");
			}
			
			String uploadDir = request.getParameter(ProjectConstant.KEY_FORM_DOMAIN_UPLOAD_DIR);
			if(su.isEmpty(uploadDir)) {
				uploadDir = ProjectConstant.KEY_FORM_DOMAIN_UPLOAD_DIR;
			}
			
			String uploadDirSub = request.getParameter(ProjectConstant.KEY_FORM_DOMAIN_UPLOAD_DIR_SUB);
			if(su.isEmpty(uploadDirSub)) {
				uploadDirSub = ProjectConstant.KEY_FORM_DOMAIN_UPLOAD_DIR_SUB;
			}
			
			MultipartRequest multipartRequest   = (MultipartRequest) request;
			Map multipartFileMap				= multipartRequest.getFileMap();
			int fileCnt						 = 0;
			int fileExcelCnt					= 0;
			int fileEditorCnt				   = 0;
			
			System.out.println("cnt : " + multipartFileMap.size());
			
			for(int idx = 1; idx <= multipartFileMap.size(); idx++) {
				MultipartFile fileTemp = multipartRequest.getFile(ProjectConstant.KEY_FORM_FILE + idx);
				if(fileTemp != null) {
					fileCnt++;
				}
			}
			
			MultipartFile[] files = new CommonsMultipartFile[fileCnt];
			for(int idx = 1; idx <= fileCnt; idx++) {
				files[idx - 1] = multipartRequest.getFile(ProjectConstant.KEY_FORM_FILE + idx);
			}

			for(int idx = 0; idx < fileCnt; idx++) {
				if(files[idx] != null && !files[idx].isEmpty()) {
					if(!file_check(files[idx].getOriginalFilename())) {
						throw new CoreException(Message.propFormat("wrongExtension"), request, response);
					}
				}
			}
			
			long uploadSize = 0;
			String maxSize  = request.getParameter(ProjectConstant.KEY_FORM_FILE_MAX_SIZE);
			if(su.isEmpty(maxSize)) {
				uploadSize = Long.parseLong(P_FILE_MAX);
			} else {
				uploadSize = Long.parseLong(maxSize);
			}
			
			for(int idx = 0; idx < fileCnt; idx++) {
				if(files[idx] != null && !files[idx].isEmpty()) {
					if(files[idx].getSize() > (uploadSize * (1024 * 1024))) {
						throw new CoreException(Message.propFormat("wrongFileSize"), request, response, "back");
					}
				}
			}
			
			String originalFileNameYN   = request.getParameter(ProjectConstant.KEY_FORM_DOMAIN_UPLOAD_ORIGINAL_NM_YN);
			if(su.isEmpty(originalFileNameYN)) {
				originalFileNameYN = "N";
			}
			
			String path = "";
			String webPath = "";
			String url = "";
			path	= rootPath + File.separator + uploadDir + File.separator + (su.isEmpty(uploadDirSub) ? "":uploadDirSub + File.separator);
			webPath = "/" + uploadDir + "/" + (su.isEmpty(uploadDirSub) ? "":uploadDirSub + "/");
			url	 = cu.getURLroot(request) + uploadDir + "/" + (su.isEmpty(uploadDirSub) ? "":uploadDirSub + "/");
			
			FileUtil fu = new FileUtil();
			ArrayList<MultipartFileWrapper> fileWrapper		 = new ArrayList<MultipartFileWrapper>();
			
			for(int idx = 0; idx < fileCnt; idx++) {
				if(files[idx] != null && !files[idx].isEmpty()) {
					logger.info("## File " + (idx + 1));
					logger.info("## File Save Name : " + files[idx].getName());
					logger.info("## File Real Name : " + files[idx].getOriginalFilename());
					logger.info("## File Size : " + files[idx].getSize());
					logger.info("## File Type : " + files[idx].getContentType());
					logger.info("## Original File : " + originalFileNameYN);

					String fileName = "";
					String fileURL  = "";
					fileName		= fu.saveFile(originalFileNameYN, path, files[idx]);
					fileURL		 = url + fileName;

					fileWrapper.add(new MultipartFileWrapper(files[idx], fileName, fileURL, path, webPath));
				}
			}
			
			Map<String, Object> fileMap = new HashMap<String, Object>();
			for(int idx = 0; idx < fileWrapper.size(); idx++) {
				if(fileWrapper.get(idx) != null && !fileWrapper.get(idx).isEmpty()) {
					fileMap.put(ProjectConstant.KEY_FORM_FILE + (idx + 1), fileWrapper.get(idx));
				}
			}

			if(handler instanceof HandlerMethod) {
				Object obj = ((HandlerMethod)handler).getBean();

				if(obj instanceof BaseController) {
					if((fileMap != null && fileMap.size() > 0) || fileExcelCnt > 0 || fileEditorCnt > 0) {
						BaseController controller	   = (BaseController)obj;
						Map<String, Object> paramMap	= controller.getParamMap();

						if(paramMap != null) {
							paramMap.put(ProjectConstant.KEY_FORM_FILE + ProjectConstant.KEY_INTERCEPTOR_VALUES_POSTFIX, fileMap);

							for(int idx = 0; idx < fileWrapper.size(); idx++) {
								if(fileWrapper.get(idx) != null) {
									paramMap.put(ProjectConstant.KEY_FORM_FILE + (idx + 1), fileWrapper.get(idx));
								}
							}
						}
					}
				}
			}
		}

		return super.preHandle(request, response, handler);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		if(handler instanceof HandlerMethod) {
			Object obj = ((HandlerMethod)handler).getBean();

			if(obj instanceof BaseController) {
				logger.info("# [ParameterFileInterceptor] File Wrapping End");

				BaseController controller	   = (BaseController)obj;
				Map<String, Object> paramMap	= controller.getParamMap();

				if(paramMap != null) {
					Map<String, Object> fileMap = (Map<String, Object>)paramMap.get(ProjectConstant.KEY_FORM_FILE + ProjectConstant.KEY_INTERCEPTOR_VALUES_POSTFIX);

					if(fileMap != null) {
						fileMap.clear();
						fileMap = null;
					}
				}
			}
		}

		super.afterCompletion(request, response, handler, ex);
	}

	private boolean file_check(String fileName) {
		String ext = "";
		int idx1 = fileName.lastIndexOf(".");

		if(idx1 != -1) {
			ext = fileName.substring(idx1, fileName.length());
		}

		if(!"".equals(ext)) {
			for(int idx2 = 0; idx2 < P_ALLOW_EXT.length; idx2++) {
				if(P_ALLOW_EXT[idx2].equals(ext.toLowerCase())) {
					return true;
				}
			}
		}

		return false;
	}

}
