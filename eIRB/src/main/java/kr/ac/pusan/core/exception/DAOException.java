/**
 * 
 */
package kr.ac.pusan.core.exception;

public class DAOException extends RuntimeException {

	private static final long serialVersionUID = 793247659062963320L;

	public DAOException() {
		super();
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}

}
