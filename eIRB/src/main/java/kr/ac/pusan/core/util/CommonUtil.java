/**
 *
 */
package kr.ac.pusan.core.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@SuppressWarnings({"rawtypes", "unchecked"})
public class CommonUtil {

	/**
	 * <p>현재 URL의 루트URL을 가져온다.</p>
	 * @param request HttpServletRequest
	 * @return 루트RUL 예) http://www.dbcore.com/
	 */
	public String getURLroot(HttpServletRequest request) {
		String contextPath = "";
		if (!"/".equals(request.getContextPath())) {
			contextPath = request.getContextPath();
		}

		String port = "";
		if (request.getServerPort() != 80) {
			port = ":" + request.getServerPort();
		}

		String protocol = "http://";
		if (request.isSecure())
			protocol = "https://";

		return protocol + request.getServerName() + port + contextPath + "/";
	}

	/**
	 * <p>현재 URL의 루트DOMAIN을 가져온다.</p>
	 * @param request HttpServletRequest
	 * @return 루트DOMAIN 예) www.dbcore.com/
	 */
	public String getURLdomain(HttpServletRequest request) {
		String contextPath = "";
		if (!"/".equals(request.getContextPath())) {
			contextPath = request.getContextPath();
		}

		String port = "";
		if (request.getServerPort() != 80) {
			port = ":" + request.getServerPort();
		}

		return request.getServerName() + port + contextPath + "/";
	}

	/**
	 * <p>현재 URL에서 파라미터만을 가져온다.</p>
	 * @param request HttpServletRequest
	 * @return 파라미터
	 */
	public String getURLparameter(HttpServletRequest request) {
		String uri = request.getRequestURI();
		if (uri.indexOf("?") > -1) {
			return uri.substring(uri.indexOf("?"));
		}

		return "";

	}

	/**
	 * <p>현재 요청의 헤더 정보를 가져온다.</p>
	 * @param request HttpServletRequest
	 * @return 헤더정보
	 */
	public String getHeaderInfo(HttpServletRequest request) {
		StringBuffer buff = new StringBuffer();
		buff.append("================ <b>* Header Information *</b> ===============<br>");

		Enumeration names = request.getHeaderNames();
		String header = "";

		while (names.hasMoreElements()) {
			header = (String) names.nextElement();
			buff.append(header + " :: " + request.getHeader(header) + "<br>");
		}

		buff.append("<br>================ <b>* Parameter Information *</b>============<br>");
		buff.append("<b>method :: " + request.getMethod() + "</b><br>");

		Enumeration parameters = request.getParameterNames();
		String parameterName = "";

		while (parameters.hasMoreElements()) {
			parameterName = (String) parameters.nextElement();
			buff.append(parameterName + " :: " + request.getParameter(parameterName) + "<br>");
		}

		return buff.toString();
	}

	/**
	 * <p>JSON 데이터를 JAVA List형태로 변환한다.</p>
	 * @param jsonArray 변환할 JSON 데이터
	 * @return 변환된 List
	 */
	public List<HashMap<String, String>> JsonToList(JSONArray jsonArray) {
		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

		for (int idx = 0; idx < jsonArray.size(); idx++) {
			HashMap<String, String> map = new HashMap<String, String>();
			JSONObject jsonObject	   = jsonArray.optJSONObject(idx);
			Iterator<String> iterator   = jsonObject.keys();

			while (iterator.hasNext()) {
				String key = iterator.next();
				map.put(key, jsonObject.getString(key));
			}

			list.add(map);
		}

		return list;
	}

	/**
	 * <p>Map에 들어있는 내용을 콘솔에 출력한다.</p>
	 * @param 확인 할 맵
	 * @return 변환된 List
	 */
	public void viewMap(Map<String, Object> map) {
		Set<String> set = map.keySet();

		for(Iterator<String> iterator = set.iterator(); iterator.hasNext();) {
			String keyName  = iterator.next();
			String value	= String.valueOf(map.get(keyName));

			System.out.printf("[%s : \t\t%s]\n", keyName, value);
		}
	}

}
