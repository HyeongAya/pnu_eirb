/**
 * 
 */
package kr.ac.pusan.core.util;

import java.io.UnsupportedEncodingException;

import kr.ac.pusan.core.ProjectConstant;

public class StringUtil {
	/**
	 * <p>문자열의 왼쪽부분에 패딩처리를한다.</p>
	 * @param target 패딩처리될 대상 문자열
	 * @param padded_length 결과문자열의 최종 길이
	 * @param pad_string 패드되어 채워질 문자
	 * @return 패딩처리된 문자열
	 */
	public String lpad(String target, int padded_length, String pad_string) {
		StringBuilder result_string = new StringBuilder();
		
		for(int idx = 0; idx < padded_length - target.length(); idx++) result_string.append(pad_string);
		result_string.append(target);
		
		return result_string.toString();
	}
	
	/**
	 * <p>문자열의 오른쪽부분에 패딩처리를한다.</p>
	 * @param target 패딩처리될 대상 문자열
	 * @param padded_length 결과문자열의 최종 길이
	 * @param pad_string 패드되어 채워질 문자
	 * @return 패딩처리된 문자열
	 */
	public String rpad(String target, int padded_length, String pad_string) {
		StringBuilder result_string = new StringBuilder();
		
		result_string.append(target);
		for(int idx = 0; idx < padded_length - target.length(); idx++) result_string.append(pad_string);
		
		return result_string.toString();
	}
	
	/**
	 * <p>null인 문자열을 검사한다.</p>
	 * @param value 판단 대상문자열
	 * @return null이면 빈값을 리턴하고 아닐 경우 대상문자열을 리턴한다.
	 */
	public String emptyCheck(String value) {
		if(value == null || "null".equals(value.trim())) {
			return "";
		} else {
			return value;
		}
	}
	
	/**
	 * <p>null이거나 비어있는 문자열을 검사한다.</p>
	 * @param value 판단 대상문자열
	 * @return 비어있는지 여부의 참, 거짓값
	 */
	public boolean isEmpty(String value) {
		if(value == null || "".equals(value.trim()) || "null".equals(value.trim())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * <p>대상문자열에서 특수문자를 제거한다.</p>
	 * @param targetValue 대상문자열
	 * @return 제거된 문자열
	 */
	public String replaceSpecialCharacters(String targetValue) {
		return replaceSpecialCharacters(targetValue, "");
	}
	
	/**
	 * <p>대상문자열에서 특수문자를 치환한다.</p>
	 * @param targetValue 대상문자열
	 * @param replaceValue 치환문자열
	 * @return 치환된 문자열
	 */
	public String replaceSpecialCharacters(String targetValue, String replaceValue) {
		String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
		return targetValue.replaceAll(match, replaceValue);
	}
	
	/**
	 * <p>대상문자열에서 태그를 제거한다.</p>
	 * @param targetValue 대상문자열
	 * @return 제거된 문자열
	 */
	public String replaceTag(String targetValue) {
		return replaceSpecialCharacters(targetValue, "");
	}
	
	/**
	 * <p>대상문자열에서 태그를 치환한다.</p>
	 * @param targetValue 대상문자열
	 * @param replaceValue 치환문자열
	 * @return 치환된 문자열
	 */
	public String replaceTag(String targetValue, String replaceValue) {
		String match = "<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>";
		return targetValue.replaceAll(match, replaceValue);
	}
	
	/**
	 * <p>대상문자열에서 태그를 이스케이프 문자로 치환한다.</p>
	 * @param targetValue 대상문자열
	 * @return 치환된 문자열
	 */
	public String tagEscape(String targetValue) {
		return targetValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}
	
	/**
	 * <p>대상문자열이 지정된 길이 보다 클 경우 '...'의 말줄임을 붙인다.</p>
	 * @param targetValue 대상문자열
	 * @param size 문자열 표현 제한 byte
	 * @return 줄여진 문자열
	 */
	public String makeEllipsis(String targetValue, int size) {
		if(isEmpty(targetValue)) {
			return "";
		}
		
		String returnValue  = targetValue;
		int fixedLength	 = 0;
		
		for(int idx = 0; idx < targetValue.length(); idx++) {
			fixedLength += (targetValue.charAt(idx) > 128) ? 2 : 1;
			if (fixedLength > size) {
				returnValue = targetValue.substring(0, idx) + "...";
				break;
			}
		}

		return returnValue;
	}
	
	/**
	 * <p>지정된 페이지인코딩에 따라 문자열을 인코딩한다.</p>
	 * @param str 인코딩할 문자열
	 * @return 인코딩된 문자열
	 */
	public String encoding(String str) throws UnsupportedEncodingException {
		return new String(str.getBytes(ProjectConstant.ENCODING_SERVER), ProjectConstant.ENCODING_PAGE);
	}
	
	/**
	 * <p>지정된 서버인코딩에 따라 문자열을 디코딩한다.</p>
	 * @param str 디코딩할 문자열
	 * @return 디코딩된 문자열
	 */
	public String decoding(String str) throws UnsupportedEncodingException {
		return new String(str.getBytes(ProjectConstant.ENCODING_PAGE), ProjectConstant.ENCODING_SERVER);
	}

}
