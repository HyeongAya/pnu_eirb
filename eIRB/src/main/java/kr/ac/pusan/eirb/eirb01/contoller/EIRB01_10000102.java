package kr.ac.pusan.eirb.eirb01.contoller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/eirb01/10000102/*")
@SuppressWarnings("unchecked")
public class EIRB01_10000102 extends BaseController {

	@RequestMapping("/")
	public ModelAndView main(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		
		StringUtil su = new StringUtil();
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		
		HashMap<String, Object> scheduleMap = new HashMap<String, Object>();
		scheduleMap.put("KIND_FG", "A");
		List<Map<String, Object>> deliList  = commonService.selectList("EIRB01_10000101.SELECT_DELI_SCHEDULE", scheduleMap);
		model.put("DELI_LIST", deliList);
		
		scheduleMap.put("KIND_FG", "B");
		List<Map<String, Object>> deliListB  = commonService.selectList("EIRB01_10000101.SELECT_DELI_SCHEDULE", scheduleMap);
		model.put("DELI_LIST_B", deliListB);
				
		return new ModelAndView("eirb01/10000102_l");
	}
	
	@RequestMapping("/view")
	public ModelAndView view(ModelMap model, HttpServletRequest request) {
		
		String DELI_SCHE_KEYNO = (String)paramMap.get("DELI_SCHE_KEYNO");
		paramMap.put("DELI_SCHE_KEYNO",DELI_SCHE_KEYNO);
		
		Object data = commonService.select("EIRB01_10000101.SELECT_DELI_SCHEDULE_INFO", paramMap);
		model.addAttribute("DELI_DATA", data);
		
		List<Map<String, Object>> attd_list  = commonService.selectList("EIRB01_10000102.SELECT_DELI_ATTD_USER", paramMap);
		model.put("ATTD_LIST", attd_list);
		
		List<Map<String, Object>> user_list  = commonService.selectList("EIRB01_10000102.SELECT_DELI_USER", paramMap);
		model.put("USER_LIST", user_list);
		
		model.put("DELI_SCHE_KEYNO", DELI_SCHE_KEYNO);

		return new ModelAndView("eirb01/10000102_v");
	}
	
	@RequestMapping("/save")
	public ModelAndView save(ModelMap model, RedirectAttributes redirectAttr) {
		String DELI_ATTD_FG_YN = (String)paramMap.get("DELI_ATTD_FG_YN");
		String DELI_SCHE_KEYNO = (String)paramMap.get("DELI_SCHE_KEYNO");
		
		if(DELI_ATTD_FG_YN != null) {
			try {
				commonService.setInsert("EIRB01_10000102.UPDATE_DELI_USER", paramMap);
				model.addAttribute("message", Message.msg("success.common.insert"));
			} catch(Exception ex) {
				model.addAttribute("message", Message.msg("fail.common.insert"));
			}
		} else {
			try {
				commonService.setUpdate("EIRB01_10000102.INSERT_DELI_USER", paramMap);
				model.addAttribute("message", Message.msg("success.common.update"));
			} catch(Exception ex) {
				model.addAttribute("message", Message.msg("fail.common.update"));
			}
		}
		
		redirectAttr.addAttribute("DELI_SCHE_KEYNO", DELI_SCHE_KEYNO);
		
		return new ModelAndView("redirect:/eirb01/10000102/view");
	}

	@RequestMapping("/attend_save")
	public ModelAndView attend_save(ModelMap model, RedirectAttributes redirectAttr) {
		String DELI_SCHE_KEYNO = (String)paramMap.get("DELI_SCHE_KEYNO");
		
		try {
			commonService.setInsert("EIRB01_10000102.UPDATE_DELI_SCHE_DECIDE", paramMap);
			model.addAttribute("message", Message.msg("success.common.insert"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.insert"));
		}
		
		redirectAttr.addAttribute("DELI_SCHE_KEYNO", DELI_SCHE_KEYNO);
		
		return new ModelAndView("redirect:/eirb01/10000102/view");
	}
	
	@RequestMapping("/deli")
	public ModelAndView deli(ModelMap model) {

		List<Map<String, Object>> delitaskList  = commonService.selectList("EIRB01_10000102.SELECT_DELI_TASK");
		model.put("DELI_TASK_LIST", delitaskList);
		
		return new ModelAndView("eirb01/10000102_d");
	}
	
	@RequestMapping("/task_save")
	public ModelAndView task_save(ModelMap model, RedirectAttributes redirectAttr) {
		List list = getListFromJson("SAVE_DATA");
		String DELI_SCHE_KEYNO = (String)paramMap.get("DELI_SCHE_KEYNO");
		
		try {
			commonService.setBatchUpdate("EIRB03_10000301.UPDATE_EDU_SCHEDULE_LIST", list);
			model.addAttribute("message", Message.msg("success.common.update"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.update"));
		}
		
		redirectAttr.addAttribute("DELI_SCHE_KEYNO", DELI_SCHE_KEYNO);
		
		return new ModelAndView("redirect:/eirb02/10000102/deli");
	}
}
