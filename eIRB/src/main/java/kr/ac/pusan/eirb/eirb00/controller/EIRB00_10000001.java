package kr.ac.pusan.eirb.eirb00.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/eirb00/10000001/*")
public class EIRB00_10000001 extends BaseController {

	@RequestMapping("/")
	public ModelAndView main(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();

		model.put("USER_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
		
		StringUtil su = new StringUtil();
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		
		paramMap.put("SITE_KEYNO", "1000");
		paramMap.put("BBS_KEYNO","1");
		paramMap.put("SEARCH_KEYWORD",null);
		paramMap.put("PAGE_SIZE","5");
		paramMap.put("PAGE_INDEX",null);
		
		/* 공지사항 */
		model.addAttribute("BBSCONF", commonService.select("EIRB05_Common.SELECT_BBSCONF", paramMap));
		model.addAttribute("BBS_DATA", commonService.selectList("EIRB05_Common.SELECT_BBS_ARTICLE_LIST_01", paramMap));
		
		/* 교육안내 */
		model.addAttribute("EDU_DATA", commonService.selectList("EIRB03_10000301.SELECT_EDU_SCHEDULE_LIST"));

		return new ModelAndView("eirb/main");
	}
	
	@RequestMapping(value="/deli_calendar", method=RequestMethod.POST)
	@ResponseBody
	public List<Map>deli_calendar(HttpServletRequest request)throws Exception {

		List<Map> deliList = commonService.selectList("EIRB01_10000101.SELECT_DELI_SCHEDULE");
		
		return deliList;
	}
	
	@RequestMapping("/mypage")
	public ModelAndView mypage(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		StringUtil su = new StringUtil();
		
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		
		HttpSession session = request.getSession();
		paramMap.put("USER_ID",session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
		
		Object data = commonService.select("Common.SELECT_USER_INFO", paramMap);
		model.addAttribute("MYDATA", data);
		
		return new ModelAndView("eirb/mypage");
	}
	
	@RequestMapping("/mypageProc")
	public ModelAndView mypageProc(ModelMap model){
		
		try {
			commonService.setInsert("Common.UPDATE_USER_INFO", paramMap);
			model.addAttribute("message", Message.msg("success.common.update"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.update"));
		}
		
		return new ModelAndView("redirect:/eirb00/10000001/mypage");
	}
	
	@RequestMapping("/pwProc")
	public ModelAndView pwProc(ModelMap model){
		
		try {
			commonService.setInsert("Common.UPDATE_PASSWORD", paramMap);
			model.addAttribute("message", Message.msg("success.common.update"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.update"));
		}
		
		return new ModelAndView("redirect:/eirb00/10000001/mypage");
	}
}
