/**
 *
 */
package kr.ac.pusan.eirb.eirb03.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/eirb03/10000302/*")
@SuppressWarnings({"unchecked"})
public class EIRB03_10000302 extends BaseController {

	@RequestMapping("/")
	public String eduScheduleRecList(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		StringUtil su = new StringUtil();
		
		model.addAttribute("DATA", commonService.selectList("EIRB03_10000302.SELECT_EDU_SCHEDULE_REC_LIST"));
		
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		return "eirb03/10000302_l";
	}
	
	@RequestMapping("/view")
	public String eduScheduleRecView(ModelMap model) {
		model.addAttribute("DATAVIEW", commonService.selectList("EIRB03_10000302.SELECT_EDU_SCHEDULE_REC_VIEW",paramMap));
		model.addAttribute("EDU_KEYNO", paramMap.get("EDU_KEYNO"));
		
		return "eirb03/10000302_v";
	}
	
	@RequestMapping("/list_save")
	public ModelAndView eduScheduleRecListSave(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		List<HashMap<String, String>> list = getListFromJson("SAVE_DATA");
		
		try {
			commonService.setBatchUpdate("EIRB03_10000302.UPDATE_EDU_SCHEDULE_REC_LIST", list);
			model.addAttribute("message", Message.msg("success.common.update"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.update"));
		}
		return new ModelAndView("redirect:/eirb03/10000302/");
	}
	
	@RequestMapping("/list_del")
	public ModelAndView eduScheduleRecListDelete(ModelMap model) {
		List<HashMap<String, String>> list = getListFromJson("SAVE_DATA");
		
		try {
			commonService.setBatchDelete("EIRB03_10000302.DELETE_EDU_SCHEDULE_REC_LIST", list);
			model.addAttribute("message", Message.msg("success.common.delete"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.delete"));
		}
		return new ModelAndView("redirect:/eirb03/10000302/");
	}
	
	@RequestMapping("/list_complete")
	public ModelAndView eduScheduleRecListComplete(ModelMap model) {
		List<HashMap<String, String>> list = getListFromJson("SAVE_DATA");
		
		try {
			commonService.setBatchInsert("EIRB03_10000302.INSERT_EDU_SCHEDULE_COMPLETE_LIST", list);
			model.addAttribute("message", Message.msg("success.common.access"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.access"));
		}
		return new ModelAndView("redirect:/eirb03/10000302/");
	}
}
