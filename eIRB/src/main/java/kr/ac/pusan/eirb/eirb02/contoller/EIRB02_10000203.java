package kr.ac.pusan.eirb.eirb02.contoller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.connector.Request;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;
import kr.ac.pusan.core.vo.MultipartFileWrapper;

@Controller
@Scope(value="request")
@RequestMapping("/eirb02/10000203/*")
@SuppressWarnings("unchecked")
public class EIRB02_10000203 extends BaseController {

	@RequestMapping("/")
	public ModelAndView main(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		paramMap.put("USER_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
		
		StringUtil su = new StringUtil();
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		
		List<Map<String, Object>> accessList  = commonService.selectList("EIRB02_10000203.SELECT_DELI_ACCESS", paramMap);
		model.put("ACCESS_LIST", accessList);
		
		return new ModelAndView("eirb02/10000203_l");
	}
}
