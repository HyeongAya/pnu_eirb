/**
 *
 */
package kr.ac.pusan.eirb.eirb05.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;

@Controller
@Scope(value="request")
@RequestMapping("/eirb05/10000504/{bbsKeyno}/*")
@SuppressWarnings({"unchecked"})
public class EIRB05_10000504 extends BaseController {
	@RequestMapping("/")
	public String newsBbsList(ModelMap model, @PathVariable String bbsKeyno, HttpServletRequest request, HttpServletResponse response) {
		paramMap.put("SITE_KEYNO", "1000");
		paramMap.put("BBS_KEYNO", bbsKeyno);
		
		model.addAttribute("BBSCONF", commonService.select("EIRB05_Common.SELECT_BBSCONF", paramMap));
		
		return "eirb05/bbs_l";
	}
	
	@RequestMapping("/view")
	public String view(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		List<List<HashMap<String, Object>>> multiList = commonService.selectMultiList("EIRB05_Common.SELECT_BBS_ARTICLE", paramMap);
		
		model.addAttribute("SITE_KEYNO", paramMap.get("SITE_KEYNO"));
		model.addAttribute("BBS_KEYNO", paramMap.get("BBS_KEYNO"));
		model.addAttribute("BBS_TYPE_CD", paramMap.get("BBS_TYPE_CD"));
		model.addAttribute("multiList", commonService.setDatasetsToHashMap(multiList));
		
		return "eirb05/bbs_v";
	}
	
	@RequestMapping("/write")
	public String write(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		model.addAttribute("BBSCONF", commonService.select("EIRB05_Common.SELECT_BBSCONF", paramMap));
		
		return "eirb05/bbs_w";
	}
	
	@RequestMapping("/modify")
	public String modify(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		model.addAttribute("BBSCONF", commonService.select("EIRB05_Common.SELECT_BBSCONF", paramMap));
		
		List<List<HashMap<String, Object>>> multiList = commonService.selectMultiList("EIRB05_Common.SELECT_BBS_ARTICLE", paramMap);
		
		model.addAttribute("SITE_KEYNO", paramMap.get("SITE_KEYNO"));
		model.addAttribute("BBS_KEYNO", paramMap.get("BBS_KEYNO"));
		model.addAttribute("BBS_TYPE_CD", paramMap.get("BBS_TYPE_CD"));
		model.addAttribute("multiList", commonService.setDatasetsToHashMap(multiList));
		
		return "eirb05/bbs_w";
	}
	
	
	
	@RequestMapping("/save")
	public ModelAndView eduApplySave(ModelMap model) {
		try {
			commonService.setInsert("EIRB05_Common.INSERT_BBS_ARTICLE", paramMap);
			model.addAttribute("message", Message.msg("success.bbs.insert"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.bbs.insert"));
		}
		return new ModelAndView("redirect:/eirb03/10000303/");
	}
	
	@RequestMapping("/delete")
	public ModelAndView eduApplyDelete(ModelMap model) {
		try {
			commonService.setDelete("EIRB03_10000303.DELETE_EDU_APPLY", paramMap);
			model.addAttribute("message", Message.msg("success.apply.delete"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.apply.delete"));
		}
		return new ModelAndView("redirect:/eirb03/10000303/");
	}
}
