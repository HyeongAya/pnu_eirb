package kr.ac.pusan.eirb.common.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;

@Controller
@SuppressWarnings("unchecked")
@Scope(value="request")
public class PageController extends BaseController {
	@RequestMapping("/index")
	public ModelAndView index(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(false);
		
		String redirectPage = "index";
		String userId = (String) session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID);
		
		if(userId != null) {
			redirectPage = "redirect:/eirb00/10000001/";
		}
		
		return new ModelAndView(redirectPage);
	}

	@RequestMapping("/include/eirb_default/header")
	public ModelAndView header(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("include/eirb_default/header");
	}

	@RequestMapping("/include/eirb_default/left")
	public ModelAndView left(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session	 = request.getSession(false);
		
		String userId = (String) session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID);
		
		paramMap.put("SITE_KEYNO","1000");
		paramMap.put("USER_ID", userId);
		
		List<HashMap<String, Object>> menuList = commonService.selectList("Common.SELECT_MENU", paramMap);
		
		model.put("MENU_LIST", menuList);
		
		return new ModelAndView("include/eirb_default/left");
	}
	
	@RequestMapping("/include/eirb_default/footer")
	public void footer(ModelMap model) {
	}
	
	@RequestMapping("/include/eirb_default/favorite")
	public void favorite(ModelMap model) {
	}
	
	@RequestMapping("/include/eirb_index/header")
	public ModelAndView indexHeader(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("include/eirb_index/header");
	}
	
	@RequestMapping("/include/eirb_index/footer")
	public void indexFooter(ModelMap model) {
	}
	
}
