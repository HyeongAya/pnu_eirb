/**
 *
 */
package kr.ac.pusan.eirb.eirb03.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/eirb03/10000303/*")
@SuppressWarnings({"unchecked"})
public class EIRB03_10000303 extends BaseController {

	@RequestMapping("/")
	public String eduApplyPossibleList(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		StringUtil su = new StringUtil();
		
		HttpSession session = request.getSession(false);
		String userId = (String) session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID);
		
		paramMap.put("USER_ID", userId);
		
		model.addAttribute("DATA", commonService.selectList("EIRB03_10000303.SELECT_EDU_APPLY_POSSIBLE_LIST", paramMap));
		
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		return "eirb03/10000303_l";
	}
	
	@RequestMapping("/view")
	public String eduApplyPossibleView(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		String userId = (String) session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID);
		
		paramMap.put("USER_ID", userId);
		
		model.addAttribute("DATAVIEW", commonService.select("EIRB03_10000303.SELECT_EDU_APPLY_POSSIBLE_VIEW",paramMap));
		
		return "eirb03/10000303_v";
	}
	
	@RequestMapping("/save")
	public ModelAndView eduApplySave(ModelMap model) {
		try {
			commonService.setInsert("EIRB03_10000303.INSERT_EDU_APPLY", paramMap);
			model.addAttribute("message", Message.msg("success.apply.insert"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.apply.insert"));
		}
		return new ModelAndView("redirect:/eirb03/10000303/");
	}
	
	@RequestMapping("/delete")
	public ModelAndView eduApplyDelete(ModelMap model) {
		try {
			commonService.setDelete("EIRB03_10000303.DELETE_EDU_APPLY", paramMap);
			model.addAttribute("message", Message.msg("success.apply.delete"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.apply.delete"));
		}
		return new ModelAndView("redirect:/eirb03/10000303/");
	}
}
