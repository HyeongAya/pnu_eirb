package kr.ac.pusan.eirb.eirb01.contoller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/eirb01/10000103/*")
@SuppressWarnings("unchecked")
public class EIRB01_10000103 extends BaseController {

	@RequestMapping("/")
	public ModelAndView main(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		
		StringUtil su = new StringUtil();
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		
		List<Map<String, Object>> deliNewList  = commonService.selectList("EIRB01_10000103.SELECT_DELI_ACCESS_NEW");
		model.put("NEW_LIST", deliNewList);
		
		List<Map<String, Object>> deliAcessList  = commonService.selectList("EIRB01_10000103.SELECT_DELI_ACCESS_LIST");
		model.put("ACCESS_LIST", deliAcessList);
		
		return new ModelAndView("eirb01/10000103_l");
	}
	
	@RequestMapping(value="/access", method=RequestMethod.POST)
	@ResponseBody
	public List<Map>access(HttpServletRequest request)throws Exception {
		
		String DELI_APPL_KEYNO = request.getParameter("DELI_APPL_KEYNO");
		paramMap.put("DELI_APPL_KEYNO",DELI_APPL_KEYNO);
		
		List<Map> deliProgList = commonService.selectList("EIRB01_10000103.SELECT_DELI_PROG", paramMap);
		
		return deliProgList;
		
	}
	
	@RequestMapping("/view")
	public ModelAndView view(ModelMap model, RedirectAttributes redirectAttr) {
		
		Object data = commonService.select("EIRB02_10000202.SELECT_APPLY_INFO", paramMap);
		model.addAttribute("APPL_DATA", data);
		
		List<Map<String, Object>> resList  = commonService.selectList("EIRB02_10000202.SELECT_RESEARCHER", paramMap);
		model.put("RES_LIST", resList);
		
		List<Map<String, Object>> fileList  = commonService.selectList("EIRB02_10000202.SELECT_FILE_LIST", paramMap);
		model.put("FILE_LIST", fileList);
		
		return new ModelAndView("eirb01/10000103_v");
	}
	
	@RequestMapping("/save")
	public ModelAndView save(ModelMap model, RedirectAttributes redirectAttr) {
		
		try {
			String ACCS_STATUS = (String)paramMap.get("DELI_ACCS_STATUS");
			
			if(ACCS_STATUS.equals("05")){
				
				SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ("yyyyMMddHHmmss");
				Calendar mCalendar = Calendar.getInstance();
				String DELI_APPL_KEYNO_CH = mSimpleDateFormat.format(mCalendar.getTime());
				paramMap.put("DELI_APPL_KEYNO_CH",DELI_APPL_KEYNO_CH);
				
				String ACCS_NOTE = (String)paramMap.get("ACCS_NOTE");
				paramMap.put("DELI_ACCS_NOTE",ACCS_NOTE);
			}
			
			commonService.setInsert("EIRB01_10000103.INSERT_DELI_ACCESS", paramMap);
			model.addAttribute("message", Message.msg("success.common.insert"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.insert"));
		}
		
		return new ModelAndView("redirect:/eirb01/10000103/");
	}
	
}
