package kr.ac.pusan.eirb.eirb00.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/eirb00/10000002/*")
public class EIRB00_10000002 extends BaseController {

	@RequestMapping("/mypage")
	public ModelAndView main(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		StringUtil su = new StringUtil();
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		return new ModelAndView("eirb/mypage");
	}
	
}
