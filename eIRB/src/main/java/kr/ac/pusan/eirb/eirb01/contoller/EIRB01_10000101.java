package kr.ac.pusan.eirb.eirb01.contoller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/eirb01/10000101/*")
@SuppressWarnings("unchecked")
public class EIRB01_10000101 extends BaseController {

	@RequestMapping("/")
	public ModelAndView main(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		StringUtil su = new StringUtil();
		
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		HashMap<String, Object> scheduleMap = new HashMap<String, Object>();
		scheduleMap.put("KIND_FG", "A");
		List<Map<String, Object>> deliList  = commonService.selectList("EIRB01_10000101.SELECT_DELI_SCHEDULE", scheduleMap);
		model.put("DELI_LIST", deliList);
		
		scheduleMap.put("KIND_FG", "B");
		List<Map<String, Object>> deliListB  = commonService.selectList("EIRB01_10000101.SELECT_DELI_SCHEDULE", scheduleMap);
		model.put("DELI_LIST_B", deliListB);
		
		return new ModelAndView("eirb01/10000101_l");
	}
	
	@RequestMapping("/view")
	public ModelAndView view(ModelMap model, HttpServletRequest request) {
		
		Object data = commonService.select("EIRB01_10000101.SELECT_DELI_SCHEDULE_INFO", paramMap);
		model.addAttribute("DELI_DATA", data);
		
		String kind_fg = (String)paramMap.get("KIND_FG");
		model.addAttribute("KIND_FG",kind_fg);
		return new ModelAndView("eirb01/10000101_v");
	}
	
	@RequestMapping("/save")
	public ModelAndView save(ModelMap model) {
		String SAVE_FLAG = (String)paramMap.get("SAVE_FLAG");
		
		if(SAVE_FLAG.equals("add")) {
			try {
				commonService.setInsert("EIRB01_10000101.INSERT_DELI_SCHEDULE", paramMap);
				model.addAttribute("message", Message.msg("success.common.insert"));
			} catch(Exception ex) {
				model.addAttribute("message", Message.msg("fail.common.insert"));
			}
		} else if(SAVE_FLAG.equals("modify")) {
			try {
				commonService.setUpdate("EIRB01_10000101.UPDATE_DELI_SCHEDULE", paramMap);
				model.addAttribute("message", Message.msg("success.common.update"));
			} catch(Exception ex) {
				model.addAttribute("message", Message.msg("fail.common.update"));
			}
		} else {
			try {
				commonService.setUpdate("EIRB01_10000101.DEL_DELI_SCHEDULE", paramMap);
				commonService.setUpdate("EIRB01_10000102.DEL_DELI_USER", paramMap);
				model.addAttribute("message", Message.msg("success.common.delete"));
			} catch(Exception ex) {
				model.addAttribute("message", Message.msg("fail.common.delete"));
			}
		}
		return new ModelAndView("redirect:/eirb01/10000101/");
	}
}
