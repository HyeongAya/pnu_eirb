package kr.ac.pusan.eirb.common.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.exception.CoreException;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.EgovHttpSessionBindingListener;

@Controller
@Scope(value="request")
@SuppressWarnings("unchecked")
public class LoginController extends BaseController {

	@RequestMapping("/login")
	public ModelAndView login(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session	 = request.getSession();
		String redirectUrl	  = "/index";
		
		if(session.getAttribute("CSRF_TOKEN") == null || paramMap.get("CT") == null || !session.getAttribute("CSRF_TOKEN").equals(paramMap.get("CT"))) {
			model.addAttribute("message", Message.msg("pnu.auth03"));
		} else {
			Map<String, Object> map = (Map<String, Object>)commonService.select("Common.SELECT_LOGIN", paramMap);
			String auth_cd = (String) map.get("AUTH_CD");
			
			if(auth_cd == null || ("").equals(auth_cd)) {
				throw new CoreException(Message.propFormat("fail.login"), request, response, "/");
			} /*else if(("00").equals(auth_cd)) {
				// 교직원이지만 eirb 유저테이블에 정보가 없는경우
				//throw new CoreException(Message.propFormat("pnu.auth01"), request, response, "/");
			} */else {
				// 로그인 시도할때 계정정보에 승인여부 체크
				if( "N".equals(map.get("CONFIRM_FG"))) {
					throw new CoreException(Message.propFormat("fail.login.confirm"), request, response, "/");
				}else {
					request.getSession().removeAttribute(map.get("USER_ID").toString());
					session.removeAttribute(ProjectConstant.KEY_SESSION_SUPER_ADMIN);
					session.removeAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID);
					session.removeAttribute(ProjectConstant.KEY_SESSION_MEMBER_NAME);
					session.removeAttribute(ProjectConstant.KEY_SESSION_MEMBER_DEPT);
					session.removeAttribute(ProjectConstant.KEY_SESSION_MEMBER_DEPT_NAME);
					session.removeAttribute(ProjectConstant.KEY_SESSION_MEMBER_AUTH_CODE);
					
					EgovHttpSessionBindingListener listener = new EgovHttpSessionBindingListener();
					request.getSession().setAttribute(map.get("USER_ID").toString(), listener);
					
					if(("601210").equals(map.get("DEPT_CD"))) {
						session.setAttribute(ProjectConstant.KEY_SESSION_SUPER_ADMIN, "Y");
					}
					session.setAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID, String.valueOf(map.get("USER_ID")));
					session.setAttribute(ProjectConstant.KEY_SESSION_MEMBER_NAME, String.valueOf(map.get("USER_NM")));
					session.setAttribute(ProjectConstant.KEY_SESSION_MEMBER_DEPT, String.valueOf(map.get("DEPT_CD")));
					session.setAttribute(ProjectConstant.KEY_SESSION_MEMBER_DEPT_NAME, String.valueOf(map.get("DEPT_NM")));
					session.setAttribute(ProjectConstant.KEY_SESSION_MEMBER_AUTH_CODE, String.valueOf(map.get("AUTH_CD")));
					
					redirectUrl = "/eirb00/10000001/";	
				}
				
				
			}
		}
		return new ModelAndView("redirect:" + redirectUrl);
	}
	
	@RequestMapping("/logout")
	public ModelAndView logout(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		String redirectUrl  = "/index";
		
		session.invalidate();
		
		return new ModelAndView("redirect:" + redirectUrl);
	}
	
	@RequestMapping("/join")
	public ModelAndView membership(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String redirectPage = "join";
		
		return new ModelAndView(redirectPage);
	}

	@RequestMapping("/joinProc")
	public ModelAndView access(ModelMap model, HttpServletRequest request, HttpServletResponse response){
		
		try {
			commonService.setInsert("Common.INSERT_USER", paramMap);
			paramMap.put("SITE_KEYNO", 1000);
			paramMap.put("AUTH_GCD", "AT");
			paramMap.put("AUTH_CD", paramMap.get("USER_TYPE_CD"));
			commonService.setInsert("Common.INSERT_USER_MENU_AUTH", paramMap);
			
			model.addAttribute("message", Message.msg("success.join.insert"));
		} catch(Exception ex) {
			//model.addAttribute("message", Message.msg("fail.common.insert"));
			throw new CoreException(Message.propFormat("fail.common.insert"), request, response, "/join");
		}
		
		return new ModelAndView("redirect:/index");
	}
	
	@RequestMapping("/id_chk")
	public void idChk(ModelMap model){
		String isDuple = (String) commonService.select("Common.SELECT_ID_CHECK", paramMap);
		model.put("chk_value", isDuple);
	}
	
	@RequestMapping("/search")
	public ModelAndView search(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String redirectPage = "search";
		
		return new ModelAndView(redirectPage);
	}
	
	@RequestMapping("/searchProc")
	public void searchProc(ModelMap model){
		String idSearch = (String) commonService.select("Common.SELECT_ID_SEARCH", paramMap);
		model.put("search_id", idSearch);
	}
}
