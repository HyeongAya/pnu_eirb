package kr.ac.pusan.eirb.bbs.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.exception.CoreException;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;
import kr.ac.pusan.core.vo.MultipartFileWrapper;

import kr.ac.pusan.core.ProjectConstant;

@Controller
@Scope(value="request")
@RequestMapping("/bbs2/*")
@SuppressWarnings("unchecked")
public class BBSController extends BaseController {

	@Value("#{prop['project.root']}")	   String   P_ROOT_PATH;
	@Value("#{prop['bbs.defaultPageSize']}")String   P_PAGE_SIZE;
	@Value("#{prop['fileUpload.root']}")	   String   FILE_ROOT_PATH;

	private final int _MIN_KEY = 201;
	private final int _MAX_KEY = 700;

	@RequestMapping("/bbs/fileDownload")
	public ModelAndView fileDownload() {
		//String rootPath		 = System.getProperty(P_ROOT_PATH);
		String rootPath		 = FILE_ROOT_PATH;
		String fileName		 = (String)paramMap.get("FILE");
		String originalfileName = (String)paramMap.get("FILE_NM");
		String fileRealPath = rootPath + fileName;

		return downloadFile(fileRealPath, originalfileName);
	}

	@RequestMapping("/commentWrite")
	private void commentWrite(ModelMap model) {
		try{
			commonService.setInsert("BBSController.INSERT_TM_BBSCOMMENT_M", paramMap);
			model.addAttribute("message", Message.msg("success.bbs.insert"));
			model.addAttribute("result_cd", "1");
		}catch(Exception ex){
			model.addAttribute("message", Message.msg("fail.bbs.insert"));
			model.addAttribute("result_cd", "0");
		}
	}

	@RequestMapping("/commentDel")
	private void commentDel(ModelMap model) {
		try{
			int cnt = commonService.setDelete("BBSController.DELETE_TM_BBSCOMMENT_M", paramMap);
			if(cnt == 1) {
				model.addAttribute("message", Message.msg("success.bbs.delete"));
				model.addAttribute("result_cd", "1");
			} else {
				model.addAttribute("message", Message.msg("fail.bbs.delete"));
				model.addAttribute("result_cd", "0");
			}
		}catch(Exception ex){
			model.addAttribute("message", Message.msg("fail.bbs.delete"));
			model.addAttribute("result_cd", "0");
		}
	}

	@RequestMapping("/commentMody")
	private void commentMody(ModelMap model) {
		try{
			int cnt = commonService.setUpdate("BBSController.UPDATE_TM_BBSCOMMENT_M", paramMap);
			if(cnt == 1) {
				model.addAttribute("message", Message.msg("success.bbs.update"));
				model.addAttribute("result_cd", "1");
			} else {
				model.addAttribute("message", Message.msg("fail.bbs.update"));
				model.addAttribute("result_cd", "0");
			}
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.bbs.update"));
			model.addAttribute("result_cd", "0");
		}
	}

	private ModelAndView save01(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		StringUtil su	   = new StringUtil();
		String group_no	 = (String)paramMap.get("GROUP_NO");
		String sort_base	= (String)paramMap.get("SORT_BASE");
		if(paramMap.get("NOTICE_YN") == null) {
			paramMap.put("NOTICE_YN", "N");
		}
		if(!su.isEmpty(group_no) && !su.isEmpty(sort_base)) {
			paramMap.put("SORT_BASE", sort_base);

			Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_MAX_SORT", paramMap);

			if(map == null) {
				String sort_key = new Character((char)(_MIN_KEY)).toString();
				paramMap.put("SORT_BASE", sort_base + sort_key + sort_key);
			} else {
				String sort_key = (String)map.get("MAX_SORT");
				int i_key1	  = sort_key.charAt(sort_key.length() - 2);
				int i_key2	  = sort_key.charAt(sort_key.length() - 1);

				if(i_key1 >= _MAX_KEY && i_key2 >= _MAX_KEY) {
					throw new CoreException(Message.propFormat("bbs.replyErr01"), request, response);
				} else if(i_key2 >= _MAX_KEY) {
					i_key1++;
					i_key2 = _MIN_KEY;
				} else {
					i_key2++;
				}

				String s_key1 = Character.toString ((char) i_key1);
				String s_key2 = Character.toString ((char) i_key2);

				paramMap.put("SORT_BASE", sort_base + s_key1 + s_key2);
			}
		} else {
			String sort_key = new Character((char)(_MIN_KEY)).toString();
			paramMap.put("SORT_BASE", sort_key + sort_key);
		}

		Map<String, Object> multipartFileMap			= (Map<String, Object>)paramMap.get(ProjectConstant.KEY_FORM_FILE + ProjectConstant.KEY_INTERCEPTOR_VALUES_POSTFIX);
		List<Map<String, Map<String, Object>>> fileList = new ArrayList<Map<String, Map<String,Object>>>();

		if(multipartFileMap != null) {
			for(int idx = 1; idx <= multipartFileMap.size(); idx++) {
				MultipartFileWrapper multipartFileWrapper = (MultipartFileWrapper)multipartFileMap.get(ProjectConstant.KEY_FORM_FILE + idx);

				HashMap<String, Object> hm = new HashMap<String, Object>();
				hm.put("BBSCONF_NO", paramMap.get("BBSCONF_NO"));
				hm.put("FILE_DESC", paramMap.get(multipartFileWrapper.getName() + "_DESC"));
				hm.put("FILE_PATH", multipartFileWrapper.getWebPath());
				hm.put("FILE_NM", multipartFileWrapper.getFileName());
				hm.put("FILE_SIZE", multipartFileWrapper.getSize());
				hm.put("FILE_EXT", multipartFileWrapper.getContentType());
				hm.put("ORIGINAL_FILE_NM", multipartFileWrapper.getOriginalFileName());

				HashMap<String, Map<String, Object>> wrapperMap = new HashMap<String, Map<String, Object>>();
				wrapperMap.put("BBSController.INSERT_TM_BBSFILE_M", hm);

				fileList.add(wrapperMap);
			}
		}

		int articleNo = (Integer)commonService.setChainMultiBatch("K_ARTICLE_NO", "BBSController.INSERT_TM_BBSARTICLE_M", paramMap, fileList);

		return new ModelAndView("redirect:/bbs/view?bbsconf_no=" + paramMap.get("BBSCONF_NO") + "&article_no=" + articleNo);

	}

	private ModelAndView write01(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("/bbs/write01");

	}

	private ModelAndView view01(ModelMap model) {
		model.addAttribute("ARTICLE", commonService.select("BBSController.SELECT_BBSARTICLE", paramMap));
		model.addAttribute("FILES", commonService.selectList("BBSController.SELECT_BBSFILE_LIST", paramMap));
		model.addAttribute("COMMENT", commonService.selectList("BBSController.SELECT_BBSCOMMENT_LIST", paramMap));

		if((Map)model.get("ARTICLE") != null) {
			/*
			if(((Map)model.get("ARTICLE")).get("PWD") == null) {
				return new ModelAndView("/bbs/view01");
			} else if(paramMap.get("P").toString().equals(((Map)model.get("ARTICLE")).get("PWD").toString())) {
				return new ModelAndView("/bbs/view01");
			} else {
				return null;
			}
			*/
			Map map = (Map)model.get("ARTICLE") ;
			map.put("CONTENT", StringEscapeUtils.unescapeHtml((String)map.get("CONTENT")));
			
			return new ModelAndView("/bbs/view01");
		} else {
			return null;
		}
	}

	private int getBBSListCount() {
		return (Integer)commonService.select("BBSController.SELECT_BBSLIST_COUNT", paramMap);
	}

	private ModelAndView list01(ModelMap model) {
		model.addAttribute("TOT_CNT", getBBSListCount());
		model.addAttribute("BBSLIST", commonService.selectList("BBSController.SELECT_BBSLIST01", paramMap));

		return new ModelAndView("/bbs/list01");
	}

	@RequestMapping("/view")
	public ModelAndView view(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		paramMap.put("GROUP_ID", request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_GROUP) == null ? "":request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_GROUP));
		paramMap.put("USER_ID", request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID) == null ? "":request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));

		int chk = (Integer)commonService.select("BBSController.SELECT_DELETEAUTH", paramMap);
		if(chk != 0) {
			paramMap.put("DEL_AUTH", "Y");
		}

		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);

		request.setAttribute("BBSCONF", map);
		paramMap.put("CONFIRM_YN", map.get("CONFIRM_YN"));
		paramMap.put("DATETERM_YN", map.get("DATETERM_YN"));
		request.setAttribute("BBSCOND", paramMap);

		if("01".equals(map.get("BBS_TYPE"))) {
			if(view01(model) == null) {
				throw new CoreException(Message.propFormat("bbs.auth01"), request, response);
			} else {
				return new ModelAndView("/bbs/view01");
			}
		} else {
			if(view01(model) == null) {
				throw new CoreException(Message.propFormat("bbs.auth01"), request, response);
			} else {
				return new ModelAndView("/bbs/view01");
			}
		}

		//return new ModelAndView("/bbs/view01");
	}

	@RequestMapping("/write")
	public ModelAndView write(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		paramMap.put("C_PAGE", paramMap.get("C_PAGE"));
		paramMap.put("S_TYPE", paramMap.get("S_TYPE"));
		paramMap.put("S_KEYWORD", paramMap.get("S_KEYWORD"));

		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);

		request.setAttribute("BBSCONF", map);
		paramMap.put("CONFIRM_YN", map.get("CONFIRM_YN"));
		paramMap.put("DATETERM_YN", map.get("DATETERM_YN"));
		request.setAttribute("BBSCOND", paramMap);

		if("01".equals(map.get("BBS_TYPE"))) {
			return write01(model, request, response);
		} else {
			return write01(model, request, response);
		}

		//return new ModelAndView("/bbs/list01");
	}

	@RequestMapping("/save")
	public ModelAndView save(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);

		request.setAttribute("BBSCONF", map);
		paramMap.put("CONFIRM_YN", map.get("CONFIRM_YN") == null || map.get("CONFIRM_YN").equals("") ? "N":map.get("CONFIRM_YN"));
		paramMap.put("DATETERM_YN", map.get("DATETERM_YN") == null || map.get("DATETERM_YN").equals("") ? "N":map.get("DATETERM_YN"));
		request.setAttribute("BBSCOND", paramMap);

		if("01".equals(map.get("BBS_TYPE"))) {
			return save01(model, request, response);
		} else {
			return save01(model, request, response);
		}

		//return new ModelAndView("/bbs/list01");
	}

	@RequestMapping("/getList")
	private void getList(ModelMap model) {
		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);

		if("01".equals(map.get("BBS_TYPE"))) {
			list01(model);
		} else {
			list01(model);
		}
	}

	@RequestMapping("/list")
	public ModelAndView list(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);

		StringUtil su = new StringUtil();
		String pageSize = map.get("PAGE_PER_CNT") == null || su.isEmpty(map.get("PAGE_PER_CNT").toString()) ? P_PAGE_SIZE:map.get("PAGE_PER_CNT").toString();
		String c_page = paramMap.get("C_PAGE") == null || su.isEmpty(paramMap.get("C_PAGE").toString()) ? "1":paramMap.get("C_PAGE").toString();

		paramMap.put("CURRENT_PAGE", c_page);
		paramMap.put("C_PAGE", c_page);
		paramMap.put("PAGE_PER_CNT", pageSize);

		request.setAttribute("BBSCONF", map);
		paramMap.put("CONFIRM_YN", map.get("CONFIRM_YN"));
		paramMap.put("DATETERM_YN", map.get("DATETERM_YN"));

		paramMap.put("GROUP_ID", request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_GROUP) == null ? "":request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_GROUP));
		paramMap.put("USER_ID", request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID) == null ? "":request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));

		int chk = (Integer)commonService.select("BBSController.SELECT_DELETEAUTH", paramMap);
		if(chk != 0) {
			paramMap.put("DEL_AUTH", "Y");
		}

		request.setAttribute("BBSCOND", paramMap);

		if("01".equals(map.get("BBS_TYPE"))) {
			return list01(model);
		} else {
			return list01(model);
		}

		//return new ModelAndView("/bbs/list01");
	}

	@RequestMapping("/delete")
	public ModelAndView delete(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		paramMap.put("GROUP_ID", request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_GROUP) == null ? "":request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_GROUP));
		paramMap.put("USER_ID", request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID) == null ? "":request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));

		int chk = (Integer)commonService.select("BBSController.SELECT_DELETEAUTH", paramMap);

		if(chk != 0) {
			List list = new ArrayList();

			Map map1 = new HashMap();
			map1.put("BBSController.DELETE_TM_BBSFILES_M", paramMap);
			list.add(map1);

			Map map2 = new HashMap();
			map2.put("BBSController.DELETE_TM_BBSCOMMENTS_M", paramMap);
			list.add(map2);

			Map map3 = new HashMap();
			map3.put("BBSController.DELETE_TM_BBSARTICLES_M", paramMap);
			list.add(map3);

			commonService.setMultiBatch(list);
		} else {
			throw new CoreException(Message.propFormat("bbs.auth01"), request, response);
		}

		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);
		if("01".equals(map.get("BBS_TYPE"))) {
			return new ModelAndView("redirect:/bbs/list.do?bbsconf_no=" + paramMap.get("BBSCONF_NO"));
		} else {
			return new ModelAndView("redirect:/bbs/list.do?bbsconf_no=" + paramMap.get("BBSCONF_NO"));
		}

		//return new ModelAndView("/bbs/list01");
	}

	@RequestMapping("/del")
	public ModelAndView del(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		paramMap.put("USER_ID", request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID) == null ? "":request.getSession().getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));

		int chk = (Integer)commonService.select("BBSController.SELECT_DELAUTH", paramMap);

		if(chk != 0) {
			List list = new ArrayList();

			Map map1 = new HashMap();
			map1.put("BBSController.DELETE_TM_BBSFILES_M", paramMap);
			list.add(map1);

			Map map2 = new HashMap();
			map2.put("BBSController.DELETE_TM_BBSCOMMENTS_M", paramMap);
			list.add(map2);

			Map map3 = new HashMap();
			map3.put("BBSController.DELETE_TM_BBSARTICLES_M", paramMap);
			list.add(map3);

			commonService.setMultiBatch(list);
		} else {
			throw new CoreException(Message.propFormat("bbs.auth01"), request, response);
		}

		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);
		if("01".equals(map.get("BBS_TYPE"))) {
			return new ModelAndView("redirect:/bbs/list.do?bbsconf_no=" + paramMap.get("BBSCONF_NO"));
		} else {
			return new ModelAndView("redirect:/bbs/list.do?bbsconf_no=" + paramMap.get("BBSCONF_NO"));
		}

		//return new ModelAndView("/bbs/list01");
	}
	
	@RequestMapping("/modify")
	public ModelAndView modify(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);

		request.setAttribute("BBSCONF", map);
		paramMap.put("CONFIRM_YN", map.get("CONFIRM_YN"));
		paramMap.put("DATETERM_YN", map.get("DATETERM_YN"));
		request.setAttribute("BBSCOND", paramMap);
		
		model.addAttribute("ARTICLE", commonService.select("BBSController.SELECT_BBSARTICLE", paramMap));
		model.addAttribute("FILES", commonService.selectList("BBSController.SELECT_BBSFILE_LIST", paramMap));
		model.addAttribute("FILESCNT", commonService.selectList("BBSController.SELECT_BBSFILE_LIST", paramMap).size());

		if((Map)model.get("ARTICLE") != null) {
			Map map1 = (Map)model.get("ARTICLE") ;
			model.remove("ARTICLE");
			
			map1.put("CONTENT", ((String)map1.get("CONTENT")).replaceAll("\r\n", ""));
			model.addAttribute("ARTICLE", map1);
			
			return new ModelAndView("/bbs/modify01");
		} else {
			throw new CoreException(Message.propFormat("bbs.auth01"), request, response);
		}
	}
	
	@RequestMapping("/modifyProc")
	public ModelAndView modifyProc(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);

		request.setAttribute("BBSCONF", map);
		paramMap.put("CONFIRM_YN", map.get("CONFIRM_YN") == null || map.get("CONFIRM_YN").equals("") ? "N":map.get("CONFIRM_YN"));
		paramMap.put("DATETERM_YN", map.get("DATETERM_YN") == null || map.get("DATETERM_YN").equals("") ? "N":map.get("DATETERM_YN"));
		request.setAttribute("BBSCOND", paramMap);

		if("01".equals(map.get("BBS_TYPE"))) {
			return modify01(model, request, response);
		} else {
			return modify01(model, request, response);
		}
	}
	
	private ModelAndView modify01(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		
		if(paramMap.get("NOTICE_YN") == null) {
			paramMap.put("NOTICE_YN", "N");
		}
		
		Map<String, Object> multipartFileMap			= (Map<String, Object>)paramMap.get(ProjectConstant.KEY_FORM_FILE + ProjectConstant.KEY_INTERCEPTOR_VALUES_POSTFIX);

		if(multipartFileMap != null) {
			
			//int fileCnt = 
			
			for(int idx = 1; idx <= multipartFileMap.size(); idx++) {
				MultipartFileWrapper multipartFileWrapper = (MultipartFileWrapper)multipartFileMap.get(ProjectConstant.KEY_FORM_FILE + idx);

				HashMap<String, Object> hm = new HashMap<String, Object>();
				hm.put("BBSCONF_NO", paramMap.get("BBSCONF_NO"));
				hm.put("K_ARTICLE_NO", paramMap.get("ARTICLE_NO"));
				hm.put("FILE_DESC", paramMap.get(multipartFileWrapper.getName() + "_DESC"));
				hm.put("FILE_PATH", multipartFileWrapper.getWebPath());
				hm.put("FILE_NM", multipartFileWrapper.getFileName());
				hm.put("FILE_SIZE", multipartFileWrapper.getSize());
				hm.put("FILE_EXT", multipartFileWrapper.getContentType());
				hm.put("ORIGINAL_FILE_NM", multipartFileWrapper.getOriginalFileName());
				
				commonService.setInsert("BBSController.INSERT_TM_BBSFILE_M", hm);
			}
			
			
		}
		
		commonService.setUpdate("BBSController.UPDATE_TM_BBSARTICLE_M", paramMap);

		return new ModelAndView("redirect:/bbs/view?bbsconf_no=" + paramMap.get("BBSCONF_NO") + "&article_no=" + paramMap.get("ARTICLE_NO"));
	}
	
	@RequestMapping("/fileDelete")
	public ModelAndView fileDelete(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		
		commonService.setDelete("BBSController.DELETE_TM_BBSFILE_M", paramMap);
		
		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBS", paramMap);

		request.setAttribute("BBSCONF", map);
		paramMap.put("CONFIRM_YN", map.get("CONFIRM_YN"));
		paramMap.put("DATETERM_YN", map.get("DATETERM_YN"));
		request.setAttribute("BBSCOND", paramMap);
		
		model.addAttribute("ARTICLE", commonService.select("BBSController.SELECT_BBSARTICLE", paramMap));
		model.addAttribute("FILES", commonService.selectList("BBSController.SELECT_BBSFILE_LIST", paramMap));
		model.addAttribute("FILESCNT", commonService.selectList("BBSController.SELECT_BBSFILE_LIST", paramMap).size());

		if((Map)model.get("ARTICLE") != null) {
			Map map1 = (Map)model.get("ARTICLE") ;
			model.remove("ARTICLE");
			
			map1.put("CONTENT", ((String)map1.get("CONTENT")).replaceAll("\r\n", ""));
			model.addAttribute("ARTICLE", map1);
			
			return new ModelAndView("/bbs/modify01");
		} else {
			throw new CoreException(Message.propFormat("bbs.auth01"), request, response);
		}
	}
	
}
