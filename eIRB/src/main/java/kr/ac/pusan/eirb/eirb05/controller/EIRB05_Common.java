/**
 *
 */
package kr.ac.pusan.eirb.eirb05.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.vo.MultipartFileWrapper;

@Controller
@Scope(value="request")
@RequestMapping("/bbs/common/{bbsKeyno}/*")
@SuppressWarnings({"unchecked"})
public class EIRB05_Common extends BaseController {
	@RequestMapping(value = "/{typeGcd}/articleList", produces = "application/json;charset=UTF-8")
	public @ResponseBody Object getArticleList(@PathVariable String typeGcd, @RequestBody Map<String, Object> jsonMap
			, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		
		String mapperId = "EIRB05_Common.SELECT_BBS_ARTICLE_LIST_"+typeGcd;
		
		if (logger.isInfoEnabled()) {
			logger.info("(EIRB05_Common getArticleList) - typeGcd : " + typeGcd 
					+ " / siteKeyno : " + jsonMap.get("SITE_KEYNO")
					+ " / bbsKeyno : " + jsonMap.get("BBS_KEYNO"));
		}
		
		List<HashMap<String, Object>> list = commonService.selectList(mapperId, jsonMap);
		model.addAttribute("list", list);
		return model;
	}
	
	@RequestMapping(value = "/save")
	public String setArticle(@PathVariable String bbsKeyno, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> multipartFileMap			= (Map<String, Object>)paramMap.get(ProjectConstant.KEY_FORM_FILE + ProjectConstant.KEY_INTERCEPTOR_VALUES_POSTFIX);
		List<Map<String, Map<String, Object>>> fileList = new ArrayList<Map<String, Map<String,Object>>>();
		
		try {
			int relArticleNo = (int) commonService.select("EIRB05_Common.INSERT_BBS_ARTICLE", paramMap);
			
			if(multipartFileMap != null) {
				for(int idx = 1; idx <= multipartFileMap.size(); idx++) {
					MultipartFileWrapper multipartFileWrapper = (MultipartFileWrapper)multipartFileMap.get(ProjectConstant.KEY_FORM_FILE + idx);
					
					HashMap<String, Object> hm = new HashMap<String, Object>();
					hm.put("SITE_KEYNO", paramMap.get("SITE_KEYNO"));
					hm.put("BBS_KEYNO", paramMap.get("BBS_KEYNO"));
					hm.put("REL_CD", "BBS");
					hm.put("REL_KEYNO", relArticleNo);
					hm.put("FILE_NM", multipartFileWrapper.getOriginalFileName());
					hm.put("FILE_NM_SERVER", multipartFileWrapper.getFileName());
					hm.put("FILE_PATH", multipartFileWrapper.getWebPath());
					hm.put("FILE_SIZE", multipartFileWrapper.getSize());
					hm.put("FILE_EXT", multipartFileWrapper.getContentType());
					hm.put("MAIN_IMAGE_FG", "N");
					hm.put("SORT_ORDER", idx);
					
					HashMap<String, Map<String, Object>> wrapperMap = new HashMap<String, Map<String, Object>>();
					wrapperMap.put("EIRB05_Common.INSERT_BBS_ATTACHFILE", hm);
					
					fileList.add(wrapperMap);
				}
			}
			commonService.setMultiBatch(fileList);
			
			model.addAttribute("message", Message.msg("success.bbs.insert"));
		} catch(Exception e) {
			model.addAttribute("message", Message.msg("fail.bbs.insert"));
		}
		return "redirect:/eirb05/1000050"+bbsKeyno+"/"+bbsKeyno+"/";
	}
	
	
	
}
