/**
 *
 */
package kr.ac.pusan.eirb.eirb03.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/eirb03/10000301/*")
@SuppressWarnings({"unchecked"})
public class EIRB03_10000301 extends BaseController {

	@RequestMapping("/")
	public String eduScheduleList(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		StringUtil su = new StringUtil();
		
		model.addAttribute("DATA", commonService.selectList("EIRB03_10000301.SELECT_EDU_SCHEDULE_LIST"));
		
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		return "eirb03/10000301_l";
	}
	
	@RequestMapping("/view")
	public String eduScheduleView(ModelMap model) {
		model.addAttribute("DATAVIEW", commonService.select("EIRB03_10000301.SELECT_EDU_SCHEDULE_VIEW",paramMap));
		
		return "eirb03/10000301_v";
	}
	
	@RequestMapping("/save")
	public ModelAndView eduScheduleSave(ModelMap model) {
		try {
			commonService.setInsert("EIRB03_10000301.INSERT_EDU_SCHEDULE", paramMap);
			model.addAttribute("message", Message.msg("success.common.insert"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.insert"));
		}
		return new ModelAndView("redirect:/eirb03/10000301/");
	}
	
	@RequestMapping("/delete")
	public ModelAndView eduScheduleDelete(ModelMap model) {
		try {
			model.addAttribute("DATAVIEW", commonService.select("EIRB03_10000301.DELETE_EDU_SCHEDULE",paramMap));
			model.addAttribute("message", Message.msg("success.common.delete"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.delete"));
		}
		return new ModelAndView("redirect:/eirb03/10000301/");
	}
}
