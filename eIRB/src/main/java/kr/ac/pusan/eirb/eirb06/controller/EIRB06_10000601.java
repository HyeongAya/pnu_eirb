package kr.ac.pusan.eirb.eirb06.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.exception.CoreException;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@SuppressWarnings("unchecked")
@RequestMapping("/eirb06/10000601/*")
public class EIRB06_10000601 extends BaseController {
	
	@RequestMapping("/")
	public String bbsConfList(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		StringUtil su = new StringUtil();
		
		model.addAttribute("USER_DATA", commonService.selectList("EIRB06_10000601.SELECT_USER_LIST", paramMap));
		
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		
		return "eirb06/10000601_l";
	}
	
	@RequestMapping("/id_info")
	public Object id_info(ModelMap model) {
		Object data = commonService.select("Common.SELECT_USER_INFO", paramMap);
		return data;
	}
	
	@RequestMapping("/joinProc")
	public ModelAndView joinProc(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		try {
			commonService.setInsert("Common.INSERT_USER", paramMap);
			paramMap.put("SITE_KEYNO", 1000);
			paramMap.put("AUTH_GCD", "AT");
			paramMap.put("AUTH_CD", paramMap.get("USER_TYPE_CD"));
			commonService.setInsert("Common.INSERT_USER_MENU_AUTH", paramMap);
			model.addAttribute("message", Message.msg("success.common.insert"));
		} catch(Exception ex) {
			//model.addAttribute("message", Message.msg("fail.common.insert"));
			throw new CoreException(Message.propFormat("fail.common.insert"), request, response, "/eirb06/10000601/");
		}
		return new ModelAndView("redirect:/eirb06/10000601/");
	}
	
	@RequestMapping("/modify")
	public ModelAndView modify(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		try {
			commonService.setUpdate("Common.UPDATE_USER_INFO", paramMap);
			
			commonService.setDelete("Common.DELETE_USER_MENU_AUTH", paramMap);
			
			paramMap.put("SITE_KEYNO", 1000);
			paramMap.put("AUTH_GCD", "AT");
			paramMap.put("AUTH_CD", paramMap.get("USER_TYPE_CD"));
			commonService.setInsert("Common.INSERT_USER_MENU_AUTH", paramMap);
			model.addAttribute("message", Message.msg("success.common.update"));
		} catch(Exception ex) {
			//model.addAttribute("message", Message.msg("fail.common.update"));
			throw new CoreException(Message.propFormat("fail.common.update"), request, response, "/eirb06/10000601/");
		}
		return new ModelAndView("redirect:/eirb06/10000601/");
	}
	
	@RequestMapping("/del")
	public ModelAndView del(ModelMap model) {
		try {
			commonService.setDelete("EIRB06_10000601.DELETE_USER", paramMap);
			commonService.setDelete("Common.DELETE_USER_MENU_AUTH", paramMap);
			model.addAttribute("message", Message.msg("success.common.delete"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.delete"));
		}	
		return new ModelAndView("redirect:/eirb06/10000601/");
	}
	
	@RequestMapping("/pwreset")
	public ModelAndView pwreset(ModelMap model) {
		try {
			commonService.setDelete("Common.UPDATE_PASSWORD", paramMap);
			model.addAttribute("message", Message.msg("success.common.update"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.update"));
		}	
		return new ModelAndView("redirect:/eirb06/10000601/");
	}
}
