package kr.ac.pusan.eirb.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.ac.pusan.core.exception.CoreException;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.service.ICommonService;

import kr.ac.pusan.core.ProjectConstant;

@Aspect
@SuppressWarnings("unchecked")
public class BBSAuthCheckInterceptor extends HandlerInterceptorAdapter {

	protected final Log logger = LogFactory.getLog("pnu_cms");

	@Autowired
	private ICommonService<Object> commonService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		boolean returnValue				 = false;
		HashMap<String, Object> paramMap	= new HashMap<String, Object>();
		HttpSession session				 = request.getSession();
		String controller				   = ((HandlerMethod)handler).getMethod().getName();
		paramMap.put("USER_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
		paramMap.put("BBSCONF_NO", request.getParameter("bbsconf_no"));
		
		paramMap.put("GROUP_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_GROUP));
		
		Map<String, Object> map = (Map<String, Object>)commonService.select("BBSController.SELECT_BBSAUTH", paramMap);
		String auth_check	   = "N";
		int adminCnt = (Integer)commonService.select("BBSController.SELECT_ADMINAUTH", paramMap);
		if(session.getAttribute(ProjectConstant.KEY_SESSION_SUPER_ADMIN) != null && (session.getAttribute(ProjectConstant.KEY_SESSION_SUPER_ADMIN).equals("Y") || adminCnt > 0)) {
			auth_check = "Y";
		} else {
			logger.info("## CHECK CONTROLLER : " + controller);
			if(map != null) {
				if("fileDownload".equals(controller)) {
					if("".equals(request.getParameter("article_no")) || request.getParameter("article_no") == null) {
						auth_check = "N";
					} else {
						paramMap.put("ARTICLE_NO", request.getParameter("article_no"));
						paramMap.put("FILE_NM", request.getParameter("nm"));

						Map<String, Object> fileAuthMap = (Map<String, Object>)commonService.select("BBSController.SELECT_FILEAUTH", paramMap);

						auth_check = (String)fileAuthMap.get("READ_AUTH");
					}
				} else {
					if("list".equals(controller) || "getList".equals(controller)) {
						auth_check = (String)map.get("LIST_AUTH");
					} else if("write".equals(controller) || "save".equals(controller)) {
						auth_check = (String)map.get("WRITE_AUTH");
					} else if("view".equals(controller)) {
						auth_check = (String)map.get("READ_AUTH");
					} else if("replyWrite".equals(controller) || "replySave".equals(controller)) {
						auth_check = (String)map.get("REPLY_AUTH");
					} else if("commentWrite".equals(controller) || "commentSave".equals(controller) || "commentMody".equals(controller) || "commentDel".equals(controller)) {
						auth_check = (String)map.get("COMMENT_AUTH");
					} else if("delete".equals(controller) || "del".equals(controller)) {
						auth_check = (String)map.get("DELETE_AUTH");
					}
				}
			}
		}

		if("N".equals(auth_check)) {
			throw new CoreException(Message.propFormat("bbs.auth01"), request, response);
		}

		try {
			returnValue = super.preHandle(request, response, handler);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return returnValue;
	}

}
