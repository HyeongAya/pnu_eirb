package kr.ac.pusan.eirb.common.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;
import kr.ac.pusan.core.vo.MultipartFileWrapper;

@Controller
@Scope(value="request")
public class CommonController extends BaseController {
	@RequestMapping("/comm/selectbox")
	public void select(HttpServletRequest request, HttpServletResponse response) {
		String v_value	  = paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_flag	   = paramMap.containsKey("FLAG") ? (String)paramMap.get("FLAG"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		String v_title	  = paramMap.containsKey("TITLE") ? " title=\"" + paramMap.get("TITLE") + "\"":"";
		String v_etc_title  = paramMap.containsKey("ETC_TITLE") ? (String)paramMap.get("ETC_TITLE"):"";
		String v_name	   = paramMap.containsKey("NAME") ? " name=\"" + paramMap.get("NAME") + "\"":"";
		String v_id		 = paramMap.containsKey("ID") ? " id=\"" + paramMap.get("ID") + "\"":"";
		String v_onchange   = paramMap.containsKey("ONCHANGE") ? " onchange=\"" + paramMap.get("ONCHANGE") + "\"":"";
		boolean v_optgroup  = paramMap.containsKey("OPTGROUP") && paramMap.get("OPTGROUP").equals("Y") ? true:false;
		StringUtil su   = new StringUtil();

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);

		stringBuilder.append("<select" + v_class + v_style + v_title + v_name + v_id + v_onchange + ">");
		if("BLANK".equals(v_flag.toUpperCase())) {
			stringBuilder.append("<option value=\"\"></option>");
		} else if("ALL".equals(v_flag.toUpperCase())) {
			stringBuilder.append("<option value=\"\">전체</option>");
		} else if("SELECT".equals(v_flag.toUpperCase())) {
			stringBuilder.append("<option value=\"\">" + su.emptyCheck(String.valueOf(paramMap.get("TITLE"))) + "선택</option>");
		} else if("ETC".equals(v_flag.toUpperCase())) {
			stringBuilder.append("<option value=\"\">" + v_etc_title + "</option>");
		}

		String chosung = "";
		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);

			if(v_optgroup) {
				if(idx == 0) {
					chosung = getChosung(String.valueOf(map.get("LABEL")));
					stringBuilder.append("<optgroup label=\"" + chosung + "\">");
				} else {
					if(!chosung.equals(getChosung(String.valueOf(map.get("LABEL"))))) {
						stringBuilder.append("</optgroup>");
						chosung = getChosung(String.valueOf(map.get("LABEL")));
						stringBuilder.append("<optgroup label=\"" + chosung + "\">");
					}
				}
			}

			if(String.valueOf(map.get("DATA")).equals(v_value)) {
				stringBuilder.append("<option value=\"" + map.get("DATA") + "\" selected=\"selected\">" + map.get("LABEL") + "</option>");
			} else {
				stringBuilder.append("<option value=\"" + map.get("DATA") + "\">" + map.get("LABEL") + "</option>");
			}

			if(v_optgroup && idx == list.size() - 1) {
				stringBuilder.append("</optgroup>");
			}
		}
		stringBuilder.append("</select>");

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/comm/checkbox")
	public void checkbox(HttpServletRequest request, HttpServletResponse response) {
		String v_value	  = paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		String v_name	   = paramMap.containsKey("NAME") ? " name=\"" + paramMap.get("NAME") + "\"":"";
		String v_id		 = paramMap.containsKey("ID") ? String.valueOf(paramMap.get("ID")):"";
		String v_onclick	= paramMap.containsKey("ONCLICK") ? " onclick=\"" + paramMap.get("ONCLICK") + "\"":"";
		StringUtil su   = new StringUtil();

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);
		String[] a_value				= v_value.split(",");

		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);
			String check_str = "";

			for(int idx1 = 0; idx1 < a_value.length; idx1++) {
				if(String.valueOf(map.get("DATA")).equals(a_value[idx1])) {
					check_str = " checked=\"checked\"";
				}
			}

			stringBuilder.append("<input type=\"checkbox\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + check_str + " /><label for=\"" + v_id + (idx + 1) + "\">" + String.valueOf(map.get("LABEL")) + "</label>");
		}

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/comm/checkbox2")
	public void checkbox2(HttpServletRequest request, HttpServletResponse response) {
		StringUtil su   = new StringUtil();
		
		String v_value	  = su.emptyCheck((String)paramMap.get("VALUE"));
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		String v_name	   = paramMap.containsKey("NAME") ? " name=\"" + paramMap.get("NAME") + "\"":"";
		String v_id		 = paramMap.containsKey("ID") ? String.valueOf(paramMap.get("ID")):"";
		String v_onclick	= paramMap.containsKey("ONCLICK") ? " onclick=\"" + paramMap.get("ONCLICK") + "\"":"";
		
		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);
		String[] a_value				= v_value.split(",");


		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);
			String check_str = "";

			for(int idx1 = 0; idx1 < a_value.length; idx1++) {
				if(String.valueOf(map.get("DATA")).equals(a_value[idx1])) {
					check_str = " checked=\"checked\"";
				}
			}

			stringBuilder.append("<li><input type=\"checkbox\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + check_str + " /><label for=\"" + v_id + (idx + 1) + "\">" + String.valueOf(map.get("LABEL")) + "</label></li>");
		}

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/comm/checkbox_deli")
	public void checkbox_deli(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		String v_value		= paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_class		= paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style		= paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		String v_name		= paramMap.containsKey("NAME") ? " name=\"" + paramMap.get("NAME") + "\"":"";
		String v_id			= paramMap.containsKey("ID") ? String.valueOf(paramMap.get("ID")):"";
		String v_onclick	= paramMap.containsKey("ONCLICK") ? " onclick=\"" + paramMap.get("ONCLICK") + "\"":"";
		String v_label		= paramMap.containsKey("LABEL") ? " class=\"" + paramMap.get("LABEL") + "\"":"";
		StringUtil su   = new StringUtil();
		String[] a_value = null;

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}
		
		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);
		if(v_value != null){
			a_value			= v_value.split(",");
		}
		
		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);
			String check_str = "";
			
			if(v_value != null){
				for(int idx1 = 0; idx1 < a_value.length; idx1++) {
					if(String.valueOf(map.get("DATA")).equals(a_value[idx1])) {
						check_str = " checked=\"checked\"";
					}
				}
			}
			
			stringBuilder.append("<div"+ v_label +"><label for=\"" + v_id + (idx + 1) + "\"><input type=\"checkbox\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + check_str + " />" + String.valueOf(map.get("LABEL")) + "</label></div>");
		}

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/comm/checkbox_deli_attr")
	public void checkbox_deli_attr(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		String v_value		= paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_class		= paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style		= paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		String v_name		= paramMap.containsKey("NAME") ? " name=\"" + paramMap.get("NAME") + "\"":"";
		String v_id			= paramMap.containsKey("ID") ? String.valueOf(paramMap.get("ID")):"";
		String v_onclick	= paramMap.containsKey("ONCLICK") ? " onclick=\"" + paramMap.get("ONCLICK") + "\"":"";
		String v_label		= paramMap.containsKey("LABEL") ? " class=\"" + paramMap.get("LABEL") + "\"":"";
		String v_attr		= paramMap.containsKey("ATTR") ? (String)paramMap.get("ATTR"):"";
		String v_map_attr	= paramMap.containsKey("MAP_ATTR") ? (String)paramMap.get("MAP_ATTR"):"";
		StringUtil su   = new StringUtil();
		String[] a_value = null;

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}
		
		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);
		
		if(v_value != null){
			a_value			= v_value.split(",");
		}
		
		List<Map<String, Object>> list_attr  = commonService.selectList(v_map_attr, jsonMap);
		
		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);
			String check_str = "";
			if(v_value != null){
				for(int idx1 = 0; idx1 < a_value.length; idx1++) {
					if(String.valueOf(map.get("DATA")).equals(a_value[idx1])) {
						check_str = " checked=\"checked\"";
					}
				}
			}
			
			Map<String, Object> map2 = list_attr.get(idx);
			if(String.valueOf(map2.get("ATTR1")).equals(v_attr)) {
				stringBuilder.append("<div"+ v_label +"><label for=\"" + v_id + (idx + 1) + "\"><input type=\"checkbox\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + check_str + " />" + String.valueOf(map.get("LABEL")) + "</label></div>");
			}
		}

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/comm/checkbox_li")
	public void checkbox_li(HttpServletRequest request, HttpServletResponse response) {
		String v_value		= paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		boolean checkFlag   = false;
		StringUtil su   = new StringUtil();
		String[] a_value = null;

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);
		
		if(v_value != null){
			a_value			= v_value.split(",");
		}
		
		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);

			if(v_value != null){
				for(int idx1 = 0; idx1 < a_value.length; idx1++) {
					if(String.valueOf(map.get("DATA")).equals(a_value[idx1])) {
						stringBuilder.append("<li>" + String.valueOf(map.get("LABEL")) + "</li>");
					}
				}
			}
		}
		
		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/comm/checkbox_deli_li")
	public void checkbox_deli_li(HttpServletRequest request, HttpServletResponse response) {
		String v_value		= paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_map_attr	= paramMap.containsKey("MAP_ATTR") ? (String)paramMap.get("MAP_ATTR"):"";
		String v_attr		= paramMap.containsKey("ATTR") ? (String)paramMap.get("ATTR"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		boolean checkFlag   = false;
		StringUtil su   = new StringUtil();
		String[] a_value = null;

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);
		
		if(v_value != null){
			a_value			= v_value.split(",");
		}
		
		List<Map<String, Object>> list_attr  = commonService.selectList(v_map_attr, jsonMap);
		
		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);
			Map<String, Object> map2 = list_attr.get(idx);

			if(v_value != null){
				for(int idx1 = 0; idx1 < a_value.length; idx1++) {
					if(String.valueOf(map.get("DATA")).equals(a_value[idx1])) {
						if(String.valueOf(map2.get("ATTR1")).equals(v_attr)) {
							stringBuilder.append("<li>" + String.valueOf(map.get("LABEL")) + "</li>");
						}
					}
				}
			}
		}
		
		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/comm/radiobox")
	public void radiobox(HttpServletRequest request, HttpServletResponse response) {
		String v_value	  = paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		String v_name	   = paramMap.containsKey("NAME") ? " name=\"" + paramMap.get("NAME") + "\"":"";
		String v_id		 = paramMap.containsKey("ID") ? String.valueOf(paramMap.get("ID")):"";
		String v_onclick	= paramMap.containsKey("ONCLICK") ? " onclick=\"" + paramMap.get("ONCLICK") + "\"":"";
		String v_label	= paramMap.containsKey("LABEL") ? " class=\"" + paramMap.get("LABEL") + "\"":"";
		boolean checkFlag   = false;
		StringUtil su   = new StringUtil();

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);

		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);

			if("".equals(v_value) && idx == 0) {
				checkFlag = true;
			} else {
				checkFlag = false;
			}

			if(String.valueOf(map.get("DATA")).equals(v_value) || checkFlag) {
				stringBuilder.append("<input type=\"radio\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + " checked=\"checked\" /><label for=\"" + v_id + (idx + 1) + "\"" + v_label +">" + String.valueOf(map.get("LABEL")) + "</label>");
			} else {
				stringBuilder.append("<input type=\"radio\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + " /><label for=\"" + v_id + (idx + 1) + "\"" + v_label +">" + String.valueOf(map.get("LABEL")) + "</label>");
			}
		}

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/comm/radiobox2")
	public void radiobox2(HttpServletRequest request, HttpServletResponse response) {
		String v_value	  = paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		String v_name	   = paramMap.containsKey("NAME") ? " name=\"" + paramMap.get("NAME") + "\"":"";
		String v_id		 = paramMap.containsKey("ID") ? String.valueOf(paramMap.get("ID")):"";
		String v_onclick	= paramMap.containsKey("ONCLICK") ? " onclick=\"" + paramMap.get("ONCLICK") + "\"":"";
		boolean checkFlag   = false;
		StringUtil su   = new StringUtil();

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);

		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);

			if("".equals(v_value) && idx == 0) {
				checkFlag = true;
			} else {
				checkFlag = false;
			}

			if(String.valueOf(map.get("DATA")).equals(v_value) || checkFlag) {
				stringBuilder.append("<li><input type=\"radio\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + " checked=\"checked\" /><label for=\"" + v_id + (idx + 1) + "\">" + String.valueOf(map.get("LABEL")) + "</label></li>");
			} else {
				stringBuilder.append("<li><input type=\"radio\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + " /><label for=\"" + v_id + (idx + 1) + "\">" + String.valueOf(map.get("LABEL")) + "</label></li>");
			}
		}

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/comm/radiobox_deli")
	public void radiobox_deli(HttpServletRequest request, HttpServletResponse response) {
		String v_value	  = paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		String v_name	   = paramMap.containsKey("NAME") ? " name=\"" + paramMap.get("NAME") + "\"":"";
		String v_id		 = paramMap.containsKey("ID") ? String.valueOf(paramMap.get("ID")):"";
		String v_onclick	= paramMap.containsKey("ONCLICK") ? " onclick=\"" + paramMap.get("ONCLICK") + "\"":"";
		String v_label	= paramMap.containsKey("LABEL") ? " class=\"" + paramMap.get("LABEL") + "\"":"";
		boolean checkFlag   = false;
		StringUtil su   = new StringUtil();

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);

		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);
			String check_str = "";
			
			if(String.valueOf(map.get("DATA")).equals("01")){
				check_str = " checked=\"checked\"";
			}
			
			if("".equals(v_value) && idx == 0) {
				checkFlag = true;
			} else {
				checkFlag = false;
			}
			
			if(String.valueOf(map.get("DATA")).equals(v_value) || checkFlag) {
				stringBuilder.append("<div"+ v_label +"><label for=\"" + v_id + (idx + 1) + "\"><input type=\"radio\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + " checked=\"checked\" />" + String.valueOf(map.get("LABEL")) + "</label></div>");
			} else {
				if(String.valueOf(map.get("DATA")).equals("01")){
					check_str = " checked=\"checked\"";
				}
				stringBuilder.append("<div"+ v_label +"><label for=\"" + v_id + (idx + 1) + "\"><input type=\"radio\"" + v_name + " id=\"" + v_id + (idx + 1) + "\" value=\"" + String.valueOf(map.get("DATA")) + "\"" + v_class + v_style + v_onclick + check_str + " />" + String.valueOf(map.get("LABEL")) + "</label></div>");
			}
		}

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/comm/deli_data")
	public void deli_data(HttpServletRequest request, HttpServletResponse response) {
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		boolean checkFlag   = false;
		StringUtil su   = new StringUtil();

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);

		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);
			
			stringBuilder.append(String.valueOf(map.get("LABEL")));
		}

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/comm/getData")
	public void getData(ModelMap model) {
		StringUtil su   = new StringUtil();
		String v_map	= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";

		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");

			for(int idx = 0; idx < paramList.size(); idx++) {
				Map<String, Object> jsonMap = paramList.get(0);
				model.put("data" + idx, commonService.selectList(v_map, jsonMap));
			}
		} else {
			model.put("data0", commonService.selectList(v_map));
		}
	}

	@ResponseBody
	@RequestMapping("/comm/getDataSession")
	public ModelMap getDataSession(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		StringUtil su	   = new StringUtil();
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		HttpSession session = request.getSession();

		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");

			for(int idx = 0; idx < paramList.size(); idx++) {
				Map<String, Object> jsonMap = paramList.get(0);
				jsonMap.put("ADMIN_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
				model.put("data" + idx, commonService.selectList(v_map, jsonMap));
			}
		} else {
			Map<String, Object> jsonMap = new HashMap<String, Object>();
			jsonMap.put("ADMIN_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
			model.put("data0", commonService.selectList(v_map, jsonMap));
		}

		return model;
	}

	@RequestMapping("/comm/update_bookmark")
	public void update_bookmark(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session			= request.getSession();
		StringUtil su				  = new StringUtil();
		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();

		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");

			for(int idx = 0; idx < paramList.size(); idx++) {
				Map<String, Object> jsonMap = paramList.get(idx);
				jsonMap.put("ADMIN_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));

				list.add(jsonMap);
			}
		}

		commonService.setBatchUpdate("Common.UPDATE_BOOKMARK", list);
	}

	@ResponseBody
	@RequestMapping("/comm/insert_bookmark")
	public ModelMap insert_bookmark(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session	 = request.getSession();
		paramMap.put("ADMIN_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));

		Map<String, String> map = (Map<String, String>)commonService.select("Common.SELECT_BOOKMARK_DUPCHECK", paramMap);

		if("0".equals(String.valueOf(map.get("CNT")))) {
			commonService.setInsert("Common.INSERT_BOOKMARK", paramMap);
			model.put("data0", "success");
		} else {
			model.put("data0", "fail");
		}

		return model;
	}

	@ResponseBody
	@RequestMapping("/comm/delete_bookmark")
	public ModelMap delete_bookmark(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session	 = request.getSession();
		paramMap.put("ADMIN_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));

		commonService.setDelete("Common.DELETE_BOOKMARK", paramMap);

		return model;
	}

	@ResponseBody
	@RequestMapping("/comm/editorUpload")
	public String editorUpload(ModelMap model) {
		String message = Message.msg("fail.common.upload");
		String fileUrl = "";

		if(paramMap.get(ProjectConstant.KEY_FORM_EDITORFILE) != null){
			MultipartFileWrapper fileInfo = (MultipartFileWrapper) paramMap.get(ProjectConstant.KEY_FORM_EDITORFILE);

			message = Message.msg("success.common.upload");
			fileUrl = fileInfo.getWebPath() + fileInfo.getFileName();
		}

		return "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(" + String.valueOf(paramMap.get("CKEDITORFUNCNUM")) + ", '"+ fileUrl + "', '" + message + "');</script>";
	}

	@ResponseBody
	@RequestMapping("/common/snsShare")
	public ModelAndView snsFaceBook(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		StringUtil su   = new StringUtil();
		String url = "null";
		String skin = "3";
		String title = "";
		String description = "";
		String facebookImgUrl = "";
		String width = "100%";
		String align = "right";

		if(!su.isEmpty( (String) paramMap.get("URL"))) url = (String) paramMap.get("URL");
		if(!su.isEmpty( (String) paramMap.get("SKIN"))) skin = (String) paramMap.get("SKIN");
		if(!su.isEmpty( (String) paramMap.get("TITLE"))) title = (String) paramMap.get("TITLE");
		if(!su.isEmpty( (String) paramMap.get("DESCRIPTION"))) description = (String) paramMap.get("DESCRIPTION");
		if(!su.isEmpty( (String) paramMap.get("FACEBOOK_IMG_URL"))) facebookImgUrl = (String) paramMap.get("FACEBOOK_IMG_URL");
		if(!su.isEmpty( (String) paramMap.get("WIDTH"))) width = (String) paramMap.get("WIDTH");
		if(!su.isEmpty( (String) paramMap.get("ALIGN"))) align = (String) paramMap.get("ALIGN");

		request.setAttribute("URL", url);
		request.setAttribute("SKIN", skin);
		request.setAttribute("TITLE", title);
		request.setAttribute("DESCRIPTION", description);
		request.setAttribute("FACEBOOK_IMG_URL", facebookImgUrl);
		request.setAttribute("WIDTH", width);
		request.setAttribute("ALIGN", align);

		return new ModelAndView("/common/sns_share");
	}

	public String getChosung(String name) {
		char charB	  = name.charAt(0);
		String chosung  = null;
		int first	   = (charB - 44032 ) / ( 21 * 28 );

		switch(first) {
		case 0:
		case 1:
			chosung = "ㄱ";
			break;
		case 2:
			chosung = "ㄴ";
			break;
		case 3:
		case 4:
			chosung = "ㄷ";
			break;
		case 5:
			chosung = "ㄹ";
			break;
		case 6:
			chosung = "ㅁ";
			break;
		case 7:
		case 8:
			chosung = "ㅂ";
			break;
		case 9:
		case 10:
			chosung = "ㅅ";
			break;
		case 11:
			chosung = "ㅇ";
			break;
		case 12:
		case 13:
			chosung = "ㅈ";
			break;
		case 14:
			chosung = "ㅊ";
			break;
		case 15:
			chosung = "ㅋ";
			break;
		case 16:
			chosung = "ㅌ";
			break;
		case 17:
			chosung = "ㅍ";
			break;
		case 18:
			chosung = "ㅎ";
			break;
		default:
			chosung = String.valueOf(charB);
		}

		return chosung;
	}

	private String getSiteNo(HttpServletRequest request) {
		String URL = request.getServerName().toString();
		URL = URL.split(":")[0];

		paramMap.put("DOMAIN", URL);
		List<Map<String, Object>> authList  = commonService.selectList("Common.SELECT_LOCATION", paramMap);

		String site_no = "";

		if(authList == null || authList.size() <= 0) {
			site_no = "";
		} else {
			site_no = ((Map)authList.get(0)).get("SITE_NO").toString();
		}

		return site_no;
	}

}
