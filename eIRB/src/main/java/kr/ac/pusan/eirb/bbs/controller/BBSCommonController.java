package kr.ac.pusan.eirb.bbs.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/bbs_comm/*")
public class BBSCommonController extends BaseController {

	@RequestMapping("/selectbox")
	public void select(HttpServletRequest request, HttpServletResponse response) {
		String v_value	  = paramMap.containsKey("VALUE") ? (String)paramMap.get("VALUE"):"";
		String v_map		= paramMap.containsKey("MAP") ? (String)paramMap.get("MAP"):"";
		String v_flag	   = paramMap.containsKey("FLAG") ? (String)paramMap.get("FLAG"):"";
		String v_class	  = paramMap.containsKey("CLASS") ? " class=\"" + paramMap.get("CLASS") + "\"":"";
		String v_style	  = paramMap.containsKey("STYLE") ? " style=\"" + paramMap.get("STYLE") + "\"":"";
		String v_title	  = paramMap.containsKey("TITLE") ? " title=\"" + paramMap.get("TITLE") + "\"":"";
		String v_etc_title  = paramMap.containsKey("ETC_TITLE") ? (String)paramMap.get("ETC_TITLE"):"";
		String v_name	   = paramMap.containsKey("NAME") ? " name=\"" + paramMap.get("NAME") + "\"":"";
		String v_id		 = paramMap.containsKey("ID") ? " id=\"" + paramMap.get("ID") + "\"":"";
		String v_onchange   = paramMap.containsKey("ONCHANGE") ? " onchange=\"" + paramMap.get("ONCHANGE") + "\"":"";
		boolean v_optgroup  = paramMap.containsKey("OPTGROUP") && paramMap.get("OPTGROUP").equals("Y") ? true:false;
		StringUtil su   = new StringUtil();

		Map<String, Object> jsonMap = null;
		if(!su.isEmpty(String.valueOf(paramMap.get("PARAM")))) {
			List<Map<String, Object>> paramList = getListJson("PARAM");
			jsonMap = paramList.get(0);
		}

		StringBuilder stringBuilder	 = new StringBuilder();
		List<Map<String, Object>> list  = commonService.selectList(v_map, jsonMap);

		stringBuilder.append("<select" + v_class + v_style + v_title + v_name + v_id + v_onchange + ">");
		if("BLANK".equals(v_flag.toUpperCase())) {
			stringBuilder.append("<option value=\"\"></option>");
		} else if("ALL".equals(v_flag.toUpperCase())) {
			stringBuilder.append("<option value=\"\">전체</option>");
		} else if("SELECT".equals(v_flag.toUpperCase())) {
			stringBuilder.append("<option value=\"\">" + su.emptyCheck(String.valueOf(paramMap.get("TITLE"))) + "선택</option>");
		} else if("ETC".equals(v_flag.toUpperCase())) {
			stringBuilder.append("<option value=\"\">" + v_etc_title + "</option>");
		}

		String chosung = "";
		for(int idx = 0; idx < list.size(); idx++) {
			Map<String, Object> map = list.get(idx);

			if(v_optgroup) {
				if(idx == 0) {
					chosung = getChosung(String.valueOf(map.get("LABEL")));
					stringBuilder.append("<optgroup label=\"" + chosung + "\">");
				} else {
					if(!chosung.equals(getChosung(String.valueOf(map.get("LABEL"))))) {
						stringBuilder.append("</optgroup>");
						chosung = getChosung(String.valueOf(map.get("LABEL")));
						stringBuilder.append("<optgroup label=\"" + chosung + "\">");
					}
				}
			}

			if(String.valueOf(map.get("DATA")).equals(v_value)) {
				stringBuilder.append("<option value=\"" + map.get("DATA") + "\" selected=\"selected\">" + map.get("LABEL") + "</option>");
			} else {
				stringBuilder.append("<option value=\"" + map.get("DATA") + "\">" + map.get("LABEL") + "</option>");
			}

			if(v_optgroup && idx == list.size() - 1) {
				stringBuilder.append("</optgroup>");
			}
		}
		stringBuilder.append("</select>");

		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(stringBuilder.toString());
			response.getWriter().flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public String getChosung(String name) {
		char charB	  = name.charAt(0);
		String chosung  = null;
		int first	   = (charB - 44032 ) / ( 21 * 28 );

		switch(first) {
		case 0:
		case 1:
			chosung = "ㄱ";
			break;
		case 2:
			chosung = "ㄴ";
			break;
		case 3:
		case 4:
			chosung = "ㄷ";
			break;
		case 5:
			chosung = "ㄹ";
			break;
		case 6:
			chosung = "ㅁ";
			break;
		case 7:
		case 8:
			chosung = "ㅂ";
			break;
		case 9:
		case 10:
			chosung = "ㅅ";
			break;
		case 11:
			chosung = "ㅇ";
			break;
		case 12:
		case 13:
			chosung = "ㅈ";
			break;
		case 14:
			chosung = "ㅊ";
			break;
		case 15:
			chosung = "ㅋ";
			break;
		case 16:
			chosung = "ㅌ";
			break;
		case 17:
			chosung = "ㅍ";
			break;
		case 18:
			chosung = "ㅎ";
			break;
		default:
			chosung = String.valueOf(charB);
		}

		return chosung;
	}

}
