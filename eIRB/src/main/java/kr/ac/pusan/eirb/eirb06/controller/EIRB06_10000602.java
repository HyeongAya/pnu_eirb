package kr.ac.pusan.eirb.eirb06.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@SuppressWarnings("unchecked")
@RequestMapping("/eirb06/10000602/*")
public class EIRB06_10000602 extends BaseController {
	
	@RequestMapping("/")
	public String bbsConfList(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		StringUtil su = new StringUtil();
		
		paramMap.put("SITE_KEYNO", "1000");
		
		model.addAttribute("DATA", commonService.selectList("EIRB06_10000602.SELECT_BBSCONF_LIST", paramMap));
		
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		
		return "eirb06/10000602_l";
	}
	
	@RequestMapping("/view")
	public String bbsConfView(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		model.addAttribute("DATAVIEW", commonService.select("EIRB06_10000602.SELECT_BBSCONF_VIEW",paramMap));
		
		return "eirb06/10000602_v";
	}
	
	@RequestMapping("/save")
	public ModelAndView bbsConfSave(ModelMap model) {
		try {
			paramMap.put("SITE_KEYNO", "1000");
			
			commonService.setInsert("EIRB06_10000602.INSERT_BBSCONF", paramMap);
			model.addAttribute("message", Message.msg("success.common.insert"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.insert"));
		}
		return new ModelAndView("redirect:/eirb06/10000602/");
	}
	
	@RequestMapping("/delete")
	public ModelAndView bbsConfDelete(ModelMap model) {
		try {
			commonService.setDelete("EIRB06_10000602.DELETE_BBSCONF", paramMap);
			model.addAttribute("message", Message.msg("success.common.delete"));
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.delete"));
		}	
		return new ModelAndView("redirect:/eirb06/10000602/");
	}
}
