package kr.ac.pusan.eirb.interceptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.exception.CoreException;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.service.ICommonService;
import kr.ac.pusan.core.util.StringUtil;

@Aspect
@SuppressWarnings({ "rawtypes", "unchecked" })
public class AuthCheckInterceptor extends HandlerInterceptorAdapter {

	protected final Log logger = LogFactory.getLog("pnu_eirb");

	@Autowired
	private ICommonService commonService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		String auth_check   = "Y";
		boolean returnValue = false;
		
		response.setHeader("X-UA-Compatible", "IE=Edge");
		
		HttpSession session = request.getSession(false);
		String userId = (String) session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID);
		
		if(handler instanceof HandlerMethod) {
			Object obj			  = ((HandlerMethod)handler).getBean();
			String packageName	  = obj.getClass().getPackage().getName();
			StringUtil stringUtil   = new StringUtil();
			
			if(hasPackageAdmin(packageName)) {
				int ret = 0;
				
				if(stringUtil.isEmpty(String.valueOf(userId))) {
					auth_check = "N";
				} else {
					HashMap map = new HashMap();
					
					map.put("USER_ID", userId);
					map.put("MENU_KEYNO", obj.getClass().getSimpleName().split("_")[1]);
					ret = (Integer)commonService.select("Common.SELECT_MENU_AUTH", map);
					
					if(ret < 1) {
						auth_check = "D";
					}
				}
			}
		}

		if("N".equals(auth_check)) {
			throw new CoreException(Message.propFormat("pnu.auth02"), request, response, "/");
		} else if("D".equals(auth_check)) {
			throw new CoreException(Message.propFormat("pnu.auth01"), request, response, "back");
		}

		try {
			returnValue = super.preHandle(request, response, handler);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return returnValue;
	}

	private boolean hasPackageAdmin(String pacName) {
		List<String> adminPackList = new ArrayList<String>();

		adminPackList.add(".eirb");

		for(int idx = 0; idx < adminPackList.size(); idx++) {
			String item = adminPackList.get(idx);
			if(pacName != null && pacName.indexOf(item) > -1) {
				return true;
			}
		}

		return false;
	}

}
