package kr.ac.pusan.eirb.eirb02.contoller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.exception.CoreException;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;
import kr.ac.pusan.core.vo.MultipartFileWrapper;

@Controller
@Scope(value="request")
@RequestMapping("/eirb02/10000201/*")
@SuppressWarnings("unchecked")
public class EIRB02_10000201 extends BaseController {

	@RequestMapping("/")
	public ModelAndView main(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		
		StringUtil su = new StringUtil();
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		
		List<Map<String, Object>> researcherList  = commonService.selectList("EIRB02_10000201.SELECT_RESEARCHER", paramMap);
		paramMap.put("MAIN_CD", "FILE");
		paramMap.put("SUB_CD", "AD");
		paramMap.put("ATTR1", "RQ");
		List<Map<String, Object>> rqFileList = commonService.selectList("EIRB02_10000201.SELECT_ATTACHFILE_LIST", paramMap);
		
		paramMap.put("ATTR1", "NRQ");
		List<Map<String, Object>> nRqFileList = commonService.selectList("EIRB02_10000201.SELECT_ATTACHFILE_LIST", paramMap);
		
		model.put("RES_LIST", researcherList);
		model.put("RQ_FILE_LIST", rqFileList);
		model.put("NRQ_FILE_LIST", nRqFileList);
		
		return new ModelAndView("eirb02/10000201");
	}
	
	@RequestMapping("/save")
	public ModelAndView save(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		String SAVE_FLAG = (String)paramMap.get("SAVE_FLAG");
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ("yyyyMMddHHmmss");
		Calendar mCalendar = Calendar.getInstance();
		String mToday = mSimpleDateFormat.format(mCalendar.getTime());
 
		try {
			String APPL_OBJ = (String)paramMap.get("DELI_APPL_OBJ_CD");
			String APPL_KEYNO = "";
			
			if(SAVE_FLAG.equals("add")) {
				if(APPL_OBJ.equals("01")){
					APPL_OBJ = "HR";
				}else{
					APPL_OBJ = "BR";
				}
				APPL_KEYNO = mToday + "_" + APPL_OBJ;
			}else{
				APPL_KEYNO = mToday;
			}

			paramMap.put("DELI_APPL_KEYNO", APPL_KEYNO);
			
			Map<String, Object> multipartFileMap = (Map<String, Object>)paramMap.get(ProjectConstant.KEY_FORM_FILE + ProjectConstant.KEY_INTERCEPTOR_VALUES_POSTFIX);
			List<Map<String, Map<String, Object>>> fileList = new ArrayList<Map<String, Map<String,Object>>>();

			String rel_cd = (String)paramMap.get("REL_CD");
			String[] relcd = rel_cd.split(",");
			
			if(multipartFileMap != null) {
				for(int idx = 1; idx <= multipartFileMap.size(); idx++) {
					MultipartFileWrapper multipartFileWrapper = (MultipartFileWrapper)multipartFileMap.get(ProjectConstant.KEY_FORM_FILE + idx);

					HashMap<String, Object> hm = new HashMap<String, Object>();
					
					hm.put("REL_CD", relcd[idx-1]);
					hm.put("REL_KEYNO", APPL_KEYNO);
					hm.put("FILE_NM", multipartFileWrapper.getOriginalFileName());
					hm.put("FILE_NM_SERVER", multipartFileWrapper.getFileName());
					hm.put("FILE_PATH", multipartFileWrapper.getWebPath());
					hm.put("FILE_SIZE", multipartFileWrapper.getSize());
					hm.put("FILE_EXT", multipartFileWrapper.getContentType());
					hm.put("MAIN_IMAGE_FG", "N");
					hm.put("SORT_ORDER", idx);
					
					hm.put("USER_ID", "ADMIN");
					hm.put("INS_IP", "127.0.0.1");

					HashMap<String, Map<String, Object>> wrapperMap = new HashMap<String, Map<String, Object>>();
					wrapperMap.put("EIRB02_10000201.INSERT_ATTACHFILE", hm);

					fileList.add(wrapperMap);
				}
				
			}
			commonService.setMultiBatch(fileList);
			
			commonService.setInsert("EIRB02_10000201.INSERT_DELI_APPLY", paramMap);
			
			String resc_cr = (String)paramMap.get("RESC_CR");
			
			if( resc_cr != null ){
				String[] result_cr = resc_cr.split(",");
				for(int i=0; i<result_cr.length; i++){
					
					paramMap.put("RESC_ID", result_cr[i]);
					paramMap.put("RESC_TYPE_CD","CR");
					commonService.setInsert("EIRB02_10000201.INSERT_RESEACHER", paramMap);
				}
			}
			
			String resc_rr = (String)paramMap.get("RESC_RR");
			
			if( resc_rr != null ){
				String[] result_rr = resc_rr.split(",");
				for(int i=0; i<result_rr.length; i++){
					
					paramMap.put("RESC_ID", result_rr[i]);
					paramMap.put("RESC_TYPE_CD","RR");
					commonService.setInsert("EIRB02_10000201.INSERT_RESEACHER", paramMap);
				}
			}
			String deli_kind_fg = (String)paramMap.get("DELI_KIND_FG");
			if( "B".equals(deli_kind_fg)) {
				paramMap.put("DELI_ACCS_STATUS", "05");
				paramMap.put("DELI_ACCS_NOTE", "02");
			}
			commonService.setInsert("EIRB01_10000103.INSERT_DELI_ACCESS", paramMap);
			
			model.addAttribute("message", Message.msg("success.common.insert"));

		} catch(Exception ex) {
			throw new CoreException(Message.propFormat("fail.common.insert"), request, response, "/eirb02/10000201/");
			//model.addAttribute("message", Message.msg("fail.common.insert"));
		}
		return new ModelAndView("redirect:/eirb02/10000202/");
	}

}
