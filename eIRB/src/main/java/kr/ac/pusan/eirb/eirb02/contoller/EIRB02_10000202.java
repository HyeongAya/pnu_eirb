package kr.ac.pusan.eirb.eirb02.contoller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.core.ProjectConstant;
import kr.ac.pusan.core.controller.BaseController;
import kr.ac.pusan.core.message.Message;
import kr.ac.pusan.core.util.StringUtil;

@Controller
@Scope(value="request")
@RequestMapping("/eirb02/10000202/*")
@SuppressWarnings("unchecked")
public class EIRB02_10000202 extends BaseController {
	
	@Value("#{prop['project.root']}")		String	P_ROOT_PATH;
	@Value("#{prop['fileUpload.root']}")	String	FILE_ROOT_PATH;

	@RequestMapping("/")
	public ModelAndView main(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		//paramMap.put("ADMIN_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
		paramMap.put("USER_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
		
		StringUtil su = new StringUtil();
		if(!su.isEmpty(String.valueOf(paramMap.get("MESSAGE")))) {
			model.addAttribute("message", paramMap.get("MESSAGE"));
		}
		
		List<Map<String, Object>> applyList  = commonService.selectList("EIRB02_10000202.SELECT_APPLY_LIST", paramMap);
		model.put("APPLY_LIST", applyList);
		
		return new ModelAndView("eirb02/10000202_l");
	}
	
	@RequestMapping("/view")
	public ModelAndView view(ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		
		paramMap.put("USER_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
		Object data = commonService.select("EIRB02_10000202.SELECT_APPLY_INFO", paramMap);
		model.addAttribute("APPL_DATA", data);
		
		List<Map<String, Object>> resList  = commonService.selectList("EIRB02_10000202.SELECT_RESEARCHER", paramMap);
		model.put("RES_LIST", resList);
		
		List<Map<String, Object>> fileList  = commonService.selectList("EIRB02_10000202.SELECT_FILE_LIST", paramMap);
		model.put("FILE_LIST", fileList);

		return new ModelAndView("eirb02/10000202_v");
	}
	
	@RequestMapping(value="/res_info", method=RequestMethod.POST)
	@ResponseBody
	public Object res_info(HttpServletRequest request)throws Exception {
		
		String USER_ID = request.getParameter("USER_ID");
		paramMap.put("USER_ID", USER_ID);
		
		Object data = commonService.select("EIRB02_10000202.SELECT_RESEARCHER_INFO", paramMap);
		
		return data;
	}
	
	@RequestMapping("/modify")
	public ModelAndView modify(ModelMap model, HttpServletRequest request) {
		
		paramMap.put("MAIN_CD", "FILE");
		paramMap.put("SUB_CD", "AD");
		paramMap.put("ATTR1", "RQ");
		
		List<Map<String, Object>> rqFileList = commonService.selectList("EIRB02_10000201.SELECT_ATTACHFILE_LIST", paramMap);
		model.put("RQ_FILE_LIST", rqFileList);
		
		paramMap.put("ATTR1", "NRQ");
		
		List<Map<String, Object>> nRqFileList = commonService.selectList("EIRB02_10000201.SELECT_ATTACHFILE_LIST", paramMap);
		model.put("NRQ_FILE_LIST", nRqFileList);
		
		HttpSession session = request.getSession();
		paramMap.put("USER_ID", session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_ID));
		Object data = commonService.select("EIRB02_10000202.SELECT_APPLY_INFO", paramMap);
		model.addAttribute("APPL_DATA", data);

		List<Map<String, Object>> resList  = commonService.selectList("EIRB02_10000201.SELECT_RESEARCHER", paramMap);
		model.put("RES_LIST", resList);

		return new ModelAndView("eirb02/10000202_m");
	}
	
	@RequestMapping("/save")
	public ModelAndView save(ModelMap model) {
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ("yyyyMMddHHmmss");
		Calendar mCalendar = Calendar.getInstance();
		String mToday = mSimpleDateFormat.format(mCalendar.getTime());
		
		String SAVE_FLAG = (String)paramMap.get("SAVE_FLAG");
		String DELI_APPL_KEYNO = (String)paramMap.get("DELI_APPL_KEYNO");
		String APPL_OBJ = (String)paramMap.get("DELI_APPL_OBJ_CD");
		String APPL_KEYNO = "";
		
		try {
			if(SAVE_FLAG.equals("apply")) {
				if(APPL_OBJ.equals("01")){
					APPL_OBJ = "HR";
				}else{
					APPL_OBJ = "BR";
				}
				APPL_KEYNO = mToday + "_" + APPL_OBJ;
			}else{
				APPL_KEYNO = DELI_APPL_KEYNO;
			}
			
			paramMap.put("DELI_APPL_KEYNO_CH", APPL_KEYNO);
			commonService.setUpdate("EIRB02_10000202.UPDATE_DELI_APPLY", paramMap);
			
			paramMap.put("DELI_APPL_KEYNO", DELI_APPL_KEYNO);
			commonService.setDelete("EIRB02_10000202.DELETE_RESEARCHER", paramMap);
			
			paramMap.put("DELI_APPL_KEYNO", APPL_KEYNO);
			
			String resc_cr = (String)paramMap.get("RESC_CR");
			
			if( resc_cr != null ){
				String[] result_cr = resc_cr.split(",");
				for(int i=0; i<result_cr.length; i++){
					paramMap.put("RESC_ID", result_cr[i]);
					paramMap.put("RESC_TYPE_CD","CR");
					commonService.setInsert("EIRB02_10000201.INSERT_RESEACHER", paramMap);
				}
			}
			
			String resc_rr = (String)paramMap.get("RESC_RR");
			
			if( resc_rr != null ){
				String[] result_rr = resc_rr.split(",");
				for(int i=0; i<result_rr.length; i++){
					paramMap.put("RESC_ID", result_rr[i]);
					paramMap.put("RESC_TYPE_CD","RR");
					commonService.setInsert("EIRB02_10000201.INSERT_RESEACHER", paramMap);
				}
			}
			
			commonService.setInsert("EIRB01_10000103.INSERT_DELI_ACCESS", paramMap);
			
			model.addAttribute("message", Message.msg("success.common.insert"));
				
		} catch(Exception ex) {
			model.addAttribute("message", Message.msg("fail.common.insert"));
		}
		
		return new ModelAndView("redirect:/eirb02/10000202/");
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(ModelMap model) {
		
		commonService.setDelete("EIRB02_10000202.DELETE_DELI_APPLY",paramMap);
		commonService.setDelete("EIRB02_10000202.DELETE_RESEARCHER", paramMap);
		
		return new ModelAndView("redirect:/eirb02/10000202/");
	}
	
	@RequestMapping("/deli/fileDownload")
	public ModelAndView fileDownload() {
		String rootPath         = System.getProperty(P_ROOT_PATH);
		//String rootPath         = (String)paramMap.get("FILE_PATH");
		String fileName         = (String)paramMap.get("FILE");
		String originalfileName = (String)paramMap.get("FILE_NM");
		String fileRealPath = rootPath + fileName;

		return downloadFile(fileRealPath, originalfileName);
	}
}
