﻿<div class="page-title">
	<h3>게시판관리</h3>
	<!-- 
	<div class="page-breadcrumb">
		<ol class="breadcrumb">
			<li><a href="#">커뮤니티</a></li>
			<li class="active">게시판관리</li>
		</ol>
	</div>
	 -->
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h3 class="panel-title"><i class="fa fa-check"></i> 게시판 상세정보</h3>
				</div>
				<div class="panel-body">
				<form class="form-horizontal" name="bbsconf_view_form" id="bbsconf_view_form" method="post">
					<div class="p-v-xs"></div>
					<input type="hidden" id="SITE_KEYNO" name="SITE_KEYNO" value="${DATAVIEW.SITE_KEYNO}"/>
					<input type="hidden" id="BBS_KEYNO" name="BBS_KEYNO" value="${DATAVIEW.BBS_KEYNO}"/>
					
					<div class="form-group">
						<label for="BBS_NM" class="col-sm-2 control-label"><span class="text-danger">*</span> 게시판명</label>
						<div class="col-sm-10">
							<input type="text" id="BBS_NM" name="BBS_NM" value="${DATAVIEW.BBS_NM}" class="form-control m-b-sm">
						</div>
					</div>
					<div class="form-group">
						<label for="BBS_TYPE_CD" class="col-sm-2 control-label"><span class="text-danger">*</span> 게시판타입</label>
						<div class="col-sm-1">
							<jsp:include page="/comm/selectbox" flush="false">
								<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
								<jsp:param name="FLAG" value="SELECT" />
								<jsp:param name="NAME" value="BBS_TYPE_CD" />
								<jsp:param name="ID" value="BBS_TYPE_CD" />
								<jsp:param name="CLASS" value="form-control m-b-sm" />
								<jsp:param name="PARAM" value="[{'MAIN_CD':'BS_TP'}]" />
								<jsp:param name="VALUE" value="${DATAVIEW.BBS_TYPE_CD}" />
							</jsp:include>
						</div>
						<label for="NEW_REVEAL_DAY_CNT" class="col-sm-2 control-label">NEW 노출일수</label>
						<div class="col-sm-1">
							<input type="text" id="NEW_REVEAL_DAY_CNT" name="NEW_REVEAL_DAY_CNT" value="${DATAVIEW.NEW_REVEAL_DAY_CNT}" class="form-control">
						</div>
						<label for="ATTACH_FILE_CNT" class="col-sm-2 control-label"><span class="text-danger">*</span> 첨부파일수</label>
						<div class="col-sm-1">
							<input type="number" id="ATTACH_FILE_CNT" name="ATTACH_FILE_CNT" value="${DATAVIEW.ATTACH_FILE_CNT}" class="form-control" min="0" max="5">
						</div>
						<label for="ATTACH_FILE_LIMIT_SIZE" class="col-sm-2 control-label"><span class="text-danger">*</span> 첨부파일제한크기(MB)</label>
						<div class="col-sm-1">
							<input type="number" id="ATTACH_FILE_LIMIT_SIZE" name="ATTACH_FILE_LIMIT_SIZE" value="${DATAVIEW.ATTACH_FILE_LIMIT_SIZE}" class="form-control" min="0" max="20">
						</div>
					</div>
					<div class="form-group">
						<label for="EDITOR_USE_FG" class="col-sm-2 control-label">에디터사용여부</label>
						<div class="col-sm-1">
							<select id="EDITOR_USE_FG" name="EDITOR_USE_FG" class="form-control m-b-sm">
								<option value="Y" <c:if test="${DATAVIEW.EDITOR_USE_FG eq 'Y' }">selected="selected"</c:if>>사용</option>
								<option value="N" <c:if test="${DATAVIEW.EDITOR_USE_FG ne 'Y' }">selected="selected"</c:if>>미사용</option>
							</select>
						</div>
						<label for="CLOSE_USE_FG" class="col-sm-2 control-label">비공개사용여부</label>
						<div class="col-sm-1">
							<select id="CLOSE_USE_FG" name="CLOSE_USE_FG" class="form-control m-b-sm">
								<option value="Y" <c:if test="${DATAVIEW.CLOSE_USE_FG eq 'Y' }">selected="selected"</c:if>>사용</option>
								<option value="N" <c:if test="${DATAVIEW.CLOSE_USE_FG ne 'Y' }">selected="selected"</c:if>>미사용</option>
							</select>
						</div>
						<label for="ANSWER_USE_FG" class="col-sm-2 control-label">답변사용여부</label>
						<div class="col-sm-1">
							<select id="ANSWER_USE_FG" name="ANSWER_USE_FG" class="form-control m-b-sm">
								<option value="Y" <c:if test="${DATAVIEW.ANSWER_USE_FG eq 'Y' }">selected="selected"</c:if>>사용</option>
								<option value="N" <c:if test="${DATAVIEW.ANSWER_USE_FG ne 'Y' }">selected="selected"</c:if>>미사용</option>
							</select>
						</div>
						<label for="CONFIRM_USE_FG" class="col-sm-2 control-label">승인사용여부</label>
						<div class="col-sm-1">
							<select id="CONFIRM_USE_FG" name="CONFIRM_USE_FG" class="form-control m-b-sm">
								<option value="Y" <c:if test="${DATAVIEW.CONFIRM_USE_FG eq 'Y' }">selected="selected"</c:if>>사용</option>
								<option value="N" <c:if test="${DATAVIEW.CONFIRM_USE_FG ne 'Y' }">selected="selected"</c:if>>미사용</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="TOP_NOTICE_USE_FG" class="col-sm-2 control-label">탑공지사용여부</label>
						<div class="col-sm-1">
							<select id="TOP_NOTICE_USE_FG" name="TOP_NOTICE_USE_FG" class="form-control m-b-sm">
								<option value="Y" <c:if test="${DATAVIEW.TOP_NOTICE_USE_FG eq 'Y' }">selected="selected"</c:if>>사용</option>
								<option value="N" <c:if test="${DATAVIEW.TOP_NOTICE_USE_FG ne 'Y' }">selected="selected"</c:if>>미사용</option>
							</select>
						</div>
						<label for="TOP_NOTICE_USE_CNT" class="col-sm-2 control-label">탑공지사용개수</label>
						<div class="col-sm-1">
							<input type="number" id="TOP_NOTICE_USE_CNT" name="TOP_NOTICE_USE_CNT" value="${DATAVIEW.TOP_NOTICE_USE_CNT}" class="form-control" min="0" max="10">
						</div>
						<label for="BBS_USE_FG" class="col-sm-2 control-label"><span class="text-danger">*</span> 사용여부</label>
						<div class="col-sm-1">
							<select id="BBS_USE_FG" name="BBS_USE_FG" class="form-control m-b-sm">
								<option value="Y" <c:if test="${DATAVIEW.BBS_USE_FG eq 'Y' }">selected="selected"</c:if>>사용</option>
								<option value="N" <c:if test="${DATAVIEW.BBS_USE_FG ne 'Y' }">selected="selected"</c:if>>미사용</option>
							</select>
						</div>
					</div>
					
					<div class="col-sm-12">
						<button type="button" class="btn btn-default btn-rounded pull-right m-l-xxs" id="btn_cancel"><i class="fa fa-repeat"></i> 취소</button>
						<c:if test="${DATAVIEW.BBS_KEYNO ne null}">
						<button type="button" class="btn btn-danger btn-rounded pull-right m-l-xxs" id="btn_del"><i class="fa fa-trash"></i> 삭제</button>
						</c:if>
						<button type="button" class="btn btn-${DATAVIEW.BBS_KEYNO ne null ? 'warning':'success'} btn-rounded pull-right m-l-xxs" id="btn_save"><i class="fa fa-check"></i> ${DATAVIEW.BBS_KEYNO ne null ? '수정':'저장'}</button>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

<script type="text/javascript">
	
	$(document).ready(function() {
		//jquery validate
		var $validator = $("#bbsconf_view_form").validate({
			rules: {
				BBS_NM: "required",
				BBS_TYPE_CD: "required",
				ATTACH_FILE_CNT: "required",
				ATTACH_FILE_LIMIT_SIZE: "required"
			},
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );
				
				if ( element.prop( "type" ) === "checkbox" ) {
					error.insertAfter( element.parent( "label" ) );
				} else {
					error.insertAfter( element );
				}
			},
			highlight: function ( element, errorClass, validClass ) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
			},
			unhighlight: function (element, errorClass, validClass) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
			}
		} );
		
		/* 저장 */
		$('#btn_save').click(function() {
			var $valid = $("#bbsconf_view_form").valid();
			
			if(!$valid) {
				$validator.focusInvalid();
				return false;
			} else {
				if(confirm("저장하시겠습니까?")) {
					$('#bbsconf_view_form').attr("action","/eirb06/10000602/save");
					$('#bbsconf_view_form').submit();
				}
			}
		});
		
		/* 삭제 */
		$('#btn_del').click(function() {
			if(confirm("삭제하시겠습니까?")) {
				$('#bbsconf_view_form').attr("action","/eirb06/10000602/delete");
				$('#bbsconf_view_form').submit();
			}
		});
		
		/* 취소 */
		$('#btn_cancel').click(function() {
			location.href = "/eirb06/10000602/";
		});
		
	});
</script>