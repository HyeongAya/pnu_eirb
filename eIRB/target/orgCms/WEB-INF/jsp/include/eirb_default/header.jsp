﻿<div class="navbar-inner">
	<div class="sidebar-pusher">
		<a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
			<i class="fa fa-bars"></i>
		</a>
	</div>
	<div class="logo-box">
		<a href="/eirb00/10000001/" class="logo-text"><span>e-IRB</span></a>
	</div><!-- Logo Box -->
	<div class="topmenu-outer">
		<div class="top-menu">
			<ul class="nav navbar-nav navbar-left">
				<!-- <li>
					<a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
				</li>
				<li>
					<a href="#cd-nav" class="waves-effect waves-button waves-classic cd-nav-trigger"><i class="fa fa-diamond"></i></a>
				</li> -->
				<li>		
					<a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="badge badge-success pull-right">3</span></a>
					<ul class="dropdown-menu title-caret dropdown-lg" role="menu">
						<li><p class="drop-title">3개의 알림메세지가 있습니다.</p></li>
						<li class="dropdown-menu-list slimscroll tasks">
							<ul class="list-unstyled">
								<li>
									<a href="#">
										<div class="task-icon badge badge-success"><i class="icon-user"></i></div>
										<span class="badge badge-roundless badge-default pull-right">1분전</span>
										<p class="task-details">회원 승인대기중입니다.</p>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="task-icon badge badge-danger"><i class="icon-list"></i></div>
										<span class="badge badge-roundless badge-default pull-right">24분전</span>
										<p class="task-details">새 게시글이 있습니다.</p>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="task-icon badge badge-info"><i class="icon-docs"></i></div>
										<span class="badge badge-roundless badge-default pull-right">1시간전</span>
										<p class="task-details">신규 신청내역이 있습니다.</p>
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
						<span class="user-name">${USER_NM}<i class="fa fa-angle-down"></i></span>
					</a>
					<ul class="dropdown-menu dropdown-list" role="menu">
						<li role="presentation"><a href="/eirb00/10000001/mypage"><i class="fa fa-user"></i> 개인정보관리</a></li>
						<!-- <li role="presentation"><a href="s_calendar.html"><i class="fa fa-calendar"></i>일정</a></li> -->
					</ul>
				</li>
				<li>
					<a href="/logout" class="log-out waves-effect waves-button waves-classic">
						<span><i class="fa fa-sign-out m-r-xs"></i>로그아웃</span>
					</a>
				</li>
			</ul><!-- Nav -->
		</div><!-- Top Menu -->
	</div>
</div>