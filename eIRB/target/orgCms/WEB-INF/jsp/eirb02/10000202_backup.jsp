﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
	.label_pR { padding-right:35px; }
	.label_pT { padding-top:20px; }
	.field_pT > div { padding-top:10px; }
	/*.form-group { border-bottom: 1px solid; padding-bottom:15px; }*/
</style>
<script type="text/javascript">
	function field_add(){
		var div = document.createElement('div');
		var delTag = "<input type='button' class='btn btn-default btn-rounded' value='삭제' onclick='field_del(this)' />";
		div.innerHTML = document.getElementById('field_add').innerHTML + delTag;
		document.getElementById('field').appendChild(div);
	}
	
	function field_del(obj){
		document.getElementById('field').removeChild(obj.parentNode);
	}
	
	function field_sec_add(){
		var div = document.createElement('div');
		var delTag = "<input type='button' class='btn btn-default btn-rounded' value='삭제' onclick='field_sec_del(this)' />";
		div.innerHTML = document.getElementById('field_sec_add').innerHTML + delTag;
		document.getElementById('field_sec').appendChild(div);
	}
	
	function field_sec_del(obj){
		document.getElementById('field_sec').removeChild(obj.parentNode);
	}
	
	$(document).ready(function(){
		/* calendar open */
		$('.date-picker').datepicker({
			orientation: "top auto",
			autoclose: true
		});
		
		if($('input[name="DELI_APPL_OBJ_CD"]:checked').val() == "HR"){
			$('#br_info').hide();
		}
		
		$('input[name="DELI_APPL_OBJ_CD"]').change(function(){
			if($(this).val() == "BR"){
				$('#br_info').show();
				$('#hr_info').hide();
			}else{
				$('#br_info').hide();
				$('#hr_info').show();
			}
		});
		
	 /*	var $validator = $("#wizardForm").validate({
			rules: {
				exampleInputName: {
					required: true
				},
				exampleInputEmail: {
					required: true,
					email: true
				},
				exampleInputPassword1: {
					required: true
				},
				exampleInputPassword2: {
					required: true,
					equalTo: '#exampleInputPassword1'
				},
				exampleInputCard: {
					required: true,
					number: true
				},
				exampleInputExpiration: {
					required: true,
					date: true
				}
			}
		});        (validator check) */
		
		$('#rootwizard').bootstrapWizard({
			'tabClass': 'nav nav-tabs',
			onTabShow: function(tab, navigation, index) {
				var $total = navigation.find('li').length;
				var $current = index+1;
				var $percent = ($current/$total) * 100;
				$('#rootwizard').find('.progress-bar').css({width:$percent+'%'});
			},
			'onNext': function(tab, navigation, index) {
				var $valid = $("#wizardForm").valid();
				if(!$valid) {
					$validator.focusInvalid();
					return false;
				}
			},
			'onTabClick': function(tab, navigation, index) {
				var $valid = $("#wizardForm").valid();
				if(!$valid) {
					$validator.focusInvalid();
					return false;
				}
			},
		});
	});
</script>
<div class="page-title">
	<h3>신규과제 심의신청</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-body">
					<div id="rootwizard">
						<ul class="nav nav-tabs nav-justified" role="tablist">
							<li role="presentation"><a href="#tab1" data-toggle="tab">연구자 정보</a></li>
							<li role="presentation"><a href="#tab2" data-toggle="tab">연구 정보</a></li>
							<li role="presentation"><a href="#tab3" data-toggle="tab">3</a></li>
							<li role="presentation"><a href="#tab4" data-toggle="tab">4</a></li>
						</ul>
						<div class="progress progress-sm m-t-sm">
							<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
							</div>
						</div>
						<form id="wizardForm">
							<div class="tab-content">
								<div class="tab-pane active fade in" id="tab1">
									<div class="col-md-12">
										<div class="row">
											<div class="form-group col-md-6 col-md-offset-3">
												<label for="deli_appl_title_ko">연구과제명 (국문)</label>
												<input type="text" class="form-control" name="deli_appl_title_ko" id="deli_appl_title_ko">
											</div>
											<div class="form-group col-md-6 col-md-offset-3">
												<label for="deli_appl_title_en">연구과제명 (영문)</label>
												<input type="text" class="form-control" name="deli_appl_title_en" id="deli_appl_title_en">
											</div>
											<div class="form-group col-md-6 col-md-offset-3">
												<label for="deli_appl_mng_id">연구책임자</label>
												<input type="text" class="form-control" name="deli_appl_mng_id" id="deli_appl_mng_id" placeholder="이름" >
											</div>
											<div class="form-group col-md-3 col-md-offset-3">
												<input type="text" class="form-control" name="deli_appl_dept_cd" id="deli_appl_dept_cd" placeholder="소속">
											</div>
											<div class="form-group col-md-3">
												<input type="text" class="form-control" name="deli_appl_mng_pos" id="deli_appl_mng_pos" placeholder="직위" >
											</div>
											<br/>
											<div class="form-group col-md-9 col-md-offset-3 label_pT">
												<label for="ins_id">
													<input type="button" class="btn btn-default btn-rounded" value="공동연구자 추가" onclick="field_add()" />
												</label>
												<div id="field_add" class="form-inline">
													<div class="input-group input-append col-md-8">
														<input type="text" class="form-control" name="ins_id" id="ins_id" readonly >
														<span class="input-group-addon add-on"><i class="fa fa-user"></i></span>
													</div>
												</div>
												<div id="field" class="field_pT form-inline"></div>
											</div>
											<div class="form-group col-md-9 col-md-offset-3">
												<label for="ins_id">
													<input type="button" class="btn btn-default btn-rounded" value="연구담당자 추가" onclick="field_sec_add()" />
												</label>
												<div id="field_sec_add" class="form-inline">
													<div class="input-group input-append col-md-8">
														<input type="text" class="form-control" name="ins_id" id="ins_id" readonly >
														<span class="input-group-addon add-on"><i class="fa fa-user"></i></span>
													</div>
												</div>
												<div id="field_sec" class="field_pT form-inline"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab2">
									<div class="col-md-12">
										<div class="row">
											<div class="form-group col-md-6 col-md-offset-3">
												<label for="deli_appl_dt">연구기간 ( IRB 승인후 ~ )</label>
												<div class="input-group input-append bootstrap-timepicker">
													<input type="text" class="form-control date-picker" name="deli_appl_dt" id="deli_appl_dt" readonly >
													<span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
											<div class="form-group col-md-6 col-md-offset-3">
												<label for="deli_appl_type_cd">연구유형</label>
												<div>
													<label class="label_pR">
														<input type="radio" name="deli_appl_type_cd" value="N" checked /> 해당 없음
													</label>
													<label class="label_pR">
														<input type="radio" name="deli_appl_type_cd" value="M" /> 석사 학위논문
													</label>
													<label class="label_pR">
														<input type="radio" name="deli_appl_type_cd" value="D" /> 박사 학위논문
													</label>
												</div>
											</div>
											<div class="form-group col-md-6 col-md-offset-3">
												<label for="deli_appl_cost_cd">연구비</label>
												<div>
													<label class="label_pR">
														<input type="radio" name="deli_appl_cost_cd" value="N" checked /> 없음
													</label>
													<label class="label_pR form-inline">
														<input type="radio" name="deli_appl_cost_cd" value="Y"/> 있음
														( 연구비 지원기관 : <input type="text" class="form-control" name="deli_appl_cost_org" id="deli_appl_cost_org" placeholder="연구비 지원기관" /> )
													</label>
												</div>
											</div>
											<div class="form-group col-md-6 col-md-offset-3">
												<label for="DELI_APPL_OBJ_CD">연구대상</label>
												<div>
													<label class="label_pR form-inline">
														<input type="radio" name="DELI_APPL_OBJ_CD" value="HR" checked/> 인간 대상
														( <input type="text" class="form-control" name="deli_appl_obj_cnt" id="deli_appl_obj_cnt" placeholder="0" /> 명 )
													</label>
													<label class="label_pR form-inline">
														<input type="radio" name="DELI_APPL_OBJ_CD" value="BR"/> 인체유래물 대상
														( <input type="text" class="form-control" name="deli_appl_obj_cnt" id="deli_appl_obj_cnt" placeholder="0" /> 명 )
													</label>
												</div>
											</div>
											<div id="hr_info">
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_obj_cd">연구대상유형 (모두 표기)</label>
													<div class="checkbox">
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 만 7세 미만
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 초, 중, 고등학생
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 대학생, 대학원생, 연구원 등
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 인지기능장애자
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 정신장애자
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 집단시설 수용자
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 문맹, 시각장애자
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 다문화인
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 법정 감염성질환자
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 임신부
	                                                    </label>
	                                                    <label class="form-inline">
	                                                        <input type="checkbox"> 기타	
	                                                        <input type="text" class="form-control" name="DELI_APPL_OBJ_TYPE_ETC" id="DELI_APPL_OBJ_TYPE_ETC" />
	                                                    </label>
	                                                </div>
												</div>
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_obj_cd">자료수집</label>
													<div class="checkbox">
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 조사연구
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 관찰연구
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 중재연구
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 기존자료이용
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 질적연구
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 기타	
	                                                    </label>
	                                                </div>
												</div>
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_obj_cd">개인식별 정보 수집</label>
													<div class="checkbox">
	                                                    <label class="col-md-12">
	                                                        <input type="checkbox"> 없음
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 이름
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 연락처
	                                                    </label>
	                                                    <label class="col-md-12">
	                                                        <input type="checkbox"> 주민번호
	                                                    </label>
	                                                    <label class="form-inline col-md-6">
	                                                        <input type="checkbox"> 기타	
	                                                        <input type="text" class="form-control" name="DELI_APPL_IDENTI_ETC" id="DELI_APPL_IDENTI_ETC" />
	                                                    </label>
	                                                </div>
												</div>
											</div>
											<div id="br_info">
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_obj_cd">연구대상유형 (모두 표기)</label>
													<div class="checkbox">
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 혈액
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 조직
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 대소변
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 머리카락
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 치아
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 배아
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 유전정보
	                                                    </label>
	                                                    <label class="form-inline col-md-6">
	                                                        <input type="checkbox"> 기타	
	                                                        <input type="text" class="form-control" name="DELI_APPL_OBJ_TYPE_ETC" id="DELI_APPL_OBJ_TYPE_ETC" />
	                                                    </label>
	                                                </div>
												</div>
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_obj_cd">자료수집</label>
													<div class="checkbox">
	                                                    <label class="col-md-12">
	                                                        <input type="checkbox"> 신규 채취
	                                                    </label>
	                                                    <label class="col-md-12">
	                                                        <input type="checkbox"> 인체유래물 은행에서 받음
	                                                    </label>
	                                                    <label class="col-md-12">
	                                                        <input type="checkbox"> 기존 검체 이용 ( 인체유래물 은행 제외 )
	                                                    </label>
	                                                </div>
												</div>
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_obj_cd">개인식별 정보 수집</label>
													<div class="checkbox">
	                                                    <label class="col-md-12">
	                                                        <input type="checkbox"> 없음
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 이름
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 연락처
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 주민번호
	                                                    </label>
	                                                    <label class="col-md-6">
	                                                        <input type="checkbox"> 유전정보
	                                                    </label>
	                                                    <label class="form-inline col-md-6">
	                                                        <input type="checkbox"> 기타	
	                                                        <input type="text" class="form-control" name="DELI_APPL_IDENTI_ETC" id="DELI_APPL_IDENTI_ETC" />
	                                                    </label>
	                                                </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab3">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_comm_org">공동연구기관</label>
													<input type="text" class="form-control" name="deli_appl_comm_org" id="deli_appl_comm_org" placeholder="※ 연구비지원기관이 있는 경우 기재해 주십시오." >
												</div>
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_agree_etc">동의취득</label>
													<div>
														<label class="label_pR">
															<input type="radio" name="deli_appl_agree_etc" value="Y" checked /> 필요
														</label>
														<label class="label_pR">
															<input type="radio" name="deli_appl_agree_etc" value="N"/> 불필요(특이요청사항 표기)
														</label>
													</div>
												</div>
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_risk_cd">연구의 위험성 정도</label>
													<div class="controls">
														<label>
															<input type="radio" name="deli_appl_risk_cd" value="A" checked /> 위험성 낮은 연구
														</label>
														<label>
															<input type="radio" name="deli_appl_risk_cd" value="B"/> 위험성 있지만 연구대상자에게 직접적인 이익이 기대되는 연구
														</label>
														<label>
															<input type="radio" name="deli_appl_risk_cd" value="C"/> 위험성 있고 연구대상자에게 직접적인 이익 없지만 일반화된 지식을 발전연구
														</label>
														<label>
															<input type="radio" name="deli_appl_risk_cd" value="D"/> 위험성 큰 연구
														</label>
													</div>
												</div>
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_remark_cd">특이요청 사항</label>
													<div class="controls">
														<label>
															<input type="radio" name="deli_appl_remark_cd" value="A" checked /> 없음
														</label>
														<label>
															<input type="radio" name="deli_appl_remark_cd" value="B"/> 심의면제
														</label>
														<label>
															<input type="radio" name="deli_appl_remark_cd" value="C"/> 동의취득면제
														</label>
														<label>
															<input type="radio" name="deli_appl_remark_cd" value="D"/> 동의서 서면면제
														</label>
													</div>
												</div>
												<div class="form-group col-md-6 col-md-offset-3">
													<label for="deli_appl_order_cd">타 IRB 제출 여부</label>
													<div>
														<label class="label_pR">
															<input type="radio" name="deli_appl_order_cd" value="N" checked /> 없음
														</label>
														<label class="label_pR form-inline">
															<input type="radio" name="deli_appl_order_cd" value="Y"/> 있음 
															( 위원회명 : <input type="text"  class="form-control" name="deli_appl_order_org" id="deli_appl_order_org" placeholder="위원회명" /> )
														</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab4">
									<h2 class="no-s">Thank You !</h2>
								</div>
								<ul class="pager wizard">
									<li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
									<li class="next"><a href="#" class="btn btn-default">Next</a></li>
								</ul>
							</div>
						</form>
					</div>
					<div class="col-md-12">
						<div class="text-center">
							<button type="button" class="btn btn-default btn-rounded">임시저장</button>
							<button type="button" class="btn btn-success btn-rounded">등록</button>
						</div>
					</div>
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
