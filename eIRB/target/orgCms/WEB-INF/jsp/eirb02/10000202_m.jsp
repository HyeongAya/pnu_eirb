﻿<c:set var="nextFileNo" value="${RQ_FILE_LIST.size()+1}"/>
<div class="page-title">
	<h3>신규과제 심의신청</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-body">
					<div id="rootwizard">
						<ul class="nav nav-tabs nav-justified" role="tablist">
							<li role="presentation"><a href="#tab1" data-toggle="tab"><i class="fa fa-user"></i> 연구자 정보</a></li>
							<li role="presentation"><a href="#tab2" data-toggle="tab"><i class="fa fa-flask"></i> 연구 정보 Ⅰ</a></li>
							<li role="presentation"><a href="#tab3" data-toggle="tab"><i class="fa fa-eyedropper"></i> 연구 정보 Ⅱ</a></li>
							<li role="presentation"><a href="#tab4" data-toggle="tab"><i class="fa fa-file-pdf-o"></i> 첨부 서류</a></li>
						</ul>
						<div class="progress progress-sm m-t-sm">
							<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
							</div>
						</div>
						<form id="deli_appl_form" name="deli_appl_form" method="post" enctype="application/x-www-form-urlencoded">
							<input type="hidden" name="SAVE_FLAG" id="SAVE_FLAG"/>
							<input type="hidden" name="DELI_APPL_KEYNO" id="DELI_APPL_KEYNO" value="${APPL_DATA.DELI_APPL_KEYNO }"/>
							<input type="hidden" name="USER_ID" id="USER_ID" value="${USER_ID}"/>
							<input type="hidden" name="USER_IP" id="USER_IP" value="127.0.0.1"/>
							<input type="hidden" name="DELI_ACCS_STEP_CD" id="DELI_ACCS_STEP_CD" value="AP_AC"/>
							<input type="hidden" name="DELI_ACCS_STATUS" id="DELI_ACCS_STATUS"/>
							
							<div class="tab-content">
								<div class="tab-pane active fade in" id="tab1">
									<div class="col-md-12">
										<div class="table-responsive">
											<div class="form-group">
												<table id="appl_table" class="table table-bordered" >
													<colgroup>
														<col width="30%"/>
														<col width="35%"/>
														<col width="35%"/>
													</colgroup>
													<tbody>
														<tr>
															<th><label for="DELI_APPL_TITLE_KO">연구과제명 (국문)</label></th>
															<td colspan="2"><input type="text" class="form-control" name="DELI_APPL_TITLE_KO" id="DELI_APPL_TITLE_KO" value="${APPL_DATA.DELI_APPL_TITLE_KO}"></td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_TITLE_EN">연구과제명 (영문)</label></th>
															<td colspan="2"><input type="text" class="form-control" name="DELI_APPL_TITLE_EN" id="DELI_APPL_TITLE_EN" value="${APPL_DATA.DELI_APPL_TITLE_EN}"></td>
														</tr>
														<tr>
															<th rowspan="2"><label for="DELI_APPL_MNG_ID">연구책임자</label></th>
															<td colspan="2"><input type="text" class="form-control" name="DELI_APPL_MNG_ID" id="DELI_APPL_MNG_ID" placeholder="이름" value="${APPL_DATA.DELI_APPL_MNG_ID}"></td>
														</tr>
														<tr>
															<td><input type="text" class="form-control" name="DELI_APPL_DEPT_CD" id="DELI_APPL_DEPT_CD" placeholder="소속" value="${APPL_DATA.DELI_APPL_DEPT_CD}"></td>
															<td><input type="text" class="form-control" name="DELI_APPL_MNG_POS" id="DELI_APPL_MNG_POS" placeholder="직위" value="${APPL_DATA.DELI_APPL_MNG_POS}"></td>
														</tr>
														<tr>
															<th>
																<label for="RESC_ID">
																	공통연구자
																</label>
															</th>
															<td colspan="2">
																<select id="RESC_CR" name="RESC_CR" class="js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%">
																	<c:forEach var="res" items="${RES_LIST}" varStatus="status">
																		<option value="${res.USER_ID}" <c:if test="${fn:contains(APPL_DATA.RESC_CR,res.USER_ID)}"> selected</c:if>>${res.DEPT_NM} / ${res.USER_POS} / ${res.USER_NM}</option>
																	</c:forEach>
																</select>
															</td>
														</tr>
														<tr>
															<th>
																<label for="RESC_ID">
																	연구담당자
																</label>
															</th>
															<td colspan="2">
																<select id="RESC_RR" name="RESC_RR" class="js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%">
																	<c:forEach var="res" items="${RES_LIST}" varStatus="status">
																		<option value="${res.USER_ID}" <c:if test="${fn:contains(APPL_DATA.RESC_RR,res.USER_ID)}"> selected</c:if>>${res.DEPT_NM} / ${res.USER_POS} / ${res.USER_NM}</option>
																	</c:forEach>
																</select>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab2">
									<div class="col-md-12">
										<div class="table-responsive">
											<div class="form-group">
												<table id="appl_table" class="table table-bordered" >
													<colgroup>
														<col width="30%"/>
														<col width="70%"/>
													</colgroup>
													<tbody>
														<tr>
															<th><label for="DELI_APPL_END_DT">연구기간<br/>( IRB 승인후 ~ )</label></th>
															<td>
																<div class="input-group input-append bootstrap-timepicker">
																	<input type="text" class="form-control date-picker" name="DELI_APPL_END_DT" id="DELI_APPL_END_DT" value="${fn:substring(APPL_DATA.DELI_APPL_END_DT,0,10)}" readonly >
																	<span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
																</div>
															</td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_TYPE_CD">연구유형</label></th>
															<td>
																<jsp:include page="/comm/radiobox_deli" flush="false">
																	<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																	<jsp:param name="NAME" value="DELI_APPL_TYPE_CD" />
																	<jsp:param name="ID" value="DELI_APPL_TYPE_CD" />
																	<jsp:param name="LABEL" value="col-md-4" />
																	<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_TP'}]" />
																	<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_TYPE_CD}" />
																</jsp:include>
															</td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_COST_CD">연구비</label></th>
															<td>
																<jsp:include page="/comm/radiobox_deli" flush="false">
																	<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																	<jsp:param name="NAME" value="DELI_APPL_COST_CD" />
																	<jsp:param name="ID" value="DELI_APPL_COST_CD" />
																	<jsp:param name="LABEL" value="col-md-4" />
																	<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_CS'}]" />
																	<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_COST_CD}" />
																</jsp:include>
															</td>
														</tr>
														<tr id="cost_org">
															<th><label for="DELI_APPL_COST_ORG">연구비 지원기관</label></th>
															<td><input type="text" class="form-control" name="DELI_APPL_COST_ORG" id="DELI_APPL_COST_ORG" value="${APPL_DATA.DELI_APPL_COST_ORG}"></td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_OBJ_CD">연구대상</label></th>
															<td>
																<jsp:include page="/comm/radiobox_deli" flush="false">
																	<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																	<jsp:param name="NAME" value="DELI_APPL_OBJ_CD" />
																	<jsp:param name="ID" value="DELI_APPL_OBJ_CD" />
																	<jsp:param name="LABEL" value="col-md-4" />
																	<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OB'}]" />
																	<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_OBJ_CD}" />
																</jsp:include>
															</td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_OBJ_CNT">연구대상자수<br/>(명)</label></th>
															<td>
																<input type="number" class="form-control" name="DELI_APPL_OBJ_CNT" id="DELI_APPL_OBJ_CNT" placeholder="0" value="${APPL_DATA.DELI_APPL_OBJ_CNT}"/>
															</td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_OBJ_TYPE_CD">연구대상유형<br/>(모두 표기)</label></th>
															<td>
																<div id="hr_obj" class="checkbox">
																	<jsp:include page="/comm/checkbox_deli_attr" flush="false">
																		<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																		<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																		<jsp:param name="NAME" value="DELI_APPL_OBJ_TYPE_CD" />
																		<jsp:param name="ID" value="DELI_APPL_OBJ_TYPE_CD" />
																		<jsp:param name="LABEL" value="col-md-6" />
																		<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OT'}]" />
																		<jsp:param name="ATTR" value="HR" />
																		<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_OBJ_TYPE_CD}" />
																	</jsp:include>
																</div>
																<div id="br_obj" class="checkbox">
																	<jsp:include page="/comm/checkbox_deli_attr" flush="false">
																		<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																		<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																		<jsp:param name="NAME" value="DELI_APPL_OBJ_TYPE_CD" />
																		<jsp:param name="ID" value="DELI_APPL_OBJ_TYPE_CD" />
																		<jsp:param name="LABEL" value="col-md-6" />
																		<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OT'}]" />
																		<jsp:param name="ATTR" value="BR" />
																		<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_OBJ_TYPE_CD}" />
																	</jsp:include>
																</div>
															</td>
														</tr>
														<tr id="obj_type_etc">
															<th><label for="DELI_APPL_OBJ_TYPE_ETC">연구대상유형 기타내용</label></th>
															<td><input type="text" class="form-control" name="DELI_APPL_OBJ_TYPE_ETC" id="DELI_APPL_OBJ_TYPE_ETC" value="${APPL_DATA.DELI_APPL_OBJ_TYPE_ETC}"></td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_COLT_CD">자료수집</label></th>
															<td>
																<div id="hr_colt" class="checkbox">
																	<jsp:include page="/comm/checkbox_deli_attr" flush="false">
																		<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																		<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																		<jsp:param name="NAME" value="DELI_APPL_COLT_CD" />
																		<jsp:param name="ID" value="DELI_APPL_COLT_CD" />
																		<jsp:param name="LABEL" value="col-md-6" />
																		<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_CL'}]" />
																		<jsp:param name="ATTR" value="HR" />
																		<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_COLT_CD}" />
																	</jsp:include>
																</div>
																<div id="br_colt" class="checkbox">
																	<jsp:include page="/comm/checkbox_deli_attr" flush="false">
																		<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																		<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																		<jsp:param name="NAME" value="DELI_APPL_COLT_CD" />
																		<jsp:param name="ID" value="DELI_APPL_COLT_CD" />
																		<jsp:param name="LABEL" value="col-md-12" />
																		<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_CL'}]" />
																		<jsp:param name="ATTR" value="BR" />
																		<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_COLT_CD}" />
																	</jsp:include>
																</div>
															</td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_IDENTI_CD">개인식별 정보 수집</label></th>
															<td>
																<div id="hr_identi" class="checkbox">
																	<jsp:include page="/comm/checkbox_deli_attr" flush="false">
																		<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																		<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																		<jsp:param name="NAME" value="DELI_APPL_IDENTI_CD" />
																		<jsp:param name="ID" value="DELI_APPL_IDENTI_CD" />
																		<jsp:param name="LABEL" value="col-md-6" />
																		<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_IT'}]" />
																		<jsp:param name="ATTR" value="HR" />
																		<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_IDENTI_CD}" />
																	</jsp:include>
																</div>
																<div id="br_identi" class="checkbox">
																	<jsp:include page="/comm/checkbox_deli_attr" flush="false">
																		<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																		<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																		<jsp:param name="NAME" value="DELI_APPL_IDENTI_CD" />
																		<jsp:param name="ID" value="DELI_APPL_IDENTI_CD" />
																		<jsp:param name="LABEL" value="col-md-6" />
																		<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_IT'}]" />
																		<jsp:param name="ATTR" value="BR" />
																		<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_IDENTI_CD}" />
																	</jsp:include>
																</div>
															</td>
														</tr>
														<tr id="identi_etc">
															<th><label for="DELI_APPL_IDENTI_ETC">개인식별 정보수집 기타내용</label></th>
															<td><input type="text" class="form-control" name="DELI_APPL_IDENTI_ETC" id="DELI_APPL_IDENTI_ETC" value="${APPL_DATA.DELI_APPL_IDENTI_ETC}"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab3">
									<div class="col-md-12">
										<div class="table-responsive">
											<div class="form-group">
												<table id="appl_table" class="table table-bordered" >
													<colgroup>
														<col width="30%"/>
														<col width="70%"/>
													</colgroup>
													<tbody>
														<tr id="comm_org">
															<th><label for="DELI_APPL_COMM_ORG">공동연구기관</label></th>
															<td><input type="text" class="form-control" name="DELI_APPL_COMM_ORG" id="DELI_APPL_COMM_ORG" placeholder="※ 연구비지원기관이 있는 경우 기재해 주십시오." value="${APPL_DATA.DELI_APPL_COMM_ORG}"></td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_AGREE_CD">동의취득</label></th>
															<td>
																<jsp:include page="/comm/radiobox_deli" flush="false">
																	<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																	<jsp:param name="NAME" value="DELI_APPL_AGREE_CD" />
																	<jsp:param name="ID" value="DELI_APPL_AGREE_CD" />
																	<jsp:param name="LABEL" value="col-md-3" />
																	<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_AR'}]" />
																	<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_AGREE_CD}" />
																</jsp:include>
															</td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_RISK_CD">연구의 위험성 정도</label></th>
															<td>
																<jsp:include page="/comm/radiobox_deli" flush="false">
																	<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																	<jsp:param name="NAME" value="DELI_APPL_RISK_CD" />
																	<jsp:param name="ID" value="DELI_APPL_RISK_CD" />
																	<jsp:param name="LABEL" value="col-md-12" />
																	<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_RS'}]" />
																	<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_RISK_CD}" />
																</jsp:include>
															</td>
														</tr>
														<tr id="remark_cd">
															<th><label for="DELI_APPL_REMARK_CD">특이요청 사항<br/>( ※ 중복 신청가능 )</label></th>
															<td>
																<div class="checkbox">
																	<jsp:include page="/comm/checkbox_deli" flush="false">
																		<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																		<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																		<jsp:param name="NAME" value="DELI_APPL_REMARK_CD" />
																		<jsp:param name="ID" value="DELI_APPL_REMARK_CD" />
																		<jsp:param name="LABEL" value="col-md-12" />
																		<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_RM'}]" />
																		<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_REMARK_CD}" />
																	</jsp:include>
																</div>
															</td>
														</tr>
														<tr>
															<th><label for="DELI_APPL_OTHER_CD">타 IRB 제출 여부</label></th>
															<td>
																<jsp:include page="/comm/radiobox_deli" flush="false">
																	<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																	<jsp:param name="NAME" value="DELI_APPL_OTHER_CD" />
																	<jsp:param name="ID" value="DELI_APPL_OTHER_CD" />
																	<jsp:param name="LABEL" value="col-md-3" />
																	<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OH'}]" />
																	<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_OTHER_CD}" />
																</jsp:include>
															</td>
														</tr>
														<tr id="other_org">
															<th><label for="DELI_APPL_OTHER_ORG">타 IRB 제출 위원회명</label></th>
															<td>
																<input type="text" class="form-control" name="DELI_APPL_OTHER_ORG" id="DELI_APPL_OTHER_ORG" value="${APPL_DATA.DELI_APPL_OTHER_ORG}">
																※ 타 IRB 승인서가 있는 경우 다른 서류와 함께 제출해 주세요.
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab4">
									<div class="col-md-12">
										<div class="table-responsive">
											<div class="form-group">
												<table id="appl_table" class="table table-bordered" >
													<colgroup>
														<col width="20%"/>
														<col width="80%"/>
													</colgroup>
													<tbody>
													<c:forEach var="vo" items="${RQ_FILE_LIST}" varStatus="status">
														<tr>
															<th><label for="${vo.ATTR2}"><c:out value="${vo.LABEL}"/></label></th>
															<td>
																<input type="file" name="<%=ProjectConstant.KEY_FORM_FILE %>${status.count}" id="file${status.count}" />
																<input type="hidden" name="FILE${status.count}" id="FILE${status.count}" value="${vo.DATA}" />
															</td>
														</tr>
													</c:forEach>
													<tr>
														<th>
															<label for="DELI_APPL_ADDFILES">추가첨부서류<c:out value="${nextFileNo}"/></label>
															<input type="button" class="btn btn-default btn-rounded" value="추가" onclick="area_add()"/>
															<!-- <input type="button" class="btn btn-default btn-rounded" value="추가" id="addFile" /> -->
														</th>
														<td>
															<div id="area" class="form-inline">
																<div class="form-group" id="select_area">
																	<jsp:include page="/comm/selectbox" flush="false">
																		<jsp:param name="MAP" value="EIRB02_10000201.SELECT_ATTACHFILE_LIST" />
																		<jsp:param name="FLAG" value="SELECT" />
																		<jsp:param name="CLASS" value="js-states form-control" />
																		<jsp:param name="TITLE" value="파일종류" />
																		<jsp:param name="NAME" value="REL_CD" />
																		<jsp:param name="ID" value="FILE${nextFileNo}" />
																		<jsp:param name="VALUE" value="" />
																		<jsp:param name="PARAM" value="[{'MAIN_CD':'FILE','SUB_CD':'AD','ATTR1':'NRQ'}]" />
																		<jsp:param name="STYLE" value="width:100%;" />
																	</jsp:include>
																</div>
																<div class="form-group" id="file_area">
																	<input type="file" class="addfileCnt" name="<%=ProjectConstant.KEY_FORM_FILE %>${nextFileNo}" />
																</div>
															</div>
															<div id="file_add_area" class="form-inline field_pT"></div>
														</td>
													</tr>
													</tbody>
												</table>
												<input type="hidden" name="<%=ProjectConstant.KEY_FORM_FILE_MAX_SIZE %>" value="10" />
											</div>
										</div>
									</div>
								</div>
								<ul class="pager wizard">
									<li class="previous" id="previous"><a href="#" class="btn btn-default">이전</a></li>
									<li class="next" id="next"><a href="#" class="btn btn-default">다음</a></li>
								</ul>
							</div>
						</form>
					</div>
					<div class="col-md-12">
						<div class="text-center">
							<c:choose>
								<c:when test="${APPL_DATA.DELI_ACCS_STATUS eq '01'}">
									<button type="button" class="btn btn-warning btn-rounded" onclick="fn_save('temp_add','임시저장','01');"><i class="fa fa-save"></i> 임시저장</button>
									<button type="button" class="btn btn-primary btn-rounded" onclick="fn_save('apply','신청','02');"><i class="fa fa-check"></i> 신청</button>
									<button type="button" class="btn btn-danger btn-rounded" onclick="fn_del();"><i class="fa fa-trash"></i> 삭제</button>
								</c:when>
								<c:when test="${APPL_DATA.DELI_ACCS_STATUS eq '04'}">
									<button type="button" class="btn btn-warning btn-rounded " onclick="fn_save('modify','수정','03');"><i class="fa fa-pencil"></i> 수정</button>
								</c:when>
							</c:choose>
							<button type="button" class="btn btn-default btn-rounded" id="btn_cancel"><i class="fa fa-repeat"></i> 취소</button>
						</div>
					</div>
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
<script type="text/javascript">
	var nextFileNo = ${nextFileNo};

	function area_add(){
		var div = createElement();
		var delTag = "<input type='button' class='btn btn-default btn-rounded' value='삭제' onclick='area_del(this);' />";
		var areaTag = $('#area').html();
		
		nextFileNo++;
		
		div.html(areaTag + delTag);
		div.find('select').attr('id','FILE'+nextFileNo);
		div.find('input').attr('name','u_file'+nextFileNo);
		$('#file_add_area').append(div);
		
	}
	
	function area_del(obj){
		document.getElementById('file_add_area').removeChild(obj.parentNode);
	}
	
	$(document).ready(function(){
		/* calendar open */
		$('.date-picker').datepicker({
			orientation: "top auto",
			autoclose: true
		});
		
		$('#RESC_CR,#RESC_RR').select2({
			placeholder:"연구자를 검색 또는 선택하십시오."
		});
		
		/* table 지정 */
		$("#research").DataTable({	
			responsive: true,
			destroy: true,
			columnDefs: [
					{"targets" : 0, "searchable": true, "orderable" : true},
					{"targets" : 1, "searchable": true, "orderable" : true},
					{"targets" : 2, "searchable": true, "orderable" : true},
					{"targets" : 3, "searchable": false, "orderable" : false}
			]
		});
		
		var DELI_APPL_COST_CD = "${APPL_DATA.DELI_APPL_COST_CD}";
		if(DELI_APPL_COST_CD == '01'){
			$('#cost_org, #comm_org').hide();
		}else{
			$('#cost_org, #comm_org').show();
		}
		
		var DELI_APPL_OBJ_TYPE_CD = "${APPL_DATA.DELI_APPL_OBJ_TYPE_CD}";
		if(DELI_APPL_OBJ_TYPE_CD.match("11")) {
			$('#obj_type_etc').show();
		}else if(DELI_APPL_OBJ_TYPE_CD.match("19")) {
			$('#obj_type_etc').show();
		}else{
			$('#obj_type_etc').hide();
		}
		
		var DELI_APPL_IDENTI_CD = "${APPL_DATA.DELI_APPL_IDENTI_CD}";
		if(DELI_APPL_IDENTI_CD.match("05")) {
			$('#identi_etc').show();
		}else if(DELI_APPL_IDENTI_CD.match("11")){
			$('#identi_etc').show();
		}else{
			$('#identi_etc').hide();
		}
		
		var DELI_APPL_AGREE_CD = "${APPL_DATA.DELI_APPL_AGREE_CD}";
		if(DELI_APPL_AGREE_CD == '01'){
			$('#remark_cd').hide();
		}else{
			$('#remark_cd').show();
		}

		var DELI_APPL_OTHER_CD = "${APPL_DATA.DELI_APPL_OTHER_CD}";
		if(DELI_APPL_OTHER_CD == '01'){
			$('#other_org').hide();
		}else{
			$('#other_org').show();
		}
		
		if($('input[name="DELI_APPL_OBJ_CD"]:checked').val() == "01"){
			$('#br_obj, #br_colt, #br_identi').hide();
		}else{
			$('#hr_obj, #hr_colt, #hr_identi').hide();
		}

		$('input[name="DELI_APPL_OBJ_TYPE_CD"]').click(function() {
			
			if($('#DELI_APPL_OBJ_TYPE_CD11').prop('checked')==true) {
				$('#obj_type_etc').show();
			} else if($('#DELI_APPL_OBJ_TYPE_CD19').prop('checked')==true) {
				$('#obj_type_etc').show();
			} else {
				$('#obj_type_etc').hide();
			}
		});
		
		$('input[name="DELI_APPL_IDENTI_CD"]').click(function() {
			if($('#DELI_APPL_IDENTI_CD5').prop('checked')==true) {
				$('#identi_etc').show();
			} else if($('#DELI_APPL_IDENTI_CD11').prop('checked')==true) {
				$('#identi_etc').show();
			} else {
				$('#identi_etc').hide();
			}
		});
		
		$('input[name="DELI_APPL_COST_CD"]').click(function(){
			if($(this).val() == "02"){
				$('#cost_org, #comm_org').show();
			}else{
				$('#cost_org, #comm_org').hide();
				$('#DELI_APPL_COST_ORG, #DELI_APPL_COMM_ORG').val("");
			}
		});
		
		$('input[name="DELI_APPL_OBJ_CD"]').click(function(){

			$('#br_obj, #br_colt, #br_identi, #hr_obj, #hr_colt, #hr_identi').children().children().children().children().removeClass('checked');
			$('input[name="DELI_APPL_OBJ_TYPE_CD"]').prop("checked",false);
			$('input[name="DELI_APPL_COLT_CD"]').prop("checked",false);
			$('input[name="DELI_APPL_IDENTI_CD"]').prop("checked",false);
			
			if($(this).val() == "02"){
				$('#br_obj, #br_colt, #br_identi').show();
				$('#hr_obj, #hr_colt, #hr_identi').hide();
			}else{
				$('#br_obj, #br_colt, #br_identi').hide();
				$('#hr_obj, #hr_colt, #hr_identi').show();
			}
			$('#obj_type_etc, #identi_etc').hide();
			$('#DELI_APPL_OBJ_TYPE_ETC, #DELI_APPL_IDENTI_ETC').val("");
		});
		
		$('input[name="DELI_APPL_AGREE_CD"]').click(function(){
			$('input[name="DELI_APPL_REMARK_CD"]').parent().removeClass('checked');
			$('input[name="DELI_APPL_REMARK_CD"]').prop("checked",false);
			
			if($(this).val() == "01"){
				$('#remark_cd').hide();
			}else{
				$('#remark_cd').show();
			}
		});
		
		$('input[name="DELI_APPL_OTHER_CD"]').click(function(){
			if($(this).val() == "02"){
				$('#other_org').show();
			}else{
				$('#other_org').hide();
				$('#DELI_APPL_OTHER_ORG').val("");
			}
		});

		var $validator = $("#deli_appl_form").validate({
			rules: {
				DELI_APPL_TITLE_KO: {
					required: true
				}
			}
		});
		
		$('#rootwizard').bootstrapWizard({
			'tabClass': 'nav nav-tabs',
			onTabShow: function(tab, navigation, index) {
				var $total = navigation.find('li').length;
				var $current = index+1;
				var $percent = ($current/$total) * 100;
				$('#rootwizard').find('.progress-bar').css({width:$percent+'%'});
			},
			'onNext': function(tab, navigation, index) {
				var $valid = $("#deli_appl_form").valid();
				if(!$valid) {
					$validator.focusInvalid();
					return false;
				}
			},
			'onTabClick': function(tab, navigation, index) {
				var $valid = $("#deli_appl_form").valid();
				if(!$valid) {
					$validator.focusInvalid();
					return false;
				}
			},
		});
		
		/* 취소 */
		$('#btn_cancel').click(function() {
			history.back();
		});
	});
	
	/* 등록 */
	function fn_save(status,text,accs_cd) {
		var frm = document.deli_appl_form;
		
		var $valid = $("#deli_appl_form").valid();
		
		var addfileCnt = $('.addfileCnt').length-1;
		
		$('.addfileCnt').each(function(index){
			var tempIdx = index+7;
			$(this).prop('name','u_file'+tempIdx);
		});
		
		var $valid = $("#deli_appl_form").valid();
		
		if(!$valid) {
			$validator.focusInvalid();
			return false;
		}else{
			var apply = "";
			if(status == "apply"){
				apply = "\n( ※ 신청하신 과제는 별도의 수정요청이 없을 경우 수정하실 수 없습니다. )";
			}
			result = confirm('작성하신 과제를 '+text+' 하시겠습니까?'+apply);
			if(result == true){
				$("#SAVE_FLAG").val(status);
				$("#DELI_ACCS_STATUS").val(accs_cd);
				frm.action = "/eirb02/10000202/save";
				frm.submit();
			}else{
				return false;
			}
		}
	}
</script>