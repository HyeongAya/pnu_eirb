﻿<div class="page-title">
	<h3>심의 진행 현황</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-list-alt"></i> 심의 진행 현황 리스트</h3>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<!-- Modal -->
					<form id="deli_access_form" name="deli_access_form" method="post" enctype="application/x-www-form-urlencoded">
						<input type="hidden" name="DELI_APPL_KEYNO" id="DELI_APPL_KEYNO"/>
						<input type="hidden" name="USER_ID" id="USER_ID" value="${USER_ID}"/>
						<input type="hidden" name="USER_IP" id="USER_IP" value="127.0.0.1"/>
						<div class="modal fade" id="deliScheduleModal" tabindex="-1" role="dialog" aria-labelledby="deliScheduleModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="deliScheduleModalLabel"><i class="fa fa-list"></i> 심의 진행현황</h4>
									</div>
									<div class="modal-body">
										<div class="form-horizontal">
											<table id='access_table' class='display table table-striped' style='width: 100%; cellspacing: 0;'>
												<thead>
													<tr>
														<th>No</th>
														<th>진행상황</th>
														<th>진행일시</th>
													</tr>
												</thead>
												<tbody id="access_List">
												</tbody>
											</table>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default btn-rounded" data-dismiss="modal"><i class="fa fa-close"></i> 닫기</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table id="accs_table" class="display table" style="width: 100%; cellspacing: 0;">
							<colgroup>
								<col style="width:5%;">
								<col style="width:10%;">
								<col style="width:10%;">
								<col style="width:10%;">
								<col />
								<col style="width:15%;">
								<col style="width:10%;">
								<col style="width:15%;">
							</colgroup>
							<thead>
								<tr>
									<th>No</th>
									<th>심의유형</th>
									<th>연구종류</th>
									<th>과제번호</th>
									<th>연구과제명</th>
									<th>연구책임자</th>
									<th>진행상황</th>
									<th>진행일자</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="accs" items="${ACCESS_LIST}" varStatus="status">
									<tr>
										<td class="text-center">${status.count}</td>
										<td class="text-center">${accs.DELI_GROUP_NM}</td>
										<td class="text-center">${accs.DELI_OBJ_CD_NM}</td>
										<td class="text-center">${accs.DELI_APPL_KEYNO}</td>
										<td><a href="javascript:fn_view('${accs.DELI_APPL_KEYNO}')" >${accs.DELI_APPL_TITLE_KO}</a></td>
										<td>${accs.DELI_APPL_MNG_ID} (${accs.DELI_APPL_DEPT_CD} / ${accs.DELI_APPL_MNG_POS} )</td>
										<td class="text-center">
											<button type="button" class="btn btn-default btn-rounded btn-xs" data-toggle="modal" data-target="#deliScheduleModal" data-whatever="${accs.DELI_APPL_KEYNO}">${accs.DELI_ACCS_STATUS_NM}</button>
										</td>
										<td class="text-center">${accs.INS_DT}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		/* table 지정 */
		$("#accs_table").DataTable({	
			responsive: true,
			destroy: true,
			"order": [[ 0, 'desc' ]],
			columnDefs: [
					{"targets" : 0, "searchable": true, "orderable" : true},
					{"targets" : 1, "searchable": true, "orderable" : true},
					{"targets" : 2, "searchable": true, "orderable" : true},
					{"targets" : 3, "searchable": true, "orderable" : true},
					{"targets" : 4, "searchable": true, "orderable" : true}
			]
		});
		
		$("#deliScheduleModal").on('show.bs.modal', function(event){
			var button = $(event.relatedTarget);
			var recipient = button.data('whatever');
			$('#DELI_APPL_KEYNO').val(recipient);
			
			$('#access_List').empty();
			
			$.ajax({
				type:'post',
				url:"/eirb01/10000103/access",
				data:{"DELI_APPL_KEYNO":recipient},
				dataType:'json',
				async:false,
				success : function(data){
					var table;
					
					for(var idx=0; idx<data.length; idx++){
						var num = data.length-idx;
						table = "<tr><td class='text-center'>"+num
						+"</td><td class='text-center'>"+data[idx].STATUS_NM
						+"</td><td class='text-center'>"+data[idx].ACCS_DT
						+"</td></tr>";
						
						$('#access_List').append(table);
					}
				}
 			});
		});
	});
	
	/* 뷰페이지 이동 */
	function fn_view(deli_appl_keyno) {
		var frm = document.deli_access_form;
		$("#DELI_APPL_KEYNO").val(deli_appl_keyno);
		frm.action = "/eirb02/10000202/view";
		frm.submit();
	}
</script>
<style type="text/css">
	.btn_block { overflow: hidden; margin: 20px 0 0 0; }
	th { text-align: center; }
</style>