﻿<div class="page-title">
	<h3>심의일정관리</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-write">
				<div class="panel-body">
					<form id="deli_sche_form" name="deli_sche_form" method="post" enctype="application/x-www-form-urlencoded">
						<input type="hidden" name="DELI_SCHE_KEYNO" id="DELI_SCHE_KEYNO"/>
					</form>
					<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-justified" role="tablist">
							<li role="presentation" class="active"><a href="#tab21" role="tab" data-toggle="tab">전체심의</a></li>
							<li role="presentation"><a href="#tab22" role="tab" data-toggle="tab">신속심의</a></li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active fade in" id="tab21">
								<div class="table-responsive">
									<table id="deliS_whole" class="display table" style="width: 100%; cellspacing: 0;">
										<colgroup>
											<col style="width:15%;">
											<col />
											<col style="width:20%;">
											<col style="width:20%;">
										</colgroup>
										<thead>
											<tr>
												<th class="text-center">No</th>
												<th class="text-center">심의명</th>
												<th class="text-center">심의일</th>
												<th class="text-center">심의참석 확정여부</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="deli" items="${DELI_LIST}" varStatus="status">
												<tr>
													<td class="text-center">${status.count}</td>
													<td><a href="javascript:fn_view(${deli.DELI_SCHE_KEYNO})">${deli.DELI_SCHE_NM}</a></td>
													<td class="text-center">${fn:substring(deli.DELI_SCHE_DT,0,10)}</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${deli.DELI_SCHE_DECIDE_FG eq 'N'}">명단 미확정</c:when>
															<c:when test="${deli.DELI_SCHE_DECIDE_FG eq 'Y'}">명단 확정</c:when>
														</c:choose>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
								<div class="p-v-xs"></div>
								<div>
									<button type="button" class="btn btn-primary btn-rounded pull-right" onclick="fn_view()"><i class="fa fa-check"></i> 등록</button>
								</div>
							</div><!-- end tab21 -->
							<div role="tabpanel" class="tab-pane fade" id="tab22">
								<div class="table-responsive">
									내용 미정
								</div><!-- end tab22 -->
							</div><!-- end tabpanel -->
						</div><!-- end tab-content -->
					</div><!-- end tab -->
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		/* table 지정 */
		$("#deliS_whole").DataTable({	
			responsive: true,
			destroy: true,
			"order": [[ 0, 'desc' ]],
			/* ordering: false, */
			columnDefs: [
					{"targets" : 0, "searchable": true, "orderable" : true},
					{"targets" : 1, "searchable": true, "orderable" : true},
					{"targets" : 2, "searchable": true, "orderable" : true},
					{"targets" : 3, "searchable": true, "orderable" : true}
			]
		});
	});
	
	/* 관리페이지 이동 */
	function fn_view(deli_sche_keyno) {
		var frm = document.deli_sche_form;
		$("#DELI_SCHE_KEYNO").val(deli_sche_keyno);
		frm.action = "/eirb01/10000101/view";
		frm.submit();
	}
</script>