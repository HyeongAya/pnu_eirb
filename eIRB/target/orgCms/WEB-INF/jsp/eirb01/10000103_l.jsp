﻿<div class="page-title">
	<h3>신청 현황</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-body">
					<!-- Modal -->
					<form id="deli_access" name="deli_access" method="post" enctype="application/x-www-form-urlencoded">
						<input type="hidden" name="DELI_APPL_KEYNO" id="DELI_APPL_KEYNO"/>
						<input type="hidden" name="DATA_UPDATE" id="DATA_UPDATE"/>
						<input type="hidden" name="USER_ID" id="USER_ID" value="${USER_ID}"/>
						<input type="hidden" name="USER_IP" id="USER_IP" value="127.0.0.1"/>
						<div class="modal fade" id="deliScheduleModal" tabindex="-1" role="dialog" aria-labelledby="deliScheduleModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="deliScheduleModalLabel"><i class="fa fa-list"></i> 심의 진행현황</h4>
									</div>
									<div class="modal-body">
										<div class="form-horizontal">
											<table id='access_table' class='display table table-striped' style='width: 100%; cellspacing: 0;'>
												<thead>
													<tr>
														<th>No</th>
														<th>진행상황</th>
														<th>진행일시</th>
													</tr>
												</thead>
												<tbody id="access_List">
												</tbody>
											</table>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default btn-rounded" data-dismiss="modal"><i class="fa fa-close"></i> 닫기</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-justified" role="tablist">
							<li role="presentation" class="active"><a href="#tab21" role="tab" data-toggle="tab">신규과제 신청현황</a></li>
							<li role="presentation"><a href="#tab22" role="tab" data-toggle="tab">진행과제 현황</a></li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active fade in" id="tab21">
								<div class="table-responsive">
									<table id="deli_access_new" class="display table" style="width: 100%; cellspacing: 0;">
										<colgroup>
											<col style="width:10%;">
											<col style="width:15%;">
											<col />
											<col style="width:20%;">
											<col style="width:10%;">
											<col style="width:15%;">
										</colgroup>
										<thead>
											<tr>
												<th>No</th>
												<th>연구종류</th>
												<th>연구과제명</th>
												<th>연구책임자</th>
												<th>진행상황</th>
												<th>신청일자</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="data" items="${NEW_LIST}" varStatus="status">
												<tr>
													<td class="text-center">${status.count}</td>
													<td>
														<jsp:include page="/comm/deli_data" flush="false">
															<jsp:param name="MAP" value="Common.SELECT_COMSUB_CODE" />
															<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OB', 'SUB_CD':'${data.DELI_APPL_OBJ_CD}'}]" />
														</jsp:include>
													</td>
													<td><a href="javascript:fn_view('${data.DELI_APPL_KEYNO}')" >${data.DELI_APPL_TITLE_KO}</a></td>
													<td>${data.DELI_APPL_MNG_ID} ( ${data.DELI_APPL_DEPT_CD} / ${data.DELI_APPL_MNG_POS} )</td>
													<td class="text-center">${data.DELI_ACCS_STATUS_NM}</td>
													<td class="text-center"><fmt:formatDate value="${data.DELI_ACCS_STAT_DT}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div><!-- end tab21 -->
							<div role="tabpanel" class="tab-pane fade" id="tab22">
								<div class="table-responsive">
									<table id="deli_access_list" class="display table" style="width: 100%; cellspacing: 0;">
										<colgroup>
											<col style="width:5%;">
											<col style="width:10%;">
											<col style="width:10%;">
											<col style="width:10%;">
											<col />
											<col style="width:15%;">
											<col style="width:10%;">
											<col style="width:15%;">
										</colgroup>
										<thead>
											<tr>
												<th>No</th>
												<th>심의유형</th>
												<th>연구종류</th>
												<th>과제번호</th>
												<th>연구과제명</th>
												<th>연구책임자</th>
												<th>진행상황</th>
												<th>진행일자</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="accs_data" items="${ACCESS_LIST}" varStatus="accs_status">
												<tr>
													<td class="text-center">${accs_status.count}</td>
													<td class="text-center">${accs_data.DELI_GROUP_NM}</td>
													<td class="text-center">${accs_data.DELI_OBJ_CD_NM}</td>
													<td class="text-center">${accs_data.DELI_APPL_KEYNO}</td>
													<td><a href="javascript:fn_view('${accs_data.DELI_APPL_KEYNO}')" >${accs_data.DELI_APPL_TITLE_KO}</a></td>
													<td>${accs_data.DELI_APPL_MNG_ID} (${accs_data.DELI_APPL_DEPT_CD} / ${accs_data.DELI_APPL_MNG_POS} )</td>
													<td class="text-center">
														<button type="button" class="btn btn-default btn-rounded btn-xs" data-toggle="modal" data-target="#deliScheduleModal" data-whatever="${accs_data.DELI_APPL_KEYNO}">${accs_data.DELI_ACCS_STATUS_NM}</button>
													</td>
													<td class="text-center"><fmt:formatDate value="${accs_data.DELI_ACCS_STAT_DT}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div><!-- end tab22 -->
							</div><!-- end tabpanel -->
						</div><!-- end tab-content -->
					</div><!-- end tab -->
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		var $validator = $("#deli_sche_form").validate({
			rules: {
				DELI_SCHE_NM: {
					required: true
				},
				DELI_SCHE_DT: {
					required: true,
					date: true
				}
			}
		});
		
		/* table 지정 */
		$("#deli_access_new,#deli_access_list").DataTable({	
			responsive: true,
			destroy: true,
			"order": [[ 0, 'desc' ]]
		});
		
		/* calendar open */
		$('.date-picker').datepicker({
			orientation: "top auto",
			autoclose: true
		});
		
		$("#deliScheduleModal").on('show.bs.modal', function(event){
			var button = $(event.relatedTarget);
			var recipient = button.data('whatever');
			$('#DELI_APPL_KEYNO').val(recipient);
			
			$('#access_List').empty();
			
			$.ajax({
				type:'post',
				url:"/eirb01/10000103/access",
				data:{"DELI_APPL_KEYNO":recipient},
				dataType:'json',
				async:false,
				success : function(data){
					var table;
					
					for(var idx=0; idx<data.length; idx++){
						var num = data.length-idx;
						table = "<tr><td class='text-center'>"+num
						+"</td><td class='text-center'>"+data[idx].STATUS_NM
						+"</td><td class='text-center'>"+data[idx].ACCS_DT
						+"</td></tr>";
						
						$('#access_List').append(table);
					}
				}
 			});
		});
	});
	
	/* 등록 */
	function fn_save() {
		var frm = document.deli_sche_form;
		var $valid = $("#deli_sche_form").valid();
		if(!$valid) {
			$validator.focusInvalid();
			return false;
		}else{
			$("#SAVE_FLAG").val('add');
			$('.modal').modal('hide');
			frm.action = "/eirb01/10000101/save";
			frm.submit();
		}
	}
	
	/* 관리페이지 이동 */
	function fn_modify(deli_sche_keyno) {
		var frm = document.deli_sche_form;
		$("#DELI_SCHE_KEYNO").val(deli_sche_keyno);
		frm.action = "/eirb01/10000101/modify";
		frm.submit();
	}
	
	/* 뷰페이지 이동 */
	function fn_view(deli_appl_keyno) {
		var frm = document.deli_access;
		$("#DELI_APPL_KEYNO").val(deli_appl_keyno);
		frm.action = "/eirb01/10000103/view";
		frm.submit();
	}
</script>
<style type="text/css">
	th { text-align: center; }
</style>