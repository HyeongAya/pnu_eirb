﻿<div class="page-title">
	<h3>심의신청 과제 정보</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-body">
					<form id="deli_access" name="deli_access" method="post" enctype="application/x-www-form-urlencoded">
						<input type="hidden" name="DELI_APPL_KEYNO" id="DELI_APPL_KEYNO" value="${APPL_DATA.DELI_APPL_KEYNO}"/>
						<input type="hidden" name="DELI_APPL_OBJ_CD" id="DELI_APPL_OBJ_CD" value="${APPL_DATA.DELI_APPL_OBJ_CD}"/>
						<input type="hidden" name="DELI_ACCS_STEP_CD" id="DELI_ACCS_STEP_CD" value="AP_AC"/>
						<input type="hidden" name="DELI_ACCS_STATUS" id="DELI_ACCS_STATUS"/>
						<input type="hidden" name="USER_ID" id="USER_ID" value="${USER_ID}"/>
						<input type="hidden" name="USER_IP" id="USER_IP" value="127.0.0.1"/>
						<div class="modal fade" id="modify_myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><i class="fa fa-pencil"></i> 수정요청</h4>
									</div>
									<div class="modal-body">
										<div class="form-horizontal">
											<table id="appl_table" class="table table-bordered">
												<tbody>
													<tr>
														<th>연구과제명</th>
														<td colspan="3">${APPL_DATA.DELI_APPL_TITLE_KO}</td>
													</tr>
													<tr>
														<th>연구책임자</th>
														<td>${APPL_DATA.DELI_APPL_MNG_ID}</td>
														<th>소속</th>
														<td>${APPL_DATA.DELI_APPL_DEPT_CD} / ${APPL_DATA.DELI_APPL_MNG_POS}</td>
													</tr>
													<tr>
														<th>수정요청사항</th>
														<td colspan="3">
															<textarea name="DELI_ACCS_NOTE" class="input-large form-control" id="message" rows="5" 
															placeholder="요청할 수정사항을 입력해주십시오.&#13;&#10;※ 해당 창이 내려가거나 닫으시더라도 작성하신 내용은 유지됩니다."></textarea>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="modal-footer">
										<div class="text-center">
											<button type="button" class="btn btn-primary btn-rounded" onclick="fn_access('04','수정요청');"><i class="fa fa-pencil"></i> 수정요청</button>
											<button type="button" class="btn btn-default btn-rounded" data-dismiss="modal"><i class="fa fa-repeat"></i> 취소</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="deliapp_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-text"></i> 접수</h4>
									</div>
									<div class="modal-body">
										<div class="form-horizontal">
											<table id="appl_table" class="table table-bordered">
												<tbody>
													<tr>
														<th>연구과제명</th>
														<td colspan="3">${APPL_DATA.DELI_APPL_TITLE_KO}</td>
													</tr>
													<tr>
														<th>연구책임자</th>
														<td>${APPL_DATA.DELI_APPL_MNG_ID}</td>
														<th>소속</th>
														<td>${APPL_DATA.DELI_APPL_DEPT_CD} / ${APPL_DATA.DELI_APPL_MNG_POS}</td>
													</tr>
													<tr>
														<th>심의 유형</th>
														<td colspan="3">
															<jsp:include page="/comm/deli_data" flush="false">
																<jsp:param name="MAP" value="Common.SELECT_COMSUB_CODE" />
																<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OB', 'SUB_CD':'${APPL_DATA.DELI_APPL_OBJ_CD}'}]" />
															</jsp:include>
														</td>
													</tr>
													<tr>
														<th>심의 분류</th>
														<td colspan="3">
															<jsp:include page="/comm/radiobox_deli" flush="false">
																<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																<jsp:param name="NAME" value="ACCS_NOTE" />
																<jsp:param name="ID" value="ACCS_NOTE" />
																<jsp:param name="LABEL" value="col-md-4" />
																<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_GR'}]" />
															</jsp:include>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="modal-footer">
										<div class="text-center">
											<button type="button" class="btn btn-primary btn-rounded" onclick="fn_access('05','접수');"><i class="fa fa-file-text"></i> 접수</button>
											<button type="button" class="btn btn-default btn-rounded" data-dismiss="modal"><i class="fa fa-repeat"></i> 취소</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div id="rootwizard">
						<ul class="nav nav-tabs nav-justified" role="tablist">
							<li role="presentation"><a href="#tab1" data-toggle="tab"><i class="fa fa-user"></i> 연구자 정보</a></li>
							<li role="presentation"><a href="#tab2" data-toggle="tab"><i class="fa fa-flask"></i> 연구 정보 Ⅰ</a></li>
							<li role="presentation"><a href="#tab3" data-toggle="tab"><i class="fa fa-eyedropper"></i> 연구 정보 Ⅱ</a></li>
							<li role="presentation"><a href="#tab4" data-toggle="tab"><i class="fa fa-file-pdf-o"></i> 첨부 서류</a></li>
						</ul>
						<div class="p-v-xs"></div>
						<div class="tab-content">
							<div class="tab-pane active fade in" id="tab1">
								<div class="col-md-12">
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel"><i class="fa fa-user"></i> 연구자 정보 조회</h4>
												</div>
												<div class="modal-body">
													<div class="form-horizontal">
														<table id="appl_table" class="table table-bordered" >
															<tbody id="res_data">
															</tbody>
														</table>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default btn-rounded" data-dismiss="modal"><i class="fa fa-close"></i> 닫기</button>
												</div>
											</div>
										</div>
									</div>
									<div class="table-responsive">
										<div class="form-group col-md-12">
											<form id="deli_sche_form" name="deli_sche_form" method="post" enctype="application/x-www-form-urlencoded">
												<input type="hidden" name="DELI_APPL_KEYNO" id="DELI_APPL_KEYNO"/>
												<input type="hidden" name="USER_ID" id="USER_ID" value="${USER_ID}"/>
											</form>
											<table id="appl_table" class="table table-bordered" >
												<colgroup>
													<col width="30%"/>
													<col width="35%"/>
													<col width="35%"/>
												</colgroup>
												<tbody>
													<tr>
														<th><label for="DELI_APPL_TITLE_KO">연구과제명 (국문)</label></th>
														<td colspan="2">${APPL_DATA.DELI_APPL_TITLE_KO}</td>
													</tr>
													<tr>
														<th><label for="DELI_APPL_TITLE_EN">연구과제명 (영문)</label></th>
														<td colspan="2">${APPL_DATA.DELI_APPL_TITLE_EN}</td>
													</tr>
													<tr>
														<th rowspan="2"><label for="DELI_APPL_MNG_ID">연구책임자</label></th>
														<td colspan="2">${APPL_DATA.DELI_APPL_MNG_ID}</td>
													</tr>
													<tr>
														<td>${APPL_DATA.DELI_APPL_DEPT_CD}</td>
														<td>${APPL_DATA.DELI_APPL_MNG_POS}</td>
													</tr>
													<tr>
														<th>
															<label for="RESC_ID">
																공통연구자
															</label>
														</th>
														<td colspan="2">
															<c:forEach var="res" items="${RES_LIST}" varStatus="status">
																<c:if test="${res.RESC_TYPE_CD eq 'CR' }">
																	<div id="field_add" class="form-inline field_pT">
																		<div class="input-group input-append col-md-3" data-toggle="modal" data-target="#myModal" data-whatever="${res.USER_ID}" >
																			<input type="text" class="form-control" name="RESC_NAME" id="RESC_NAME" value="${res.USER_NM}" readonly=readonly>
																			<span class="input-group-addon add-on"><i class="fa fa-user"></i></span>
																		</div>
																	</div>
																</c:if>
															</c:forEach>
														</td>
													</tr>
													<tr>
														<th>
															<label for="RESC_ID">
																연구담당자
															</label>
														</th>
														<td colspan="2">
															<c:forEach var="res" items="${RES_LIST}" varStatus="status">
																<c:if test="${res.RESC_TYPE_CD eq 'RR' }">
																	<div id="field_add" class="form-inline field_pT">
																		<div class="input-group input-append col-md-3" data-toggle="modal" data-target="#myModal" data-whatever="${res.USER_ID}">
																			<input type="text" class="form-control" name="RESC_NAME" id="RESC_NAME" value="${res.USER_NM}" readonly=readonly>
																			<span class="input-group-addon add-on">
																				<i class="fa fa-user"></i>
																			</span>
																		</div>
																	</div>
																</c:if>
															</c:forEach>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="tab2">
								<div class="col-md-12">
									<div class="table-responsive">
										<div class="form-group">
											<table id="appl_table" class="table table-bordered" >
												<colgroup>
													<col width="30%"/>
													<col width="70%"/>
												</colgroup>
												<tbody>
													<tr>
														<th><label for="DELI_APPL_END_DT">연구기간</label></th>
														<td> IRB 승인후 ~ ${fn:substring(APPL_DATA.DELI_APPL_END_DT,0,10)}</td>
													</tr>
													<tr>
														<th><label for="DELI_APPL_TYPE_CD">연구유형</label></th>
														<td>
															<jsp:include page="/comm/deli_data" flush="false">
																<jsp:param name="MAP" value="Common.SELECT_COMSUB_CODE" />
																<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_TP', 'SUB_CD':'${APPL_DATA.DELI_APPL_TYPE_CD}'}]" />
															</jsp:include>
														</td>
													</tr>
													<tr>
														<th><label for="DELI_APPL_COST_CD">연구비</label></th>
														<td>
															<jsp:include page="/comm/deli_data" flush="false">
																<jsp:param name="MAP" value="Common.SELECT_COMSUB_CODE" />
																<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_CS', 'SUB_CD':'${APPL_DATA.DELI_APPL_COST_CD}'}]" />
															</jsp:include>
														</td>
													</tr>
													<c:if test="${APPL_DATA.DELI_APPL_COST_CD eq '02'}">
														<tr>
															<th><label for="DELI_APPL_COST_ORG">연구비 지원기관</label></th>
															<td>${APPL_DATA.DELI_APPL_COST_ORG}</td>
														</tr>
													</c:if>
													<tr>
														<th><label for="DELI_APPL_OBJ_CD">연구대상</label></th>
														<td>
															<jsp:include page="/comm/deli_data" flush="false">
																<jsp:param name="MAP" value="Common.SELECT_COMSUB_CODE" />
																<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OB', 'SUB_CD':'${APPL_DATA.DELI_APPL_OBJ_CD}'}]" />
															</jsp:include>
														</td>
													</tr>
													<tr>
														<th><label for="DELI_APPL_OBJ_CNT">연구대상자수</label></th>
														<td>${APPL_DATA.DELI_APPL_OBJ_CNT} 명</td>
													</tr>
													<tr>
														<th><label for="DELI_APPL_OBJ_TYPE_CD">연구대상유형</label></th>
														<td>
															<ul>
																<c:choose>
																	<c:when test="${APPL_DATA.DELI_APPL_OBJ_CD eq '01'}">
																		<jsp:include page="/comm/checkbox_deli_li" flush="false">
																			<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																			<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																			<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OT'}]" />
																			<jsp:param name="ATTR" value="HR" />
																			<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_OBJ_TYPE_CD}" />
																		</jsp:include>
																	</c:when>
																	<c:when test="${APPL_DATA.DELI_APPL_OBJ_CD eq '02'}">
																		<jsp:include page="/comm/checkbox_deli_li" flush="false">
																			<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																			<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																			<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OT'}]" />
																			<jsp:param name="ATTR" value="BR" />
																			<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_OBJ_TYPE_CD}" />
																		</jsp:include>
																	</c:when>
																</c:choose>
															</ul>
														</td>
													</tr>
													<c:if test="${fn:contains(APPL_DATA.DELI_APPL_OBJ_TYPE_CD,'11') || fn:contains(APPL_DATA.DELI_APPL_OBJ_TYPE_CD,'19')}">
														<tr>
															<th><label for="DELI_APPL_OBJ_TYPE_ETC">연구대상유형 기타내용</label></th>
															<td>${APPL_DATA.DELI_APPL_OBJ_TYPE_ETC}</td>
														</tr>
													</c:if>
													<tr>
														<th><label for="DELI_APPL_COLT_CD">자료수집</label></th>
														<td>
															<ul>
																<c:choose>
																	<c:when test="${APPL_DATA.DELI_APPL_OBJ_CD eq '01'}">
																		<jsp:include page="/comm/checkbox_deli_li" flush="false">
																			<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																			<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																			<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_CL'}]" />
																			<jsp:param name="ATTR" value="HR" />
																			<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_COLT_CD}" />
																		</jsp:include>
																	</c:when>
																	<c:when test="${APPL_DATA.DELI_APPL_OBJ_CD eq '02'}">
																		<jsp:include page="/comm/checkbox_deli_li" flush="false">
																			<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																			<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																			<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_CL'}]" />
																			<jsp:param name="ATTR" value="BR" />
																			<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_COLT_CD}" />
																		</jsp:include>
																	</c:when>
																</c:choose>
															</ul>
														</td>
													</tr>
													<tr>
														<th><label for="DELI_APPL_IDENTI_CD">개인식별 정보 수집</label></th>
														<td>
															<ul>
																<c:choose>
																	<c:when test="${APPL_DATA.DELI_APPL_OBJ_CD eq '01'}">
																		<jsp:include page="/comm/checkbox_deli_li" flush="false">
																			<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																			<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																			<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_IT'}]" />
																			<jsp:param name="ATTR" value="HR" />
																			<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_IDENTI_CD}" />
																		</jsp:include>
																	</c:when>
																	<c:when test="${APPL_DATA.DELI_APPL_OBJ_CD eq '02'}">
																		<jsp:include page="/comm/checkbox_deli_li" flush="false">
																			<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																			<jsp:param name="MAP_ATTR" value="Common.SELECT_COMATTR_CODE" />
																			<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_IT'}]" />
																			<jsp:param name="ATTR" value="BR" />
																			<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_IDENTI_CD}" />
																		</jsp:include>
																	</c:when>
																</c:choose>
															</ul>
														</td>
													</tr>
													<c:if test="${fn:contains(APPL_DATA.DELI_APPL_IDENTI_CD,'05') || fn:contains(APPL_DATA.DELI_APPL_IDENTI_CD,'11')}">
														<tr>
															<th><label for="DELI_APPL_IDENTI_ETC">개인식별 정보수집 기타내용</label></th>
															<td>${APPL_DATA.DELI_APPL_IDENTI_ETC}</td>
														</tr>
													</c:if>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="tab3">
								<div class="col-md-12">
									<div class="table-responsive">
										<div class="form-group">
											<table id="appl_table" class="table table-bordered" >
												<colgroup>
													<col width="30%"/>
													<col width="70%"/>
												</colgroup>
												<tbody>
													<c:if test="${APPL_DATA.DELI_APPL_COST_CD eq '02'}">
														<tr>
															<th><label for="DELI_APPL_COMM_ORG">공동연구기관</label></th>
															<td>${APPL_DATA.DELI_APPL_COMM_ORG}</td>
														</tr>
													</c:if>
													<tr>
														<th><label for="DELI_APPL_AGREE_CD">동의취득</label></th>
														<td>
															<jsp:include page="/comm/deli_data" flush="false">
																<jsp:param name="MAP" value="Common.SELECT_COMSUB_CODE" />
																<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_AR', 'SUB_CD':'${APPL_DATA.DELI_APPL_AGREE_CD}'}]" />
															</jsp:include>
														</td>
													</tr>
													<tr>
														<th><label for="DELI_APPL_RISK_CD">연구의 위험성 정도</label></th>
														<td>
															<jsp:include page="/comm/deli_data" flush="false">
																<jsp:param name="MAP" value="Common.SELECT_COMSUB_CODE" />
																<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_RS', 'SUB_CD':'${APPL_DATA.DELI_APPL_RISK_CD}'}]" />
															</jsp:include>
														</td>
													</tr>
													<c:if test="${APPL_DATA.DELI_APPL_AGREE_CD eq '02'}">
														<tr>
															<th><label for="DELI_APPL_REMARK_CD">특이요청 사항</th>
															<td>
																<ul>
																	<jsp:include page="/comm/checkbox_li" flush="false">
																		<jsp:param name="MAP" value="Common.SELECT_COM_CODE" />
																		<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_RM'}]" />
																		<jsp:param name="VALUE" value="${APPL_DATA.DELI_APPL_REMARK_CD}" />
																	</jsp:include>
																</ul>
															</td>
														</tr>
													</c:if>
													<tr>
														<th><label for="DELI_APPL_OTHER_CD">타 IRB 제출 여부</label></th>
														<td>
															<jsp:include page="/comm/deli_data" flush="false">
																<jsp:param name="MAP" value="Common.SELECT_COMSUB_CODE" />
																<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OH', 'SUB_CD':'${APPL_DATA.DELI_APPL_OTHER_CD}'}]" />
															</jsp:include>
														</td>
													</tr>
													<c:if test="${APPL_DATA.DELI_APPL_OTHER_CD eq '02'}">
														<tr>
															<th><label for="DELI_APPL_OTHER_ORG">타 IRB 제출 위원회명</label></th>
															<td>${APPL_DATA.DELI_APPL_OTHER_ORG}</td>
														</tr>
													</c:if>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="tab4">
								<div class="col-md-12">
									<div class="table-responsive">
										<div class="form-group">
											<table id="appl_table" class="table table-bordered" >
												<colgroup>
													<col width="30%"/>
													<col width="70%"/>
												</colgroup>
												<tbody>
													<c:forEach var="file" items="${FILE_LIST}" varStatus="status">
														<tr>
															<th>${file.REL_CD_NM}</th>
															<td>
																<i class="fa fa-download"></i> <a href="#dc_contents_wrap" onclick="fn_download('${file.FILE_PATH }${file.FILE_NM_SERVER }', '${file.FILE_NM }', '${file.FILE_NM_SERVER }')">${file.FILE_NM }</a>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<ul class="pager wizard">
								<li class="previous" id="previous"><a href="#" class="btn btn-default">이전</a></li>
								<li class="next" id="next"><a href="#" class="btn btn-default">다음</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-12">
						<div class="text-center">
							<c:choose>
								<c:when test="${APPL_DATA.DELI_ACCS_STATUS eq '02'}">
									<button type="button" class="btn btn-success btn-rounded" onclick="fn_access('03','접수 및 검토')"><i class="fa fa-file-text-o"></i> 접수 및 검토</button>
								</c:when>
								<c:when test="${APPL_DATA.DELI_ACCS_STATUS eq '03'}">
									<button type="button" class="btn btn-warning btn-rounded" data-toggle="modal" data-target="#modify_myModal"><i class="fa fa-pencil"></i> 수정요청</button>
									<button type="button" class="btn btn-primary btn-rounded" data-toggle="modal" data-target="#deliapp_Modal"><i class="fa fa-file-text"></i> 접수</button>
								</c:when>
							</c:choose>
							<a href="javascript:history.go(-1)" class="btn btn-default btn-rounded"><i class="fa fa-repeat"></i> 목록</a>
						</div>
					</div>
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
<script type="text/javascript">
$(document).ready(function(){
	$('#rootwizard').bootstrapWizard({
		'tabClass': 'nav nav-tabs'
	});
	
	$("#myModal").on('show.bs.modal', function(event){
		var button = $(event.relatedTarget);
		var recipient = button.data('whatever');
		$('#USER_ID').val(recipient);
		
		$('#res_data').empty();
		
		$.ajax({
			type:'post',
			url:"/eirb02/10000202/res_info",
			data:{"USER_ID":recipient},
			dataType:'json',
			async:false,
			success : function(data){
				var table;
				
				table = "<tr><th>권한</th><td colspan='3'>"+data.AUTH_NM
				+"</td></tr><tr><th>부서</th><td>"+data.DEPT_NM
				+"</td><th>직위</th><td>"+data.USER_POS
				+"</td></tr><tr><th>이름</th><td>"+data.USER_NM
				+"</td><th>아이디</th><td>"+data.USER_ID
				+"</td></tr><tr><th>연락처</th><td colspan='3'><i class='icon-call-end'></i> "+data.USER_TEL
				+"    ( <i class='icon-screen-smartphone'></i> "+data.USER_PHONE
				+" )</td></tr><tr><th>이메일</th><td colspan='3'>"+data.USER_EMAIL
				+"</td></tr>";
				
				$('#res_data').append(table);
			}
		});
	});
});

function fn_access(accs_cd,accs_nm){
	var frm = document.deli_access;
	result = confirm('선텍하신 과제를 '+accs_nm+' 하시겠습니까?');
	if(result == true){
		$('#DELI_ACCS_STATUS').val(accs_cd);
		frm.action = "/eirb01/10000103/save";
		frm.submit();
	}else{
		return false;
	}
}
</script>