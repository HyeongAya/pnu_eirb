<!DOCTYPE html>
<html lang="ko">
	<head>
		<jsp:include page="${contextPath}/include/eirb_index/header" />
	</head>
	<body>
		<div id="dc_wrap">
			<div id="dc_contents" class="join_wrap">
				<div class="join_form">
					<div class="join_input">
						<div class="join_title">
							<h1><i class="fa fa-clipboard"></i> 회원가입</h1>
						</div>
						<form id="frm_join" name="frm_join" method="post" action="joinProc">
							<input type="hidden" id="CONFIRM_FG" name="CONFIRM_FG" value="N"/>
							<select id="USER_TYPE_CD" name="USER_TYPE_CD">
								<option value="">사용자 구분</option>
								<option value="02">전문간사</option>
								<option value="06">위원장</option>
								<option value="05">위원</option>
								<option value="07">외부기관위원</option>
								<option value="04">외부연구자</option>
							</select>
							<div style="position:absolute; margin-left:510px; width:100px; ">
								<button type="button" id="btn_id_chk" style="width:90px; padding-left:0px; height:45px; border-radius: 5px; border: solid 1px;">중복확인</button>
							</div>
							<div>
								<input type="text" id="USER_ID" name="USER_ID" maxlength="20" placeholder="아이디 (6자리 이상)"/>
							</div>
							<div>
								<input type="password" id="USER_PW" name="USER_PW" maxlength="20" autocomplete="off" placeholder="비밀번호 (영문+특수문자 조합, 8자리 이상)"/>
							</div>
							<div>
								<input type="password" id="USER_PW_COMP" name="USER_PW_COMP" maxlength="20" autocomplete="off" placeholder="비밀번호 확인"/>
							</div>
							<div>
								<input type="text" id="USER_NM" name="USER_NM" maxlength="20" placeholder="이름"/>
							</div>
							<div>
								<input type="text" id="DEPT_NM" name="DEPT_NM" maxlength="20" placeholder="소속"/>
							</div>
							<div>
								<input type="text" id="USER_POS" name="USER_POS" maxlength="20" placeholder="직위"/>
							</div>
							<div>
								<input type="text" id="USER_TEL" name="USER_TEL" maxlength="13" placeholder="전화번호 (예 : 051-1234-5678)"/>
							</div>
							<div>
								<input type="text" id="USER_PHONE" name="USER_PHONE" maxlength="13" placeholder="휴대폰 (예 : 010-1234-5678)"/>
							</div>
							<div>
								<input type="text" id="USER_EMAIL" name="USER_EMAIL" maxlength="20" placeholder="이메일"/>
							</div>
							<span class="btn_join_area"><a href="#dc_wrap" id="btn_join">회원가입</a><a href="/" class="btn_join_cancel" id="btn_cancel">취소</a></span>
						</form>
					</div>
				</div>
			</div>
			<jsp:include page="${contextPath}/include/eirb_index/footer" />
		</div>
	</body>
	
	<script type="text/javascript">
		var temp_id = "";
		var dupChk = false;
		
		$(document).ready(function() {
			//연락처 유효성 체크 함수 추가
			$.validator.addMethod('customphone', function (value, element) {
				return this.optional(element) || /^\d{2,3}-\d{2,4}-\d{4}$/.test(value);
			}, "유효한 연락처 정보를 입력하세요.");
			
			var $validator = $("#frm_join").validate({
				rules: {
					USER_TYPE_CD: {
						required: true
					},
					USER_ID: {
						required: true
					},
					USER_PW: {
						required: true
					},
					USER_NM: {
						required: true
					},
					DEPT_NM: {
						required: true
					},
					USER_POS: {
						required: true
					},
					USER_PW_COMP: {
						required: true,
						equalTo: '#USER_PW'
					},
					USER_TEL: {
						required: true, 
						customphone: true, 
						minlength: 10, 
						maxlength: 13
					},
					USER_PHONE: {
						required: true, 
						customphone: true, 
						minlength: 10, 
						maxlength: 13
					},
					USER_EMAIL: {
						required: true, 
						email: true, 
						maxlength: 20
					}
				}, 
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					error.addClass( "help-block" );
					
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				},
				submitHandler: function (form) {
				},
			} );	
			
			$('#btn_join').click(function() {
				var $valid = $("#frm_join").valid();
				
				if(!$valid) {
					$validator.focusInvalid();
					return false;
				} else if(dupChk == false || temp_id != $('#USER_ID').val()) {
					alert("사용하실 아이디를 중복확인 해주세요.");
				} else {
					var frm = document.frm_join;
					
					frm.action = "/joinProc";
					frm.submit();
				}
			});
			
			$('#btn_id_chk').click(function() {
				var chkId = $('#USER_ID').val();
				
				if(chkId.length == 0) {
					alert("아이디를 입력해주세요.");
					$('#USER_ID').focus();
					return false;
				}
				if(chkId.length < 5) {
					alert("6자리 이상의 아이디를 입력해주세요.");
					$('#USER_ID').focus();
					return false;
				}
				
				$.ajax({
					url : '/id_chk.json',
					type : 'post',
					data : {"USER_ID" : chkId},
					success : function(data) {
						if(data.chk_value == 'N') {
							alert("사용가능한 아이디입니다.");
							dupChk = true;
							temp_id = chkId;
						} else {
							alert("이미 사용중인 아이디입니다.");
						}
					},
					error : function(data, status, err) {
						alert("아이디 중복조회를 실패했습니다.\n관리자에게 문의 바랍니다.");
					}
				});
			});
		});
	</script>
</html>