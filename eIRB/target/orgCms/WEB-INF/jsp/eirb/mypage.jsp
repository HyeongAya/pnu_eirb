﻿<div class="page-title">
	<h3>개인정보관리</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-lg-7 col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil-square-o"></i> 회원 정보</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="열기/접기" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:setBoundary();" data-toggle="tooltip" data-placement="top" title="새로고침" class="panel-reload"><i class="icon-reload"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<form class="form-horizontal" id="mypage_form" name="mypage_form">
						<input type="hidden" class="form-control" id="USER_TYPE_CD" name="USER_TYPE_CD" value="${MYDATA.USER_TYPE_CD}"/>
						<input type="hidden" class="form-control" id="CONFIRM_FG" name="CONFIRM_FG" value="${MYDATA.CONFIRM_FG}"/>
						<input type="hidden" class="form-control" id="UPD_ID" name="UPD_ID" value="${USER_ID}"/>
						<input type="hidden" class="form-control" id="UPD_IP" name="UPD_IP" value="127.0.0.1"/>

						<div class="form-group">
							<label for="USER_TYPE_CD" class="col-sm-2 control-label">사용자구분</label>
							<div class="col-sm-10"><input type="text" id="AUTH_NM" name="AUTH_NM" class="form-control" value="${MYDATA.AUTH_NM}" readonly="readonly"/></div>
						</div>
						<div class="form-group">
							<label for="USER_ID" class="col-sm-2 control-label">아이디</label>
							<div class="col-sm-10"><input type="text" id="USER_ID" name="USER_ID" class="form-control" value="${MYDATA.USER_ID}" readonly="readonly"/></div>
						</div>
						<div class="form-group">
							<label for="USER_NM" class="col-sm-2 control-label">이름</label>
							<div class="col-sm-10">
								<input type="text" id="USER_NM" name="USER_NM" class="form-control" value="${MYDATA.USER_NM}"/>
							</div>
						</div>
						<div class="form-group">
							<label for="DEPT_NM" class="col-sm-2 control-label">소속</label>
							<div class="col-sm-10">
								<input type="text" id="DEPT_NM" name="DEPT_NM" class="form-control" value="${MYDATA.DEPT_NM}"/>
							</div>
						</div>
						<div class="form-group">
							<label for="USER_POS" class="col-sm-2 control-label">직위</label>
							<div class="col-sm-10">
								<input type="text" id="USER_POS" name="USER_POS" class="form-control" value="${MYDATA.USER_POS}"/>
							</div>
						</div>
						<div class="form-group">
							<label for="USER_TEL" class="col-sm-2 control-label">전화번호</label>
							<div class="col-sm-10">
								<input type="text" id="USER_TEL" name="USER_TEL" class="form-control" maxlength="13" placeholder="(예 : 051-1234-5678)" value="${MYDATA.USER_TEL}"/>
							</div>
						</div>
						<div class="form-group">
							<label for="USER_PHONE" class="col-sm-2 control-label">휴대폰</label>
							<div class="col-sm-10">
								<input type="text" id="USER_PHONE" name="USER_PHONE" class="form-control" maxlength="13" placeholder="(예 : 010-1234-5678)" value="${MYDATA.USER_PHONE}"/>
							</div>
						</div>
						<div class="form-group">
							<label for="USER_EMAIL" class="col-sm-2 control-label">이메일</label>
							<div class="col-sm-10">
								<input type="text" id="USER_EMAIL" name="USER_EMAIL" class="form-control" value="${MYDATA.USER_EMAIL}"/>
							</div>
						</div>
						<div class="form-group">
							<label for="CONFIRM_FG" class="col-sm-2 control-label">승인여부</label>
							<div class="col-sm-10">
								<c:choose>
									<c:when test="${MYDATA.CONFIRM_FG eq 'Y'}"><c:set var="confirm_fg" value="승인"/></c:when>
									<c:otherwise><c:set var="confirm_fg" value="미승인"/></c:otherwise>
								</c:choose>
								<input type="text" id="CONFIRM_FG_KO" name="CONFIRM_FG_KO" class="form-control" value="${confirm_fg}" readonly="readonly"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<button type="button" class="btn btn-info btn-rounded pull-right m-l-xxs" id="btn_modify"><i class="fa fa-check"></i> 회원정보 수정</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-5 col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil-square-o"></i> 비밀번호 변경</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="열기/접기" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:setBoundary();" data-toggle="tooltip" data-placement="top" title="새로고침" class="panel-reload"><i class="icon-reload"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<form class="form-horizontal" id="mypage_pw_form" name="mypage_pw_form">
						<input type="hidden" id="USER_ID" name="USER_ID" value="${MYDATA.USER_ID}"/>
						<input type="hidden" id="USER_PW" name="USER_PW" value="${MYDATA.USER_PW}"/>
						<input type="hidden" id="UPD_ID" name="UPD_ID" value="${USER_ID}"/>
						<input type="hidden" id="UPD_IP" name="UPD_IP" value="127.0.0.1"/>

						<div class="form-group">
							<label for="USER_PW_COMP" class="col-sm-3 control-label">기존 비밀번호</label>
							<div class="col-sm-9"><input type="password" class="form-control" id="USER_PW_COMP" name="USER_PW_COMP" maxlength="20"/></div>
						</div>
						<div class="form-group">
							<label for="NEW_USER_PW" class="col-sm-3 control-label">새 비밀번호</label>
							<div class="col-sm-9"><input type="password" class="form-control" id="NEW_USER_PW" name="NEW_USER_PW" maxlength="20" autocomplete="off" placeholder=" 영문+특수문자 조합, 8자리 이상"/></div>
						</div>
						<div class="form-group">
							<label for="NEW_USER_PW_COMP" class="col-sm-3 control-label">새 비밀번호확인</label>
							<div class="col-sm-9"><input type="password" class="form-control" id="NEW_USER_PW_COMP" name="NEW_USER_PW_COMP" maxlength="20"/></div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<button type="button" class="btn btn-default btn-rounded pull-right m-l-xxs" id="btn_cancel"><i class="fa fa-repeat"></i> 취소</button>
								<button type="button" class="btn btn-success btn-rounded pull-right m-l-xxs" id="btn_lock"><i class="fa fa-lock"></i> 비밀번호 변경</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

<script type="text/javascript">
	
	$(document).ready(function() {
		//연락처 유효성 체크 함수 추가
		$.validator.addMethod('customphone', function (value, element) {
			return this.optional(element) || /^\d{2,3}-\d{2,4}-\d{4}$/.test(value);
		}, "유효한 연락처 정보를 입력하세요.");
		
		//연락처 유효성 체크 함수 추가
		$.validator.addMethod('lock', function (value, element) {
			return this.optional(element) || /^${MYDATA.USER_PW}$/.test(value);
		}, "일치하지 않습니다. 다시 입력하여 주세요.");
		
		var $validator = $("#mypage_form").validate({
			rules: {
				USER_NM: {
					required: true
				},
				DEPT_NM: {
					required: true
				},
				USER_POS: {
					required: true
				},
				USER_TEL: {
					required: true, 
					customphone: true, 
					minlength: 10, 
					maxlength: 13
				},
				USER_PHONE: {
					required: true, 
					customphone: true, 
					minlength: 10, 
					maxlength: 13
				},
				USER_EMAIL: {
					required: true, 
					email: true, 
					maxlength: 50
				}
			}, 
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );
				
				if ( element.prop( "type" ) === "checkbox" ) {
					error.insertAfter( element.parent( "label" ) );
				} else {
					error.insertAfter( element );
				}
			},
			highlight: function ( element, errorClass, validClass ) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
			},
			unhighlight: function (element, errorClass, validClass) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
			},
			submitHandler: function (form) {
			},
		} );	
		
		var $validator_pw = $("#mypage_pw_form").validate({
			rules: {
				USER_PW_COMP: {
					required: true,
					lock: true
				},
				NEW_USER_PW_COMP: {
					required: true,
					equalTo: '#NEW_USER_PW'
				}
			}, 
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );
				
				if ( element.prop( "type" ) === "checkbox" ) {
					error.insertAfter( element.parent( "label" ) );
				} else {
					error.insertAfter( element );
				}
			},
			highlight: function ( element, errorClass, validClass ) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
			},
			unhighlight: function (element, errorClass, validClass) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
			},
			submitHandler: function (form) {
			},
		} );	
		
		$('#btn_modify').click(function() {
			var $valid = $("#mypage_form").valid();
			
			if(!$valid) {
				$validator.focusInvalid();
				return false;
			} else {
				var frm = document.mypage_form;
				
				frm.action = "/eirb00/10000001/mypageProc";
				frm.submit();
			}
		});
		
		$('#btn_lock').click(function() {
			var $valid = $("#mypage_pw_form").valid();
			
			if(!$valid) {
				$validator_pw.focusInvalid();
				return false;
			} else {
				var frm = document.mypage_pw_form;
				result = confirm('비밀번호 변경을 하시겠습니까?');
				if(result == true){
					$('#USER_PW').val($('#NEW_USER_PW').val());
					frm.action = "/eirb00/10000001/pwProc";
					frm.submit();
				}else{
					return false;
				}
			}
		});
		
		$('#btn_cancel').click(function() {
			$('#mypage_pw_form')[0].reset();
			$('em').remove();
		});
	});
</script>