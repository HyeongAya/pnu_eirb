﻿<div class="page-title">
	<h3>부산대학교 생명윤리위원회 전자심의시스템</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<form name="main_form" id="main_form" method="post">
			<input type="hidden" name="EDU_KEYNO" id="EDU_KEYNO"/>
			<input type="hidden" id="SITE_KEYNO" name="SITE_KEYNO" value="${BBSCONF.SITE_KEYNO}">
			<input type="hidden" id="BBS_KEYNO" name="BBS_KEYNO" value="${BBSCONF.BBS_KEYNO}">
			<input type="hidden" id="BBS_TYPE_CD" name="BBS_TYPE_CD" value="${BBSCONF.BBS_TYPE_CD}">
			<input type="hidden" name="BBS_ARTICLE_NO" id="BBS_ARTICLE_NO">
			<input type="hidden" name="DELI_SCHE_KEYNO" id="DELI_SCHE_KEYNO">
		</form>
		<div class="col-lg-8 col-md-12">
			<div class="panel panel-primary">
				<div class="stats-info">
					<div class="panel-heading clearfix">
						<h4 class="panel-title"><i class="fa fa-line-chart"></i> e-IRB 심의 현황 (건수)</h4>
						<div class="panel-control">
							<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Expand/Collapse" class="panel-collapse"><i class="icon-arrow-down"></i></a>
							<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload" class="panel-reload"><i class="icon-reload"></i></a>
							<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Remove" class="panel-remove"><i class="icon-close"></i></a>
						</div>
					</div>
					<div class="panel-body">
						<div class="p-v-xs"></div>
						<ul class="list-unstyled">
							<li>신규과제 심의신청<div class="pull-right">3 건</div></li>
							<li>신규과제 심의면제신청<div class="pull-right">1 건</div></li>
							<li>재심의 신청<div class="pull-right">2 건</div></li>
							<li>계획변경 및 보고신청<div class="pull-right">2 건</div></li>
							<li>심의승인 과제<div class="pull-right">3 건</div></li>
							<li>심의종료 과제<div class="pull-right">0 건</div></li>
							<li>보고대상 과제<div class="pull-right">2 건</div></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-12">
			<div class="panel panel-primary">
				<div class="stats-info">
					<div class="panel-heading clearfix">
						<h4 class="panel-title"><i class="fa fa-cog"></i> 사용자 정보관리</h4>
						<div class="panel-control">
							<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Expand/Collapse" class="panel-collapse"><i class="icon-arrow-down"></i></a>
							<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload" class="panel-reload"><i class="icon-reload"></i></a>
							<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Remove" class="panel-remove"><i class="icon-close"></i></a>
						</div>
					</div>
					<div class="panel-body">
						<div class="p-v-xs"></div>
						<ul class="list-unstyled">
							<li><a href="#"><p class="inbox-item-author"><span class="icon-user"></span> 개인정보 관리</p></a></li>
							<li><a href="#"><p class="inbox-item-author"><span class="icon-docs"></span> 심의신청 이력조회</p></a></li>
							<li><a href="#"><p class="inbox-item-author"><span class="icon-badge"></span> 교육 이수 현황</p></a></li>
							<li><a href="#"><p class="inbox-item-author"><span class="icon-social-dropbox"></span> 기타1</p></a></li>
							<li><a href="#"><p class="inbox-item-author"><span class="icon-social-dropbox"></span> 기타2</p></a></li>
							<li><a href="#"><p class="inbox-item-author"><span class="icon-social-dropbox"></span> 기타3</p></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h3 class="panel-title"><i class="fa fa-calendar"></i> 심의 일정</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Expand/Collapse" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload" class="panel-reload"><i class="icon-reload"></i></a>
						<a href="/eirb01/10000101/" data-toggle="tooltip" data-placement="top" title="Plus"><i class="icon-plus"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<div id="main_calendar"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h3 class="panel-title"><i class="fa fa-bookmark"></i> 공지사항</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Expand/Collapse" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload" class="panel-reload"><i class="icon-reload"></i></a>
						<a href="/eirb05/10000501/1/" data-toggle="tooltip" data-placement="top" title="Plus"><i class="icon-plus"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<table class="table table-striped">
						<colgroup>
							<col style="width:10%;">
							<col/>
							<col style="width:15%;">
							<col style="width:15%;">
						</colgroup>
						<tbody>
							<c:forEach var="bbs_item" items="${BBS_DATA}">
								<tr style="cursor:pointer;" onclick="javascript:goViewPage(${bbs_item.ARTICLE_NO})">
									<td class="text-center"><c:if test="${bbs_item.NOTICE_FG eq 'Y'}"><span class="label label-success">공지</span></c:if></td>
									<td>${bbs_item.TITLE}</td>
									<td class="text-center">${bbs_item.INS_ID}</td>
									<td class="text-center">${bbs_item.INS_DT}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4 col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h3 class="panel-title"><i class="fa fa-graduation-cap"></i> 교육안내</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Expand/Collapse" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload" class="panel-reload"><i class="icon-reload"></i></a>
						<a href="/eirb03/10000301/" data-toggle="tooltip" data-placement="top" title="Plus"><i class="icon-plus"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<table class="table table-striped">
						<colgroup>
							<col style="width:2%;">
							<col/>
							<col style="width:35%;">
							<col style="width:10%;">
							<col style="width:2%;">
						</colgroup>
						<tbody>
							<c:forEach var="edu_item" items="${EDU_DATA}">
								<tr style="cursor:pointer;" onclick="javascript:fn_edu_view('${edu_item.EDU_KEYNO}')">
									<td></td>
									<td>${edu_item.EDU_NM}</td>
									<td class="text-center">${edu_item.EDU_START_DT} ~ ${edu_item.EDU_END_DT}</td>
									<td class="text-center">${edu_item.EDU_ORG_NM}</td>
									<td></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div><!-- Main Wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		$("#main_calendar").fullCalendar({
			lang:"ko",
			header: {
				left: 'prev',
				center: 'title',
				right: 'next'
			},
			defaultDate:moment().format('YYYY-MM-DD'),
			editable: false,
			droppable: false, // this allows things to be dropped onto the calendar
			eventLimit: true, // allow "more" link when too many events
			events: function(start,end,timezone,callback){
				$.ajax({
					type:'post',
					url:"/eirb00/10000001/deli_calendar",
					dataType:'json',
					success : function(data){
						var events = [];
						
						$(data).each(function(){
							var url = 'javascript:getView(\'' + $(this).attr('DELI_SCHE_KEYNO') + '\');';
							events.push({
								title: $(this).attr('DELI_SCHE_NM'),
								start: $(this).attr('SCHE_DT'),
								url: url
							});
						});
						callback(events);
					}
				});
			}
		});
	});
	
	/* 교육안내 조회 이동 */
	function fn_edu_view(param) {
		var form = document.main_form;
		var url = "/eirb03/10000301/view";
		$("#EDU_KEYNO").val(param);
		
		form.action = url;
		form.submit();
	}
	
	/* 공지사항 조회 이동 */
	function goViewPage(articleNo) {
		var form = document.main_form;
		var url = "/eirb05/10000501/1/view";
		$("#BBS_ARTICLE_NO").val(articleNo);
		
		form.action = url;
		form.submit();
	}
	
	/* 일정 조회 이동 */
	function getView(keyno) {
		var form = document.main_form;
		var url = "/eirb01/10000101/view";
		$("#DELI_SCHE_KEYNO").val(keyno);
		
		form.action = url;
		form.submit();
	}
</script>