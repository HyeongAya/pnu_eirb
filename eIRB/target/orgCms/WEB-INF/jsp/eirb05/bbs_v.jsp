﻿<c:set var="u_auth_cd" value="<%=session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_AUTH_CODE) %>" />

<div class="page-title">
	<h3>커뮤니티 | ${u_auth_cd}</h3>
	<!-- 
	<div class="page-breadcrumb">
		<ol class="breadcrumb">
			<li><a href="#">커뮤니티</a></li>
			<li class="active">공지사항</li>
		</ol>
	</div>
	 -->
</div>
<!-- Main Wrapper -->
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-list"></i> ${multiList.dataset1[0].BBS_NM}</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="열기/접기" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="새로고침" class="panel-reload"><i class="icon-reload"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="col-md-12">
						<div class="mailbox-content">
							<div class="message-header">
								<h3>${multiList.dataset1[0].TITLE}</h3>
								<p class="message-date">조회수 : ${multiList.dataset1[0].HIT_CNT}</p>
							</div>
							<div class="message-sender">
								<p>
									<span class="padding_right_20"><i class="fa fa-calendar"></i> 작성일자: ${multiList.dataset1[0].INS_DT}</span>
									<span class="padding_right_20"><i class="fa fa-user"></i> 작성자: ${multiList.dataset1[0].WRITER}</span>
									<c:if test="${'01' eq BBS_TYPE_CD}">
										<span><i class="fa fa-newspaper-o"></i> 게시기간: ${multiList.dataset1[0].ARTICLE_START_DT} ~ ${multiList.dataset1[0].ARTICLE_END_DT}</span>
									</c:if>
								</p>
							</div>
							<div class="message-content bbs_textarea">
								<c:out value="${multiList.dataset1[0].CONTENT}" escapeXml="false"/>
							</div>
							<div class="message-attachments">
								<p>
									<i class="fa fa-paperclip m-r-xs"></i>첨부파일 ${fn:length(multiList.dataset2)} 개
								</p>
								<c:forEach var="fileItem" items="${multiList.dataset2}" varStatus="status">
									<div class="message-attachment">
										<a href="${fileItem.FILE_URL}" title="${fileItem.FILE_NM}">
											<div class="attachment-info">
												<p class="text_overflow_ellipsis">${fileItem.FILE_NM}.${fileItem.FILE_EXT}</p>
												<span>${fileItem.FILE_SIZE} KB</span>
											</div>
										</a>
									</div>
								</c:forEach>
							</div>
							
							<div class="message-options pull-right">
								<c:if test="${'02' eq BBS_TYPE_CD}">
									<button type="button" class="btn btn-success btn-rounded m-l-xxs" onclick="goReply();"><i class="fa fa-reply m-r-xs"></i> 답글</button>
								</c:if>
								
								<c:if test="${u_auth_cd eq '99' or u_auth_cd eq '01'}">
									<button type="button" class="btn btn-warning btn-rounded m-l-xxs" onclick="goWritePage(${multiList.dataset1[0].ARTICLE_NO});"><i class="fa fa-check"></i> 수정</button>
									<button type="button" class="btn btn-danger btn-rounded m-l-xxs" onclick="goDelete();"><i class="fa fa-trash"></i> 삭제</button>
									<button type="button" class="btn btn-default btn-rounded m-l-xxs" onclick="goList(${multiList.dataset1[0].BBS_KEYNO});"><i class="fa fa-repeat"></i> 목록</button>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<form id="bbsForm" name="bbsForm" method="POST">
		<input type="hidden" id="SITE_KEYNO" name="SITE_KEYNO" value="${SITE_KEYNO}">
		<input type="hidden" id="BBS_KEYNO" name="BBS_KEYNO" value="${BBS_KEYNO}">
		<input type="hidden" id="BBS_TYPE_CD" name="BBS_TYPE_CD" value="${BBS_TYPE_CD}">
		<input type="hidden" id="BBS_ARTICLE_NO" name="BBS_ARTICLE_NO" value="${multiList.dataset1[0].ARTICLE_NO}">
	</form>
</div>
<!-- Main Wrapper -->
<script src="${contextPath}/assets/js/bbs/bbs.js"></script>
<script type="text/javascript">
$(document).ready(function() {
});
</script>