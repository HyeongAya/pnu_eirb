﻿<c:set var="u_auth_cd" value="<%=session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_AUTH_CODE) %>" />

<%-- <link type="text/css" href="${contextPath}/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet"> --%>
<div class="page-title">
	<h3>커뮤니티 | ${u_auth_cd}</h3>
	<!-- 
	<div class="page-breadcrumb">
		<ol class="breadcrumb">
			<li><a href="#">커뮤니티</a></li>
			<li class="active">공지사항</li>
		</ol>
	</div>
	 -->
</div>
<!-- Main Wrapper -->
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-list"></i> ${BBSCONF.BBS_NM}</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="열기/접기" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="새로고침" class="panel-reload"><i class="icon-reload"></i></a>
					</div>
			    </div>
				<div class="panel-body">
					<div class="row m-t-md">
						<!-- 게시글 목록 -->
						<div class="col-md-12">
							<div class="table-responsive">
								<table id="tbl_bbsType01_l" class="display table width100per">
									<thead>
										<tr>
											<th class="text-center">번호</th>
											<th class="text-center">제목</th>
											<th class="text-center">작성자</th>
											<th class="text-center" id="th_noticeTerm">게시기간</th>
											<th class="text-center">등록일자</th>
											<th class="text-center">조회수</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							
							<!-- 버튼 -->
							<div class="form-group margin_top_10">
							<c:choose>
								<c:when test="${BBSCONF.BBS_TYPE_CD eq '01'}">
									<c:if test="${u_auth_cd eq '99' or u_auth_cd eq '01'}">
									<div class="col-sm-12">
										<button type="button" class="btn btn-info btn-rounded pull-right m-l-xxs" onclick="goWritePage();"><i class="fa fa-pencil"></i> 글쓰기</button>
									</div>
									</c:if>
								</c:when>
								<c:otherwise>
									<div class="col-sm-12">
										<button type="button" class="btn btn-info btn-rounded pull-right m-l-xxs" onclick="goWritePage();"><i class="fa fa-pencil"></i> 글쓰기</button>
									</div>
								</c:otherwise>
							</c:choose>
							</div>
							<!--// 버튼 -->
						</div>
						<!--// 게시글 목록 -->
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<form id="bbsForm" name="bbsForm" method="POST">
		<input type="hidden" id="SITE_KEYNO" name="SITE_KEYNO" value="${BBSCONF.SITE_KEYNO}">
		<input type="hidden" id="BBS_KEYNO" name="BBS_KEYNO" value="${BBSCONF.BBS_KEYNO}">
		<input type="hidden" id="BBS_TYPE_CD" name="BBS_TYPE_CD" value="${BBSCONF.BBS_TYPE_CD}">
		<input type="hidden" id="BBS_ARTICLE_NO" name="BBS_ARTICLE_NO" value="">
		<!-- <input type="hidden" id="BBS_STATESAVE" name="BBS_STATESAVE" value=""> -->
	</form>
</div>
<!-- Main Wrapper -->
<script src="${contextPath}/assets/js/bbs/bbs.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var siteKeyno = $('#SITE_KEYNO').val();
		var bbsKeyno = $('#BBS_KEYNO').val();
		var bbsTypeCd = $('#BBS_TYPE_CD').val();
		//var bbsStateSave = $('#BBS_STATESAVE').val();
		
		//getBbsArticleList(siteKeyno, bbsKeyno, bbsTypeCd, bbsStateSave);
		getBbsArticleList(siteKeyno, bbsKeyno, bbsTypeCd);
	});
</script>