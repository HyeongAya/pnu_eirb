﻿<div class="page-title">
	<h3>교육관리</h3>
	<!-- 
	<div class="page-breadcrumb">
		<ol class="breadcrumb">
			<li><a href="#">공통관리</a></li>
			<li class="active">관리자관리</li>
		</ol>
	</div>
	 -->
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-calendar"></i> 교육 일정 리스트</h3>
				</div>
				<div class="panel-body">
					<form name="edu_rec_list_form" id="edu_rec_list_form" method="post">
						<input type="hidden" name="EDU_KEYNO" id="EDU_KEYNO"/>
					</form>
					<div class="p-v-xs"></div>
					<div class="table-responsive">
						<table id="data_table" class="display table" style="width: 100%; cellspacing: 0;">
						<colgroup>
							<col style="width:5%;">
							<col style="width:25%;">
							<col style="width:15%;">
							<col style="width:15%;">
							<col style="width:10%;">
							<col style="width:12%;">
							<col style="width:10%;">
							<col style="width:8%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col" class="text-center">교육번호</th>
								<th scope="col" class="text-center">교육명</th>
								<th scope="col" class="text-center">신청기간</th>
								<th scope="col" class="text-center">교육기간</th>
								<th scope="col" class="text-center">교육시간</th>
								<th scope="col" class="text-center">교육주최기관</th>
								<th scope="col" class="text-center">강사명</th>
								<th scope="col" class="text-center">접수인원</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${DATA}">
							<tr style="cursor:pointer;" onclick="javascript:fn_view('${item.EDU_KEYNO}')">
								<td>${item.EDU_KEYNO}</td>
								<td>${item.EDU_NM}</td>
								<td class="text-center">${item.EDU_REC_START_DT} ~ ${item.EDU_REC_END_DT}</td>
								<td class="text-center">${item.EDU_START_DT} ~ ${item.EDU_END_DT}</td>
								<td class="text-center">${item.EDU_START_TM} ~ ${item.EDU_END_TM}</td>
								<td class="text-center">${item.EDU_ORG_NM}</td>
								<td class="text-center">${item.EDU_PROF_NM}</td>
								<td class="text-center">${item.APPLY_CNT} / ${item.EDU_PER}</td>
							</tr>
							</c:forEach>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

<script type="text/javascript">
	$(document).ready(function() {
		var req  = new custom_request();
		var msg  = req.getParameter('message');
		
		if(msg != '') {
			alert(msg);
		}
		
		$('#data_table').DataTable({
			"search": { caseInsensitive: false },
			"order": [[ 2, 'desc' ]],
			"columnDefs": [
				{"targets" : 0, "searchable": true, "orderable" : true},
				{"targets" : 1, "searchable": true, "orderable" : true},
				{"targets" : 2, "searchable": true, "orderable" : true},
				{"targets" : 3, "searchable": true, "orderable" : true},
				{"targets" : 4, "searchable": true, "orderable" : true},
				{"targets" : 5, "searchable": true, "orderable" : true},
				{"targets" : 6, "searchable": true, "orderable" : true},
			]
		});
	});
	
	// 상세조회
	function fn_view(param) {
		var frm = document.edu_rec_list_form;
		$("#EDU_KEYNO").val(param);
		frm.action = "/eirb03/10000302/view";
		frm.submit();
	}
	
</script>