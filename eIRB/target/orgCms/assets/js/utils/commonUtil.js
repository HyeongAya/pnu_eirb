Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).lpad(2, '0');
            case "MM": return (d.getMonth() + 1).lpad(2, '0');
            case "dd": return d.getDate().lpad(2, '0');
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().lpad(2, '0');
            case "hh": return ((h = d.getHours() % 12) ? h : 12).lpad(2, '0');
            case "mm": return d.getMinutes().lpad(2, '0');
            case "ss": return d.getSeconds().lpad(2, '0');
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};

Date.prototype.addYear = function(year) {
    this.setDate(this.getFullYear() + year);
    return this;
};

Date.prototype.addMonth = function(month) {
    this.setDate(this.getMonth() + month);
    return this;
};

Date.prototype.addDay = function(day) {
    this.setDate(this.getDate() + day);
    return this;
};

Date.prototype.addMin = function(min) {
    this.setMinutes(this.getMinutes() + min);
    return this;
};

String.prototype.lpad = function(len, pad) {
	var ret = this;
	for(var idx = ret.length; idx < len; idx++) {
		ret = pad + ret;
	}
	return ret;
};
String.prototype.rpad = function(len, pad) {
	var ret = this;
	for(var idx = ret.length; idx < len; idx++) {
		ret = ret + pad;
	}
	return ret;
};
Number.prototype.lpad = function(len, pad) {
	return this.toString().lpad(len, pad);
};
Number.prototype.rpad = function(len, pad) {
	return this.toString().rpad(len, pad);
};

function getTime(timeToggle) {
	var hh = new Date().getHours() + '';
	var mi = new Date().getMinutes() + '';
	var tm = '';
	
	if(timeToggle) {
		tm = hh.lpad(2, '0') + ' <span>:</span> ' + mi.lpad(2, '0');
	} else {
		tm = hh.lpad(2, '0') + ' <span></span> ' + mi.lpad(2, '0');
	}
		
	return tm;
}

function popupOpen(url, width, height){
	var popOption = 'width=' + width + ', height=' + height + ', resizable=yes, scrollbars=yes, status=no;';
	window.open(url,"",popOption);
}
