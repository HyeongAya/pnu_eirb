var LogOutTimer = function() {
	var S = {
		timer : null,
		limit : 1000 * 60 * 90,
		fnc   : function() {},
		start : function() {
			S.timer = window.setTimeout(S.fnc, S.limit);
		},
		reset : function() {
			window.clearTimeout(S.timer);
			S.start();
		},
		stop : function() {
			window.clearTimeout(S.timer);
		}
	};
    
	document.onclick = function() { S.reset(); };
	document.onkeypress = function() { S.reset(); };
    
	return S;
}();
  
// 로그아웃 체크시간 설정
LogOutTimer.limit = 1000 * 60 * 90;
  
// 로그아웃 함수 설정
LogOutTimer.fnc = function() {
	document.location.href = '/logout';
}

$(document).ready(function() {
	
	$('a[href=#dc_contents_wrap]').click(function(e) {
		e.preventDefault();
	});

	$(window).scroll(function() {
		$('#dc_fixed_quick').addClass('dc_fixed_quick');
		if ($(document).scrollTop() < 145) {
			$('#dc_fixed_quick').removeClass('dc_fixed_quick');
		}
	});

	$(window).resize(function() {
		resizeContent(540);
	});

	$('.dc_table_h td:last-child').css('border-right-width', '1px');
	$('.dc_table_hv td:last-child').css('border-right-width', '1px');
	$('.dc_table_v td:last-child').css('border-right-width', '1px');
	$("#dc_mm_flip a").click(function() {
		$("#dc_mm_panel").slideToggle("slow");
		resizeContent(540);
	});

	resizeContent(540);

	/* side nav */
	$('#dc_sub_nav_acc > ul > li:has(ul)').addClass("has-sub");

	$('#dc_sub_nav_acc > ul > li > a').click(function() {
		fn_sideMenuClickHdlr(this);
	});

	$('.dc_button_block .wide').parent().click(function() {
		$('#dc_contents_wrap').css('width', '');
		resizeContent(540);
	});

	$('.dc_button_block .narrow').parent().click(function() {
		$('#dc_contents_wrap').css('width', '800px');
		resizeContent(540);
	});
});

function fn_sideMenuClickHdlr(target) {
	var checkElement = $(target).next();
	$('#dc_sub_nav_acc li').removeClass('active');
	$(this).closest('li').addClass('active');
	if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
		$(this).closest('li').removeClass('active');
		checkElement.slideUp('normal');
	}
	if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
		$('#dc_sub_nav_acc ul ul:visible').slideUp('normal');
		checkElement.slideDown('normal');
	}
	if (checkElement.is('ul')) {
		return false;
	} else {
		return true;
	}
}

function resizeContent(minHeight, addHeight) {
	var centerHeight;
	
	centerHeight = $("#dc_contents_wrap").height();
	
	if(minHeight != undefined && parseInt(centerHeight, 10) < 800) {
		minHeight=850;
	}
	
	if (minHeight != undefined && parseInt(centerHeight, 10) < minHeight) {
		centerHeight = minHeight;
	}
	if (addHeight != undefined) {
		centerHeight = parseInt(centerHeight, 10) + addHeight;
	}

	$('#dc_sub_nav_wrap').css({
		'height' : (centerHeight) + 'px'
	});
	$('#dc_container').css({
		'height' : (centerHeight) + 'px'
	});
	$('#dc_quick_wrap').css({
		'height' : (centerHeight) + 'px'
	});
}

function fixedSizeContent(centerHeight) {
	$('#dc_sub_nav_wrap').css({
		'height' : (centerHeight) + 'px'
	});
	$('#dc_container').css({
		'height' : (centerHeight) + 'px'
	});
	$('#dc_quick_wrap').css({
		'height' : (centerHeight) + 'px'
	});
}

function setCookie(cName, cValue, cDay){
    var expire = new Date();
    
    expire.setDate(expire.getDate() + cDay);
    cookies = cName + '=' + escape(cValue) + '; path=/ ';
    
    if(typeof cDay != 'undefined') {
    	cookies += ';expires=' + expire.toGMTString() + ';';
    }
    
    document.cookie = cookies;
}

function getCookie(cName) {
    cName = cName + '=';
    
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cName);
    var cValue = '';
    
    if(start != -1) {
		start += cName.length;
		
		var end = cookieData.indexOf(';', start);
		
		if(end == -1) {
			end = cookieData.length;
		}
		
		cValue = cookieData.substring(start, end);
    }
    
    return unescape(cValue);
}

function deleteCookie(cName) {
	var expire = new Date();
	 
	expire.setDate(expire.getDate() - 1);
	document.cookie = cName + "= " + "; expires=" + expire.toGMTString() + "; path=/";
}

function setMask(id, viewLoader) {
	$('#' + id).append($('<div>').css({
        position			: 'fixed',
        width				: '100%',
        height				: '100%',
        'background-color'	: '#000',
        opacity				: 0.6,
        'z-index'			: 999,
        top					: 0,
        left				: 0,
        'text-align'		: 'center'
    }).attr('id', id + '_cover'));
	if(viewLoader) {
		$('#' + id + '_cover').append($('<img>').attr({'src' : '/img/common/loader.gif', 'alt' : '로딩중'}).css({'position' : 'absolute', 'top' : '50%', 'left' : (($('#' + id + '_cover').width() / 2) - 64) + 'px'}));
	}
}

function setUnMask(id) {
	$('#' + id + '_cover').remove();
}