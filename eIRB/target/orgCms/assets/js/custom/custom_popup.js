var CustomPop = (function() {

	var _m_id = '';
	var _closeCallback_f;
	var _dcConHeight = "";
	/*
	function _minimize() {
		$('#' + _m_id).toggle('blind', {}, 500);
	}
	
	function _open() {
		$('#' + _m_id).parent('.dc_layer_wrap').css({'left': 0,'top': -155, 'position': 'absolute'});
		$('#' + _m_id).toggle(true);
		$('#' + _m_id).parent('.dc_layer_wrap').show();
		
		$('a[href=#dc_contents_wrap]').click(function(e) {
			e.preventDefault();
		});
	};
	
	function _close() {
		$('#' + _m_id).parent('.dc_layer_wrap').hide();
		if($('#' + _m_id).parents("form").length > 0) {
			$('#' + _m_id).parents("form")[0].reset();
		}
		
		$('#' + _m_id).parents("form > *").find('input, select, textarea').each(function() {
			$(this).rules('remove');
			if($(this).prop('readonly')) {
				$(this).prop('readonly', false);
			}
		});

		if(typeof _closeCallback_f == 'function') {
			_closeCallback_f();
		}
	};
	*/
	function _init(id, title) {
		_m_id = id;
		
		//$('#' + _m_id).parent('.dc_layer_wrap').hide();
		//$('#' + _m_id).parent('.dc_layer_wrap').css('position', 'absolute');
	
		var head = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="'+title+'Label">' + title + '</h4></div>';
		var body = $('#' + _m_id).parent('.modal-layer').html();
		
		$('#' + _m_id).parent('.modal-layer').html(head + body);
	};

	return {
		closeCallback: function(targetF) {
			_closeCallback_f = targetF;
		},
		width: function(width) {
			$('#' + _m_id).parent('.modal-layer').width(width);
		},
		height: function(height) {
			$('#' + _m_id).parent('.modal-layer').height(height);
		},
		popClose: function() {
			_close();
		},
		popOpen: function() {
			_open();
		},
		init: function(id, title) {
			_init(id, title);
		}
	}

});