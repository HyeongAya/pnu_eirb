var custom_request = (function() {

	this.getParameter = function(name) {
		var rtnval = '';
		var nowAddress = decodeURIComponent(location.href);
		var parameters = (nowAddress.slice(nowAddress.indexOf('?') + 1, nowAddress.length)).split('&');
		
		for(var idx = 0; idx < parameters.length; idx++) {
			var varName = parameters[idx].split('=')[0];
			
			if(varName.toUpperCase() == name.toUpperCase()) {
				rtnval = parameters[idx].split('=')[1];
				break;
			}
		}
		
		return rtnval.replace(/\+/gi, ' ');
	}

});