/**
 * @function createElement
 * @param tag : string : DOM 객체의 태그 이름. 비어있을 경우 빈 div 엘리먼트를 리턴한다.
 * @param options : object : DOM 객체의 여러가지 옵션들을 지정한다.
 * @param options.id : string : DOM 객체의 id
 * @param options.cls : [string | array] : DOM 객체의 class를 설정할 수 있다. 여러개의 class를 가지고 있다면 array로 넘기면 됨.
 * @param options.text : string : DOM 객체의 text
 * @param options.attr : object : DOM 객체의 속성을 지정할 수 있다.
 * @param options.css : object : DOM 객체의 style을 지정할 수 있다.
 * @returns jQuery DOM object
 */
function createElement(tag, options) {
    if (!tag) return $('<div />');
    var $elem = $('<' + tag + '/>');
    if (options){
        if (options.id)
            $elem.attr('id', options.id);
        if (options.cls) {
            if (typeof options.cls == 'null' || typeof options.cls == 'undefined') {
                // do nothing
            } else if (typeof options.cls == 'string') {
                $elem.addClass(options.cls);
            } else if (typeof options.cls == 'object') {
                for ( var i in options.cls) {
                    $elem.addClass(options.cls[i]);
                }
            }
        }
        if (options.text)
            $elem.text(options.text);
        if (typeof options.attr != 'null' && typeof options.attr == 'object') {
            for (var i in options.attr) {
                $elem.attr(i, options.attr[i]);
            }
        }
        if (typeof options.css != 'null' && typeof options.css == 'object') {
            for (var i in options.css) {
                $elem.css(i, options.css[i]);
            }
        }
    }
    return $elem;
}