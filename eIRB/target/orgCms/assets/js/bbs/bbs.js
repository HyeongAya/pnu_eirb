/*
 * 공백문자열 체크
 */
function nvl(str) {
	if (str == null || str == undefined) {
		return "";
	} else {
		return str;
	}
}

var bbs = (function() {
	var siteKeyno = function () {
		return nvl($("#SITE_KEYNO").val());
	}
	
	var bbsTypeCd = function () {
		return nvl($("#BBS_TYPE_CD").val());
	}

	var bbsKeyno = function () {
		return nvl($("#BBS_KEYNO").val());
	}
	
	var bbsListTypeGcd = function () {
		return "01";
	}
	
	var bbsTblId = function () {
		return "#tbl_bbsType" + bbsListTypeGcd() + "_l";
	}
	
	return {
		siteKeyno : siteKeyno,
		bbsTypeCd : bbsTypeCd,
		bbsKeyno : bbsKeyno,
		bbsListTypeGcd : bbsListTypeGcd,
		bbsTblId : bbsTblId
	};
	
}());


/* 게시글 목록 이동 */
function goList(bbsKeyno) {
	var form = document.bbsForm;
	var url = "/eirb05/1000050"+ bbsKeyno + "/" + bbsKeyno + "/";
	
	form.action = url;
	form.submit();
}

/* 게시글 조회 이동 */
function goViewPage(articleNo) {
	var form = document.bbsForm;
	var url = "/eirb05/1000050"+ bbs.bbsKeyno() + "/" + bbs.bbsKeyno() + "/view";
	$("#BBS_ARTICLE_NO").val(articleNo);
	
	form.action = url;
	form.submit();
}

/* 게시글 쓰기/수정 이동 */
function goWritePage(articleNo) {
	var form = document.bbsForm;
	var url = "/eirb05/1000050"+ bbs.bbsKeyno() + "/" + bbs.bbsKeyno() + "/";
	
	if (nvl(articleNo) == "") {
		url += "write";
	} else {
		url += "modify";
	}
	form.action = url;
	form.submit();
}


/* 게시글 목록 */
function getBbsArticleList(siteKeyno, bbsKeyno, bbsTypeCd) {
	eval("getBbsArticleList" + bbs.bbsListTypeGcd() + "(siteKeyno, bbsKeyno, bbsTypeCd);");
	// 향후 게시판타입 추가 고려
}

/* 공지사항,QnA,FAQ,양식함 게시글 목록 */
function getBbsArticleList01(siteKeyno, bbsKeyno, bbsTypeCd) {
	$("#tbl_bbsType01_l").DataTable({
		/*language: {
			"lengthMenu": "_MENU_ 건씩 보기",
			"zeroRecords": "조회된 데이터가 없습니다.",
			"info": "_PAGE_ / _PAGES_ 페이지",
			"infoEmpty": "조회된 데이터가 없습니다.",
			"infoFiltered": "(총 _MAX_ 건의 항목에서 검색 됨)",
			"search": "검색어 (제목/내용):",
			"paginate": {
				"sFirst":	"처음",
				"sLast":	 "마지막",
				"sNext":	 "다음",
				"sPrevious": "이전"
			}
		},*/
		destroy: true,
		processing : true,
		serverSide: true,
		ordering: false,
		ajax: {
			url: "/bbs/common/"+bbsKeyno+"/" + bbsTypeCd + "/articleList.json",
			type : "POST",
			dataType : "json",
			data: function (d) {
				var table = $("#tbl_bbsType01_l").DataTable();
				var info = table.page.info();
				var searchKeyword = d.search.value;
				var pageSize = d.length;
				var pageIndex = (info.page + 1);
				var parameters = {
					"SITE_KEYNO":siteKeyno,
					"BBS_KEYNO":bbsKeyno,
					"SEARCH_KEYWORD":searchKeyword,
					"PAGE_SIZE":pageSize,
					"PAGE_INDEX":pageIndex,
				};
				return JSON.stringify(parameters);
			},
			contentType : "application/json",
			dataFilter: function(data){
				var json = $.parseJSON(data);
				var listSize = json.list.length;
				var totalCnt = 0;
				if (listSize > 0) {
					totalCnt = json.list[0].TOTAL_CNT;
				}
				json.recordsTotal = totalCnt;
				json.recordsFiltered = totalCnt;
				json.data = json.list;
				
				return JSON.stringify(json);
			}
		},
		createdRow: function (row, data, index){
			if (data.LEVEL > 1) {
				$(row).addClass("bg_lightgrey");
			}
			if ("01" != data.BBS_TYPE_CD) {
				$("td", row).eq(3).addClass("hide");
				$("#th_noticeTerm").addClass("hide");
			} else {
				$("td", row).eq(3).removeClass("hide");
				$("#th_noticeTerm").removeClass("hide");
			}
		},
		columnDefs: [
			 {targets: 0, width: "8%", className: "text-center"},
			 {targets: 1, width: "35%"},
			 {targets: 2, width: "12%", className: "text-center"},
			 {targets: 3, width: "15%", className: "text-center"},
			 {targets: 4, width: "12%", className: "text-center"},
			 {targets: 5, width: "8%", className: "text-center"}
		],
		columns: [
			{data: "ROWNUM", defaultContent: "-"},
			{ 
				data: "TITLE", 
				defaultContent: "-",
				render: function (data, type, row) {
					var html = "";
					
					if ("Y" == row.NOTICE_FG) {
						html += "<span class='label label-success'>공지</span> ";
					}
					
					var level = row.LEVEL;
					if (1 < level) {
						for(i=1; i < level; i ++) {
							html += "<i class='glyphicon glyphicon-arrow-right icn-xs text-primary' aria-hidden='true'></i> ";
						}
					}
					
					var hrefHtml = "javascript:goViewPage(" + row.ARTICLE_NO + ");";
					html += "<a href='" + hrefHtml + "'>" + data + "</a>";
					
					if (0 < row.FILE_CNT) {
						html += " <i class='fa fa-paperclip'></i>";
					}
					
					if ("Y" == row.NEW_FG) {
						html += " <i class='fa fa-star icon-state-warning'></i>";
					}
					
					return html;
				}
			},
			{ data: "INS_ID", defaultContent: "-" },
			{ 
				data: "ARTICLE_START_DT", 
				defaultContent: "-",
				render: function (data, type, row) {
					var html = "";
					if(row.ARTICLE_END_DT != null && "9999" != (row.ARTICLE_END_DT).substring(0,4)) {
						html += nvl(data) + " ~ " + nvl(row.ARTICLE_END_DT);
					} else {
						html += "영구게시";
					}
					return html;
				}
			},
			{ data: "INS_DT", defaultContent: "-" },
			{ data: "HIT_CNT", defaultContent: "-" }
		]
	});
}


/* 게시글 삭제 */
function goDelete(){
	if (confirm('게시물을 삭제하시겠습니까?')) {
		var form = document.bbsForm;
		var url = "/pnu04/10000409/delete";
		form.action = url;
		form.submit();
	} 
}