var CorePaginate = (function(options) {
	
	var _options = {
		'totalCount':0,
		'pagePerCount':0,
		'currentPage':1,
		'classId':'',
		'parentObjectId':'',
		'pageFirstCls':'dc_pg_first',
		'pagePrevCls':'dc_pg_prev',
		'pageNextCls':'dc_pg_next',
		'pageEndCls':'dc_pg_end',
		'pageFirstStr':'맨앞',
		'pagePrevStr':'이전',
		'pageNextStr':'다음',
		'pageEndStr':'맨뒤',
		'change':null
	};
	
	$.extend( _options, options );	
	
	function _fn_draw() {
		var totalPage	= Math.ceil(parseInt(_options.totalCount, 10) / parseInt(_options.pagePerCount, 10));
		var htmlStr		= '';
		
		$('#' + _options.parentObjectId).html('');

		htmlStr += '<span><button type="button" title="' + _options.pageFirstStr + '" class="' + _options.classId + ' ' + _options.pageFirstCls + '" page="1"><span>' + _options.pageFirstStr + '</span></button></span> ';
		htmlStr += '<span><button type="button" title="' + _options.pagePrevStr + '" class="' + _options.classId + ' ' + _options.pagePrevCls + '" page="' + ((parseInt(_options.currentPage, 10) - 1) < 1 ? 1:(parseInt(_options.currentPage, 10) - 1)) + '"><span>' + _options.pagePrevStr + '</span></button></span> ';
		
		var startPage	= parseInt(_options.currentPage, 10) - 5;
		var endPage		= parseInt(_options.currentPage, 10) + 4;
		
		if(totalPage - endPage < 0) {
			startPage = totalPage - 9;
		}
		
		if(totalPage <= 10) {
			startPage	= 1;
			endPage		= totalPage;
		} else {
			if(startPage < 1) {
				startPage	= 1;
				endPage		= 10;
			} else {
				if(endPage > totalPage) {
					endPage	= totalPage;
				}
			}
		}
		
		for(var idx = startPage; idx <= endPage; idx++) {
			var pageNum = idx + '';
			
			if(parseInt(pageNum, 10) == _options.currentPage) {
				htmlStr += '<strong>' + pageNum + '</strong> ';
			} else {
				
				htmlStr += '<a href="#' + _options.parentObjectId + '" class="' + _options.classId + '" page="' + pageNum + '">' + pageNum + '</a> ';
			}
		}
		
		htmlStr += '<span><button type="button" title="' + _options.pageNextStr + '" class="' + _options.classId + ' ' + _options.pageNextCls + '" page="' + ((parseInt(_options.currentPage, 10) + 1) > parseInt(totalPage, 10) ? parseInt(totalPage, 10):(parseInt(_options.currentPage, 10) + 1)) + '"><span>' + _options.pageNextStr + '</span></button></span> ';
		htmlStr += '<span><button type="button" title="' + _options.pageEndStr + '" class="' + _options.classId + ' ' + _options.pageEndCls + '" page="' + totalPage + '"><span>' + _options.pageEndStr + '</span></button></span>';
		
		$('#' + _options.parentObjectId).append(htmlStr);
		
		$('.' + _options.classId).click(function(e) {
			e.preventDefault();
			
			_options.currentPage = $(this).attr('page');
			_fn_draw();
			
			if(typeof _options.change === 'function') {
				_options.change(_options.currentPage);
			}
		});
	}
    
	return {
		set setCurrentPage(currentPage) {
			_options.currentPage = currentPage;
		},
		get getCurrentPage() {
			return _options.currentPage;
		},
		set setTotalCount(totalCount) {
			_options.totalCount = totalCount;
		},
		set setPagePerCount(pagePerCount) {
			_options.pagePerCount = pagePerCount;
		},
		init: function() {
			_fn_draw();
		},
		redraw: function() {
			_fn_draw();
		}
	}
	
});
