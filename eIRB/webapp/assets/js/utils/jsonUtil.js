function getJSONData(target_obj) {
	var dataArray = new Array();
	
	for (var idx1 = 0; idx1 < target_obj.length; idx1++) {
		var obj		= JSON.parse(JSON.stringify(target_obj[idx1].serializeArray()));
		var jsonObj	= new Object();
		
		for(var idx2 = 0; idx2 < obj.length; idx2++) {
			jsonObj[obj[idx2].name] = obj[idx2].value;
		}
		
		dataArray.push(jsonObj);
	}
	
	return JSON.stringify(dataArray);
}