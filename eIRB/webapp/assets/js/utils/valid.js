/*
 * ## 옵션 리스트 ##
 * 
 * required:	필수값체크				{true|false}
 * remote:		DB값비교				{ajax형식 API참조(http://jqueryvalidation.org/remote-method)}
 * email:		이메일 형식				{true|false}
 * url:			url 형식				{true|false}
 * date:		날짜 형식				{true|false}
 * dateISO:		날짜 형식( ISO )		{true|false}
 * number:		실수 형식				{true|false}
 * digits:		숫자만 입력				{true|false}
 * creditcard:	신용카드 형식			{true|false}
 * equalTo:		동일값 입력체크			{비교할 Object [예) #sample]}
 * maxlength:	최대 글자수				{최대글자수}
 * minlength:	최소 글자수				{최소글자수}
 * rangelength:	입력가능한 글자수 범위	{최소글자수, 최대글자수}
 * range:		입력가능한 숫자 범위	{최소숫자, 최대숫자}
 * max:			최대숫자				{최대숫자}
 * min:			최소숫자				{최소숫자}
 * kor:			한글입력체크			{true:false}
 * eng:			영문입력체크			{true:false}
 * kor_eng:		한글 및 영문입력체크	{true:false}
 * ssn:			주민번호 형식 체크		{true:false}
 * bssn:		사업자번호 형식 체크	{true:false}
 * 
 * ## 사용법 ##
 * - 추가 -
 * $("#sample").rules("add", {
 * 	   required: true,
 *     minlength: 2,
 *     messages: {
 *         minlength: jQuery.format("최소 입력길이는 {0}자 입니다.")
 *     }
 * });
 * - 삭제 -
 * $("#sample").rules("remove");
 * $("#sample").rules("remove", "required minlength");
 */

$(document).ready(function() {    
    jQuery.validator.setDefaults({
//    	debug: true,
    	onclick: false,
    	onkeyup: function(element) {
    		if(element.value.trim() != '') {
        		$(element).valid();	
    		} 
			G_BEFORE_INPUT_VALUE = element.value; 
    	},
		showErrors:function(errorMap, errorList){
			if(this.numberOfInvalids()) {
				alert(errorList[0].message);
				if(errorList[0].method == 'number' || errorList[0].method == 'digits' || errorList[0].method == 'maxlength' || 
						errorList[0].method == 'min' || errorList[0].method == 'max' || errorList[0].method == 'range' || 
						errorList[0].method == 'kor' || errorList[0].method == 'eng' || errorList[0].method == 'kor_eng') {
					$(errorList[0].element).val(G_BEFORE_INPUT_VALUE);
					$(errorList[0].element).focus();
				}
			}
		}
//        errorPlacement: function(error, element) {
//            var pos = element.offset();
//             
//            error.css({
//                left: pos.left,
//                top: pos.top + 20
//            });
//             
//            error.insertAfter(element);
//        }
    });	

    jQuery.validator.addMethod('required2', function (value, element, params) {
        return (value.trim() != '');
    }, '{0}은(는)필수 입력 항목입니다.');   
    
    $('form').each(function() {
        $(this).validate();
    });
});
