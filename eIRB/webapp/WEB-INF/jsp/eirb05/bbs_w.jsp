﻿<c:set var="u_auth_cd" value="<%=session.getAttribute(ProjectConstant.KEY_SESSION_MEMBER_AUTH_CODE) %>" />

<!-- datetimepicker -->
<link rel="stylesheet" href="${contextPath}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" media="all">
<script type="text/javascript" src="${contextPath}/assets/plugins/bootstrap-datetimepicker/js/moment.js"></script>
<script type="text/javascript" src="${contextPath}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="${contextPath}/assets/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.ko.custom.js"></script>

<div class="page-title">
	<h3>커뮤니티 | ${u_auth_cd}</h3>
	<!-- 
	<div class="page-breadcrumb">
		<ol class="breadcrumb">
			<li><a href="#">커뮤니티</a></li>
			<li class="active">공지사항</li>
		</ol>
	</div>
	 -->
</div>
<!-- Main Wrapper -->
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">${BBSCONF.BBS_NM}</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="열기/접기" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="새로고침" class="panel-reload"><i class="icon-reload"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<!-- 등록폼 -->
					<div class="col-md-12">
						<div class="mailbox-content">
							<form id="form_bbsWrite" class="form-horizontal" method="post" enctype="multipart/form-data">
								<input type="hidden" name="SITE_KEYNO" value="${BBSCONF.SITE_KEYNO}">
								<input type="hidden" name="BBS_KEYNO" value="${BBSCONF.BBS_KEYNO}">
								<input type="hidden" name="BBS_TYPE_CD" value="${BBSCONF.BBS_TYPE_CD}">
								<input type="hidden" name="ARTICLE_NO" value="${multiList.dataset1[0].ARTICLE_NO}">
								<input type="hidden" name="UPPPER_ARTICLE_NO" value="${multiList.dataset1[0].UPPPER_ARTICLE_NO}">
								<input type="hidden" name="PUSH_USE_FG" value="N">
								<div class="compose-body">
									<!-- 탑공지 사용시 (공지여부, 게시시작/종료일시) -->
									<c:if test="${'Y' eq BBSCONF.TOP_NOTICE_USE_FG}">
									<div class="form-group">
										<label for="to" class="col-md-2 control-label"><span class="text-danger">*</span> 작성자</label>
										<div class="col-md-2">
											<input type="text" id="WRITER" name="WRITER" class="form-control" value="${multiList.dataset1[0].WRITER eq null ? USER_NM : multiList.dataset1[0].WRITER}" disabled>
										</div>
										<label for="to" class="col-md-2 control-label">공지사용여부</label>
										<div class="col-md-1">
											<select class="form-control" id="optNoticeFg" name="NOTICE_FG">
												<option value="Y" ${multiList.dataset1[0].NOTICE_FG eq 'Y' ? 'selected' : ''}>사용</option>
												<option value="N" ${multiList.dataset1[0].NOTICE_FG eq 'N' ? 'selected' : ''}>사용안함</option>
											</select>
										</div>
										<label for="to" class="col-md-1 control-label">게시기간</label>
										<div class="col-md-2">
											<div id="datetimepickerStartDt" class="input-group date">
												<input type="text" id="tfArticleStartDt" name="ARTICLE_START_DT" class="form-control" placeholder="시작일시" value="${multiList.dataset1[0].ARTICLE_START_DT}">
												<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
											</div>
										</div>
										<div class="col-md-2">
											<div id="datetimepickerEndDt" class="input-group date">
												<input type="text" id="tfArticleEndDt" name="ARTICLE_END_DT" class="form-control" placeholder="종료일시" value="${multiList.dataset1[0].ARTICLE_END_DT}">
												<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
											</div>
										</div>
									</div>
									</c:if>
									<!--// 탑공지 사용시 (공지여부, 게시시작/종료일시) -->
									
									<div class="form-group">
										<label for="subject" class="col-md-2 control-label"><span class="text-danger">*</span> 제목</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="tfTitle" name="TITLE" value="${multiList.dataset1[0].TITLE}" required>
										</div>
									</div>
									
									<!-- 내용 -->
									<div class="form-group">
										<label for="content" class="col-md-2 control-label"><span class="text-danger">*</span> 내용</label>
										<div class="col-md-10">
											<div class="compose-message">
											<c:choose>
												<c:when test="${'Y' eq BBSCONF.EDITOR_USE_FG}">
													<!-- 에디터사용 -->
													<textarea id="tfContent" name="CONTENT" class="form-control summernote" style="width:100%">${fn:escapeXml(multiList.dataset1[0].CONTENT)}</textarea>
													<!--// 에디터사용 -->
												</c:when>
												<c:otherwise>
													<textarea id="tfContent" name="CONTENT" class="bbs_textarea">${fn:escapeXml(multiList.dataset1[0].CONTENT)}</textarea>
												</c:otherwise>
											</c:choose>
											</div>
										</div>
									</div>
									<!--// 내용 -->
									
									<!-- 파일 조건 -->
									<c:if test="${(not empty BBSCONF.ATTACH_FILE_CNT) and (0 lt BBSCONF.ATTACH_FILE_CNT)}">
									<div class="form-group">
										<label for="attachFile" class="col-md-2 control-label">첨부파일</label>
										<div class="col-md-10">
											<c:forEach begin="1" end="${BBSCONF.ATTACH_FILE_CNT }" step="1" varStatus="status">
												<input type="file" name="<%=ProjectConstant.KEY_FORM_FILE %>${status.count}" id="file${status.count}" class="form-control" />
											</c:forEach>
											<input type="hidden" name="<%=ProjectConstant.KEY_FORM_FILE_MAX_SIZE %>" value="${BBSCONF.ATTACH_FILE_LIMIT_SIZE}" />
										</div>
									</div>
									<div class="form-group">
										<label for="to" class="col-md-2 control-label"><i class="glyphicon glyphicon-info-sign text-danger"></i> 첨부파일 유의사항</label>
										<div class="col-md-10">
											<ul class="help-block wordbreak_keepall">
												<li>최대 <span class="text-primary">${BBSCONF.ATTACH_FILE_CNT}개</span>까지 첨부가능합니다.</li>
												<li>파일 하나당 <span class="text-primary">${BBSCONF.ATTACH_FILE_LIMIT_SIZE}MB</span> 이하 파일만 첨부가능합니다.</li>
											</ul>
										</div>
									</div>
									</c:if>
								</div>
								
								<!-- 버튼 -->
								<c:if test="${u_auth_cd eq '99' or u_auth_cd eq '01'}">
								<div class="message-options pull-right">
									<button type="button" class="btn btn-default btn-rounded m-l-xxs" onclick="javascript:history.back();"><i class="fa fa-repeat"></i> 취소</button>
									<button type="button" class="btn btn-success btn-rounded m-l-xxs" id="btn_save"><i class="fa fa-check"></i> 저장</button>
								</div>
								</c:if>
							</form>
						</div>
					</div>
					<!--// 등록폼 -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Main Wrapper -->
<script src="${contextPath}/assets/js/bbs/bbs.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#tfArticleStartDt").val("${empty multiList.dataset1[0].ARTICLE_START_DT ? NOW_DATE : multiList.dataset1[0].ARTICLE_START_DT}");
	$("#tfArticleEndDt").val("${empty multiList.dataset1[0].ARTICLE_END_DT ? '9999-12-31 23:59' : multiList.dataset1[0].ARTICLE_END_DT}");
	
	$(".summernote").summernote({
		height : 300,
		lang : "ko-KR"
	});
	
	$("#datetimepickerStartDt").datetimepicker({
		format : "YYYY-MM-DD HH:mm",
		sideBySide : true,
		showTodayButton : true,
		locale : "ko"
	});
	
	$("#datetimepickerEndDt").datetimepicker({
		format : "YYYY-MM-DD HH:mm",
		sideBySide : true,
		showTodayButton : true,
		locale : "ko",
		useCurrent: false //Important! See issue #1075
	});
	
	$("#datetimepickerStartDt").on("dp.change", function (e) {
		$('#datetimepickerEndDt').data("DateTimePicker").minDate(e.date);
	});
	$("#datetimepickerEndDt").on("dp.change", function (e) {
		$('#datetimepickerStartDt').data("DateTimePicker").maxDate(e.date);
	});
	
	//jquery validate
	var $validator = $("#form_bbsWrite").validate({
		rules: {
			tfTitle: "required",
			tfContent: "required"
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	} );
	
	/* 게시글 저장 */
	$('#btn_save').click(function() {
		var formId = "#form_bbsWrite";
		var $valid = $(formId).valid();
		
		if(!$valid) {
			$validator.focusInvalid();
			return false;
		} else {
			if(confirm("저장하시겠습니까?")) {
				var url = "/bbs/common/"+ ${BBSCONF.BBS_KEYNO} + "/save";
				
				$(formId).attr("action", url);
				$(formId).submit();
			}
		}
	});
});

</script>