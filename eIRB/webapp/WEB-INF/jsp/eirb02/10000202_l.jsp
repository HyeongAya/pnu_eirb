﻿<div class="page-title">
	<h3>나의 과제 관리</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-file-text-o"></i> 나의 과제 리스트</h3>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<form id="deli_sche_form" name="deli_sche_form" method="post" enctype="application/x-www-form-urlencoded">
						<input type="hidden" name="DELI_APPL_KEYNO" id="DELI_APPL_KEYNO"/>
						<div class="table-responsive">
							<table id="appl_management" class="display table" style="width: 100%; cellspacing: 0;">
								<colgroup>
									<col style="width:15%;">
									<col style="width:15%;">
									<col />
									<col style="width:15%;">
									<col style="width:15%;">
								</colgroup>
								<thead>
									<tr>
										<th>No</th>
										<th>과제번호</th>
										<th>연구과제명</th>
										<th>작성일자</th>
										<th>진행상태</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="apply" items="${APPLY_LIST}" varStatus="status">
										<tr>
											<td class="text-center">${status.count}</td>
											<td class="text-center"><a href="javascript:fn_view('${apply.DELI_APPL_KEYNO}')" >${apply.DELI_APPL_KEYNO}</a></td>
											<td><a href="javascript:fn_view('${apply.DELI_APPL_KEYNO}')" >${apply.DELI_APPL_TITLE_KO}</a></td>
											<td class="text-center">
												<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${apply.UPD_DT}"/>
												<c:if test="${empty apply.UPD_DT}"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${apply.INS_DT}"/></c:if>
											</td>
											<td class="text-center">${apply.DELI_ACCS_STATUS_NM}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</form>
					<div class="btn_block">
						<a href="/eirb02/10000201/" class="btn btn-primary btn-rounded pull-right"><i class="fa fa-check"></i> 신규 과제 신청하기</a>
					</div>
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		/* table 지정 */
		$("#appl_management").DataTable({	
			responsive: true,
			destroy: true,
			"order": [[ 0, 'desc' ]],
			columnDefs: [
					{"targets" : 0, "searchable": true, "orderable" : true},
					{"targets" : 1, "searchable": true, "orderable" : true},
					{"targets" : 2, "searchable": true, "orderable" : true},
					{"targets" : 3, "searchable": true, "orderable" : true},
					{"targets" : 4, "searchable": true, "orderable" : true}
			]
		});
	});
	
	/* 뷰페이지 이동 */
	function fn_view(deli_appl_keyno) {
		var frm = document.deli_sche_form;
		$("#DELI_APPL_KEYNO").val(deli_appl_keyno);
		frm.action = "/eirb02/10000202/view";
		frm.submit();
	}
</script>
<style type="text/css">
	.btn_block { overflow: hidden; margin: 20px 0 0 0; }
	th { text-align: center; }
</style>