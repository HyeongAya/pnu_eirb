﻿<%
String[] uri = request.getAttribute("javax.servlet.forward.request_uri").toString().split("/");
%>

<script type="text/javascript">
</script>

<header>
	<h3>자주찾는 메뉴</h3>
	<a href="#0" class="cd-close-nav">Close</a>
</header>
<ul class="cd-nav list-unstyled">
	<li class="cd-selected" data-menu="index">
		<a href="javsacript:void(0);">
			<span>
				<i class="glyphicon glyphicon-home"></i>
			</span>
			<p>생명윤리위원회<br>전자심의시스템</p>
		</a>
	</li>
	<li data-menu="profile">
		<a href="javsacript:void(0);">
			<span>
				<i class="glyphicon glyphicon-user"></i>
			</span>
			<p>즐겨찾기1</p>
		</a>
	</li>
	<li data-menu="inbox">
		<a href="javsacript:void(0);">
			<span>
				<i class="glyphicon glyphicon-envelope"></i>
			</span>
			<p>즐겨찾기2</p>
		</a>
	</li>
	<li data-menu="#">
		<a href="javsacript:void(0);">
			<span>
				<i class="glyphicon glyphicon-tasks"></i>
			</span>
			<p>즐겨찾기3</p>
		</a>
	</li>
	<li data-menu="#">
		<a href="javsacript:void(0);">
			<span>
				<i class="glyphicon glyphicon-cog"></i>
			</span>
			<p>즐겨찾기3</p>
		</a>
	</li>
	<li data-menu="calendar">
		<a href="javsacript:void(0);">
			<span>
				<i class="glyphicon glyphicon-calendar"></i>
			</span>
			<p>즐겨찾기4</p>
		</a>
	</li>
</ul>