﻿<%
	String[] uri = request.getAttribute("javax.servlet.forward.request_uri").toString().split("/");
	String selectedMenu = uri[2];
%>
<c:set var="MENU_KEYNO" value="<%=selectedMenu %>"/>
<c:set var="OPEN_MENU" value="${fn:substring(MENU_KEYNO,0,6)}" />
<script type="text/javascript">
</script>
<div class="page-sidebar-inner slimscroll">
	<div class="sidebar-header">
		<div class="sidebar-profile">
			<!--
				<div class="sidebar-profile-image">
					<img src="assets/images/profile-menu-image.png" class="img-circle img-responsive" alt="">
				</div>
			-->
			<div class="sidebar-profile-details">
				<span>${USER_NM} 님,<br>반갑습니다!<br><small></small></span>
			</div>
		</div>
	</div>
	<ul class="menu accordion-menu">
		<c:forEach var="item" items="${MENU_LIST}" varStatus="status">
			<c:if test="${item.UPPER_MENU_KEYNO eq 1000}">
				<li class="droplink <c:if test="${MENU_KEYNO ne 'main' and item.MENU_KEYNO eq OPEN_MENU}">open active</c:if>">
			</c:if>
			<c:choose>
				<c:when test="${item.DEPTH eq 1}">
					<a href="#" class="waves-effect waves-button"><p>${item.MENU_NM}</p><span class="arrow"></span></a>
				</c:when>
				<c:when test="${item.DEPTH eq 2}">
					<c:if test="${item.UPPER_MENU_KEYNO eq UPPER_MENU}"><ul class="sub-menu"></c:if>
						<li <c:if test="${MENU_KEYNO ne 'main' and item.MENU_KEYNO eq MENU_KEYNO}">class="active"</c:if>>
							<a href="${item.MENU_URL}"><p>${item.MENU_NM}</p></a>
						</li>
					<c:if test="${MENU_LIST[status.index +1].UPPER_MENU_KEYNO eq 1000 || status.last eq true}"></ul></li></c:if>
				</c:when>
			</c:choose>
			<c:set var="UPPER_MENU" value="${item.MENU_KEYNO}" />
		</c:forEach>
	</ul>
</div><!-- Page Sidebar Inner -->