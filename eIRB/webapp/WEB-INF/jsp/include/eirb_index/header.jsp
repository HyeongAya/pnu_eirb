﻿<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>부산대학교 생명윤리위원회 전자심의시스템</title>

<link href="${contextPath}/assets/css/common/base.css" rel="stylesheet" type="text/css">
<link href="${contextPath}/assets/css/common/style.css" rel="stylesheet" type="text/css">
<link href="${contextPath}/assets/css/common/login.css" rel="stylesheet" type="text/css">
<link href="${contextPath}/assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel="stylesheet" href="/assets/css/common/ie8.css" type="text/css" />
<![endif]-->

<!--[if lt IE 9]>
<script src="/assets/js/custom/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="${contextPath}/assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="${contextPath}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="${contextPath}/assets/plugins/jquery-validation/messages_ko.js"></script>
<script type="text/javascript" src="${contextPath}/assets/js/common/common.js"></script>
<script type="text/javascript" src="${contextPath}/assets/js/custom/custom_request.js"></script>