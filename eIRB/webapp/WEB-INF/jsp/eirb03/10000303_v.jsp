﻿<div class="page-title">
	<h3>교육관리</h3>
	<!-- 
	<div class="page-breadcrumb">
		<ol class="breadcrumb">
			<li><a href="#">공통관리</a></li>
			<li class="active">관리자관리</li>
		</ol>
	</div>
	 -->
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h3 class="panel-title"><i class="fa fa-check"></i> 교육 정보</h3>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<form class="form-horizontal" name="edu_apply_view_form" id="edu_apply_view_form" method="post">
						<input type="hidden" name="EDU_KEYNO" id="EDU_KEYNO" value="${DATAVIEW.EDU_KEYNO}"/>
						<input type="hidden" name="EDU_APPL_KEYNO" id="EDU_APPL_KEYNO" value="${DATAVIEW.EDU_APPL_KEYNO}"/>
						<input type="hidden" name="EDU_APPL_STATUS" id="EDU_APPL_STATUS" value="${DATAVIEW.EDU_APPL_STATUS}"/>
					</form>
					
					<div class="table-responsive">
						<div class="form-group col-sm-12">
							<table class="table table-bordered" id="edu_appl_table">
								<colgroup>
									<col width="20%"/>
									<col width="30%"/>
									<col width="20%"/>
									<col width="30%"/>
								</colgroup>
								<tbody>
									<tr>
										<th class="text-center"><label for="EDU_NM">교육명</label></th>
										<td>${DATAVIEW.EDU_NM}</td>
										<th class="text-center"><label for="EDU_APPL_STATUS_NM">신청여부</label></th>
										<td>${DATAVIEW.EDU_APPL_STATUS_NM eq null ? '미신청' : DATAVIEW.EDU_APPL_STATUS_NM}</td>
									</tr>
									<tr>
										<th class="text-center"><label for="EDU_PLACE">교육장소</label></th>
										<td>${DATAVIEW.EDU_PLACE}</td>
										<th class="text-center"><label for="EDU_ORG_NM">교육추최기관</label></th>
										<td>${DATAVIEW.EDU_ORG_NM}</td>
									</tr>
									<tr>
										<th class="text-center"><label for="EDU_CONTENT">교육내용</label></th>
										<td colspan="3">${DATAVIEW.EDU_CONTENT}</td>
									</tr>
									<tr>
										<th class="text-center"><label for="EDU_DT">교육일자</label></th>
										<td>${DATAVIEW.EDU_START_DT} ~ ${DATAVIEW.EDU_END_DT} | ${DATAVIEW.EDU_START_TM} ~ ${DATAVIEW.EDU_END_TM}</td>
										<th class="text-center"><label for="EDU_REC_DT">교육접수일자</label></th>
										<td>${DATAVIEW.EDU_REC_START_DT} ~ ${DATAVIEW.EDU_REC_END_DT}</td>
									</tr>
									<tr>
										<th class="text-center"><label for="EDU_PROF_NM">강사명</label></th>
										<td>${DATAVIEW.EDU_PROF_NM}</td>
										<th class="text-center"><label for="EDU_PER">정원수</label></th>
										<td>${DATAVIEW.EDU_PER}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="col-sm-12">
						<button type="button" class="btn btn-default btn-rounded pull-right m-l-xxs" id="btn_cancel"><i class="fa fa-list"></i> 목록</button>
						<c:choose>
						<c:when test="${DATAVIEW.EDU_APPL_STATUS eq null}">
							<button type="button" class="btn btn-success btn-rounded pull-right m-l-xxs" id="btn_save"><i class="fa fa-check"></i> 신청</button>
						</c:when>
						<c:when test="${DATAVIEW.EDU_APPL_STATUS eq '01'}">
							<button type="button" class="btn btn-danger btn-rounded pull-right m-l-xxs" id="btn_del"><i class="fa fa-remove"></i> 신청취소</button>
						</c:when>
						<c:otherwise>
						</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

<script type="text/javascript">
	
	$(document).ready(function() {
		/* 저장 */
		$('#btn_save').click(function() {
			if(confirm("신청하시겠습니까?")) {
				$('#edu_apply_view_form').attr("action","/eirb03/10000303/save");
				$('#edu_apply_view_form').submit();
			}
		});
		
		/* 삭제 */
		$('#btn_del').click(function() {
			if(confirm("취소하시겠습니까?")) {
				$('#edu_apply_view_form').attr("action","/eirb03/10000303/delete");
				$('#edu_apply_view_form').submit();
			}
		});
		
		/* 취소 */
		$('#btn_cancel').click(function() {
			location.href = "/eirb03/10000303/";
		});
		
	});
</script>