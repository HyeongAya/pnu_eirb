﻿<div class="page-title">
	<h3>교육관리</h3>
	<!-- 
	<div class="page-breadcrumb">
		<ol class="breadcrumb">
			<li><a href="#">공통관리</a></li>
			<li class="active">관리자관리</li>
		</ol>
	</div>
	 -->
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h3 class="panel-title"><i class="fa fa-check"></i> 교육 일정 상세</h3>
				</div>
				<div class="panel-body">
				<form class="form-horizontal" name="edu_view_form" id="edu_view_form" method="post">
					<div class="p-v-xs"></div>
					<input type="hidden" name="EDU_KEYNO" id="EDU_KEYNO" value="${DATAVIEW.EDU_KEYNO}"/>
					
					<div class="form-group">
						<label for="EDU_NM" class="col-sm-2 control-label"><span class="text-danger">*</span> 교육명</label>
						<div class="col-sm-10">
							<input type="text" id="EDU_NM" name="EDU_NM" value="${DATAVIEW.EDU_NM}" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="EDU_PLACE" class="col-sm-2 control-label"><span class="text-danger">*</span> 교육장소</label>
						<div class="col-sm-4">
							<input type="text" id="EDU_PLACE" name="EDU_PLACE" value="${DATAVIEW.EDU_PLACE}" class="form-control">
						</div>
						<label for="EDU_ORG_NM" class="col-sm-2 control-label"><span class="text-danger">*</span> 교육주최기관</label>
						<div class="col-sm-4">
							<input type="text" id="EDU_ORG_NM" name="EDU_ORG_NM" value="${DATAVIEW.EDU_ORG_NM}" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="EDU_CONTENT" class="col-sm-2 control-label">교육내용</label>
						<div class="col-sm-10">
							<textarea id="EDU_CONTENT" name="EDU_CONTENT" class="form-control summernote"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="EDU_START_DT" class="col-sm-2 control-label"><span class="text-danger">*</span> 교육시작일</label>
						<div class="col-sm-4 m-b-xxs">
							<div class="input-group input-append bootstrap-timepicker">
								<input type="text" id="EDU_START_DT" name="EDU_START_DT" value="${DATAVIEW.EDU_START_DT}" class="form-control date-picker">
								<span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
						<label for="EDU_END_DT" class="col-sm-2 control-label"><span class="text-danger">*</span> 교육종료일</label>
						<div class="col-sm-4">
							<div class="input-group input-append bootstrap-timepicker">
								<input type="text" id="EDU_END_DT" name="EDU_END_DT" value="${DATAVIEW.EDU_END_DT}" class="form-control date-picker">
								<span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="EDU_START_TM" class="col-sm-2 control-label"><span class="text-danger">*</span> 교육시작시간</label>
						<div class="col-sm-4 m-b-xxs">
							<div class="input-group input-append bootstrap-timepicker">
								<input type="text" id="EDU_START_TM" name="EDU_START_TM" value="${DATAVIEW.EDU_START_TM}" class="form-control">
								<span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
							</div>
						</div>
						<label for="EDU_END_TM" class="col-sm-2 control-label"><span class="text-danger">*</span> 교육종료시간</label>
						<div class="col-sm-4">
							<div class="input-group input-append bootstrap-timepicker">
								<input type="text" id="EDU_END_TM" name="EDU_END_TM" value="${DATAVIEW.EDU_END_TM}" class="form-control">
								<span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="EDU_REC_START_DT" class="col-sm-2 control-label"><span class="text-danger">*</span> 교육접수시작일</label>
						<div class="col-sm-4 m-b-xxs">
							<div class="input-group input-append bootstrap-timepicker">
								<input type="text" id="EDU_REC_START_DT" name="EDU_REC_START_DT" value="${DATAVIEW.EDU_REC_START_DT}" class="form-control date-picker">
								<span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
						<label for="EDU_REC_END_DT" class="col-sm-2 control-label"><span class="text-danger">*</span> 교육접수종료일</label>
						<div class="col-sm-4">
							<div class="input-group input-append bootstrap-timepicker">
								<input type="text" id="EDU_REC_END_DT" name="EDU_REC_END_DT" value="${DATAVIEW.EDU_REC_END_DT}" class="form-control date-picker">
								<span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="EDU_PROF_NM" class="col-sm-2 control-label">강사명</label>
						<div class="col-sm-4">
							<input type="text" id="EDU_PROF_NM" name="EDU_PROF_NM" value="${DATAVIEW.EDU_PROF_NM}" class="form-control">
						</div>
						<label for="EDU_PER" class="col-sm-2 control-label">정원수</label>
						<div class="col-sm-2">
							<input type="number" id="EDU_PER" name="EDU_PER" value="${DATAVIEW.EDU_PER}" class="form-control" min="1" max="100">
						</div>
						<label for="EDU_USE_FG" class="col-sm-1 control-label"><span class="text-danger">*</span> 사용여부</label>
						<div class="col-sm-1">
							<select id="EDU_USE_FG" name="EDU_USE_FG" class="form-control m-b-sm">
								<option value="Y" <c:if test="${DATAVIEW.EDU_USE_FG eq 'Y' }">selected="selected"</c:if>>사용</option>
								<option value="N" <c:if test="${DATAVIEW.EDU_USE_FG ne 'Y' }">selected="selected"</c:if>>미사용</option>
							</select>
						</div>
					</div>
					<div class="col-sm-12">
						<button type="button" class="btn btn-default btn-rounded pull-right m-l-xxs" id="btn_cancel"><i class="fa fa-repeat"></i> 취소</button>
						<c:if test="${DATAVIEW.EDU_KEYNO ne null}">
						<button type="button" class="btn btn-danger btn-rounded pull-right m-l-xxs" id="btn_del"><i class="fa fa-trash"></i> 삭제</button>
						</c:if>
						<button type="button" class="btn btn-${DATAVIEW.EDU_KEYNO ne null ? 'warning':'success'} btn-rounded pull-right m-l-xxs" id="btn_save"><i class="fa fa-check"></i> ${DATAVIEW.EDU_KEYNO ne null ? '수정':'저장'}</button>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

<script type="text/javascript">
	
	$(document).ready(function() {
		//jquery validate
		var $validator = $("#edu_view_form").validate({
			rules: {
				EDU_NM: "required",
				EDU_PLACE: "required",
				EDU_ORG_NM: "required",
				EDU_START_DT: {
					required: true,
					date: true
				},
				EDU_END_DT: {
					required: true,
					date: true
				},
				EDU_REC_START_DT: {
					required: true,
					date: true
				},
				EDU_REC_END_DT: {
					required: true,
					date: true
				}
			},
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );
				
				if ( element.prop( "type" ) === "checkbox" ) {
					error.insertAfter( element.parent( "label" ) );
				} else {
					error.insertAfter( element );
				}
			},
			highlight: function ( element, errorClass, validClass ) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
			},
			unhighlight: function (element, errorClass, validClass) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
			}
		} );
		
		/* 저장 */
		$('#btn_save').click(function() {
			var $valid = $("#edu_view_form").valid();
			
			if(!$valid) {
				$validator.focusInvalid();
				return false;
			} else {
				if(confirm("저장하시겠습니까?")) {
					$('textarea[name="EDU_CONTENT"]').html($(".summernote").code());
					
					$('#edu_view_form').attr("action","/eirb03/10000301/save");
					$('#edu_view_form').submit();
				}
			}
		});
		
		/* 삭제 */
		$('#btn_del').click(function() {
			if(confirm("삭제하시겠습니까?")) {
				$('#edu_view_form').attr("action","/eirb03/10000301/delete");
				$('#edu_view_form').submit();
			}
		});
		
		/* 취소 */
		$('#btn_cancel').click(function() {
			location.href = "/eirb03/10000301/";
		});
		
		
		$('.summernote').summernote({
			height: 150,
			lang: 'ko-KR'
		});
		
		$('#EDU_START_TM').timepicker({
			showMeridian: false
		});
		$('#EDU_END_TM').timepicker({
			showMeridian: false
		});
		
		$(".summernote").code('${DATAVIEW.EDU_CONTENT}');
		
	});
</script>