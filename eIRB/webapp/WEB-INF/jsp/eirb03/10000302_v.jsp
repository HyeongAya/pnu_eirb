﻿<div class="page-title">
	<h3>교육관리</h3>
	<!-- 
	<div class="page-breadcrumb">
		<ol class="breadcrumb">
			<li><a href="#">공통관리</a></li>
			<li class="active">관리자관리</li>
		</ol>
	</div>
	 -->
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h3 class="panel-title"><i class="fa fa-check"></i> 교육 접수 상세</h3>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<div class="row">
						<div class="table-responsive">
							<div class="form-group col-md-12">
								<form name="edu_rec_userList_form" id="edu_rec_userList_form" method="post">
									<input type="hidden" name="EDU_KEYNO" id="EDU_KEYNO" value="${EDU_KEYNO}"/>
									<input type="hidden" name="save_data" />
								</form>
								
								<table id="data_table" class="display table" style="width: 100%; cellspacing: 0;">
									<thead>
										<tr>
											<th class="text-center"><input type="checkbox" id="check_all" name="check_all" title="전체 선택"></th>
											<th class="text-center">접수번호</th>
											<th class="text-center">신청자 ID</th>
											<th class="text-center">신청자 성명</th>
											<th class="text-center">신청자 소속</th>
											<th class="text-center">신청일자</th>
											<th class="text-center">상태</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="item" items="${DATAVIEW}" varStatus="status">
											<tr>
												<td onclick="event.cancelBubble = true;" class="text-center">
													<div id="check_item">
														<input type="checkbox" name="CHECK_KEY" title="선택" value="${item.EDU_APPL_KEYNO}">
													</div>
												</td>
												<td class="text-center">${item.EDU_KEYNO}-${item.EDU_APPL_KEYNO}</td>
												<td class="text-center">${item.EDU_APPL_ID}</td>
												<td class="text-center">${item.EDU_APPL_NM}</td>
												<td class="text-center">${item.EDU_APPL_DEPT_NM}</td>
												<td class="text-center">${item.EDU_APPL_DT}</td>
												<td onclick="event.cancelBubble = true;" class="text-center">
													<select class="js-states form-control no-m" title="진행상태" name="EDU_APPL_STATUS" id="EDU_APPL_STATUS" style="width:70%;">
														<option value="01" <c:if test="${item.EDU_APPL_STATUS eq '01' }">selected="selected"</c:if>>접수</option>
														<option value="02" <c:if test="${item.EDU_APPL_STATUS eq '02' }">selected="selected"</c:if>>승인</option>
													</select>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="col-sm-12">
							<button type="button" class="btn btn-default btn-rounded pull-right m-l-xxs" id="btn_cancel"><i class="fa fa-repeat"></i> 취소</button>
							<c:if test="${DATAVIEW.size() > 0}">
							<button type="button" class="btn btn-danger btn-rounded pull-right m-l-xxs" id="btn_del"><i class="fa fa-trash"></i> 삭제</button>
							<button type="button" class="btn btn-success btn-rounded pull-right m-l-xxs" id="btn_save"><i class="fa fa-check"></i> 저장</button>
							<button type="button" class="btn btn-info btn-rounded pull-right m-l-xxs" id="btn_complete"><i class="fa fa-mortar-board"></i> 교육이수</button>
							</c:if>
						</div>
					</div><!-- end row -->
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

<script type="text/javascript">
	$(document).ready(function() {
		$('#data_table').DataTable({
			"destroy": true,
			"search": { caseInsensitive: false },
			"order": [[ 1, 'desc' ]],
			"columnDefs": [
				{"targets" : 1, "searchable": true, "orderable" : true},
				{"targets" : 2, "searchable": true, "orderable" : true},
				{"targets" : 3, "searchable": true, "orderable" : true},
				{"targets" : 4, "searchable": true, "orderable" : true},
				{"targets" : 5, "searchable": true, "orderable" : true},
			]
		});
		
		$("#check_all").click(function() {
			if(this.checked) {
				$("input[name=CHECK_KEY]:checkbox").each(function() {
					$(this).prop("checked", true);
					$('.checker').children().addClass('checked');
				});
			} else {
				$("input[name=CHECK_KEY]:checkbox").each(function() {
					$(this).prop("checked", false);
					$('.checker').children().removeClass('checked');
				});
			}
		});
		
		$('input[name=CHECK_KEY]').click(function() {
			$('#check_all').prop('checked', false);
			$('#uniform-check_all').children().removeClass('checked');
		});
		
		$('#btn_save').click(function() {
			fn_setData('저장 하시겠습니까?', '/eirb03/10000302/list_save');
		});
		
		$('#btn_del').click(function() {
			fn_setData('삭제 하시겠습니까?', '/eirb03/10000302/list_del');
		});
		
		$('#btn_complete').click(function() {
			fn_setData('교육이수 하시겠습니까?', '/eirb03/10000302/list_complete');
		});
		
		/* 취소 */
		$('#btn_cancel').click(function() {
			location.href = "/eirb03/10000302/";
		});
	});
	
	function fn_setData(msg, target) {
		var saveFlag = '';
		if(target == '/eirb03/10000302/list_del') {
			saveFlag = 'del';
			if($('input[name=CHECK_KEY]:checked').length < 1) {
				alert('하나 이상 선택해 주세요.');
				return;
			}
		}
		if(target == '/eirb03/10000302/list_complete') {
			saveFlag = 'complete';
			if($('input[name=CHECK_KEY]:checked').length < 1) {
				alert('하나 이상 선택해 주세요.');
				return;
			}
		}
		if(target == '/eirb03/10000302/list_save') {
			saveFlag = 'save';
			if($('input[name=CHECK_KEY]:checked').length < 1) {
				alert('하나 이상 선택해 주세요.');
				return;
			}
		}
		
		var paramArray = new Array();
		
		$('input[name=CHECK_KEY]').each(function(idx) {
			if($(this).prop('checked')) {
				var obj = new Object();
				
				if(saveFlag == 'save') {
					obj['EDU_KEYNO'] = $('#EDU_KEYNO').val();
					obj['EDU_APPL_KEYNO'] = $(this).val();
					obj['EDU_APPL_STATUS'] = $(this).parent().parent().parent().parent().parent().find('select[name=EDU_APPL_STATUS]').val();
				}
				if(saveFlag == 'complete') {
					obj['EDU_KEYNO'] = $('#EDU_KEYNO').val();
					obj['EDU_APPL_KEYNO'] = $(this).val();
				}
				if(saveFlag == 'del') {
					obj['EDU_KEYNO'] = $('#EDU_KEYNO').val();
					obj['EDU_APPL_KEYNO'] = $(this).val();
				}
				paramArray.push(obj);
			}
		});
		
		$('input[name=save_data]').val(JSON.stringify(paramArray));
		
		if(confirm(msg)) {
			$('#edu_rec_userList_form')[0].action = target;
			$('#edu_rec_userList_form')[0].submit();
		}
	}
</script>