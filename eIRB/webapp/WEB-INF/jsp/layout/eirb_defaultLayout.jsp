﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html>
	<head>
		<title>부산대학교 생명윤리위원회 전자심의시스템</title>
		<meta content="width=device-width, initial-scale=1" name="viewport"/>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		
		<!-- Styles -->
		<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'> -->
		<link href="${contextPath}/assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
		<link href="${contextPath}/assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
		<link href="${contextPath}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>	
		<link href="${contextPath}/assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>	
		<link href="${contextPath}/assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>	
		<link href="${contextPath}/assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/>	
		<link href="${contextPath}/assets/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/>	
		<link href="${contextPath}/assets/plugins/datatables/css/jquery.datatables_themeroller.css" rel="stylesheet" type="text/css"/>	
		<link href="${contextPath}/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet" type="text/css"/>	
		<link href="${contextPath}/assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>	
		<link href="${contextPath}/assets/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/plugins/summernote-master/summernote.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
		
		<!-- Theme Styles -->
		<link href="${contextPath}/assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/css/themes/purple.css" class="theme-color" rel="stylesheet" type="text/css"/>
		<link href="${contextPath}/assets/css/custom.css" rel="stylesheet" type="text/css"/>
		
		<!-- Javascripts -->
		<script src="${contextPath}/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
		<script src="${contextPath}/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
		<script src="${contextPath}/assets/plugins/pace-master/pace.min.js"></script>
		<script src="${contextPath}/assets/plugins/jquery-blockui/jquery.blockui.js"></script>
		<script src="${contextPath}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="${contextPath}/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="${contextPath}/assets/plugins/switchery/switchery.min.js"></script>
		<script src="${contextPath}/assets/plugins/uniform/jquery.uniform.min.js"></script>
		<script src="${contextPath}/assets/plugins/offcanvasmenueffects/js/classie.js"></script>
		<script src="${contextPath}/assets/plugins/offcanvasmenueffects/js/main.js"></script>
		<script src="${contextPath}/assets/plugins/waves/waves.min.js"></script>
		<script src="${contextPath}/assets/plugins/3d-bold-navigation/js/main.js"></script>
		<script src="${contextPath}/assets/plugins/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="${contextPath}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
		<script src="${contextPath}/assets/plugins/jquery-validation/messages_ko.js"></script>
		<script src="${contextPath}/assets/plugins/jquery-mockjax-master/jquery.mockjax.js"></script>
		<script src="${contextPath}/assets/plugins/moment/moment.js"></script>
		<script src="${contextPath}/assets/plugins/datatables/js/jquery.datatables.min.js"></script>
		<script src="${contextPath}/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="${contextPath}/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
		<script src="${contextPath}/assets/plugins/fullcalendar/lib/moment.min.js"></script>
		<script src="${contextPath}/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
		<script src="${contextPath}/assets/plugins/fullcalendar/lang-all.js"></script>
		<script src="${contextPath}/assets/plugins/summernote-master/summernote.min.js"></script>
		<script src="${contextPath}/assets/plugins/summernote-master/summernote-ko-KR.js"></script>
		<script src="${contextPath}/assets/js/modern.min.js"></script>
		<script src="${contextPath}/assets/plugins/waypoints/jquery.waypoints.min.js"></script>
		<script src="${contextPath}/assets/plugins/jquery-counterup/jquery.counterup.min.js"></script>
		<script src="${contextPath}/assets/plugins/toastr/toastr.min.js"></script>
		<script src="${contextPath}/assets/plugins/metrojs/MetroJs.min.js"></script>
		<%-- <script src="${contextPath}/assets/js/pages/calendar.js"></script> --%>
		<script src="${contextPath}/assets/js/custom/custom_createElement.js"></script>
		<script src="${contextPath}/assets/js/common/common.js"></script>
		<script src="${contextPath}/assets/js/custom/custom_request.js"></script>
		<script src="${contextPath}/assets/plugins/select2/js/select2.min.js"></script>
		
		<script src="${contextPath}/assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
		<script src="${contextPath}/assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="page-header-fixed compact-menu">
		<div class="overlay"></div>
		<main class="page-content content-wrap">
		<div class="navbar">
			<tiles:insertAttribute name="header" />
		</div>
		<div class="page-sidebar sidebar">
			<tiles:insertAttribute name="left" />
		</div>
		<div class="page-inner">
			<tiles:insertAttribute name="body" />
			<div class="page-footer">
				<tiles:insertAttribute name="footer" />
			</div>
		</div>
		</main>
		<nav class="cd-nav-container" id="cd-nav">
			<tiles:insertAttribute name="favorite" />
		</nav>
		<div class="cd-overlay"></div>
		
		<div id="flotTip" style="display: none; position: absolute;"></div>
	</body>

</html>