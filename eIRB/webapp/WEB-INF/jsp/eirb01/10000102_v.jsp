﻿<div class="page-title">
	<h3>심의참석자 관리</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-calendar"></i> ${DELI_DATA.DELI_SCHE_NM} / ${DELI_DATA.SCHE_DT}</h3>
					<c:if test="${DELI_DATA.DELI_SCHE_DECIDE_FG eq 'Y'}">
						<div class="panel-control">
							<i class="fa icon-users"></i> ${DELI_DATA.TOTAL}명 ( 내  ${DELI_DATA.INNER_USER}명, 외  ${DELI_DATA.OUTER_USER}명 )
						</div>
					</c:if>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<div class="row">
						<form id="deli_attend_form" name="deli_attend_form" method="post" enctype="application/x-www-form-urlencoded">
							<input type="hidden" name="DELI_SCHE_KEYNO" id="DELI_SCHE_KEYNO" value="${DELI_SCHE_KEYNO}"/>
							<input type="hidden" name="DELI_SCHE_DECIDE_FG" id="DELI_SCHE_DECIDE_FG"/>
							<input type="hidden" name="DELI_ATTD_ID" id="DELI_ATTD_ID"/>
							<input type="hidden" name="DELI_ATTD_FG" id="DELI_ATTD_FG"/>
							<input type="hidden" name="DELI_ATTD_FG_YN" id="DELI_ATTD_FG_YN"/>
							<input type="hidden" name="USER_ID" id="USER_ID" value="${USER_ID}"/>
							<input type="hidden" name="USER_IP" id="USER_IP" value="127.0.0.1"/>
							
							<div class="table-responsive">
								<div class="form-group col-md-12">
									<c:choose>
										<c:when test="${DELI_DATA.DELI_SCHE_DECIDE_FG eq 'Y'}">
											<table class="table table-striped">
												<thead>
													<tr>
														<th>No</th>
														<th>권한</th>
														<th>이름</th>
														<th>연락처</th>
														<th>메일</th>
														<th>관리</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="attd" items="${ATTD_LIST}" varStatus="status">
														<tr>
															<td class="text-center">${status.count}</td>
															<td class="text-center">${attd.AUTH_NM}</td>
															<td class="text-center">${attd.USER_NM}</td>
															<td class="text-center">${attd.USER_TEL}</td>
															<td class="text-center">${attd.USER_EMAIL}</td>
															<td class="text-center">과제배정</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</c:when>
										<c:otherwise>
											<table id="deli_User" class="display table" style="width: 100%; cellspacing: 0;">
												<thead>
													<tr>
														<th>No</th>
														<th>권한</th>
														<th>이름</th>
														<th>연락처</th>
														<th>메일</th>
														<th>관리</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="user" items="${USER_LIST}" varStatus="status">
														<tr>
															<td class="text-center">${status.count}</td>
															<td class="text-center">${user.AUTH_NM}</td>
															<td class="text-center">${user.USER_NM}</td>
															<td class="text-center">${user.USER_PHONE}</td>
															<td class="text-center">${user.USER_EMAIL}</td>
															<td class="text-center">
																<c:choose>
																	<c:when test="${user.DELI_ATTD_FG eq 'Y'}">
																		<button type="button" class="btn btn-info btn-rounded"  onclick="fn_save('${user.USER_ID}','N','${user.DELI_ATTD_FG}')"><i class="fa fa-check"></i> 참석</button>
																	</c:when>
																	<c:otherwise>
																		<button type="button" class="btn btn-default btn-rounded"  onclick="fn_save('${user.USER_ID}','Y','${user.DELI_ATTD_FG}')"><i class="fa fa-times"></i> 미참석</button>
																	</c:otherwise>
																</c:choose>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div class="col-sm-12">
								<button type="button" class="btn btn-default btn-rounded pull-right m-l-xxs" onclick="location.href='/eirb01/10000102/';"><i class="fa fa-repeat"></i> 취소</button>
								<c:choose>
									<c:when test="${DELI_DATA.DELI_SCHE_DECIDE_FG eq 'N'}">
										<button type="button" class="btn btn-info btn-rounded pull-right" onclick="fn_attend_save('Y');"><i class="fa fa-file-text-o"></i> 명단확정</button>
									</c:when>
									<c:otherwise>
										<button type="button" class="btn btn-warning btn-rounded pull-right" onclick="fn_attend_save('N');"><i class="fa fa-file-text"></i> 명단수정</button>
									</c:otherwise>
								</c:choose>
							</div>
						</form>
					</div><!-- end row -->
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		/* table 지정 */
		var table = $('#deli_User').DataTable({
			"order": [[ 5, 'desc' ]]
		});
	});

	/* 등록  */
	function fn_save(deli_attd_id,deli_attd_fg,deli_attd_fg_yn) {
		var frm = document.deli_attend_form;
		$("#DELI_ATTD_ID").val(deli_attd_id);
		$("#DELI_ATTD_FG").val(deli_attd_fg);
		$("#DELI_ATTD_FG_YN").val(deli_attd_fg_yn);
		frm.action = "/eirb01/10000102/save";
		frm.submit();
	}
	
	/* 명단확정  */
	function fn_attend_save(deli_decide_fg) {
		var frm = document.deli_attend_form;
		$("#DELI_SCHE_DECIDE_FG").val(deli_decide_fg);
		frm.action = "/eirb01/10000102/attend_save";
		frm.submit();
	}
	
	/* 심의과제관리 */
	function fn_deli_check(deli_sche_keyno) {
		var frm = document.deli_sche_form;
		$("#DELI_SCHE_KEYNO").val(deli_sche_keyno);
		frm.action = "/eirb01/10000102/deli";
		frm.submit();
	}
</script>
<style type="text/css">
	th { text-align: center; }
</style>