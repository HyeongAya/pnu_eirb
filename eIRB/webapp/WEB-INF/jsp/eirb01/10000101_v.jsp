﻿<div class="page-title">
	<h3>심의일정관리</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-calendar"></i> 심의 일정 
						<c:choose>
							<c:when test="${empty DELI_DATA.DELI_SCHE_KEYNO}">등록</c:when>
							<c:otherwise>
								정보 관리
							</c:otherwise>
						</c:choose>
					</h3>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<form id="deli_sche_form" name="deli_sche_form" method="post" enctype="application/x-www-form-urlencoded">
						<input type="hidden" name="DELI_SCHE_KEYNO" id="DELI_SCHE_KEYNO" value="${DELI_DATA.DELI_SCHE_KEYNO}"/>
						<input type="hidden" name="DELI_SCHE_TYPE" id="DELI_SCHE_TYPE" value="W"/>
						<input type="hidden" name="KIND_FG" id="KIND_FG" value="${KIND_FG }"/>
						<input type="hidden" name="SAVE_FLAG" id="SAVE_FLAG"/>
						<input type="hidden" name="INS_ID" id="INS_ID" value="${USER_ID}"/>
						<input type="hidden" name="INS_IP" id="INS_IP" value="127.0.0.1"/>
						<input type="hidden" name="UPD_ID" id="UPD_ID" value="${USER_ID}"/>
						<input type="hidden" name="UPD_IP" id="UPD_IP" value="127.0.0.1"/>
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6 col-md-offset-3">
									<label for="deli_appl_comm_org">심의 일정명</label>
									<input type="text" class="form-control" name="DELI_SCHE_NM" id="DELI_SCHE_NM" value="${DELI_DATA.DELI_SCHE_NM}" >
								</div>
								<div class="form-group col-md-6 col-md-offset-3">
									<label for="deli_appl_dt">심의 일자</label>
									<div class="input-group input-append bootstrap-timepicker">
										<input type="text" class="form-control date-picker" name="DELI_SCHE_DT" id="DELI_SCHE_DT" value="${DELI_DATA.SCHE_DT}" readonly >
										<span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="col-md-12">
						<div class="text-center">
							<c:choose>
								<c:when test="${empty DELI_DATA.DELI_SCHE_KEYNO}"><button type="button" class="btn btn-primary btn-rounded " onclick="fn_save('add','등록');"><i class="fa fa-check"></i> 등록</button></c:when>
								<c:otherwise>
									<button type="button" class="btn btn-warning btn-rounded " onclick="fn_save('modify','수정');"><i class="fa fa-check"></i> 수정</button>
									<button type="button" class="btn btn-danger btn-rounded" onclick="fn_save('del','삭제');"><i class="fa fa-trash"></i> 삭제</button>
								</c:otherwise>
							</c:choose>
							<button type="button" class="btn btn-default btn-rounded" onclick="location.href='/eirb01/10000101/';"><i class="fa fa-repeat"></i> 취소</button>
						</div>
					</div>
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		/* calendar open */
		$('.date-picker').datepicker({
			orientation: "top auto",
			autoclose: true
		});
		
		var $validator = $("#deli_sche_form").validate({
			rules: {
				DELI_SCHE_NM: {
					required: true
				},
				DELI_SCHE_DT: {
					required: true,
					date: true
				}
			}
		});
	});
	
	/* 등록 */
	function fn_save(state,text) {
		var frm = document.deli_sche_form;
		var $valid = $("#deli_sche_form").valid();
		if(!$valid) {
			$validator.focusInvalid();
			return false;
		}else{
			$("#SAVE_FLAG").val(state);
			
			result = confirm('작성하신 심의일정을 '+text+'하시겠습니까?');
			if(result == true){
				frm.action = "/eirb01/10000101/save";
				frm.submit();
			}else{
				return false;
			}
		}
	}
</script>