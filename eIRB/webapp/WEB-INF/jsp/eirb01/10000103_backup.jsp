﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="page-title">
	<h3>심의신청서관리</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-3" style="padding-bottom:25px">
			<select class="js-states form-control" tabindex="-1" style="width: 100%">
				<optgroup label="전체심의">
					<option value="심의코드값">1월 전체심의, 2017.01.01</option>
					<option value="심의코드값">2월 전체심의, 2017.02.01</option>
					<option value="심의코드값">3월 전체심의, 2017.03.01</option>
				</optgroup>
				<optgroup label="신속심의">
					<option value="심의코드값">1월 신속심의, 2017.01.01</option>
					<option value="심의코드값">2월 신속심의, 2017.02.01</option>
					<option value="심의코드값">3월 신속심의, 2017.03.01</option>
				</optgroup>
			</select>
		</div>
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel">진행상황 기록</h4>
								</div>
								<div class="modal-body">
									<div class="form-horizontal">
										<table id="table" class="display table table-striped" style="width: 100%; cellspacing: 0;">
											<thead>
												<tr>
													<th>No</th>
													<th>진행상황</th>
													<th>진행일시</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>심의신청</td>
													<td>2017.01.01</td>
												</tr>
												<tr>
													<td>2</td>
													<td>수정요청</td>
													<td>2017.01.02</td>
												</tr>
												<tr>
													<td>3</td>
													<td>수정본 업로드</td>
													<td>2017.01.03</td>
												</tr>
												<tr>
													<td>4</td>
													<td>접수완료</td>
													<td>2017.01.04</td>
												</tr>
												<tr>
													<td>5</td>
													<td>사전심의배정</td>
													<td>2017.01.05</td>
												</tr>
												<tr>
													<td>6</td>
													<td>사전심의</td>
													<td>2017.01.06</td>
												</tr>
												<tr>
													<td>7</td>
													<td>전체심의</td>
													<td>2017.01.07</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
								</div>
							</div>
						</div>
					</div>
					<div class="modal fade" id="modify_myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel">수정요청</h4>
								</div>
								<div class="modal-body">
									<div class="form-horizontal">
										<table id="table" class="display table table-bodered" style="width: 100%; cellspacing: 0;">
											<tbody>
												<tr>
													<th style="background-color:#F6F6F6">연구과제명</th>
													<td colspan="3">위기청소년의 성격강점에 관한 연구</td>
												</tr>
												<tr>
													<th style="background-color:#F6F6F6">연구책임자</th>
													<td>박 연구자</td>
													<th style="background-color:#F6F6F6">소속</th>
													<td>정보전산원</td>
												</tr>
												<tr>
													<th style="background-color:#F6F6F6">수정요청사항</th>
													<td colspan="3">
														<textarea class="input-large form-control" id="message" rows="5" placeholder="요청할 수정사항을 입력해주십시오."></textarea>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
									<button type="submit" id="add-row" class="btn btn-success">수정요청</button>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table id="deli_table" class="display table" style="width: 100%; cellspacing: 0;">
							<thead>
								<tr>
									<th>No</th>
									<th>심의유형</th>
									<th>연구종류</th>
									<th>과제번호</th>
									<th>연구과제명</th>
									<th>진행상황</th>
									<th>진행일자</th>
									<th>관리</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>초기심의</td>
									<td>인체유래물</td>
									<td> - </td>
									<td>위기청소년의 성격강점에 관한 연구</td>
									<td><button type="button" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#myModal">심의신청</button></td>
									<td>2017.01.01</td>
									<td>
										<button type="button" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#modify_myModal">수정요청</button>
										<button type="button" class="btn btn-default btn-rounded">접수</button>
									</td>
								</tr>
								<tr>
									<td>2</td>
									<td>초기심의</td>
									<td>인간대상</td>
									<td>eirb IRB/2017_0002_HR</td>
									<td>흡연자의 성격강점에 관한 연구</td>
									<td><button type="button" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#myModal">접수서류 검토</button></td>
									<td>2017.01.03</td>
									<td>
										<button type="button" class="btn btn-default btn-rounded">수정요청</button>
										<button type="button" class="btn btn-default btn-rounded">접수</button>
									</td>
								</tr>
								<tr>
									<td>3</td>
									<td>초기심의</td>
									<td>인간대상</td>
									<td>eirb IRB/2017_0003_HR</td>
									<td>현대 근로자의 스트레스에 관한 연구</td>
									<td><button type="button" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#myModal">접수완료</button></td>
									<td>2017.01.05</td>
									<td>
										<button type="button" class="btn btn-default btn-rounded">사전심의위원 배정</button>
									</td>
								</tr>
								<tr>
									<td>4</td>
									<td>초기심의</td>
									<td>인체유래물</td>
									<td>eirb IRB/2017_0004_BR</td>
									<td>우울증세 환자의 성격강점에 관한 연구</td>
									<td><button type="button" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#myModal">사전심의위원 배정완료 - 사전심의 중</button></td>
									<td>2017.01.07</td>
									<td>
										<button type="button" class="btn btn-default btn-rounded">점검표 작성</button>
									</td>
								</tr>
								<tr>
									<td>5</td>
									<td>초기심의</td>
									<td>인체유래물</td>
									<td>eirb IRB/2017_0005_BR</td>
									<td>인간의 수명에 관한 연구</td>
									<td><button type="button" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#myModal">사전심의 완료 - 회의의제</button></td>
									<td>2017.01.09</td>
									<td>
										<button type="button" class="btn btn-default btn-rounded">사전심의록</button>
										<button type="button" class="btn btn-default btn-rounded">회의관리</button>
									</td>
								</tr>
								<tr>
									<td>6</td>
									<td>초기심의</td>
									<td>인간대상</td>
									<td>eirb IRB/2017_0006_BR</td>
									<td>암 환자의 성격강점에 관한 연구</td>
									<td><button type="button" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#myModal">회의의제 완료 - 대면회의</button></td>
									<td>2017.01.10</td>
									<td>
										<button type="button" class="btn btn-default btn-rounded">사전심의록</button>
										<button type="button" class="btn btn-default btn-rounded">회의관리</button>
										<button type="button" class="btn btn-default btn-rounded">결과통보서작성</button>
									</td>
								</tr>
								<tr>
									<td>7</td>
									<td>초기심의</td>
									<td>인체유래물</td>
									<td>eirb IRB/2017_0007_HR</td>
									<td>행복한 가정을 가진 사람들의 성격강점에 관한 연구</td>
									<td><button type="button" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#myModal">대면회의 완료 - 결과통보</button></td>
									<td>2017.01.10</td>
									<td>
										<button type="button" class="btn btn-default btn-rounded">사전심의록</button>
										<button type="button" class="btn btn-default btn-rounded">회의관리</button>
										<button type="button" class="btn btn-default btn-rounded">결과통보서확인</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div><!-- end table-responsive -->
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
