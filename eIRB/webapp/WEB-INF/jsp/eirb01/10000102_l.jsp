﻿<div class="page-title">
	<h3>심의참석관리</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-body">
				<form id="deli_sche_form" name="deli_sche_form" method="post" enctype="application/x-www-form-urlencoded">
					<input type="hidden" name="DELI_SCHE_KEYNO" id="DELI_SCHE_KEYNO"/>
				</form>
				<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-justified" role="tablist">
							<li role="presentation" class="active"><a href="#tab21" role="tab" data-toggle="tab">전체심의</a></li>
							<li role="presentation"><a href="#tab22" role="tab" data-toggle="tab">신속심의</a></li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active fade in" id="tab21">
							
								<div class="p-v-xs"></div>
								<div class="table-responsive">
									
									<table id="deli_attend_A" class="display table" style="width: 100%; cellspacing: 0;">
										<colgroup>
											<col style="width:5%;">
											<col style="width:15%;">
											<col />
											<col style="width:15%;">
											<col style="width:15%;">
											<col style="width:15%;">
										</colgroup>
										<thead>
											<tr>
												<th>No</th>
												<th>심의분류</th>
												<th>심의명</th>
												<th>심의일</th>
												<th>심의참석 확정여부</th>
												<th>위원 충족수</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="deli" items="${DELI_LIST}" varStatus="status">
												<tr>
													<td class="text-center">${status.count}</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${deli.DELI_SCHE_TYPE eq 'W'}">전체심의</c:when>
															<c:otherwise>신속심의</c:otherwise>
														</c:choose>
													</td>
													<td><a href="javascript:fn_view(${deli.DELI_SCHE_KEYNO})" >${deli.DELI_SCHE_NM}</a></td>
													<td class="text-center">${deli.SCHE_DT}</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${deli.DELI_SCHE_DECIDE_FG eq 'Y'}">명단 확정</c:when>
															<c:otherwise>명단 미확정</c:otherwise>
														</c:choose>
													</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${deli.DELI_SCHE_DECIDE_FG eq 'Y'}">
																<a href="javascript:fn_view(${deli.DELI_SCHE_KEYNO})" >총  ${deli.TOTAL}명 ( 내부  ${deli.INNER_USER}명, 외부  ${deli.OUTER_USER}명 )</a>
															</c:when>
															<c:otherwise>-</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab22">
								<div class="p-v-xs"></div>
								<div class="table-responsive">
									<table id="deli_attend_B" class="display table" style="width: 100%; cellspacing: 0;">
										<colgroup>
											<col style="width:5%;">
											<col style="width:15%;">
											<col />
											<col style="width:15%;">
											<col style="width:15%;">
											<col style="width:15%;">
										</colgroup>
										<thead>
											<tr>
												<th>No</th>
												<th>심의분류</th>
												<th>심의명</th>
												<th>심의일</th>
												<th>심의참석 확정여부</th>
												<th>위원 충족수</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="deli" items="${DELI_LIST_B}" varStatus="status">
												<tr>
													<td class="text-center">${status.count}</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${deli.DELI_SCHE_TYPE eq 'W'}">전체심의</c:when>
															<c:otherwise>신속심의</c:otherwise>
														</c:choose>
													</td>
													<td><a href="javascript:fn_view(${deli.DELI_SCHE_KEYNO})" >${deli.DELI_SCHE_NM}</a></td>
													<td class="text-center">${deli.SCHE_DT}</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${deli.DELI_SCHE_DECIDE_FG eq 'Y'}">명단 확정</c:when>
															<c:otherwise>명단 미확정</c:otherwise>
														</c:choose>
													</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${deli.DELI_SCHE_DECIDE_FG eq 'Y'}">
																<a href="javascript:fn_view(${deli.DELI_SCHE_KEYNO})" >총  ${deli.TOTAL}명 ( 내부  ${deli.INNER_USER}명, 외부  ${deli.OUTER_USER}명 )</a>
															</c:when>
															<c:otherwise>-</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div><!-- end table-responsive -->
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		/* table 지정 */
		$("#deli_attend_A").DataTable({	
			responsive: true,
			destroy: true,
			"order": [[ 0, 'desc' ]],
			columnDefs: [
					{"targets" : 0, "searchable": true, "orderable" : true},
					{"targets" : 1, "searchable": true, "orderable" : true},
					{"targets" : 2, "searchable": true, "orderable" : true},
					{"targets" : 3, "searchable": true, "orderable" : true},
					{"targets" : 4, "searchable": false, "orderable" : false}
			]
		});
		
		$("#deli_attend_B").DataTable({	
			responsive: true,
			destroy: true,
			"order": [[ 0, 'desc' ]],
			columnDefs: [
					{"targets" : 0, "searchable": true, "orderable" : true},
					{"targets" : 1, "searchable": true, "orderable" : true},
					{"targets" : 2, "searchable": true, "orderable" : true},
					{"targets" : 3, "searchable": true, "orderable" : true},
					{"targets" : 4, "searchable": false, "orderable" : false}
			]
		});
	
		<c:if test="${!empty message }">alert('${message}');</c:if>
	});
	
	/* 심의 참석자 조회 */
	function fn_view(deli_sche_keyno) {
		var frm = document.deli_sche_form;
		$("#DELI_SCHE_KEYNO").val(deli_sche_keyno);
		frm.action = "/eirb01/10000102/view";
		frm.submit();
	}
</script>
<style type="text/css">
	th { text-align: center; }
</style>