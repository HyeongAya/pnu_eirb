﻿<script type="text/javascript">
	$('.summernote').summernote({
		height: 150,
		lang: 'ko-KR'
	});
</script>
<div class="page-title">
	<h3>점검표</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="col-md-12">
						<h2 class="col-md-offset-2">초기심의 제출서류 점검표</h2>
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<thead>
										<th></th>
										<th>제출목록</th>
										<th>제출여부</th>
										<th>비 고</th>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>초기심의 신청서</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>2</td>
											<td>연구계획서</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>3</td>
											<td>연구계획서 요약</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>4</td>
											<td>조사도구 또는 조사기록지 ( 설문지 포함 )</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>5</td>
											<td>연구대상자 설명문과 동의서</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>6</td>
											<td>연구자 이력서</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>7</td>
											<td>이해상충공개서</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>8</td>
											<td>연구윤리 교육이수증</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>9</td>
											<td>연구비내역서</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>10</td>
											<td>연구대상자 모집관련 자료 ( 해당되는 경우 )</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>11</td>
											<td>물질양도협약서 ( 해당되는 경우 )</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>12</td>
											<td>심의면제 신청서 ( 해당되는 경우 )</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>13</td>
											<td>동의면제 신청서 ( 해당되는 경우 )</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>14</td>
											<td>동의서면제 신청서 ( 해당되는 경우 )</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
										<tr>
											<td>15</td>
											<td>기타</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD"></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						<h2 class="col-md-offset-2">연구계획서 점검표(인간대상연구)</h2>
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col width="5%" />
										<col width="30%" />
										<col width="5%" />
										<col width="5%" />
										<col width="5%" />
										<col width="5%" />
										<col width="30%" />
										<col width="5%" />
										<col width="5%" />
										<col width="5%" />
									</colspan>
									<thead>
										<th></th>
										<th>제출목록</th>
										<th>있음</th>
										<th>요청</th>
										<th>해당없음</th>
										<th></th>
										<th>제출목록</th>
										<th>있음</th>
										<th>요청</th>
										<th>해당없음</th>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>초기심의 신청서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>7</td>
											<td>연구비내역서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>2</td>
											<td>연구계획서 & 요약</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>8</td>
											<td>연구대상자 모집관련 자료</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>3</td>
											<td>조사도구 또는 조사기록지(설문지 포함)</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>9</td>
											<td>물질양도협약서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>4</td>
											<td>연구대상자 설명문과 동의서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>10</td>
											<td>심의면제 신청서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>5</td>
											<td>연구자 이력서 & 교육이수증</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>11</td>
											<td>동의면제 신청서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>6</td>
											<td>이해상충공개서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>12</td>
											<td>동의서면제 신청서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col width="5%"/>
										<col width="20%"/>
										<col width="60%"/>
										<col width="5%"/>
										<col width="5%"/>
										<col width="5%"/>
									</colspan>
									<thead>
										<th></th>
										<th colspan="2">점검내용</th>
										<th>적절</th>
										<th>미흡</th>
										<th>비해당</th>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td colspan="2">연구설계자가 연구목적을 해결하는데 적절하다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td> <!-- insufficiency -->
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>2</td>
											<td colspan="2">타당한 연구설계로 연구대상자에게 부가되는 위험 수준이 최소화 되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>3</td>
											<td colspan="2">연구대상자에게 예상되는 위험수준은 예상되는 이득과 결과를 고려할 때 적절한 수준이다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>4</td>
											<td colspan="2">연구대상자 선정 및 배정은 공정하게 이루어진다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>5</td>
											<td colspan="2">연구대상자에게 지급되는 사례비는 적절한 수준이다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>6</td>
											<td colspan="2">연구대상자의 안전을 보장하기 위한 적절한 모니터링 계획을 가지고 있으며 위기 및 돌발상황이 발생할 가능성이 있는 경우 이에 대한 대처방안이 수립되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>7</td>
											<td colspan="2">연구자료, 연구대상자의 개인식별정보 처리 및 사생활 보호를 위한 충분한 조치가 되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>8</td>
											<td colspan="2">연구종료 후 개인정보 및 연구자료 처리에 대한 방안이 수립되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>9</td>
											<td colspan="2">동의취득과정이 적절하다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>10</td>
											<td colspan="2">동의는 적절히 서면화(documentation)된다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td rowspan="2">11</td>
											<td colspan="2">취약한 환경의 연구대상자가 참여하며, 강압이나 부당한 영향에 취약할 가능성이 있는 연구대상자의 권리와 복지를 위해 추가적인 안전장치가 마련되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td colspan="5">
												취약한 연구대상자라면 다음 중 어디에 해당하는가?
												<div class="checkbox">
													<label>
														<input type="checkbox"> 임산부, 태아
													</label>
													<label>
														<input type="checkbox"> 신생아
													</label>
													<label>
														<input type="checkbox"> 미성년자
													</label>
													<label>
														<input type="checkbox"> 노인
													</label>
													<label>
														<input type="checkbox"> 군인
													</label>
													<label>
														<input type="checkbox"> 수감자
													</label>
													<label>
														<input type="checkbox"> 원내 직원 또는 학생
													</label>
													<label>
														<input type="checkbox"> 말기 환자
													</label>
													<label>
														<input type="checkbox"> 의사표시를 할 수 없는 정신지체자 및 장애인
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>12</td>
											<td colspan="2">전반적인 위험/이익의 비 (risk/benefit ratio)는 수용할 만한가?</td>
											<td colspan="3">
												<input type="radio" name="DELI_CHK_RIT_CD" value="Y">예
												<input type="radio" name="DELI_CHK_RIT_CD" value="N">아니오
											</td>
										</tr>
										<tr>
											<td rowspan="2">13</td>
											<td colspan="2">취약한 연구대상자인가?</td>
											<td colspan="3">
												<input type="radio" name="DELI_CHK_RIT_CD" value="Y">예
												<input type="radio" name="DELI_CHK_RIT_CD" value="N">아니오
											</td>
										</tr>
										<tr>
											<td colspan="2">'예'라면 취약성의 배경은?</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 연령
													</label>
													<label>
														<input type="checkbox"> 질병
													</label>
													<label>
														<input type="checkbox"> 상활
													</label>
													<label>
														<input type="checkbox"> 기타
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>14</td>
											<td colspan="2">동의(informed consent)가 필요한가?</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 연구대상자 동의
													</label>
													<label>
														<input type="checkbox"> 보호자 동의
													</label>
													<label>
														<input type="checkbox"> 불필요 ( 면제가능 )
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>15</td>
											<td colspan="2">동의의 서면화(동의서)가 필요한가?</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 서면동의 필요
													</label>
													<label>
														<input type="checkbox"> 구두동의 필요
													</label>
													<label>
														<input type="checkbox"> 불필요 ( 면제가능 )
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>16</td>
											<td colspan="2">연구대상자 승낙(assent)이 필요한가?</td>
											<td colspan="3">
												<input type="radio" name="DELI_CHK_RIT_CD" value="Y">예
												<input type="radio" name="DELI_CHK_RIT_CD" value="N">아니오
											</td>
										</tr>
										<tr>
											<td>17</td>
											<td colspan="2">최종 의사결정</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 승인
													</label>
													<label>
														<input type="checkbox"> 시정승인
													</label>
													<label>
														<input type="checkbox"> 보완
													</label>
													<label>
														<input type="checkbox"> 반려
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>18</td>
											<td>지속심의 주기</td>
											<td>
												<div class="checkbox">
													<label>
														<input type="checkbox"> 1단계 ( 위험성이 낮은 연구 )
													</label>
													<label>
														<input type="checkbox"> 2단계 ( 위험성이 있지만 연구대상자에게 직접적인 이익이 기대되는 연구 )
													</label>
													<label> 
														<input type="checkbox"> 3단계 ( 위험성이 있고 연구대상자에게 직접적인 이익이 없지만 일반화된 지식 발전에 도움이 되는 연구 )
													</label>
													<label>
														<input type="checkbox"> 4단계 ( 위험성이 큰 연구 )
													</label>
												</div>
											</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 12개월미만 ( 개월 미만)
													</label>
													<label>
														<input type="checkbox"> 12개월마다
													</label>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					
						<h2 class="col-md-offset-2">연구계획서 점검표(인체유래물연구)</h2>
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col width="5%" />
										<col width="30%" />
										<col width="5%" />
										<col width="5%" />
										<col width="5%" />
										<col width="5%" />
										<col width="30%" />
										<col width="5%" />
										<col width="5%" />
										<col width="5%" />
									</colspan>
									<thead>
										<th></th>
										<th>제출목록</th>
										<th>있음</th>
										<th>요청</th>
										<th>해당없음</th>
										<th></th>
										<th>제출목록</th>
										<th>있음</th>
										<th>요청</th>
										<th>해당없음</th>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>초기심의 신청서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>7</td>
											<td>연구비내역서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>2</td>
											<td>연구계획서 & 요약</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>8</td>
											<td>연구대상자 모집관련 자료</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>3</td>
											<td>조사도구 또는 조사기록지(설문지 포함)</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>9</td>
											<td>물질양도협약서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>4</td>
											<td>연구대상자 설명문과 동의서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>10</td>
											<td>심의면제 신청서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>5</td>
											<td>연구자 이력서 & 교육이수증</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>11</td>
											<td>동의면제 신청서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>6</td>
											<td>이해상충공개서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
											<td>12</td>
											<td>동의서면제 신청서</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="R"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col width="5%"/>
										<col width="20%"/>
										<col width="60%"/>
										<col width="5%"/>
										<col width="5%"/>
										<col width="5%"/>
									</colspan>
									<thead>
										<th></th>
										<th colspan="2">점검내용</th>
										<th>적절</th>
										<th>미흡</th>
										<th>비해당</th>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td colspan="2">연구내용이 인체유래물을 대상으로 하여야 적절한 연구이다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td> <!-- insufficiency -->
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>2</td>
											<td colspan="2">타당한 연구설계로 연구대상자에게 부가되는 위험 수준이 최소화 되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>3</td>
											<td colspan="2">연구대상자에게 예상되는 위험수준은 예상되는 이득과 결과를 고려할 때 적절한 수준이다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>4</td>
											<td colspan="2">연구대상자 선정 및 배정은 공정하게 이루어진다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>5</td>
											<td colspan="2">연구대상자에게 지급되는 사례비는 적절한 수준이다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>6</td>
											<td colspan="2">연구대상자의 안전을 보장하기 위한 적절한 모니터링 계획을 가지고 있으며 위기 및 돌발상황이 발생할 가능성이 있는 경우 이에 대한 대처방안이 수립되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>7</td>
											<td colspan="2">인체유래물의 보관, 보존 및 폐기절차 등이 적절하며 이를 구체적으로 명시하고 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>8</td>
											<td colspan="2">연구대상자의 개인정보 및 사생활 보호를 위한 충분한 조치가 되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>9</td>
											<td colspan="2">인체유래물과 그로부터 얻은 유전정보를 포함한 연구 자료의 유출 위험을 최소화할 수 있는 충분한 조치가 준비되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>10</td>
											<td colspan="2">인체유래물 제공자에게 중요한 정보가 발견될 경우 이를 알려줄 계획이 수립되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>11</td>
											<td colspan="2">동의취득과정이 적절하다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>12</td>
											<td colspan="2">동의는 적절히 서면화(documentation)된다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td rowspan="2">13</td>
											<td colspan="2">취약한 환경의 연구대상자가 참여하며, 강압이나 부당한 영향에 취약할 가능성이 있는 연구대상자의 권리와 복지를 위해 추가적인 안전장치가 마련되어 있다.</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td colspan="5">
												취약한 연구대상자라면 다음 중 어디에 해당하는가?
												<div class="checkbox">
													<label>
														<input type="checkbox"> 임산부, 태아
													</label>
													<label>
														<input type="checkbox"> 신생아
													</label>
													<label>
														<input type="checkbox"> 미성년자
													</label>
													<label>
														<input type="checkbox"> 노인
													</label>
													<label>
														<input type="checkbox"> 군인
													</label>
													<label>
														<input type="checkbox"> 수감자
													</label>
													<label>
														<input type="checkbox"> 원내 직원 또는 학생
													</label>
													<label>
														<input type="checkbox"> 말기 환자
													</label>
													<label>
														<input type="checkbox"> 의사표시를 할 수 없는 정신지체자 및 장애인
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>14</td>
											<td colspan="2">전반적인 위험/이익의 비 (risk/benefit ratio)는 수용할 만한가?</td>
											<td colspan="3">
												<input type="radio" name="DELI_CHK_RIT_CD" value="Y">예
												<input type="radio" name="DELI_CHK_RIT_CD" value="N">아니오
											</td>
										</tr>
										<tr>
											<td rowspan="2">15</td>
											<td colspan="2">취약한 연구대상자인가?</td>
											<td colspan="3">
												<input type="radio" name="DELI_CHK_RIT_CD" value="Y">예
												<input type="radio" name="DELI_CHK_RIT_CD" value="N">아니오
											</td>
										</tr>
										<tr>
											<td colspan="2">'예'라면 취약성의 배경은?</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 연령
													</label>
													<label>
														<input type="checkbox"> 질병
													</label>
													<label>
														<input type="checkbox"> 상활
													</label>
													<label>
														<input type="checkbox"> 기타
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>16</td>
											<td colspan="2">동의(informed consent)가 필요한가?</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 연구대상자 동의
													</label>
													<label>
														<input type="checkbox"> 보호자 동의
													</label>
													<label>
														<input type="checkbox"> 불필요 ( 면제가능 )
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>17</td>
											<td colspan="2">동의의 서면화(동의서)가 필요한가?</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 서면동의 필요
													</label>
													<label>
														<input type="checkbox"> 구두동의 필요
													</label>
													<label>
														<input type="checkbox"> 불필요 ( 면제가능 )
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>18</td>
											<td colspan="2">연구대상자 승낙(assent)이 필요한가?</td>
											<td colspan="3">
												<input type="radio" name="DELI_CHK_RIT_CD" value="Y">예
												<input type="radio" name="DELI_CHK_RIT_CD" value="N">아니오
											</td>
										</tr>
										<tr>
											<td>19</td>
											<td colspan="2">최종 의사결정</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 승인
													</label>
													<label>
														<input type="checkbox"> 시정승인
													</label>
													<label>
														<input type="checkbox"> 보완
													</label>
													<label>
														<input type="checkbox"> 반려
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>20</td>
											<td>지속심의 주기</td>
											<td>
												<div class="checkbox">
													<label>
														<input type="checkbox"> 1단계 ( 위험성이 낮은 연구 )
													</label>
													<label>
														<input type="checkbox"> 2단계 ( 위험성이 있지만 연구대상자에게 직접적인 이익이 기대되는 연구 )
													</label>
													<label> 
														<input type="checkbox"> 3단계 ( 위험성이 있고 연구대상자에게 직접적인 이익이 없지만 일반화된 지식 발전에 도움이 되는 연구 )
													</label>
													<label>
														<input type="checkbox"> 4단계 ( 위험성이 큰 연구 )
													</label>
												</div>
											</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 12개월미만 ( 개월 미만)
													</label>
													<label>
														<input type="checkbox"> 12개월마다
													</label>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					
						<h2 class="col-md-offset-2">동의서 점검표 (인간대상연구)</h2>
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col width="5%"/>
										<col />
										<col width="5%"/>
										<col width="5%"/>
										<col width="5%"/>
									</colspan>
									<thead>
										<th></th>
										<th>점검내용</th>
										<th>적절</th>
										<th>미흡</th>
										<th>비해당</th>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>연구 목적 및 목적으로 실시된다는 사실</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>2</td>
											<td>연구대상자 선정 및 제외기준</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>3</td>
											<td>연구대상자가 받게 될 각종 검사나 절차</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>4</td>
											<td>연구군 또는 대조군에 무작위 배정될 확률</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>5</td>
											<td>연구대상자가 준수해야 할 사항</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>6</td>
											<td>연구대상자에게 미칠 것으로 예견되는 위험이나 불편</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>7</td>
											<td>기대되는 이익 또는 기대되는 이익이 없는 경우에는 없다는 사실</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>8</td>
											<td>연구대상자의 안전보호대책</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>9</td>
											<td>연구 참여와 관련된 손상 발생 시 연구대상자에게 주어질 보상이나 치료방법</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>10</td>
											<td>연구 참여로 인해 방게 될 금전적 보상</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>11</td>
											<td>연구 참여로 인해 연구대상자가 부담해야 할 예상 비용</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>12</td>
											<td>연구 참여 결정은 자발적인 것이며 연구도중 언제라도 중도에 참여를 포기할 수 있음</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>13</td>
											<td>연구대상자가 연구의 참가에 동의하지 않더라도 어떠한 불이익도 받지 않음</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>14</td>
											<td>연구자, 의뢰자, 정부당국 등 관련자가 자료를 열람할 수 있음</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>15</td>
											<td>연구대상자의 신원을 파악할 수 있는 기록은 비밀 보장됨</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>16</td>
											<td>연구와 관련한 새로운 정보가 수집되면 연구대상자에게 알려줌</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>17</td>
											<td>연구자와 심의위원회 연락처</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>18</td>
											<td>연구참여를 제한하는 경우 및 해당 사유</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>19</td>
											<td>대략의 연구대상자 수</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>20</td>
											<td>연구참여 기간</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>21</td>
											<td>동의능력이 없거나 불완전하여 대리인의 동의가 필요</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>22</td>
											<td>대리인 동의 필요시 연구대상자에 대한 충분한 설명과 승낙 취득</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td></td>
											<td>최종 의사결정</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 승인
													</label>
													<label>
														<input type="checkbox"> 시정승인
													</label>
													<label>
														<input type="checkbox"> 보완
													</label>
													<label>
														<input type="checkbox"> 반려
													</label>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<h2 class="col-md-offset-2">동의서 점검표 (인체유래물연구)</h2>
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col width="5%"/>
										<col />
										<col width="5%"/>
										<col width="5%"/>
										<col width="5%"/>
									</colspan>
									<thead>
										<th></th>
										<th>점검내용</th>
										<th>적절</th>
										<th>미흡</th>
										<th>비해당</th>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>연구목적과 연구 목적으로 실시된다는 사실</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>2</td>
											<td>연구대상자가 받게 될 각종 검사나 절차</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>3</td>
											<td>연구군 또는 대조군에 무작위 배정될 확률</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>4</td>
											<td>연구대상자가 준수해야 할 사항</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>5</td>
											<td>연구대상자에게 미칠 것으로 예견되는 위험이나 불편</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>6</td>
											<td>인체유래물 보관, 보존 및 폐기 방법과 절차에 대한 사항</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>7</td>
											<td>연구대상자의 개인식별정보 및 사생활 보호 및 처리방안</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>8</td>
											<td>인체유래물과 그로부터 얻는 유전정보를 포함한 연구 자료의 유출 위험을 최소화할 수 있는 조치</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>9</td>
											<td>기대되는 이익 또는 기대되는 이익이 없는 경우에는 없다는 사실</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>10</td>
											<td>연구대상자가 선택할 수 있는 다른 중재</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>11</td>
											<td>연구 참여와 관련된 손상 발생 시 연구대상자에게 주어질 보상이나 치료방법</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>12</td>
											<td>연구 참여로 인해 받게 될 금전적 보상</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>13</td>
											<td>연구 참여로 인해 연구대상자가 부담해야 할 예상 비용</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>14</td>
											<td>연구 참여 결정은 자발적인 것임</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>15</td>
											<td>연구도중 언제라도 중도에 참여를 포기할 수 있음</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>16</td>
											<td>연구대상자가 연구의 참가에 동의하지 않더라도 어떠한 불이익도 받지 않음</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>17</td>
											<td>연구자, 의뢰자, 정부당국 등 관련자가 자료를 열람할 수 있음</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>18</td>
											<td>연구대상자의 신원을 파악할 수 있는 기록은 비밀 보장됨</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>19</td>
											<td>인체유래물 제공자에게 중요한 정보가 발견된 경우 연구대상자에게 알려줌</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>20</td>
											<td>연구자 연락처</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>21</td>
											<td>심의위원회 연락처</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>22</td>
											<td>연구참여를 제한하는 경우 및 해당 사유</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>23</td>
											<td>대략의 연구대상자 수</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td>24</td>
											<td>연구참여 기간</td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="Y"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="I"></td>
											<td><input type="radio" name="DELI_CHK_RIT_CD" value="N"></td>
										</tr>
										<tr>
											<td></td>
											<td>최종 의사결정</td>
											<td colspan="3">
												<div class="checkbox">
													<label>
														<input type="checkbox"> 승인
													</label>
													<label>
														<input type="checkbox"> 시정승인
													</label>
													<label>
														<input type="checkbox"> 보완
													</label>
													<label>
														<input type="checkbox"> 반려
													</label>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					
						<h2 class="col-md-offset-2">심의면제 점검표</h2>
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col width="15%"/>
										<col width="40%"/>
										<col width="40%"/>
										<col width="5%"/>
									</colspan>
									<thead>
										<th colspan="3">내용</th>
										<th>표시</th>
									</thead>
									<tbody>
										<tr>
											<td rowspan="8">인간대상연구</td>
											<td colspan="2">1. 일반 대중에게 공개된 정보를 이용하거나 개인식별정보를 수집 기록하지 않는 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td rowspan="4">2-1. 연구대상자를 직접 조작하거나 그 환경을 조작하는 연구인 경우</td>
											<td>1) 약물투여, 혈액채취 등 침습성 행위가 없는 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>2) 신체적 변화가 따르지 않는 단순 접촉 측정 또는 관찰 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>3) 판매 등이 허용되는 식품이나 식품첨가물을 이용한 맛이나 질 평가 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>4) 안전기준에 맞는 화장품의 사용감 또는 만족감 조사 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td colspan="2">2-2. 연구대상자 등을 직면 대면하더라도 연구대상자가 특정되지 않고 민감정보를 수집 기록하지 않는 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td colspan="2">2-3. 연구대상자 등에 대한 기존의 자료나 문서를 이용하는 연구 ( 이차자료연구 )</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td colspan="2">3. 취약한 환경에 있는 연구대상자를 포함하지 않는 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td colspan="4">심의면제대상 : 1 AND 2-1, 2-2, 2-3 중 하나 AND 3</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col width="15%"/>
										<col />
										<col width="5%"/>
									</colspan>
									<thead>
										<th colspan="2">내용</th>
										<th>표시</th>
									</thead>
									<tbody>
										<tr>
											<td rowspan="7">인체유래물연구</td>
											<td>1. 개인식별정보를 수집 기록하지 않는 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>2-1. 인체유래물 은행의 인체유래물과 그로부터 얻은 유전정보를 이용하는 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>2-2. 의료기관에서 사용 후 남은 인체유래물 등으로 검사실 정도관리 및 검사법을 평가하는 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>2-3. 일반 대중이 이용할 수 있도록 인체유래물로부터 분리 가공된 연구재료 사용 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>2-4. 연구결과가 기증자 개인의 유전적 특징과 무관한 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>3. 통상적인 교육과정의 범위에서 실무와 관련된 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>4. 공중보건상 긴급한 조치가 필요한 상황에서 국가 또는 지방자치단체가 수행하는 연구</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td colspan="4">심의면제대상 : 1 AND 2-1 ~ 4 중 하나</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<tbody>
										<tr>
											<th>IRB 위원 작성란</th>
										</tr>
										<tr>
											<td>
												<div class="checkbox">
													<label class="col-md-12">
														<input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"> 제출된 연구과제는 심의되었으며, IRB 심의면제가 가능합니다.
													</label>
													<label class="col-md-12">
														<input type="checkbox" name="DELI_CHK_RIT_CD" value="N"> 제출된 연구과제는 심의되었으며, IRB 심의면제가 불가능합니다.
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<th>심의자 의견</th>
										</tr>
										<tr>
											<td><textarea id="DELI_CHK_RIT_CD" name="DELI_CHK_RIT_CD" class="form-control summernote"></textarea></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					
						<h2 class="col-md-offset-2">동의면제 점검표</h2>
						<span class="col-md-offset-2">※ 다음의 질문에 모두 해당하면 동의면제가 가능합니다.</span>
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col />
										<col width="10%"/>
									</colspan>
									<thead>
										<th>내용</th>
										<th>표시</th>
									</thead>
									<tbody>
										<tr>
											<td>1. 연구대상자에게 부과되는 위험이 매우 낮은 연구 ( 최소위험 연구 )인가?</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>2. 동의면제가 연구대상자의 권리나 복지를 침해하지 않는가?</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>3. 동의를 받는 것이 현실적으로 불가능하거나 연구타당도에 심각한 영향을 주는가?</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>4. 연구대상자가 연구참여를 동의하지 않을 것이라고 추정하기 어려운가?</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>5. 대리인 동의를 받지 않아도 되는가?</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>6. 취약한 연구대상자는 포함되어 있지 않은가?</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<tbody>
										<tr>
											<th>IRB 위원 작성란</th>
										</tr>
										<tr>
											<td>
												※ 이상의 점검표를 확인한 결과 동의면제가 가능하다.
												<input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"> 예
												<input type="checkbox" name="DELI_CHK_RIT_CD" value="N"> 아니오
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						<h2 class="col-md-offset-2">동의서면제 점검표</h2>
						<span class="col-md-offset-2">※ 다음의 세 가지 경우 모두에 해당하면 서면동의를 면제할 수 있습니다.</span>
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<colspan>
										<col />
										<col width="10%"/>
									</colspan>
									<thead>
										<th>내용</th>
										<th>표시</th>
									</thead>
									<tbody>
										<tr>
											<td>1. 동의서에만 연구대상자가 연구에 참여했다는 사실이 기록되는가?</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>2. 연구기록의 비밀이 유지되지 않으면 연구대상자에게 중대한 피해가 예상되는가?</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
										<tr>
											<td>3. 연구대상자가 연구참여에 동의하지 않을 이유가 없다고 추정되는가?</td>
											<td><input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="appl_table" class="table table-bordered" >
									<tbody>
										<tr>
											<th>IRB 위원 작성란</th>
										</tr>
										<tr>
											<td>
												※ 이상의 점검표를 확인한 결과 동의 서면화 면제가 가능하다.
												<input type="checkbox" name="DELI_CHK_RIT_CD" value="Y"> 예
												<input type="checkbox" name="DELI_CHK_RIT_CD" value="N"> 아니오
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
