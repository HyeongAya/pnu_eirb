﻿<script type="text/javascript">
	$(document).ready(function(){
		/* table 지정 */
		var table = $('#deli_Task').DataTable({
			"columnDefs": [ 
				{
					targets: [ 0 ],
					searchable: false ,
					orderable : false
				}
			]
		});
		
		$("#check_all").click(function() {
			if(this.checked) {
				$("input[name=CHECK_KEY]:checkbox").each(function() {
					$(this).prop("checked", true);
					$('.checker').children().addClass('checked');
				});
			} else {
				$("input[name=CHECK_KEY]:checkbox").each(function() {
					$(this).prop("checked", false);
					$('.checker').children().removeClass('checked');
				});
			}
		});
		
		$('input[name=CHECK_KEY]').click(function() {
			$('#check_all').prop('checked', false);
			$('#uniform-check_all').children().removeClass('checked');
		});
		
		var paramArray = new Array();
		
		$('input[name=CHECK_KEY]').each(function(idx) {
			if($(this).prop('checked') || saveFlag == 'save') {
				var obj = new Object();
				obj['EDU_KEYNO'] = $(this).val();
				paramArray.push(obj);
			}
		});
	});

	/* 등록  */
	function fn_save(statue, text) {
		
		var paramArray = new Array();
		
		$('input[name=CHECK_KEY]').each(function(idx) {
			if($(this).prop('checked')) {
				var obj = new Object();
				obj['DELI_APPL_KEYNO'] = $(this).val();
				paramArray.push(obj);
			}
		});
		
		$('input[name=SAVE_DATA]').val(JSON.stringify(paramArray));
		
		alert($('input[name=SAVE_DATA]').val());
	}
	/* 뷰페이지 이동 */
	function fn_view(deli_appl_keyno) {
		var frm = document.deli_attend_form;
		$("#DELI_APPL_KEYNO").val(deli_appl_keyno);
		frm.action = "/eirb01/10000103/view";
		frm.submit();
	}
</script>
<div class="page-title">
	<h3>심의과제 관리</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-body">
					<form id="deli_attend_form" name="deli_attend_form" method="post" enctype="application/x-www-form-urlencoded">
						<input type="hidden" name="DELI_SCHE_KEYNO" id="DELI_SCHE_KEYNO" value="${DELI_SCHE_KEYNO}"/>
						<input type="hidden" name="DELI_APPL_KEYNO" id="DELI_APPL_KEYNO" value="${DELI_APPL_KEYNO}"/>
						<input type="hidden" name="SAVE_DATA" id="SAVE_DATA"/>
						<input type="hidden" name="USER_ID" id="USER_ID" value="ADMIN"/>
						<input type="hidden" name="INS_IP" id="INS_IP" value="127.0.0.1"/>
						<input type="hidden" name="UPD_IP" id="UPD_IP" value="127.0.0.1"/>

						<div class="table-responsive">
							<div class="form-group col-md-8 col-md-offset-2">
								<table id="deli_Task" class="display table" style="width: 100%; cellspacing: 0;">
									<thead>
										<tr>
											<th><input type="checkbox" id="check_all" name="check_all" title="전체 선택"></th>
											<th>연구유형</th>
											<th>과제번호</th>
											<th>과제명</th>
											<th>연구책임자</th>
											<th>등록 일자</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="deliT" items="${DELI_TASK_LIST}" varStatus="status">
											<tr>
												<td onclick="event.cancelBubble = true;">
													<div id="check_item">
														<input type="checkbox" name="CHECK_KEY" value="${deliT.DELI_APPL_KEYNO}">
													</div>
												</td>
												<td>
													<jsp:include page="/comm/deli_data" flush="false">
														<jsp:param name="MAP" value="Common.SELECT_COMSUB_CODE" />
														<jsp:param name="PARAM" value="[{'MAIN_CD':'AP_OB', 'SUB_CD':'${deliT.DELI_APPL_OBJ_CD}'}]" />
													</jsp:include>
												</td>
												<td><a href="javascript:fn_view('${deliT.DELI_APPL_KEYNO}')" >${deliT.DELI_APPL_KEYNO}</a></td>
												<td>${deliT.DELI_APPL_TITLE_KO}</td>
												<td>${deliT.DELI_APPL_MNG_ID} (${deliT.DELI_APPL_DEPT_CD} / ${deliT.DELI_APPL_MNG_POS} )</td>
												<td>${deliT.INS_DT}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</form>
					<div class="col-md-12">
						<div class="text-center">
							<button type="button" class="btn btn-primary btn-rounded" onclick="fn_save('temp_add','임시저장');">과제선정</button>
							<a href="/eirb01/10000102/" class="btn btn-default btn-rounded">목록</a>
						</div>
					</div>
				</div><!-- end panel-body -->
			</div><!-- end panel panel-white -->
		</div><!-- end col-md-12 -->
	</div><!-- end row -->
</div><!-- end main-wrapper -->
