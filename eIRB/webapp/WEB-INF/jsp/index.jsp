<%@page import="java.awt.Color"%>
<%@page import="java.util.Random"%>
<%@page import="kr.ac.pusan.core.ProjectConstant"%>

<%
	Random random = new Random();
	Color color = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
	String randValue = Integer.toHexString(color.getRGB());
	session.setAttribute("CSRF_TOKEN", randValue);
%>

<!DOCTYPE html>

<html lang="ko">
	<head>
		<jsp:include page="${contextPath}/include/eirb_index/header" />
	</head>
	<body>
		<div id="dc_accessibility">
			<a href="#dc_contents">본문 바로가기</a>
			<a href="#dc_footer">페이지 하단 바로가기</a>
		</div>
		<div id="dc_wrap">
			<div id="dc_contents" class="dc_login_wrap">
				<div class="dc_login">
					<div class="dc_login_input">
						<h1><img src="/assets/images/common/login_logo.gif" alt="부산대학교" /></h1>
						<div class="dc_login_title">
							<h2>생명윤리위원회 전자심의시스템</h2>
						</div>
						<form id="frm_login" name="frm_login" method="post" action="login">
						<input type="hidden" name="CT" value="<%=randValue %>" />
						<fieldset>
							<legend class="dc_blind">로그인</legend>
							<label for="user_id" class="dc_blind">아이디(<%=randValue%>)</label><input type="text" id="user_id" name="user_id" size="20" />
							<label for="user_pw" class="dc_blind">패스워드</label><input type="password" id="user_pw" name="user_pw" size="20" autocomplete="off" />
							<span class="btn_login"><a href="#dc_wrap">로그인</a></span>
							<span class="join"><a href="/join">회원가입</a>&nbsp;|&nbsp;<a href="/search">아이디/비밀번호 찾기</a></span>
						</fieldset>
						</form>
					</div>
				</div>
			</div>
			<jsp:include page="${contextPath}/include/eirb_index/footer" />
		</div>
	</body>
	
	<script type="text/javascript">
		
	$(document).ready(function() {
		var req  = new custom_request();
		var msg  = req.getParameter('message');
		
		if(msg != '') {
			alert(msg);
		}
		
		$('#user_pw, #user_id').keypress(function(event) {
			if(event.charCode == '13') {
				$('.btn_login').click();
			}
		});
		
		$('.btn_login').click(function() {
			if($('#user_id').val() == '') {
				alert('아이디를 입력해 주십시오.');
				$('#user_id').focus();
				
				return;
			}
			
			if($('#user_pw').val() == '') {
				alert('비밀번호를 입력해 주십시오.');
				$('#user_pw').focus();
				
				return;
			}
			$('#frm_login').submit();
		});
	});
	
	</script>
</html>