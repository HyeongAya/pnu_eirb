﻿<div class="page-title">
	<h3>회원관리</h3>
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-lg-7 col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-list"></i> 회원 리스트</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="열기/접기" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:setBoundary();" data-toggle="tooltip" data-placement="top" title="새로고침" class="panel-reload"><i class="icon-reload"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<div class="table-responsive">
						<form name="user_list_form" id="user_list_form" method="post">
							<input type="hidden" name="SITE_KEYNO" id="SITE_KEYNO"/>
							<input type="hidden" name="BBS_KEYNO" id="BBS_KEYNO"/>
							<table id="data_table" class="display table" style="width:100%; cellspacing:0;">
								<colgroup>
									<col width="10%"/>
									<col width="20%"/>
									<col width="15%"/>
									<col width="15%"/>
									<col />
									<col width="15%"/>
									<col width="10%"/>
								</colgroup>
								<thead>
									<tr>
										<th class="text-center">NO</th>
										<th class="text-center">사용자구분</th>
										<th class="text-center">아이디</th>
										<th class="text-center">이름</th>
										<th class="text-center">소속</th>
										<th class="text-center">직위</th>
										<th class="text-center">승인</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${USER_DATA}" var="item" varStatus="status">
										<tr style="cursor:pointer;" onclick="javascript:fn_view('${item.USER_ID}')">
											<td class="text-center">${status.index + 1}</td>
											<td class="text-center">${item.AUTH_NM}</td>
											<td class="text-center">${item.USER_ID}</td>
											<td class="text-center">${item.USER_NM}</td>
											<td class="text-center">${item.DEPT_NM}</td>
											<td class="text-center">${item.USER_POS}</td>
											<td class="text-center">
											<c:choose>
												<c:when test="${item.CONFIRM_FG eq 'Y'}">승인</c:when>
												<c:otherwise><span class="text-danger">미승인</span></c:otherwise>
											</c:choose>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5 col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil-square-o"></i> 회원 정보</h3>
					<div class="panel-control">
						<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="열기/접기" class="panel-collapse"><i class="icon-arrow-down"></i></a>
						<a href="javascript:setBoundary();" data-toggle="tooltip" data-placement="top" title="새로고침" class="panel-reload"><i class="icon-reload"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<form class="form-horizontal" id="user_form" name="user_form">
						<input type="hidden" id="USER_PW" name="USER_PW"/>
						<input type="hidden" id="INS_ID" name="INS_ID" value="${USER_ID}"/>
						<input type="hidden" id="INS_IP" name="INS_IP" value="127.0.0.1"/>
						<input type="hidden" id="UPD_ID" name="UPD_ID" value="${USER_ID}"/>
						<input type="hidden" id="UPD_IP" name="UPD_IP" value="127.0.0.1"/>
						<div class="form-group">
							<label for="USER_TYPE_CD" class="col-sm-2 control-label">사용자구분</label>
							<div class="col-sm-10">
								<jsp:include page="/comm/selectbox" flush="false">
									<jsp:param name="MAP" value="EIRB06_10000601.SELECT_AUTH_CD_LIST" />
									<jsp:param name="FLAG" value="SELECT" />
									<jsp:param name="CLASS" value="js-states form-control"/>
									<jsp:param name="TITLE" value="사용자구분 " />
									<jsp:param name="NAME" value="USER_TYPE_CD" />
									<jsp:param name="ID" value="USER_TYPE_CD" />
									<jsp:param name="VALUE" value="" />
									<jsp:param name="STYLE" value="width: 100%" />
								</jsp:include>
							</div>
						</div>
						<div class="form-group">
							<label for="USER_ID" class="col-sm-2 control-label">아이디</label>
							<div class="col-sm-10">
								<div class="input-group" id="input-group">
									<input type="text" id="USER_ID" name="USER_ID" class="form-control" placeholder="(6자리 이상)"/>
									<span class="input-group-addon btn btn-success" id="btn_id_chk">중복체크</span>
								</div>
							</div>
						</div>
						<!-- <div class="form-group">
							<label for="USER_PW" class="col-sm-2 control-label">비밀번호</label>
							<div class="col-sm-10">
								<input type="password" id="USER_PW" name="USER_PW" class="form-control" maxlength="20" autocomplete="off" placeholder="(영문+특수문자 조합, 8자리 이상)"/>
							</div>
						</div> -->
						<div class="form-group">
							<label for="USER_NM" class="col-sm-2 control-label">이름</label>
							<div class="col-sm-10">
								<input type="text" id="USER_NM" name="USER_NM" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="DEPT_NM" class="col-sm-2 control-label">소속</label>
							<div class="col-sm-10">
								<input type="text" id="DEPT_NM" name="DEPT_NM" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="USER_POS" class="col-sm-2 control-label">직위</label>
							<div class="col-sm-10">
								<input type="text" id="USER_POS" name="USER_POS" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="USER_TEL" class="col-sm-2 control-label">전화번호</label>
							<div class="col-sm-10">
								<input type="text" id="USER_TEL" name="USER_TEL" class="form-control" maxlength="13" placeholder="(예 : 051-1234-5678)"/>
							</div>
						</div>
						<div class="form-group">
							<label for="USER_PHONE" class="col-sm-2 control-label">휴대폰</label>
							<div class="col-sm-10">
								<input type="text" id="USER_PHONE" name="USER_PHONE" class="form-control" maxlength="13" placeholder="(예 : 010-1234-5678)"/>
							</div>
						</div>
						<div class="form-group">
							<label for="USER_EMAIL" class="col-sm-2 control-label">이메일</label>
							<div class="col-sm-10">
								<input type="text" id="USER_EMAIL" name="USER_EMAIL" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="CONFIRM_FG" class="col-sm-2 control-label">승인여부</label>
							<div class="col-sm-10">
								<div class="checkbox">
									<select id="CONFIRM_FG" name="CONFIRM_FG" class="form-control">
										<option value="Y">승인</option>
										<option value="N">미승인</option>
									</select>
								</div>
							</div>
						</div>		
						<div class="form-group">
							<div class="col-sm-12">
								<button type="button" class="btn btn-default btn-rounded pull-right m-l-xxs" id="btn_cancel"><i class="fa fa-repeat"></i> 취소</button>
								<button type="button" class="btn btn-danger btn-rounded pull-right m-l-xxs" id="btn_del"><i class="fa fa-trash"></i> 탈퇴</button>
								<button type="button" class="btn btn-warning btn-rounded pull-right m-l-xxs" id="btn_modify"><i class="fa fa-check"></i> 수정</button>
								<button type="button" class="btn btn-info btn-rounded pull-right m-l-xxs" id="btn_write"><i class="fa fa-pencil"></i> 등록</button>
								<button type="button" class="btn btn-success btn-rounded m-l-xxs" id="btn_lock"><i class="fa fa-lock"></i> 비밀번호 초기화</button>
							</div>
						</div>
					</form>
					<div class="alert alert-warning" role="alert">
						<ul class="list-unstyled">
							<li>
								<span class="text-info"><strong><i class="fa fa-lock"></i> 관리자에 의한 계정 생성과 비밀번호 초기화 시 비밀번호는 아이디로 설정됩니다.</strong></span>
							</li>
							<li>
								<i class="fa fa-long-arrow-right"></i><span> 추후 사용자에게 비밀번호 변경을 안내하십시오.</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

<script type="text/javascript">
	$(document).ready(function() {
		var req  = new custom_request();
		var msg  = req.getParameter('message');
		var temp_id = "";
		var dupChk = false;
		
		if(msg != '') {
			alert(msg);
		}
		
		$('#btn_lock, #btn_modify, #btn_del').hide();
		$('.input-group-addon').css('background-color','#12AFCB');
		$('.input-group-addon').css('border-color','transparent');
		
		$('#data_table').DataTable({
			"search": { caseInsensitive: false },
			"order": [[ 0, 'desc' ]],
			"columnDefs": [
				{"targets" : 0, "searchable": true, "orderable" : true},
				{"targets" : 1, "searchable": true, "orderable" : true},
				{"targets" : 2, "searchable": true, "orderable" : true},
			]
		});
		
		//연락처 유효성 체크 함수 추가
		$.validator.addMethod('customphone', function (value, element) {
			return this.optional(element) || /^\d{2,3}-\d{2,4}-\d{4}$/.test(value);
		}, "유효한 연락처 정보를 입력하세요.");
		
		var $validator = $("#user_form").validate({
			rules: {
				USER_TYPE_CD: {
					required: true
				},
				USER_ID: {
					required: true
				},
				USER_NM: {
					required: true
				},
				DEPT_NM: {
					required: true
				},
				USER_POS: {
					required: true
				},
				USER_TEL: {
					required: true, 
					customphone: true, 
					minlength: 10, 
					maxlength: 13
				},
				USER_PHONE: {
					required: true, 
					customphone: true, 
					minlength: 10, 
					maxlength: 13
				},
				USER_EMAIL: {
					required: true, 
					email: true, 
					maxlength: 50
				}
			}, 
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );
				
				if ( element.prop( "type" ) === "checkbox" ) {
					error.insertAfter( element.parent( "label" ) );
				} else {
					error.insertAfter( element );
				}
			},
			highlight: function ( element, errorClass, validClass ) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
			},
			unhighlight: function (element, errorClass, validClass) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
			},
			submitHandler: function (form) {
			},
		} );	
		
		/* 등록 */
		$('#btn_write').click(function() {
			var $valid = $("#user_form").valid();
			
			if(!$valid) {
				$validator.focusInvalid();
				return false;
			} else if(dupChk == false || temp_id != $('#USER_ID').val()) {
				alert("사용하실 아이디를 중복확인 해주세요.");
			} else {
				
				$('#USER_PW').val($('#USER_ID').val());
				
				var frm = document.user_form;
				frm.action = "/eirb06/10000601/joinProc";
				frm.submit();
				
				/* $.ajax({
					url : '/eirb06/10000601/joinProc.json',
					type : 'post',
					data:{
						USER_ID: $('#USER_ID').val(), 
						USER_NM: $('#USER_NM').val(), 
						USER_PW:  $('#USER_PW').val(), 
						DEPT_NM: $('#DEPT_NM').val(), 
						USER_POS: $('#USER_POS').val(), 
						USER_TEL: $('#USER_TEL').val(),
						USER_PHONE: $('#USER_PHONE').val(), 
						USER_EMAIL: $('#USER_EMAIL').val(), 
						USER_TYPE_CD: $('#USER_TYPE_CD').val(), 
						CONFIRM_FG: $('#CONFIRM_FG').val(), 
						INS_ID: $('#INS_ID').val(), 
						INS_IP: $('#INS_IP').val()
					},
					success : function(data) {
						var json = data;
						var dataList = json.result;
						if(dataList) {
							alert("등록이 완료되었습니다.");
							clearData();
							$('#data_table').load('/eirb06/10000601/#data_table');
						} else {
							alert("등록할 수 없는 데이터입니다.");
						}
					},
					error : function(data, status, err) {
						alert("회원 등록에 실패했습니다.\n관리자에게 문의 바랍니다.");
					}
				}); */
			}
		});
		
		$('#btn_id_chk').click(function() {
			var chkId = $('#USER_ID').val();
			
			if(chkId.length == 0) {
				alert("아이디를 입력해주세요.");
				$('#USER_ID').focus();
				return false;
			}
			if(chkId.length < 5) {
				alert("6자리 이상의 아이디를 입력해주세요.");
				$('#USER_ID').focus();
				return false;
			}
			
			$.ajax({
				url : '/id_chk.json',
				type : 'post',
				data : {"USER_ID" : chkId},
				success : function(data) {
					if(data.chk_value == 'N') {
						alert("사용가능한 아이디입니다.");
						dupChk = true;
						temp_id = chkId;
					} else {
						alert("이미 사용중인 아이디입니다.");
					}
				},
				error : function(data, status, err) {
					alert("아이디 중복조회를 실패했습니다.\n관리자에게 문의 바랍니다.");
				}
			});
		});
		
		/* 수정 */
		$('#btn_modify').click(function() {
			var $valid = $("#user_form").valid();
			
			if(!$valid) {
				$validator.focusInvalid();
				return false;
			} else {
				var frm = document.user_form;
				frm.action = "/eirb06/10000601/modify";
				frm.submit();
			}
		});
		
		/* 취소 */
		$('#btn_cancel').click(function() {
			clearData();
		});
		
		/* 탈퇴 */
		$('#btn_del').click(function(){
			var frm = document.user_form;
			result = confirm('선택하신 회원을 탈퇴하시겠습니까?');
			if(result == true){
				frm.action = "/eirb06/10000601/del";
				frm.submit();
			}else{
				return false;
			}
		});
		
		/* 비밀번호 초기화  */
		$('#btn_lock').click(function(){
			var frm = document.user_form;
			result = confirm('선택하신 회원의 비밀번호를 초기화 하시겠습니까?\n( ※ 초기화 된 비밀번호는 아이디로 설정됩니다. )');
			if(result == true){
				$('#USER_PW').val($('#USER_ID').val());
				frm.action = "/eirb06/10000601/pwreset";
				frm.submit();
			}else{
				return false;
			}
		});
	});
	
	// 상세조회
	function fn_view(param) {
		$.ajax({
			url : '/eirb06/10000601/id_info.json',
			type : 'post',
			data : {"USER_ID" : param},
			success : function(data) {
				$('#USER_TYPE_CD').val(data.USER_TYPE_CD);
				$('#USER_ID').val(data.USER_ID);
				$('#USER_ID').attr('readonly','readonly');
				$('#USER_NM').val(data.USER_NM);
				$('#USER_PW').val(data.USER_PW);
				$('#DEPT_NM').val(data.DEPT_NM);
				$('#USER_POS').val(data.USER_POS);
				$('#USER_TEL').val(data.USER_TEL);
				$('#USER_PHONE').val(data.USER_PHONE);
				$('#USER_EMAIL').val(data.USER_EMAIL);
				$('#CONFIRM_FG').val(data.CONFIRM_FG);
				
				$('#input-group').css('display','block');
				$('#btn_write, #btn_id_chk').hide();
				$('#btn_lock, #btn_modify, #btn_del').show();
				$('em').remove();
			},
			error : function(data, status, err) {
				alert("회원정보 가져오기를 실패했습니다.\n관리자에게 문의 바랍니다.");
			}
		});
	}
	
	function clearData() {
		$('#user_form')[0].reset();
		$('#user_form').find('input[type=hidden]').val('');
		$('#USER_ID').removeAttr('readonly');
		$('#input-group').css('display','table');
		$('#btn_write, #btn_id_chk').show();
		$('#btn_lock, #btn_modify, #btn_del').hide();
		$('em').remove();
	}
</script>