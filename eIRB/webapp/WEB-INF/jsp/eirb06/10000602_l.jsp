﻿<div class="page-title">
	<h3>게시판관리</h3>
	<!-- 
	<div class="page-breadcrumb">
		<ol class="breadcrumb">
			<li><a href="#">커뮤니티</a></li>
			<li class="active">게시판관리</li>
		</ol>
	</div>
	 -->
</div>
<div id="main-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-list"></i> 게시판 리스트</h3>
				</div>
				<div class="panel-body">
					<div class="p-v-xs"></div>
					<div class="table-responsive">
						<form name="bbsconf_list_form" id="bbsconf_list_form" method="post">
							<input type="hidden" name="SITE_KEYNO" id="SITE_KEYNO"/>
							<input type="hidden" name="BBS_KEYNO" id="BBS_KEYNO"/>
							<table id="data_table" class="display table" style="width:100%; cellspacing:0;">
								<thead>
									<tr>
										<th class="text-center">게시판 명</th>
										<th class="text-center">게시판 유형</th>
										<th class="text-center">사용여부</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${DATA}" var="item" varStatus="status">
										<tr style="cursor:pointer;" onclick="javascript:fn_view('${item.SITE_KEYNO}', '${item.BBS_KEYNO}')">
											<td class="text-center">${item.BBS_NM}</td>
											<td class="text-center">${item.BBS_TYPE_NM}</td>
											<td class="text-center">${item.BBS_USE_FG}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</form>
					</div>
					<div class="col-sm-12 margin_top_10">
						<button type="button" class="btn btn-info btn-rounded pull-right m-l-xxs" id="btn_write"><i class="fa fa-pencil"></i> 등록</button>
					</div>
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

<script type="text/javascript">
	$(document).ready(function() {
		var req  = new custom_request();
		var msg  = req.getParameter('message');
		
		if(msg != '') {
			alert(msg);
		}
		
		$('#data_table').DataTable({
			"search": { caseInsensitive: false },
			"order": [[ 2, 'desc' ]],
			"columnDefs": [
				{"targets" : 0, "searchable": true, "orderable" : true},
				{"targets" : 1, "searchable": true, "orderable" : true},
				{"targets" : 2, "searchable": true, "orderable" : true},
			]
		});
		
		/* 등록 */
		$('#btn_write').click(function() {
			fn_view();
		});
	});
	
	// 상세조회
	function fn_view(param1, param2) {
		var frm = document.bbsconf_list_form;
		$("#SITE_KEYNO").val(param1);
		$("#BBS_KEYNO").val(param2);
		frm.action = "/eirb06/10000602/view";
		frm.submit();
	}
	
</script>