<!DOCTYPE html>
<html lang="ko">
	<head>
		<jsp:include page="${contextPath}/include/eirb_index/header" />
	</head>
	<body>
		<div id="dc_wrap">
			<div id="dc_contents" class="search_wrap">
				<div class="search_form">
					<div class="search_input">
						<div class="search_title">
							<div class="btn_close"><a href="/">×</a></div>
							<h1><i class="fa fa-search"></i> 아이디/비밀번호 찾기</h1>
						</div>
						<form id="frm_search" name="frm_search" method="post" action="searchProc">
							<div>
								<input type="text" id="USER_NM" name="USER_NM" maxlength="20" placeholder="이름"/>
							</div>
							<div>
								<input type="text" id="USER_EMAIL" name="USER_EMAIL" maxlength="50" placeholder="이메일"/>
							</div>
							<span class="btn_search"><a href="#dc_wrap" id="btn_search" data-toggle="modal" date-target="#myModal">아이디 찾기</a></span>
							
							<div class="search_pw">
								※ 비밀번호 분실시 관리자에게 문의하세요.
								051-510-0000
							</div>
						</form>
					</div>
				</div>
			</div>
			<jsp:include page="${contextPath}/include/eirb_index/footer" />
		</div>
	</body>
	
	<script type="text/javascript">
		
		$(document).ready(function() {

			var $validator = $("#frm_search").validate({
				rules: {
					USER_NM: {
						required: true
					},
					USER_EMAIL: {
						required: true, 
						email: true, 
						maxlength: 50
					}
				}, 
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					error.addClass( "help-block" );
					
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				},
				submitHandler: function (form) {
				},
			} );	
			
			$('#btn_search').click(function() {
				var $valid = $("#frm_search").valid();
				
				if(!$valid) {
					$validator.focusInvalid();
					return false;
				} else {
					
					var searchNm = $('#USER_NM').val();
					var searchEmail = $('#USER_EMAIL').val();
					
					$.ajax({
						url : '/searchProc.json',
						type : 'post',
						data : {"USER_NM" : searchNm, "USER_EMAIL" : searchEmail},
						success : function(data) {
							if(data.search_id != null) {
								alert("조회하신 정보의 아이디는 '"+data.search_id+"' 입니다.");
								
								var frm = document.frm_search;
								frm.action = "/";
								frm.submit();
								
							} else {
								alert("입력하신 정보와 일치하는 회원정보가 없습니다.");
							}
						},
						error : function(data, status, err) {
							alert("아이디 찾기를 실패했습니다.\n관리자에게 문의 바랍니다.");
						}
					});
				}
			});
		});
	</script>
</html>